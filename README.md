# frag.jetzt Docker Orchestration

[frag.jetzt](https://frag.jetzt/) consists of several parts and requires certain dependencies to run correctly. This repository contains configuration files and scripts to simplify application setup with Docker and Docker Compose for both development and production purposes.

## Whats included?

The following parts are included by default and can be configured individually.

1. [frag.jetzt (Frontend)](https://git.thm.de/arsnova/frag.jetzt)
2. [frag.jetzt (Backend)](https://git.thm.de/arsnova/frag.jetzt-backend)
3. [frag.jetzt (Websocket Gateway)](https://git.thm.de/arsnova/arsnova-ws-gateway)
4. [RabbitMQ (Message Broker)](https://hub.docker.com/_/rabbitmq)
5. [PostgreSQL (Database)](https://hub.docker.com/_/postgres)
6. Email Service (Depending on configuration)
    1. [Mailhog (E-Mail Testing, for development)](https://hub.docker.com/r/mailhog/mailhog)
    2. [Postfix (E-Mail Server, for production)](https://hub.docker.com/r/danilsmirnov/postfix)

Mailhog is an email catcher, which means it intercepts all the system's emails and makes them available through a simple web interface at [localhost:8025](http://localhost:8025/). This makes it easy to examine any emails from the system in development mode.

## Requirements

### Mandatory Requirements

1. GNU/Linux compliant operating system, for example:
    1. Debian based: Debian, Ubuntu, Mint, ...
    2. Arch based: Arch, Manjaro, ...
    3. Red Hat based: Red Hat, RHEL, Fedora, CentOS, ...
    4. ...
2. Docker
    1. [Installation Instructions](https://docs.docker.com/engine/install/)
    2. [Docker Reference](https://docs.docker.com/reference/)
3. Docker Compose
    1. [Installation Instructions](https://docs.docker.com/compose/install/)
    2. [Docker Compose Reference](https://docs.docker.com/compose/reference/)

### Optional requirements

1. If you would like to make use of DeepL API, you will need an DeepL Auth Key
    1. [Accessing the API](https://www.deepl.com/docs-api/accessing-the-api/)
    2. Although specifying the key is optional, AI supported text enhancement will not work without specifying the key.
2. If you would like to setup the application for production, you will need at least
    1. A fully qualified domain name (FQDN)
    2. A ssl key pair (private & public key)
    3. openssl

### Other OS's

**We strongly recommend that you use a GNU/Linux compliant OS! Despite many optimizations, frag.jetzt doesn't run nearly as fast on Windows or MacOS as it does on a GNU/Linux OS!**

If you still want to use Windows, follow these instructions for setting up docker on windows: [frag.jetzt - Installing Docker on Windows via WSL 2](./docker_win_readme.md).

If you still want to use MacOS, you need to install the GNU Core Utilities package.

## Getting Started

Make sure, that you have setup a valid SSH key for git.thm.de in the [SSH key settings](https://git.thm.de/-/profile/keys).

Clone this repository:
```bash
git clone git@git.thm.de:arsnova/frag.jetzt-docker-orchestration.git
```

This repository contains a script `setup.sh` that configures the entire Docker Compose setup. Make sure, it is executable. If not, make it executable:
```bash
chmod u+x setup.sh
```

### Configure Setup

Use the `setup.sh` script from the root of this repository to configure the setup:
```bash
./setup.sh [OPTIONS]
```

#### Configure which parts of the application should be started

By default all frag.jetzt components will be setup in development configuration. This means that the application can be started completely.

This is a good way to test whether your system meets the requirements. However, to work on parts of the application (for example, the frag.jetzt frontend), it makes sense not to start exactly these parts, since they will be further developed in another repository.

On the other hand, you may want to disable parts of the setup because you have exactly those parts natively installed on your system.

To disable parts of the setup, you may use one or more of the following options:

| Option | Effect |
| --- | --- |
| --no-frontend | Disable the frag.jetzt frontend |
| --no-backend | Disable the frag.jetzt backend |
| --no-ws-gateway | Disable the frag.jetzt websocket gateway |
| --no-email | Disable the email service |
| --no-rabbit | Disable the message broker |
| --no-psql | Disable the database |

For example, call the setup as follows:

```bash
# if you want to develop on the frag.jetzt frontend only
./setup.sh --no-frontend

# if you want to develop on the frag.jetzt frontend and backend
./setup.sh --no-frontend --no-backend

# if you want to develop on the frag.jetzt backend only
./setup.sh --no-backend

# and so on
```

#### Reconfigure Setup

If you have already created a configuration, you can always call the setup script to enable or disable parts of the application without changing the environment configuration or secrets.

If you want to regenerate the environment configuration and secrets (for example, when switching from development to production environment), you can call the setup with the following options:

| Option | Effect |
| --- | --- |
| --recreate-env | Recreate environment files |
| --recreate-config | Recreate config files |

#### Use a DeepL Auth Key

To use text enhancement with AI support, you can specify the `--deepl-key` option during setup:

```bash
./setup.sh --deepl-key "your-deepl-key-here"
```

If you add the deepl key afterwards, you must call the option alongside with the `--recreate-config` option. If you do not want to recreate the configuration, you can also manually enter the API key in the nginx configuration: `configs/nginx.conf`.

#### Configure production Setup

**Note: the simplified setup for production is still in beta.**

If you want to use the setup for production, the following options are semi-mandatory:

| Option | Effect |
| --- | --- |
| --prod | Tells the setup to configure production |
| --domain "domain" | Your FQDN |
| --ssl-priv-key "path" | Path to your private SSL key |
| --ssl-pub-key "path" | Path to your public SSL key |

Example:

```bash
./setup.sh --prod \
  --ssl-priv-key "/etc/ssl/private/example.com_priv.pem" \
  --ssl-pub-key "/etc/ssl/certs/example.com_pub.pem" \
  --deepl-key "abcdef-012345-ghijkl-6789AB" \
  --domain "example.com"

# The use of a DeepL API key is not mandatory, but strongly recommended.
```

If these options are not specified, they must be configured manually.

Moreover, the Postfix server configuration (`configs/postfix`) should be adjusted to your given and individual circumstances.

### Running

The application is started with Docker Compose. The base configuration can be found in the `docker-compose.yml` file.

The setup script extends the Docker Compose configuration with the file `docker-compose.override.yml`.

**To use Docker, you need root privileges. You may need to invoke the commands with `sudo`.**

```bash
# Start the app in foreground (not recommended)
docker-compose up

# Start the app in background
docker-compose up -d

# Update the app (restart of app required)
docker-compose pull

# Show and follow the logs of all services
docker-compose logs -f

# Show and follow the logs of a single (or multiple) service(s)
docker-compose logs -f SERVICE [SERVICE...]

# Stop the app
docker-compose stop

# Shutdown app
docker-compose down

# Shutdown app and remove volumes
docker-compose down -v
```

## Troubleshooting

If there is any problem, the first thing to do is to gather all the information. To do this, use the log function of Compose to display the logs.

Find out which part of the application is causing the problem.

The most typical problem is a faulty network configuration. Check this first.

In case of problems, contact your tutor or the maintainers of this repository, or the maintainers of the frag.jetzt or frag.jetzt-backend repository. Provide all the information you have collected. You can only be helped if you describe your problem in detail and in a comprehensible way.

# Getting started with sample data

You can only use sample data if the containers are not yet created and the setup is running in development mode.

If the containers are already created, you need to destroy the postgres or backend container.
This can be done with `sudo docker compose rm -f -s -v postgres` (**WARNING: All existing data will be removed**) for postgres and `sudo docker compose rm -f -s backend` for the backend in the directory.

Now, if you want to activate the sample data, run `setup.sh` with the parameter `--sample-data`.

After using the sample data for postgres and for the backend, a user is created in the Super-Admin role with the email admin@admin and the password admin.

# Restarting components

If changes are made, the corresponding component must usually be shut down and deleted beforehand so that all changes are applied.
After the changes have been made, the component can be started up with `sudo docker compose up -d <name>`.

Deleting can involve several steps. If only environment variables are deleted, it is usually sufficient to delete only the container. This is accomplished with `sudo docker compose rm -f -s <name>`. If the data is also changed, the volume must also be deleted. This is done with `sudo docker compose rm -f -s -v <name>`.

## Clean start

If you always want to have a current state and do not need your data, you can use `sudo docker compose down -v`. **This will delete all services and their data. This cannot be undone!**
After making the changes, you can use `sudo docker compose up -d` to start up all services.

The default settings can always be restored with `--recreate-env` and `--recreate-config`.

The fastest way to test the latest features is: `sudo docker compose down -v` followed by updating the settings: `./setup.sh --recreate-env --recreate-config --sample-data` and a restart `sudo docker compose up -d`.

## Total clean-up of the Docker environment

Docker provides a single command to clean up all resources – images, containers, volumes and networks – that are unreferenced (i.e. not associated with a container):

`sudo docker system prune`

To also remove stopped containers and any unused images (not just unreferenced images), add the `-a` flag to the command:

`sudo docker system prune -a`
