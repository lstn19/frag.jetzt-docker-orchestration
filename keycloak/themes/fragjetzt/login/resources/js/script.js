if (location.pathname.endsWith('/login-actions/registration')) {
  addEventListener("load", () => {
    // firstname
    const firstnameInput = document.getElementById('firstName');
    firstnameInput.parentElement.parentElement.style.display = 'none';
    // lastname
    const lastnameInput = document.getElementById('lastName');
    lastnameInput.parentElement.parentElement.style.display = 'none';
  });
}
