--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--
-- CREATE ROLE fragjetzt;
ALTER ROLE fragjetzt WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'md5ed827e4d57b5f74df7560a72781cd5c5';


--
-- Databases
--

--
-- Database "template1" dump
--

\connect template1

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1 (Debian 12.1-1.pgdg100+1)
-- Dumped by pg_dump version 12.1 (Debian 12.1-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- PostgreSQL database dump complete
--

--
-- Database "fragjetzt" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1 (Debian 12.1-1.pgdg100+1)
-- Dumped by pg_dump version 12.1 (Debian 12.1-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: fragjetzt; Type: DATABASE; Schema: -; Owner: fragjetzt
--

-- CREATE DATABASE fragjetzt WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';
ALTER DATABASE fragjetzt OWNER TO fragjetzt;

\connect fragjetzt

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- Name: increase_comment_number(); Type: FUNCTION; Schema: public; Owner: fragjetzt
--

CREATE FUNCTION public.increase_comment_number() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
previousString TEXT;
BEGIN
    IF (NEW.comment_reference IS NOT NULL) THEN
        SELECT comment.number
            INTO previousString
            FROM comment
            WHERE comment.id = NEW.comment_reference;
        SELECT previousString || '/' || (COALESCE(MAX(regexp_replace(comment.number, '(?:\d+/)*(\d+)', '\1', '')::int), 0) + 1)
            INTO NEW.number
            FROM comment
            WHERE (comment.room_id = NEW.room_id) AND (comment.comment_reference = NEW.comment_reference);
    ELSE
        SELECT (COALESCE(MAX(regexp_replace(comment.number, '(?:\d+/)*(\d+)', '\1', '')::int), 0) + 1)::text
            INTO NEW.number
            FROM comment
            WHERE (comment.room_id = NEW.room_id) AND (comment.comment_reference IS NULL);
    END IF;
    RETURN NEW;
END;
$$;


ALTER FUNCTION public.increase_comment_number() OWNER TO fragjetzt;

--
-- Name: trigger_brainstorming_vote_func(); Type: FUNCTION; Schema: public; Owner: fragjetzt
--

CREATE FUNCTION public.trigger_brainstorming_vote_func() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    tempUpvotes INTEGER;
    tempDownvotes INTEGER;
BEGIN
    IF (TG_OP = 'DELETE') OR (TG_OP = 'UPDATE') THEN
        BEGIN
            SELECT upvotes, downvotes INTO tempUpvotes, tempDownvotes FROM brainstorming_word WHERE id = OLD.word_id;
            IF OLD.is_upvote THEN
                tempUpvotes = tempUpvotes - 1;
            ELSE
                tempDownvotes = tempDownvotes - 1;
            END IF;
            UPDATE brainstorming_word
            SET upvotes = tempUpvotes, downvotes = tempDownvotes
            WHERE id = OLD.word_id;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN -- cascaded delete
                RETURN OLD;
        END;
    END IF;
    IF (TG_OP = 'INSERT') OR (TG_OP = 'UPDATE') THEN
        SELECT upvotes, downvotes INTO tempUpvotes, tempDownvotes FROM brainstorming_word WHERE id = NEW.word_id;
        IF NEW.is_upvote THEN
            tempUpvotes = tempUpvotes + 1;
        ELSE
            tempDownvotes = tempDownvotes + 1;
        END IF;
        UPDATE brainstorming_word
        SET upvotes = tempUpvotes, downvotes = tempDownvotes
        WHERE id = NEW.word_id;
    END IF;
    IF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    ELSEIF (TG_OP = 'UPDATE') OR (TG_OP = 'INSERT') THEN
        RETURN NEW;
    END IF;
    RETURN NULL;
END;
$$;


ALTER FUNCTION public.trigger_brainstorming_vote_func() OWNER TO fragjetzt;

--
-- Name: trigger_calculate_comment_depth_func(); Type: FUNCTION; Schema: public; Owner: fragjetzt
--

CREATE FUNCTION public.trigger_calculate_comment_depth_func() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    tempDepth INTEGER;
BEGIN
    IF (TG_OP = 'INSERT') THEN
        IF (NEW.comment_reference IS NOT NULL) THEN
            SELECT comment.comment_depth
                INTO tempDepth
                FROM comment
                WHERE id = NEW.comment_reference;
            tempDepth = tempDepth + 1;
            NEW.comment_depth = tempDepth;
        ELSE
            NEW.comment_depth = 0;
        END IF;
        RETURN NEW;
    ELSEIF (TG_OP = 'UPDATE') THEN
        IF (OLD.comment_depth <> NEW.comment_depth) THEN
            NEW.comment_depth = OLD.comment_depth;
        END IF;
        RETURN NEW;
    END IF;
    RETURN NULL;
END;
$$;


ALTER FUNCTION public.trigger_calculate_comment_depth_func() OWNER TO fragjetzt;

--
-- Name: trigger_comment_downvotes(); Type: FUNCTION; Schema: public; Owner: fragjetzt
--

CREATE FUNCTION public.trigger_comment_downvotes() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    newScore INTEGER;
    newDownvotes INTEGER;
BEGIN
    IF (TG_OP = 'DELETE') OR ((TG_OP = 'UPDATE') AND (OLD.comment_id <> NEW.comment_id)) THEN
        BEGIN
            SELECT score + 1, downvotes - 1 INTO newScore, newDownvotes FROM comment WHERE comment.id = OLD.comment_id;
            UPDATE comment
            SET score = newScore, downvotes = newDownvotes
            WHERE comment.id = OLD.comment_id;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN -- cascaded delete
                RETURN OLD;
        END;
    END IF;
    IF (TG_OP = 'INSERT') OR ((TG_OP = 'UPDATE') AND (OLD.comment_id <> NEW.comment_id)) THEN
        SELECT score - 1, downvotes + 1 INTO newScore, newDownvotes FROM comment WHERE comment.id = NEW.comment_id;
        UPDATE comment
        SET score = newScore, downvotes = newDownvotes
        WHERE comment.id = NEW.comment_id;
    END IF;
    IF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    ELSEIF (TG_OP = 'UPDATE') OR (TG_OP = 'INSERT') THEN
        RETURN NEW;
    END IF;
    RETURN NULL;
END;
$$;


ALTER FUNCTION public.trigger_comment_downvotes() OWNER TO fragjetzt;

--
-- Name: trigger_comment_upvotes(); Type: FUNCTION; Schema: public; Owner: fragjetzt
--

CREATE FUNCTION public.trigger_comment_upvotes() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    newScore INTEGER;
    newUpvotes INTEGER;
BEGIN
    IF (TG_OP = 'DELETE') OR ((TG_OP = 'UPDATE') AND (OLD.comment_id <> NEW.comment_id)) THEN
        BEGIN
            SELECT score - 1, upvotes - 1 INTO newScore, newUpvotes FROM comment WHERE comment.id = OLD.comment_id;
            UPDATE comment
            SET score = newScore, upvotes = newUpvotes
            WHERE comment.id = OLD.comment_id;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN -- cascaded delete
                RETURN OLD;
        END;
    END IF;
    IF (TG_OP = 'INSERT') OR ((TG_OP = 'UPDATE') AND (OLD.comment_id <> NEW.comment_id)) THEN
        SELECT score + 1, upvotes + 1 INTO newScore, newUpvotes FROM comment WHERE comment.id = NEW.comment_id;
        UPDATE comment
        SET score = newScore, upvotes = newUpvotes
        WHERE comment.id = NEW.comment_id;
    END IF;
    IF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    ELSEIF (TG_OP = 'UPDATE') OR (TG_OP = 'INSERT') THEN
        RETURN NEW;
    END IF;
    RETURN NULL;
END;
$$;


ALTER FUNCTION public.trigger_comment_upvotes() OWNER TO fragjetzt;

--
-- Name: trigger_ensure_unique_days(); Type: FUNCTION; Schema: public; Owner: fragjetzt
--

CREATE FUNCTION public.trigger_ensure_unique_days() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    dayCount INTEGER;
BEGIN
    IF (TG_OP = 'INSERT') OR (TG_OP = 'UPDATE') THEN
        dayCount = NEW.notification_setting >> 11;
        IF (dayCount > 6) THEN
            RAISE EXCEPTION 'Day is invalid! Must be between 0 and 6!';
        END IF;
        SELECT COUNT(*) INTO dayCount FROM comment_notification
            WHERE (room_id = NEW.room_id) AND (account_id = NEW.account_id);
        IF (dayCount > 7) THEN
            RAISE EXCEPTION 'Cannot create more than 7 notifications!';
        END IF;
        RETURN NEW;
    ELSEIF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    END IF;
    RETURN NULL;
END;
$$;


ALTER FUNCTION public.trigger_ensure_unique_days() OWNER TO fragjetzt;

--
-- Name: trigger_timestamp_create_update_func(); Type: FUNCTION; Schema: public; Owner: fragjetzt
--

CREATE FUNCTION public.trigger_timestamp_create_update_func() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    IF (TG_OP = 'INSERT') THEN
        NEW.created_at = NOW();
        NEW.updated_at = NULL;
        RETURN NEW;
    ELSEIF (TG_OP = 'UPDATE') THEN
        IF (OLD.created_at <> NEW.created_at) THEN
            NEW.created_at = OLD.created_at;
        END IF;
        NEW.updated_at = NOW();
        RETURN NEW;
    ELSEIF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    END IF;
    RETURN NULL;
END;
$$;


ALTER FUNCTION public.trigger_timestamp_create_update_func() OWNER TO fragjetzt;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: account; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.account (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    email character varying(255),
    last_login timestamp without time zone,
    last_active timestamp without time zone,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.account OWNER TO fragjetzt;

--
-- Name: bonus_token; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.bonus_token (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    comment_id uuid NOT NULL,
    room_id uuid NOT NULL,
    account_id uuid NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.bonus_token OWNER TO fragjetzt;

--
-- Name: bookmark; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.bookmark (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    account_id uuid NOT NULL,
    room_id uuid NOT NULL,
    comment_id uuid NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.bookmark OWNER TO fragjetzt;

--
-- Name: brainstorming_session; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.brainstorming_session (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    room_id uuid NOT NULL,
    title character varying(255) DEFAULT NULL::character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    max_word_length integer DEFAULT 20 NOT NULL,
    max_word_count integer DEFAULT 1 NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.brainstorming_session OWNER TO fragjetzt;

--
-- Name: brainstorming_vote; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.brainstorming_vote (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    account_id uuid NOT NULL,
    word_id uuid NOT NULL,
    is_upvote boolean NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.brainstorming_vote OWNER TO fragjetzt;

--
-- Name: brainstorming_word; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.brainstorming_word (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    session_id uuid NOT NULL,
    word character varying(255) NOT NULL,
    upvotes integer DEFAULT 0 NOT NULL,
    downvotes integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.brainstorming_word OWNER TO fragjetzt;

--
-- Name: comment; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.comment (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    room_id uuid NOT NULL,
    creator_id uuid NOT NULL,
    number text NOT NULL,
    ack boolean NOT NULL,
    body text,
    correct integer NOT NULL,
    favorite boolean NOT NULL,
    read boolean NOT NULL,
    tag character varying(20) DEFAULT NULL::character varying,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    bookmark boolean DEFAULT false NOT NULL,
    keywords_from_questioner text DEFAULT '[]'::text NOT NULL,
    keywords_from_spacy text DEFAULT '[]'::text NOT NULL,
    score integer DEFAULT 0 NOT NULL,
    upvotes integer DEFAULT 0 NOT NULL,
    downvotes integer DEFAULT 0 NOT NULL,
    language character varying(20) DEFAULT 'AUTO'::character varying NOT NULL,
    questioner_name character varying(127) DEFAULT NULL::character varying,
    brainstorming_question boolean DEFAULT false NOT NULL,
    updated_at timestamp without time zone,
    comment_reference uuid,
    deleted_at timestamp without time zone,
    comment_depth integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.comment OWNER TO fragjetzt;

--
-- Name: comment_change; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.comment_change (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    comment_id uuid NOT NULL,
    room_id uuid NOT NULL,
    type character varying(255) NOT NULL,
    previous_value_string text,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone,
    initiator_id uuid NOT NULL,
    initiator_role character varying(20) NOT NULL,
    current_value_string text
);


ALTER TABLE public.comment_change OWNER TO fragjetzt;

--
-- Name: comment_change_subscription; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.comment_change_subscription (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    comment_id uuid NOT NULL,
    room_id uuid NOT NULL,
    account_id uuid NOT NULL
);


ALTER TABLE public.comment_change_subscription OWNER TO fragjetzt;

--
-- Name: comment_notification; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.comment_notification (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    account_id uuid NOT NULL,
    room_id uuid NOT NULL,
    notification_setting smallint NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.comment_notification OWNER TO fragjetzt;

--
-- Name: downvote; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.downvote (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    comment_id uuid NOT NULL,
    account_id uuid NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.downvote OWNER TO fragjetzt;

--
-- Name: flyway_schema_history; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.flyway_schema_history (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


ALTER TABLE public.flyway_schema_history OWNER TO fragjetzt;

--
-- Name: frag_jetzt_credentials; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.frag_jetzt_credentials (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(100) NOT NULL,
    activation_key character varying(100),
    activation_key_time timestamp without time zone,
    password_reset_key character varying(100),
    password_reset_time timestamp without time zone,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.frag_jetzt_credentials OWNER TO fragjetzt;

--
-- Name: motd; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.motd (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    start_timestamp timestamp without time zone NOT NULL,
    end_timestamp timestamp without time zone NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.motd OWNER TO fragjetzt;

--
-- Name: motd_message; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.motd_message (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    motd_id uuid NOT NULL,
    language character varying(255) NOT NULL,
    message text NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.motd_message OWNER TO fragjetzt;

--
-- Name: rating; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.rating (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    account_id uuid NOT NULL,
    rating double precision NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.rating OWNER TO fragjetzt;

--
-- Name: room; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.room (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    owner_id uuid NOT NULL,
    short_id text NOT NULL,
    name character varying(100) NOT NULL,
    description text,
    closed boolean DEFAULT false NOT NULL,
    bonus_archive_active boolean DEFAULT true NOT NULL,
    direct_send boolean NOT NULL,
    threshold integer NOT NULL,
    questions_blocked boolean DEFAULT false NOT NULL,
    blacklist text DEFAULT '[]'::text NOT NULL,
    profanity_filter character varying(50) DEFAULT 'LANGUAGE_SPECIFIC'::character varying NOT NULL,
    blacklist_active boolean DEFAULT true NOT NULL,
    tag_cloud_settings text,
    moderator_room_reference uuid,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone,
    last_visit_creator timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    conversation_depth integer DEFAULT 3,
    quiz_active boolean DEFAULT true,
    brainstorming_active boolean DEFAULT true
);


ALTER TABLE public.room OWNER TO fragjetzt;

--
-- Name: room_access; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.room_access (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    room_id uuid NOT NULL,
    account_id uuid NOT NULL,
    role character varying(20) NOT NULL,
    last_visit timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.room_access OWNER TO fragjetzt;

--
-- Name: room_comment_change_subscription; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.room_comment_change_subscription (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    room_id uuid NOT NULL,
    account_id uuid NOT NULL
);


ALTER TABLE public.room_comment_change_subscription OWNER TO fragjetzt;

--
-- Name: tag; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.tag (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    room_id uuid NOT NULL,
    tag character varying(20),
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.tag OWNER TO fragjetzt;

--
-- Name: upvote; Type: TABLE; Schema: public; Owner: fragjetzt
--

CREATE TABLE public.upvote (
    id uuid DEFAULT public.uuid_generate_v1() NOT NULL,
    comment_id uuid NOT NULL,
    account_id uuid NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.upvote OWNER TO fragjetzt;

--
-- Data for Name: account; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.account (id, email, last_login, last_active, created_at, updated_at) FROM stdin;
bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	admin@admin	2022-10-24 14:06:26.806767	2022-10-24 14:06:26.806771	2022-10-24 14:06:06.749507	2022-10-24 14:06:26.812388
\.


--
-- Data for Name: bonus_token; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.bonus_token (id, comment_id, room_id, account_id, token, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: bookmark; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.bookmark (id, account_id, room_id, comment_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: brainstorming_session; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.brainstorming_session (id, room_id, title, active, created_at, max_word_length, max_word_count, updated_at) FROM stdin;
\.


--
-- Data for Name: brainstorming_vote; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.brainstorming_vote (id, account_id, word_id, is_upvote, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: brainstorming_word; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.brainstorming_word (id, session_id, word, upvotes, downvotes, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: comment; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.comment (id, room_id, creator_id, number, ack, body, correct, favorite, read, tag, created_at, bookmark, keywords_from_questioner, keywords_from_spacy, score, upvotes, downvotes, language, questioner_name, brainstorming_question, updated_at, comment_reference, deleted_at, comment_depth) FROM stdin;
6419d034-2ef3-11eb-87ab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	1	t	["4\\n"]	0	f	f	\N	2022-10-24 14:06:06.766296	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
369cae3e-2ef2-11eb-a778-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	2	t	["8\\n"]	0	f	f	\N	2022-10-24 14:06:06.773835	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
e2bec9e0-2d5d-11eb-8811-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	3	t	["Moin \\n"]	0	f	f	\N	2022-10-24 14:06:06.780264	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
1e4c2c24-2d67-11eb-95ba-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	4	t	["Bitte umschalten ;)\\n\\n"]	0	f	f	\N	2022-10-24 14:06:06.786139	f	[]	[]	2	2	0	DE	\N	f	\N	\N	\N	0
d1060eba-2d5e-11eb-9e65-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	5	t	["Die Kathode ist Nekathiv\\n"]	0	f	f	\N	2022-10-24 14:06:06.792183	f	[]	[]	1	4	3	AUTO	\N	f	\N	\N	\N	0
4864a99a-c4ac-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	1	t	["What is it\\n"]	0	f	f	\N	2022-10-24 14:06:06.798	f	[]	[]	0	1	1	AUTO		f	\N	\N	2022-04-25 20:07:37.277	0
9a77e77c-ca2b-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	2	t	["What does aes() does and Why do we need it?\\n"]	0	f	f	\N	2022-10-24 14:06:06.811719	f	[]	[{"dep":["nsubj"],"text":"aes"}]	0	0	0	AUTO		f	\N	\N	\N	0
28f951d8-cfaf-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	3	t	["where does the df data set come from?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:06.820353	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
c85ef5ee-d28d-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	4	t	["That means the simulation can only test the hypothesis, but not whether the data is correct? \\n"]	0	f	f	\N	2022-10-24 14:06:06.827136	f	[]	[{"dep":["nsubj"],"text":"simulation"},{"dep":["dobj"],"text":"hypothesis"},{"dep":["nsubj"],"text":"data"}]	0	0	0	AUTO		f	\N	\N	\N	0
e2a8bf6e-dd89-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	5	t	["What is the difference between binomial distribution and geometric distribution?\\n"]	0	f	f	\N	2022-10-24 14:06:06.84401	f	[]	[{"dep":["nsubj"],"text":"difference"},{"dep":["conj","pobj"],"text":"distribution"}]	0	0	0	AUTO		f	\N	\N	2022-05-27 07:45:02.059	0
ee95b0ee-e305-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	6	t	["What shows replicate colimn?\\n"]	0	f	f	\N	2022-10-24 14:06:06.849781	f	[]	[{"dep":["nsubj"],"text":"program"}]	0	0	0	AUTO		f	\N	\N	2022-06-03 08:02:38.878	0
c587d388-fe8c-11ec-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	7	t	["for slide 8, why is c no correct and where do we know that b is true?\\n"]	0	f	f	\N	2022-10-24 14:06:06.861088	f	[]	[{"dep":["pobj"],"text":"slide"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:15:24.622	0
b6e17d8e-0404-11ed-938f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	8	t	["If there are two same AIC values, would we add both, one or none variable?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:06.871415	f	[]	[{"dep":["nummod"],"text":"two"},{"dep":["amod"],"text":"aic"},{"dep":["attr"],"text":"value"}]	0	0	0	AUTO		f	\N	\N	2022-07-15 08:46:15.854	0
51eedf8a-06b0-11ed-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	9	t	["R+0\\n"]	0	f	f	\N	2022-10-24 14:06:06.882601	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-19 09:16:15.808	0
b1e96d18-65fa-11eb-aa57-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	6	t	["Warum ist im Film das Glas bei der Mischung von F2 und H2 nicht kaputt gegangen?\\n"]	0	f	f	\N	2022-10-24 14:06:06.888478	f	[]	[{"dep":["nk"],"text":"Film"},{"dep":["sb"],"text":"Glas"},{"dep":["nk"],"text":"Mischung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
21aa2c36-2d67-11eb-bc8d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	7	t	["könnten sie umschalten?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:06.893591	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
2eaad032-af81-11ec-97de-0242ac120007	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	8	t	["Hello I like dumplings\\n"]	0	f	f	\N	2022-10-24 14:06:06.898843	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-03-29 16:56:48.384	0
b17f83ca-2d61-11eb-8811-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	9	t	["OBM+ ist auch eine gute Hilfe\\n"]	0	f	f	\N	2022-10-24 14:06:06.904169	f	[]	[{"dep":["sb"],"text":"OBM+"},{"dep":["pd"],"text":"Hilfe"}]	0	0	0	DE	\N	f	\N	\N	\N	0
2686a6de-38c9-11ed-a9f5-0242ac120009	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	10	t	["Hallo, ich bin eine Ente und ich mag große Kekse.\\n"]	0	f	f	\N	2022-10-24 14:06:06.910954	f	[{"text":"Ente","dep":["pd"]},{"text":"Hallo","dep":["ROOT"]},{"text":"Keks","dep":["oa"]}]	[{"dep":["ROOT"],"text":"Hallo"},{"dep":["pd"],"text":"Ente"},{"dep":["oa"],"text":"Keks"}]	0	0	0	DE		f	\N	\N	2022-09-20 09:47:12.744	0
45ba01c4-5af7-11eb-a058-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	11	t	["Ja\\n"]	0	f	f	\N	2022-10-24 14:06:06.916284	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
f6562a64-3f7a-11eb-ba23-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	12	t	["Vielen Dank für sie auch !!\\n"]	0	f	f	\N	2022-10-24 14:06:06.921683	f	[]	[{"dep":["ROOT"],"text":"Dank"}]	0	0	0	DE	\N	f	\N	\N	\N	0
60f364fe-32e2-11eb-a5e3-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	13	t	["Bei den Kommentaren hatten den einige schon\\n"]	0	f	f	\N	2022-10-24 14:06:06.927573	f	[]	[{"dep":["nk"],"text":"Kommentaren"}]	18	18	0	DE	\N	f	\N	\N	\N	0
cecd9cb8-39f1-11eb-831c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	14	t	["es rauscht zwischendurch recht unangenehm \\n"]	0	f	f	\N	2022-10-24 14:06:06.932956	f	[]	[]	1	1	0	DE	\N	f	\N	\N	\N	0
5b1e702c-346e-11eb-a2c7-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	15	t	["Hola Senior Buchsbaum.\\n"]	0	f	f	\N	2022-10-24 14:06:06.938419	f	[]	[]	4	6	2	AUTO	\N	f	\N	\N	\N	0
200846b8-5afa-11eb-80ec-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	16	t	["weird flex but ok\\n"]	0	f	f	\N	2022-10-24 14:06:06.943409	f	[]	[]	3	3	0	AUTO	\N	f	\N	\N	\N	0
3b44e7f4-5af7-11eb-a1c0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	17	t	["JA \\n"]	0	f	f	\N	2022-10-24 14:06:06.948779	f	[]	[]	2	2	0	AUTO	\N	f	\N	\N	\N	0
4c8a732e-5ee1-11eb-ada1-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	18	t	["Der Körper, besser gesagt das Immunsystem reagiert auf den Fremdkörper und da kann es auch zu einer Veränderung des Hormonhaushalts führen. Meistens ist da  Serotonin und Dopamin im Spiel\\n"]	0	f	f	\N	2022-10-24 14:06:06.956457	f	[]	[{"dep":["sb"],"text":"Körper"},{"dep":["sb"],"text":"Immunsystem"},{"dep":["nk"],"text":"Fremdkörper"},{"dep":["nk"],"text":"Veränderung"},{"dep":["sb"],"text":"Serotonin"},{"dep":["cj"],"text":"Dopamin"},{"dep":["nk"],"text":"Spiel"}]	1	1	0	DE	\N	f	\N	\N	\N	0
cce6b3b8-5ee9-11eb-b352-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	19	t	["Sie haben in der Übung 6 Aufgabe 1 (Oxidationszahlen der Verbindungen) die Verbindung HD angegeben \\nHaben sie sich da verschrieben? Oder wofür steht: D?\\n"]	0	f	f	\N	2022-10-24 14:06:06.963829	f	[]	[{"dep":["nk"],"text":"Übung"},{"dep":["oa"],"text":"Aufgabe"},{"dep":["oa","ag"],"text":"Verbindung"}]	1	1	0	DE	\N	f	\N	\N	\N	0
72504e34-5ee7-11eb-baab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	20	t	["cool :)\\n"]	0	f	f	\N	2022-10-24 14:06:06.969201	f	[]	[]	3	3	0	AUTO	\N	f	\N	\N	\N	0
822b48dc-2ef8-11eb-931a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	21	t	["sie haben zu schnell das blatt gewechselt\\n\\n"]	0	f	f	\N	2022-10-24 14:06:06.974643	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
a752a500-5ee7-11eb-a331-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	22	t	["Wie teuer ist die Herstellung von einer Kanne flüssigem Stickstoff?\\n"]	0	f	f	\N	2022-10-24 14:06:06.980933	f	[]	[{"dep":["sb"],"text":"Herstellung"},{"dep":["nk"],"text":"Kanne"},{"dep":["pd"],"text":"Stickstoff"}]	3	3	0	DE	\N	f	\N	\N	\N	0
d6ae72ac-5ee7-11eb-a331-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	23	t	["Trockeneis war CO2(s) oder?\\n"]	0	f	f	\N	2022-10-24 14:06:06.986201	f	[]	[]	3	3	0	AUTO	\N	f	\N	\N	\N	0
e2be1e2e-5ee9-11eb-9567-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	24	t	["Hat Spaß gemacht, danke!\\n"]	0	f	f	\N	2022-10-24 14:06:06.991888	f	[]	[]	3	3	0	AUTO	\N	f	\N	\N	\N	0
acacd366-5ee4-11eb-9567-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	25	t	["Müssen wir solche Gleichungen bzw Reaktionen selber aufstellen können ? Ich bin dezent überfordert , weil ich sowas noch nie gemacht habe \\n"]	0	f	f	\N	2022-10-24 14:06:06.998287	f	[]	[{"dep":["oa"],"text":"Gleichung"},{"dep":["ag"],"text":"Reaktion"},{"dep":["oa"],"text":"sowas"}]	2	2	0	DE	\N	f	\N	\N	\N	0
440dee62-5ee4-11eb-b352-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	26	t	["Kommt nur dir so vor ;)\\n"]	0	f	f	\N	2022-10-24 14:06:07.003327	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
750d1e86-5ee7-11eb-baab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	27	t	["dürfen Sie den flüssigen Stickstoff einfach so ausschütten? passiert da nichts?\\n"]	0	f	f	\N	2022-10-24 14:06:07.009059	f	[]	[{"dep":["oa"],"text":"Stickstoff"}]	1	1	0	DE	\N	f	\N	\N	\N	0
126bab80-639c-11eb-b654-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	28	t	["Bitte die alten Fragen löschen (unter Einstellungen auf der Session-Seite: Zahnrad-Icon) oder eine neue Session erstellen.\\n\\nMit fast 700 Fragen dauert die Anzeige mittlerweile bis zu 10 Sekunden.\\n"]	0	f	f	\N	2022-10-24 14:06:07.017082	f	[]	[{"dep":["nk","sb"],"text":"Frage"},{"dep":["nk"],"text":"Einstellung"},{"dep":["nk"],"text":"Session-Seite"},{"dep":["par"],"text":"Zahnrad-Icon"},{"dep":["cj"],"text":"Session"},{"dep":["sb"],"text":"Anzeige"},{"dep":["oa"],"text":"Sekunde"}]	1	1	0	DE	\N	f	\N	\N	2022-07-02 06:37:15.363	0
a2f8ed82-6074-11eb-9f71-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	29	t	["umschalten bitte\\n"]	0	f	f	\N	2022-10-24 14:06:07.022959	f	[]	[]	3	3	0	AUTO	\N	f	\N	\N	\N	0
4ab51a8e-6073-11eb-a259-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	30	t	["Eigentlich ist die Evaluation unnötig wir wissen sowieso alle, dass sie klasse sind ! \\n"]	0	f	f	\N	2022-10-24 14:06:07.029309	f	[]	[{"dep":["sb"],"text":"Evaluation"},{"dep":["pd"],"text":"klasse"}]	4	4	0	DE	\N	f	\N	\N	\N	0
a165bc20-6074-11eb-a259-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	31	t	["Bitte umschalten ;)\\n"]	0	f	f	\N	2022-10-24 14:06:07.040025	f	[]	[]	3	3	0	DE	\N	f	\N	\N	\N	0
0192ba02-6076-11eb-a2a1-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	32	t	["Warum ist die Explosion in dem Fall eigentlich schief gelaufen , wenn es vorher immer funktioniert hat ?\\n"]	0	f	f	\N	2022-10-24 14:06:07.048788	f	[]	[{"dep":["sb"],"text":"Explosion"},{"dep":["nk"],"text":"Fall"}]	1	1	0	DE	\N	f	\N	\N	\N	0
c280b982-6074-11eb-960c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	33	t	["No3- struktur nochmal zeigen bitte\\n"]	0	f	f	\N	2022-10-24 14:06:07.054928	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
df04db2c-65f3-11eb-ab68-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	34	t	["Woran erkennt man den Unterschied von schwer und leichtlöslichen Sulfaten?\\n"]	0	f	f	\N	2022-10-24 14:06:07.061329	f	[]	[{"dep":["oa"],"text":"Unterschied"},{"dep":["nk"],"text":"Sulfat"}]	1	1	0	DE	\N	f	\N	\N	\N	0
0812b56a-32e2-11eb-856c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	35	t	["Warum steht Hende unter dem Anfang der gleichung \\n"]	0	f	f	\N	2022-10-24 14:06:07.066952	f	[]	[]	-1	1	2	AUTO	\N	f	\N	\N	\N	0
e58360a2-3f76-11eb-ba23-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	36	t	["Ist die Formel für den ph Wert bei den schwachen Säuren auf alle schwachen Säuren anwendbar\\n"]	0	f	f	\N	2022-10-24 14:06:07.07332	f	[]	[{"dep":["sb"],"text":"Formel"},{"dep":["nk"],"text":"ph Wert"},{"dep":["nk"],"text":"Säure"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
59a01be2-5ee4-11eb-ba5a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	37	t	["Waaas? Es gibt ein Gewichtsunterschied? :0\\n"]	0	f	f	\N	2022-10-24 14:06:07.079007	f	[]	[]	-2	1	3	DE	\N	f	\N	\N	\N	0
3e648bfa-2d67-11eb-af1e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	38	t	["Können sie die Kamera umstellen bitte?\\n"]	0	f	f	\N	2022-10-24 14:06:07.084746	f	[]	[{"dep":["oa"],"text":"Kamera"}]	0	0	0	DE	\N	f	\N	\N	\N	0
ee34a63e-2ef0-11eb-a778-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	39	t	["Keine\\n"]	0	f	f	\N	2022-10-24 14:06:07.089961	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
f62e56de-2eee-11eb-8a62-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	40	t	["Vielen dank\\n"]	0	f	f	\N	2022-10-24 14:06:07.095696	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
daf61fba-2d5d-11eb-8811-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	41	t	["klappt :)\\n\\n"]	0	f	f	\N	2022-10-24 14:06:07.101102	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
962efaca-2ef0-11eb-ac45-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	42	t	["Warum muss man Wasser vorlegen? Kann man nicht einfach festes NaOH in den Kolben legen und dann 1L Wasser auffüllen?\\n"]	0	f	f	\N	2022-10-24 14:06:07.107709	f	[]	[{"dep":["oa"],"text":"Wasser"},{"dep":["oa"],"text":"NaOH"},{"dep":["nk"],"text":"Kolbe"}]	0	0	0	DE	\N	f	\N	\N	\N	0
5bfa39aa-3ddc-11eb-9b23-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	43	t	["Guten Morgen :3\\n"]	0	f	f	\N	2022-10-24 14:06:07.11326	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
f72583a8-2ef0-11eb-b394-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	44	t	["Eine weiße kugel\\n"]	0	f	f	\N	2022-10-24 14:06:07.118928	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
e28cf146-2d66-11eb-8811-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	45	t	["müsste da nicht w(O2)=0,4995 stehen?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:07.125019	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
757d13dc-2ef7-11eb-87ab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	46	t	["hin\\n"]	0	f	f	\N	2022-10-24 14:06:07.130527	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
2badef92-2ef2-11eb-87ab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	47	t	["1\\n"]	0	f	f	\N	2022-10-24 14:06:07.135723	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
044a18f0-2ef1-11eb-931a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	48	t	["1\\n\\n"]	0	f	f	\N	2022-10-24 14:06:07.141193	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
5d533228-2ef2-11eb-931a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	49	t	["4\\n"]	0	f	f	\N	2022-10-24 14:06:07.146396	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
2996fac8-2d67-11eb-9ff0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	50	t	["Sie haben nicht auf die Ksmera umgeschaltet\\n"]	0	f	f	\N	2022-10-24 14:06:07.152229	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
f0be9bbc-2ef0-11eb-8a62-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	51	t	["kann keinen QR-Code scannen \\n"]	0	f	f	\N	2022-10-24 14:06:07.158161	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
cb5b7d36-2ef0-11eb-87ab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	52	t	["Eine \\n"]	0	f	f	\N	2022-10-24 14:06:07.163902	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
7cc53470-2d68-11eb-b524-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	53	t	["Lädt er seine tollen Blätter eigentlich auf Olat hoch?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:07.169653	f	[]	[{"dep":["oa"],"text":"blättern"},{"dep":["nk"],"text":"Olat"}]	0	0	0	DE	\N	f	\N	\N	\N	0
aba5d536-2d60-11eb-b524-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	54	t	["Wieso nimmt der Atomradius in der Periode durch zunehmende Protonenzahl ab? Die Elektronenzahl steigt proportional ja auch?\\n"]	0	f	f	\N	2022-10-24 14:06:07.176521	f	[]	[{"dep":["sb"],"text":"Atomradius"},{"dep":["nk"],"text":"Periode"},{"dep":["nk"],"text":"Protonenzahl"},{"dep":["mo"],"text":"proportional"}]	0	0	0	DE	\N	f	\N	\N	\N	0
19e6a854-3f72-11eb-b250-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	55	t	["bezogen auf das nebenblatt\\n"]	0	f	f	\N	2022-10-24 14:06:07.181937	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
480e90de-2d61-11eb-b524-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	56	t	["mol^-1; da könnte man auch sagen teilchen pro mol bzw teilchen bruchstrich mol\\n"]	0	f	f	\N	2022-10-24 14:06:07.18784	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
061787c6-2d61-11eb-95ba-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	57	t	["Wieso mol^-1 ?\\n"]	0	f	f	\N	2022-10-24 14:06:07.193778	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
520ba938-2d65-11eb-b524-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	58	t	["Was steht vor Endstoffe?\\n"]	0	f	f	\N	2022-10-24 14:06:07.199308	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
4cf5c5c8-2d65-11eb-a175-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	59	t	["Was steht vor endtsoffe \\n"]	0	f	f	\N	2022-10-24 14:06:07.204595	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
b7ed3f86-2d61-11eb-a175-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	60	t	["Können Sie Blatt 5 bitte nochmal erklären \\n"]	0	f	f	\N	2022-10-24 14:06:07.209999	f	[]	[{"dep":["oa"],"text":"Blatt"}]	0	0	0	DE	\N	f	\N	\N	\N	0
72d89a7a-2d62-11eb-95ba-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	61	t	["Was ist der unterschied zwischen molaren- und relativen atommasse?\\n"]	0	f	f	\N	2022-10-24 14:06:07.215627	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
62b64ea6-2d64-11eb-95ba-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	62	t	["Kann seit 20 min das Video nicht laden. Hab eigentlich sehr gutes Internet...\\n"]	0	f	f	\N	2022-10-24 14:06:07.222202	f	[]	[{"dep":["sb"],"text":"min"},{"dep":["sb"],"text":"Video"},{"dep":["nk"],"text":"Hab"},{"dep":["ROOT"],"text":"Internet"}]	0	0	0	DE	\N	f	\N	\N	\N	0
01ae60a4-3475-11eb-a957-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	131	t	["Ok danke. :)\\n"]	0	f	f	\N	2022-10-24 14:06:07.643478	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
1315e1fa-2d63-11eb-9ff0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	63	t	["Gute Merkhilfe ist auch mit dem Dreieck. m ust in der Spitze und M und n sind auf der Grundseite\\n"]	0	f	f	\N	2022-10-24 14:06:07.22939	f	[]	[{"dep":["nk"],"text":"Dreieck"},{"dep":["nk"],"text":"spitzen"},{"dep":["nk"],"text":"Grundseite"}]	0	0	0	DE	\N	f	\N	\N	\N	0
c0cd6e50-2d5c-11eb-95ba-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	64	t	["hallo\\n"]	0	f	f	\N	2022-10-24 14:06:07.234609	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
a4dc8190-2ef6-11eb-87ab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	65	t	["könnnen sie die vorherige folie nochmal zeigen\\n"]	0	f	f	\N	2022-10-24 14:06:07.240012	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
ddba2b38-2d5d-11eb-af1e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	66	t	["Ja passt jetzt geht's 👌\\n"]	0	f	f	\N	2022-10-24 14:06:07.245037	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
dd4309e0-2d5d-11eb-af1e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	67	t	["Funktioniert, danke :3\\n"]	0	f	f	\N	2022-10-24 14:06:07.251806	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
26d04be0-2d5e-11eb-8811-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	68	t	["Entspringt daher der stöchiometrische Koeffizient?\\n"]	0	f	f	\N	2022-10-24 14:06:07.258051	f	[]	[{"dep":["nk"],"text":"stöchiometrische"},{"dep":["sb"],"text":"Koeffizient"}]	0	0	0	DE	\N	f	\N	\N	\N	0
dbd87ebe-2d5d-11eb-af1e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	69	t	["Es hat funktioniert, danke! :)\\n"]	0	f	f	\N	2022-10-24 14:06:07.263329	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
dd8fb322-2d61-11eb-af1e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	70	t	["Was ist wenn man mehr oder weniger e- in einer Verbindung hat ? Zieht oder addiert man diese dann zusammen ?\\n"]	0	f	f	\N	2022-10-24 14:06:07.26943	f	[]	[{"dep":["nk"],"text":"Verbindung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
e0cbab40-2d61-11eb-af1e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	71	t	["Müssen wir beim Berechnen von polaren Massen mit den drei Nachkommastellen rechnen oder dürfen wir auch auf ganze Beträge runden?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:07.275875	f	[]	[{"dep":["nk"],"text":"Masse"},{"dep":["nk"],"text":"Nachkommastellen"},{"dep":["nk"],"text":"Betrag"}]	0	0	0	DE	\N	f	\N	\N	\N	0
50c2d01e-2ef1-11eb-ac45-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	72	t	["Soll es so sein, dass ich nur die Optionen 1-5 da stehen habe, ohne zu sehen, wofür diese stehen?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:07.282229	f	[]	[{"dep":["oa"],"text":"Option"}]	0	0	0	DE	\N	f	\N	\N	\N	0
6b5e2b74-2d68-11eb-9e65-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	73	t	["Dankee und Tschüß \\n"]	0	f	f	\N	2022-10-24 14:06:07.287911	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
5be94012-346f-11eb-bb87-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	74	t	["Können wir Ihr Skript zur Vorlesung haben, weil ich finde die nicht auf Olat\\n"]	0	f	f	\N	2022-10-24 14:06:07.294607	f	[]	[{"dep":["oa"],"text":"Skript"},{"dep":["nk"],"text":"Vorlesung"},{"dep":["nk"],"text":"Olat"}]	0	0	0	DE	\N	f	\N	\N	\N	0
bce7235a-2ef4-11eb-a952-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	75	t	["Muss die empirische Formel ganzzahlige Zahlen beinhalten? Oder gehen auch Kommazahlen?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:07.301266	f	[]	[{"dep":["ROOT"],"text":"Muss"},{"dep":["sb"],"text":"Formel"},{"dep":["oa"],"text":"Zahl"}]	0	0	0	DE	\N	f	\N	\N	\N	0
bd4c0c2e-2ef5-11eb-ac45-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	76	t	["Was bedeutet ein nach unten zeigendes Pfeik\\n"]	0	f	f	\N	2022-10-24 14:06:07.306736	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
53d560fe-2ef2-11eb-a952-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	77	t	["Könnten sie bitte nochmal den QR Code für die Umfragen zeigen? \\n"]	0	f	f	\N	2022-10-24 14:06:07.312777	f	[]	[{"dep":["oa"],"text":"QR Code"},{"dep":["nk"],"text":"Umfrage"}]	0	0	0	DE	\N	f	\N	\N	\N	0
085bb2b0-2ef5-11eb-87ab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	78	t	["Wenn 1/2 bei der Empirischen Formel rauskommt, stimmt etwas nicht oder? \\n\\n"]	0	f	f	\N	2022-10-24 14:06:07.318861	f	[]	[{"dep":["nk"],"text":"Formel"}]	0	0	0	DE	\N	f	\N	\N	\N	0
b4fb1154-2ef6-11eb-a952-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	79	t	["dann\\n"]	0	f	f	\N	2022-10-24 14:06:07.324475	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
c5585396-2efa-11eb-a952-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	80	t	["Dankeschön !!\\n"]	0	f	f	\N	2022-10-24 14:06:07.330112	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
cb3671de-2ef6-11eb-bb4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	81	t	["Wie liest man den Pfeil bei der kompakten Gleichung vor? \\n"]	0	f	f	\N	2022-10-24 14:06:07.33605	f	[]	[{"dep":["oa"],"text":"Pfeil"},{"dep":["nk"],"text":"Gleichung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
80886760-2efa-11eb-87ab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	82	t	["z.Bsp. Mehr Edukt hinzugeben und Produkte entfernen\\n"]	0	f	f	\N	2022-10-24 14:06:07.341799	f	[]	[{"dep":["oa"],"text":"Edukt"},{"dep":["oa"],"text":"Produkt"}]	0	0	0	DE	\N	f	\N	\N	\N	0
bd950528-2efa-11eb-87ab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	83	t	[" Vielen dank\\n"]	0	f	f	\N	2022-10-24 14:06:07.347071	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
d203c300-2efa-11eb-a778-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	84	t	["Danke und auf wiedersehen\\n"]	0	f	f	\N	2022-10-24 14:06:07.352431	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
a7a70da0-2ef6-11eb-a778-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	85	t	["Ein Katalysator beeinflusst das chemische Gleichgewicht nicht \\n"]	0	f	f	\N	2022-10-24 14:06:07.35887	f	[]	[{"dep":["sb"],"text":"Katalysator"},{"dep":["ROOT"],"text":"Gleichgewicht"}]	0	0	0	DE	\N	f	\N	\N	\N	0
af3dddfa-2ef6-11eb-ac45-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	86	t	["Ist NH3 nicht dnn auch eigentlich ein Edukt?\\n"]	0	f	f	\N	2022-10-24 14:06:07.367385	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
56439cd8-32e0-11eb-ae7b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	87	t	["was steht da? Wärme ist eine ....\\n"]	0	f	f	\N	2022-10-24 14:06:07.372892	f	[]	[{"dep":["sb"],"text":"Wärme"}]	0	0	0	DE	\N	f	\N	\N	\N	0
ab590ee4-32e8-11eb-970c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	88	t	["Bis Mittwoch danke \\n"]	0	f	f	\N	2022-10-24 14:06:07.37823	f	[]	[{"dep":["nk"],"text":"Mittwoch"}]	0	0	0	DE	\N	f	\N	\N	\N	0
671c0abc-32e6-11eb-970c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	89	t	["was hat es mit diesem dt und dc auf sich\\n"]	0	f	f	\N	2022-10-24 14:06:07.384057	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
aa23d3b8-2ef8-11eb-b394-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	90	t	["Wie bestimme ich die Konzentration c von Stoffen ?  c= n/V ?\\n"]	0	f	f	\N	2022-10-24 14:06:07.390209	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
fe01ab02-3ddc-11eb-8f4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	91	t	["Muss man sich nochmal für den 2.3. anmelden, wenn man sich schon vorher für die Klausur angemeldet hatte? Also als die noch am 1.3. stattfinden sollte?\\n"]	0	f	f	\N	2022-10-24 14:06:07.397236	f	[]	[{"dep":["nk"],"text":"Klausur"}]	0	0	0	DE	\N	f	\N	\N	\N	0
737118c8-3473-11eb-8452-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	92	t	["Was sagt uns das t1/2 genau?\\n"]	0	f	f	\N	2022-10-24 14:06:07.403016	f	[]	[{"dep":["sb"],"text":"t1/2"}]	0	0	0	DE	\N	f	\N	\N	\N	0
b4a4346e-3861-11eb-84d2-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	93	t	["Ist Stärke kristasllin? Sie besteht ja aus Amylose und Amylopektin.\\n"]	0	f	f	\N	2022-10-24 14:06:07.409326	f	[]	[{"dep":["nk"],"text":"Amylose"},{"dep":["cj"],"text":"Amylopektin"}]	0	0	0	DE	\N	f	\N	\N	\N	0
0be263f6-3860-11eb-831c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	94	t	["Bindet das partial positiv geladene H-Atom genauer gesagt an das freie Elektronenpaar des O-Atoms oder an den Sauerstoff allgemein? (Erbsenzählerei, ich weiß...)\\n"]	0	f	f	\N	2022-10-24 14:06:07.41708	f	[]	[{"dep":["sb"],"text":"H-Atom"},{"dep":["nk"],"text":"Elektronenpaar"},{"dep":["ag"],"text":"O-Atoms"},{"dep":["nk"],"text":"Sauerstoff"},{"dep":["ROOT"],"text":"Erbsenzählerei"}]	0	0	0	DE	\N	f	\N	\N	\N	0
3616b4f0-3861-11eb-97a5-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	95	t	["Wäre ein Phospholipid amorph ?\\n"]	0	f	f	\N	2022-10-24 14:06:07.423178	f	[]	[{"dep":["sb"],"text":"Phospholipid"}]	0	0	0	DE	\N	f	\N	\N	\N	0
a3b044c2-3861-11eb-97a5-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	96	t	["Woran liegt es ob zB SiO2 kristallin oder amorph vorkommt? \\n"]	0	f	f	\N	2022-10-24 14:06:07.430118	f	[]	[{"dep":["sb"],"text":"Woran"},{"dep":["cj"],"text":"amorph"}]	0	0	0	DE	\N	f	\N	\N	\N	0
e25d78ac-385c-11eb-97a5-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	97	t	["Guten Morgen :) \\n"]	0	f	f	\N	2022-10-24 14:06:07.436239	f	[]	[{"dep":["ROOT"],"text":"Guten Morgen :)"}]	0	0	0	DE	\N	f	\N	\N	\N	0
991b39b4-3865-11eb-97a5-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	98	t	["Deswegen platzen Glasflaschen Wenn man sie einfriert \\n"]	0	f	f	\N	2022-10-24 14:06:07.442116	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
b8d7e058-3866-11eb-8548-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	99	t	["Hält der Wasserdruck das untere Wasser zusätzlich flüssig?\\n"]	0	f	f	\N	2022-10-24 14:06:07.448399	f	[]	[{"dep":["sb"],"text":"Wasserdruck"},{"dep":["oa"],"text":"Wasser"}]	0	0	0	DE	\N	f	\N	\N	\N	0
8a74c97e-3861-11eb-8548-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	100	t	["Kriegt man amorphe Stoffe leichter kaputt?\\n"]	0	f	f	\N	2022-10-24 14:06:07.454412	f	[]	[{"dep":["oa"],"text":"Stoff"}]	0	0	0	DE	\N	f	\N	\N	\N	0
cd8507fe-3868-11eb-8548-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	101	t	["tschüss\\n"]	0	f	f	\N	2022-10-24 14:06:07.460678	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
e5fc35fe-385f-11eb-8548-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	102	t	["Also treten bei Wasser im gasförmigen zustand keine HBB auf oder nur wenige?\\n"]	0	f	f	\N	2022-10-24 14:06:07.467046	f	[]	[{"dep":["nk"],"text":"Wasser"},{"dep":["sb"],"text":"HBB"}]	0	0	0	DE	\N	f	\N	\N	\N	0
9661af68-3866-11eb-9750-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	103	t	["Die Frage passt zwar nicht direkt aber wann genau benutzt man Grad Celsius und wann Kelvin? \\n"]	0	f	f	\N	2022-10-24 14:06:07.473793	f	[]	[{"dep":["oa"],"text":"Frage"},{"dep":["oa"],"text":"Grad Celsius"},{"dep":["cj"],"text":"Kelvin"}]	0	0	0	DE	\N	f	\N	\N	\N	0
6ef71d12-3868-11eb-9750-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	104	t	["Läuft der Lösungsvorgang freiwillig ab oder muss Energie reingesteckt werden?\\n"]	0	f	f	\N	2022-10-24 14:06:07.479714	f	[]	[{"dep":["sb"],"text":"Energie"}]	0	0	0	DE	\N	f	\N	\N	\N	0
700af7dc-3868-11eb-9750-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	105	t	["Ist die 80 die Dielektrizitätskonstante von Wasser?\\n"]	0	f	f	\N	2022-10-24 14:06:07.486032	f	[]	[{"dep":["pd"],"text":"Dielektrizitätskonstante"},{"dep":["nk"],"text":"Wasser"}]	0	0	0	DE	\N	f	\N	\N	\N	0
8611dc34-3864-11eb-9750-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	106	t	["Könnten Sie bitte Seite 7 nochmal kurz zeigen\\nDanke\\n"]	0	f	f	\N	2022-10-24 14:06:07.492625	f	[]	[{"dep":["oa"],"text":"Seite"},{"dep":["oa"],"text":"Dank"}]	0	0	0	DE	\N	f	\N	\N	\N	0
b6d1f66a-385f-11eb-b27c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	107	t	["Können sie den Satz bitte nochmlal vorlesen (Gase/flüssigkeiten) ?\\n"]	0	f	f	\N	2022-10-24 14:06:07.498762	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
f7820c72-32dd-11eb-803e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	108	t	["@7481 es wird 0,1 mol umgesetzt 55,51-,1=55,41\\n"]	0	f	f	\N	2022-10-24 14:06:07.504544	f	[]	[{"dep":["mo"],"text":"mol"}]	0	0	0	DE	\N	f	\N	\N	\N	0
c93fe942-3f76-11eb-929d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	109	t	["bei einer Säurekonzentration, die größer ist als 1 mol/L lässt sich der pH-Wert nicht berechnen... wie geht man dann vor?\\n"]	0	f	f	\N	2022-10-24 14:06:07.511214	f	[]	[{"dep":["pnc"],"text":"mol/L"},{"dep":["sb"],"text":"pH-Wert"}]	0	0	0	DE	\N	f	\N	\N	\N	0
d2497ec0-32df-11eb-ae7b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	110	t	["könnte man nicht auch einen katalysator verwenden?\\n"]	0	f	f	\N	2022-10-24 14:06:07.516805	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
45f278da-3f75-11eb-bbdb-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	111	t	["Man kann ja sagen, dass pKs + pKb = 14 = pH + pOH oder?\\n"]	0	f	f	\N	2022-10-24 14:06:07.522703	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
45ed48bc-3f78-11eb-bbdb-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	112	t	["warum ist diese verhältnis nochmal wichtig für das puffersystem? ich habe es nicht richtg verstanden.\\n\\n"]	0	f	f	\N	2022-10-24 14:06:07.52933	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
92b3f740-3f78-11eb-bbdb-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	113	t	["Im skript sind der pKs und pH- Wert in der Rechnung vertauscht\\n\\n"]	0	f	f	\N	2022-10-24 14:06:07.534923	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
1d458670-3f75-11eb-bbdb-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	114	t	["Was ist der Unterschied zwischen dem pKs und Ks wert?\\n"]	0	f	f	\N	2022-10-24 14:06:07.540696	f	[]	[{"dep":["pd"],"text":"Unterschied"},{"dep":["nk"],"text":"pKs"}]	0	0	0	DE	\N	f	\N	\N	\N	0
66a0a25e-32e7-11eb-958e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	115	t	["Du kannst sie dir ja später nochmal anhören.\\n"]	0	f	f	\N	2022-10-24 14:06:07.54609	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
048ae138-32e7-11eb-958e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	116	t	["danke\\n"]	0	f	f	\N	2022-10-24 14:06:07.551386	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
386190ea-2ef2-11eb-bb4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	117	t	["Na 1mal Cl 1mal\\n"]	0	f	f	\N	2022-10-24 14:06:07.556892	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
5a8094d4-2ef0-11eb-bb4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	118	t	["Warum bei n=cxV die  2 ?\\n"]	0	f	f	\N	2022-10-24 14:06:07.562506	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
023e811c-2d62-11eb-b9da-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	119	t	["molaren nicht polaren Massen meinte ich\\n"]	0	f	f	\N	2022-10-24 14:06:07.568368	f	[]	[{"dep":["nk"],"text":"molaren"},{"dep":["oa"],"text":"Masse"}]	0	0	0	DE	\N	f	\N	\N	\N	0
4298857a-2d60-11eb-b524-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	120	t	["Die Stoffmenge ist doch ein kleines n? Warum schreiben Sie dann ein großes N?\\n"]	0	f	f	\N	2022-10-24 14:06:07.574267	f	[]	[{"dep":["sb"],"text":"Stoffmenge"}]	0	0	0	DE	\N	f	\N	\N	\N	0
7e2c124a-5575-11eb-b3c0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	121	t	["Und wofür macht man das ganze \\n"]	0	f	f	\N	2022-10-24 14:06:07.579723	f	[]	[]	-4	0	4	AUTO	\N	f	\N	\N	\N	0
82792668-2d65-11eb-b9da-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	122	t	["Wieso nimmt der Atomradius in der Periode durch zunehmende Protonenzahl ab? Die Elektronenzahl steigt proportional ja auch?\\n"]	0	f	f	\N	2022-10-24 14:06:07.587307	f	[]	[{"dep":["sb"],"text":"Atomradius"},{"dep":["nk"],"text":"Periode"},{"dep":["nk"],"text":"Protonenzahl"},{"dep":["mo"],"text":"proportional"}]	0	0	0	DE	\N	f	\N	\N	\N	0
892560bc-2d65-11eb-95ba-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	123	t	["Können Sie bitte erklären warum die Ladung neutral sein müssen?\\n"]	0	f	f	\N	2022-10-24 14:06:07.593451	f	[]	[{"dep":["sb"],"text":"Ladung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
eba96a6e-2eee-11eb-a71c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	124	t	["Ja...Wiederholung \\n"]	0	f	f	\N	2022-10-24 14:06:07.599396	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
a6475e42-3ddd-11eb-b97a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	125	t	["Doch, beide sind synchron. Aktualisier einmal deinen Browser.\\n"]	0	f	f	\N	2022-10-24 14:06:07.605663	f	[]	[{"dep":["pd"],"text":"synchron"},{"dep":["oa"],"text":"Browser"}]	2	2	0	DE	\N	f	\N	\N	\N	0
3a86a702-2ef2-11eb-a71c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	126	t	["elementarzelle bitte zeigen \\n"]	0	f	f	\N	2022-10-24 14:06:07.611455	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
1f6eed4c-53e8-11eb-8edf-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	127	t	["Worin liegt der Unterschied zwischen dem \\"mit Wasser\\" und der Hydratisierung von bspw. Ionen in Wasser? Für mich ist der Unterschied nicht klar.\\n"]	0	f	f	\N	2022-10-24 14:06:07.619116	f	[]	[{"dep":["mo"],"text":"Worin"},{"dep":["sb"],"text":"Unterschied"},{"dep":["nk"],"text":"Wasser"},{"dep":["cj"],"text":"Hydratisierung"},{"dep":["nk"],"text":"Ionen"}]	1	1	0	DE	\N	f	\N	\N	\N	0
3a3e1806-2ef8-11eb-8a62-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	128	t	["Ist es nicht krück durch khin?\\n"]	0	f	f	\N	2022-10-24 14:06:07.624962	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
90c74bf0-2efa-11eb-8a62-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	129	t	["Ist die letzte Reaktion mit dem Chlor endo- oder exotherm\\n"]	0	f	f	\N	2022-10-24 14:06:07.631388	f	[]	[{"dep":["sb"],"text":"Reaktion"},{"dep":["nk"],"text":"Chlor"}]	0	0	0	DE	\N	f	\N	\N	\N	0
b37204e2-3472-11eb-a957-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	130	t	["Müssten man die Zeit in Sekunden umrechnen ?\\n"]	0	f	f	\N	2022-10-24 14:06:07.637781	f	[]	[{"dep":["oa"],"text":"Zeit in Sekunden"}]	0	0	0	DE	\N	f	\N	\N	\N	0
ef5f0d06-3472-11eb-aab8-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	132	t	["warum kommt da d hoch -1 raus\\n\\n"]	0	f	f	\N	2022-10-24 14:06:07.649068	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
17ded506-3476-11eb-a2c7-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	133	t	["Müssen sich alle physikalischen Eigenschaften ändern?\\n"]	0	f	f	\N	2022-10-24 14:06:07.655406	f	[]	[{"dep":["sb"],"text":"Eigenschaft"}]	0	0	0	DE	\N	f	\N	\N	\N	0
a885d77c-3475-11eb-a2c7-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	134	t	["Q=K da sich die Reaktion im Ggw befindet\\n"]	0	f	f	\N	2022-10-24 14:06:07.661699	f	[]	[{"dep":["sb"],"text":"Reaktion"},{"dep":["nk"],"text":"Ggw"}]	0	0	0	DE	\N	f	\N	\N	\N	0
e47294c0-39fa-11eb-bb0b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	135	t	["tschö ^^\\n"]	0	f	f	\N	2022-10-24 14:06:07.667359	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
329f8456-39fa-11eb-bb0b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	136	t	["Wie kommen sie auf ΔH Gitter ? Wir haben sie das berechnet ?\\n"]	0	f	f	\N	2022-10-24 14:06:07.673022	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
8d16b12a-39fa-11eb-bb0b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	137	t	["gerne :)\\n"]	0	f	f	\N	2022-10-24 14:06:07.678557	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
e135c9ea-39f4-11eb-bb0b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	138	t	["Ist KL in der Klausur immer gegeben ?\\n"]	0	f	f	\N	2022-10-24 14:06:07.684435	f	[]	[{"dep":["nk"],"text":"Klausur"}]	0	0	0	DE	\N	f	\N	\N	\N	0
6125a784-39f4-11eb-8bb0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	139	t	["alles klar, vielen Dank!\\n"]	0	f	f	\N	2022-10-24 14:06:07.690103	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
705e6596-39fa-11eb-93b0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	140	t	["@3705 Danke das habe ich gemeint hat sich damit erledigt :) \\n"]	0	f	f	\N	2022-10-24 14:06:07.696093	f	[]	[{"dep":["ROOT"],"text":"Dank"}]	0	0	0	DE	\N	f	\N	\N	\N	0
4ad713ce-39f8-11eb-93b0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	141	t	["ist bei o2- nicht  [Ne]?\\n"]	0	f	f	\N	2022-10-24 14:06:07.701561	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
62b5af02-39f1-11eb-8bb0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	142	t	["Woher weiß man dass ein Gleichgewicht vorliegt? \\n"]	0	f	f	\N	2022-10-24 14:06:07.70738	f	[]	[{"dep":["nk"],"text":"Gleichgewicht"}]	0	0	0	DE	\N	f	\N	\N	\N	0
16c77eda-39f2-11eb-831c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	143	t	["Es klingt wie als käme das Geräusch von der Kamera/dem Anschluss und nicht von ihnen\\n"]	0	f	f	\N	2022-10-24 14:06:07.714327	f	[]	[{"dep":["sb"],"text":"Geräusch"},{"dep":["nk"],"text":"Anschluss"}]	0	0	0	DE	\N	f	\N	\N	\N	0
5ad39aaa-39f7-11eb-831c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	144	t	["Bei mir stand leider, dass die Umfrage noch nicht gestartet ist\\n"]	0	f	f	\N	2022-10-24 14:06:07.720357	f	[]	[{"dep":["sb"],"text":"Umfrage"}]	0	0	0	DE	\N	f	\N	\N	\N	0
a085da30-39f3-11eb-831c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	145	t	["Könnten Sie nochmal erklären warum die Konzentration von Cl und Ag gleich ist? \\n"]	0	f	f	\N	2022-10-24 14:06:07.726376	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
443b584e-385f-11eb-84d2-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	146	t	["Was ist eine \\"Formalladung\\"?\\n"]	0	f	f	\N	2022-10-24 14:06:07.732416	f	[]	[{"dep":["pd"],"text":"Formalladung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
6e808042-39fa-11eb-831c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	147	t	["Ich meinte Δ H F. Tut mir leid \\n"]	0	f	f	\N	2022-10-24 14:06:07.738208	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
8902ffea-3864-11eb-831c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	148	t	["sollte man auch die druck/Temp zahlen vom diagramm kennen?\\n"]	0	f	f	\N	2022-10-24 14:06:07.744493	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
192da910-3862-11eb-831c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	149	t	["Deswegen sind auch Keramikbremsscheiben hochwertiger :)\\n"]	0	f	f	\N	2022-10-24 14:06:07.750487	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
5922f4cc-3866-11eb-9cd2-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	150	t	["Können sie noch einmal das roh-T Diagramm ganz zeigen?\\n"]	0	f	f	\N	2022-10-24 14:06:07.756861	f	[]	[{"dep":["oa"],"text":"roh-T Diagramm"}]	0	0	0	DE	\N	f	\N	\N	\N	0
6668b7c8-3868-11eb-9cd2-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	151	t	["1/80 oder 1/80^2\\n"]	0	f	f	\N	2022-10-24 14:06:07.763032	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
5020cf1a-3867-11eb-9cd2-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	152	t	["Warum klebt die Zunge an kalten Sachen fest ?\\n"]	0	f	f	\N	2022-10-24 14:06:07.769498	f	[]	[{"dep":["mo"],"text":"Warum"},{"dep":["sb"],"text":"Zunge"},{"dep":["nk"],"text":"Sache"}]	0	0	0	DE	\N	f	\N	\N	\N	0
e76270b8-2eee-11eb-a71c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	153	t	["Ja, schau auf Backup, gibt 2 links auf olat\\n"]	0	f	f	\N	2022-10-24 14:06:07.775582	f	[]	[{"dep":["sb"],"text":"schau"},{"dep":["nk"],"text":"Backup"},{"dep":["nk"],"text":"olat"}]	0	0	0	DE	\N	f	\N	\N	\N	0
bb4f84ca-2ef3-11eb-8a62-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	154	t	["Könnten Sie den Würfel mal drehen und nochmal zeigen wie genau er aussieht?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:07.781786	f	[]	[{"dep":["oa"],"text":"Würfel"}]	0	0	0	DE	\N	f	\N	\N	\N	0
37063c04-f62a-11ec-8cfa-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	10	t	["What is Yij ?\\n"]	0	f	f	\N	2022-10-24 14:06:07.787268	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
1bd0be12-2d65-11eb-95ba-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	155	t	["Nochmal die Bedeutung der Pfeile kurz erklären bitte \\n"]	0	f	f	\N	2022-10-24 14:06:07.793134	f	[]	[{"dep":["oa"],"text":"Bedeutung"},{"dep":["ag"],"text":"Pfeil"}]	0	0	0	DE	\N	f	\N	\N	\N	0
ec261a0e-2d64-11eb-9ff0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	156	t	["@2624 bei mir war so am anfang...aber durch handy „fliesst“! Guck gerade mit dem handy! Versuchma auch!\\n"]	0	f	f	\N	2022-10-24 14:06:07.79917	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
fa64b292-2ef4-11eb-bb4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	157	t	["Sorry, dass ich so blöd frage aber warum haben wir das Würfel-Experiment in diesem Kontext behandelt?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:07.805964	f	[]	[{"dep":["oa"],"text":"Sorry"},{"dep":["sb"],"text":"fragen"},{"dep":["oa"],"text":"Würfel-Experiment"},{"dep":["nk"],"text":"Kontext"}]	0	0	0	DE	\N	f	\N	\N	\N	0
ed8da37a-32dd-11eb-ae7b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	158	t	["55,51 - 0,1 = 55,41 \\n"]	0	f	f	\N	2022-10-24 14:06:07.811868	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
ceb504ce-2ef6-11eb-a71c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	159	t	["Ich weiß, aber man kann die Aktivierungsenergie damit senken\\n"]	0	f	f	\N	2022-10-24 14:06:07.818334	f	[]	[{"dep":["oa"],"text":"Aktivierungsenergie"}]	0	0	0	DE	\N	f	\N	\N	\N	0
ceefc29a-2efa-11eb-8a62-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	160	t	["Vielen Dank!\\n"]	0	f	f	\N	2022-10-24 14:06:07.82428	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
c1d6b05a-3de6-11eb-8f4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	161	t	["Woher weiß ich welche Produkte entstehen?\\n"]	0	f	f	\N	2022-10-24 14:06:07.830702	f	[]	[{"dep":["oa"],"text":"Produkt"}]	0	0	0	DE	\N	f	\N	\N	\N	0
57f51f7a-2ef8-11eb-9ede-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	162	t	["wie denn wenn du durch k(rück) teilst?\\n"]	0	f	f	\N	2022-10-24 14:06:07.836844	f	[]	[{"dep":["par"],"text":"rück"},{"dep":["ROOT"],"text":"teilen"}]	0	0	0	DE	\N	f	\N	\N	\N	0
b812b4f4-346f-11eb-b7c6-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	163	t	["Kann eine Reaktion exotherm und trotzdem Endergon sein ? Denn eine exotherme Reaktion läuft ja auch durch eine Aktivierungsenergie ab. Bei einer  endergonen Reaktion muss doch durchgängig Energie gegeben werden, denn dann hört sie doch auf.\\n"]	0	f	f	\N	2022-10-24 14:06:07.844583	f	[]	[{"dep":["nk","sb"],"text":"Reaktion"},{"dep":["pd"],"text":"Endergon"},{"dep":["nk"],"text":"Aktivierungsenergie"},{"dep":["sb"],"text":"Energie"}]	0	0	0	DE	\N	f	\N	\N	\N	0
8945a1bc-3475-11eb-bb87-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	164	t	["wie kamen sie von ln Q auf ln K\\n"]	0	f	f	\N	2022-10-24 14:06:07.850455	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
f607d200-3472-11eb-bb87-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	165	t	["@6229 kommt drauf an, welche einheiten verfügbar sind für die anderen Größen\\n"]	0	f	f	\N	2022-10-24 14:06:07.857139	f	[]	[{"dep":["sb"],"text":"einheiten"},{"dep":["nk"],"text":"Größe"}]	0	0	0	DE	\N	f	\N	\N	\N	0
c12900a2-3474-11eb-bb87-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	166	t	["Hat sich das Gleichgewicht noch nicht eingestellt, so ist Q nicht konstant, also man kann in diesem Fall nur ein momentanes Q angeben wie bei der Momentangeschwindigkeit, oder? D.h. Q ändert sich bis zum Gleichgewicht ständig?\\n"]	0	f	f	\N	2022-10-24 14:06:07.86501	f	[]	[{"dep":["nk","sb"],"text":"Gleichgewicht"},{"dep":["nk"],"text":"Fall"},{"dep":["nk"],"text":"Momentangeschwindigkeit"},{"dep":["ROOT"],"text":"D.h"}]	0	0	0	DE	\N	f	\N	\N	\N	0
e7b2263a-347a-11eb-bb87-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	167	t	["sind Vdw Kräfte nicht temporäre Dipole?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:07.870774	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
b21dc68e-3ddd-11eb-bae4-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	168	t	["Ja synchroner \\n"]	0	f	f	\N	2022-10-24 14:06:07.876408	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
a47ab558-3475-11eb-aab8-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	169	t	["Wie bei Wasser und Öl ?\\n"]	0	f	f	\N	2022-10-24 14:06:07.882112	f	[]	[{"dep":["nk"],"text":"Wasser"}]	0	0	0	DE	\N	f	\N	\N	\N	0
8f50628c-3479-11eb-9feb-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	170	t	["Können sie das Blatt gleich bitte nochmal komplett zeigen\\n"]	0	f	f	\N	2022-10-24 14:06:07.88818	f	[]	[{"dep":["oa"],"text":"Blatt"}]	0	0	0	DE	\N	f	\N	\N	\N	0
5ab7c54c-3479-11eb-9feb-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	171	t	["78%\\n"]	0	f	f	\N	2022-10-24 14:06:07.893876	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
5c61ccda-3479-11eb-8452-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	172	t	["20%?\\n"]	0	f	f	\N	2022-10-24 14:06:07.899401	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
6501d182-3479-11eb-8e78-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	173	t	["ca 78 %\\n"]	0	f	f	\N	2022-10-24 14:06:07.904924	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
5f88b518-3479-11eb-8452-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	174	t	["78%\\n"]	0	f	f	\N	2022-10-24 14:06:07.910679	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
3d58fd1e-3478-11eb-8e78-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	175	t	["was steht im letzten bruch\\n"]	0	f	f	\N	2022-10-24 14:06:07.916743	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
dbd53438-347a-11eb-8e78-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	176	t	["Danke für die Vorlesung :)\\n"]	0	f	f	\N	2022-10-24 14:06:07.923068	f	[]	[{"dep":["ROOT"],"text":"Dank"},{"dep":["nk"],"text":"Vorlesung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
0adc840e-39f6-11eb-8bb0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	177	t	["warum ist die e-dichte in I2 größer, wenn I2 eine höhere Ordnungszahl und daher mehr elektronen hat, die sich in die hülle verteilen können\\n\\n"]	0	f	f	\N	2022-10-24 14:06:07.930132	f	[]	[{"dep":["sb"],"text":"e-dichte"},{"dep":["oa"],"text":"Ordnungszahl"},{"dep":["nk"],"text":"hüllen"}]	0	0	0	DE	\N	f	\N	\N	\N	0
3325deee-39f4-11eb-9fde-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	178	t	["Wieso ist bei dem Beispiel wo die Konzentration von Cl- gesucht ist nicht wieder c(Cl-)=c(Ag+)? denn das Stoffmengenverhältnis ist ja laut reaktionsgleichung noch immer 1/1\\n"]	0	f	f	\N	2022-10-24 14:06:07.936698	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
785ce0d4-3864-11eb-a58e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	179	t	["Müssen wir die Werte der Kurven kennen? \\n"]	0	f	f	\N	2022-10-24 14:06:07.943121	f	[]	[{"dep":["oa"],"text":"Wert"},{"dep":["ag"],"text":"Kurven"}]	0	0	0	DE	\N	f	\N	\N	\N	0
02055018-3866-11eb-a58e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	180	t	["Deswegen lässt man Bier nicht über Nacht in der Tiefkühltruhe?\\n"]	0	f	f	\N	2022-10-24 14:06:07.949821	f	[]	[{"dep":["oa"],"text":"Bier"},{"dep":["nk"],"text":"Nacht"},{"dep":["nk"],"text":"Tiefkühltruhe"}]	0	0	0	DE	\N	f	\N	\N	\N	0
c45a37a8-3868-11eb-a58e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	181	t	["Tschööööö\\n"]	0	f	f	\N	2022-10-24 14:06:07.95582	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
06381dd8-3f72-11eb-9f30-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	182	t	["müsste bei pH=1 nicht c(h3o+)= 0,1 (10^-1) sein\\n"]	0	f	f	\N	2022-10-24 14:06:07.96187	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
7b8c9cd8-3862-11eb-9cd2-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	183	t	["Wofür steht Smp?\\n"]	0	f	f	\N	2022-10-24 14:06:07.967537	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
0e13bdb8-3f73-11eb-b250-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	184	t	["Hat die Dissoziation was mit der Reaktivität.  der Säure zu tun ? Oder mit was anderem ? \\n"]	0	f	f	\N	2022-10-24 14:06:07.973468	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
50109762-3f79-11eb-9f30-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	185	t	["bedeutet das, dass alle bronstedt säuren bzw. basen auch Lewis-Säuren sind?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:07.979595	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
5a775308-3f79-11eb-9f30-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	186	t	["* und Lewis B>asen\\n"]	0	f	f	\N	2022-10-24 14:06:07.985335	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
6e7341dc-3f74-11eb-8736-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	187	t	["Ahh okay also genau andersherum\\n"]	0	f	f	\N	2022-10-24 14:06:07.991387	f	[]	[{"dep":["mo"],"text":"Ahh"}]	0	0	0	DE	\N	f	\N	\N	\N	0
c2d5ccbe-3f73-11eb-8736-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	188	t	["Können Sie Blatt 6 nochmal kurz zeigen, bitte?\\n"]	0	f	f	\N	2022-10-24 14:06:07.997385	f	[]	[{"dep":["oa"],"text":"Blatt"}]	0	0	0	DE	\N	f	\N	\N	\N	0
a9168260-3f77-11eb-8736-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	189	t	["ich hatte meine Frage doof gestellt. Was ich meine ist, dass wenn man den pH einer Lösung mit bsp c(Säure)=10mol/L berechnet (bin von einer starken säure ausgegangen) ergibt sich ein pH von -1. Meine Frage ist nun. was einem das genau sagen soll, denn der pH-Bereich den wir definiert haben geht von 1 bis 14 ... was sagt einem dann der pH von -1\\n"]	0	f	f	\N	2022-10-24 14:06:08.005759	f	[]	[{"dep":["pd","oa"],"text":"Frage"},{"dep":["ag"],"text":"Lösung"},{"dep":["oa"],"text":"Säure)=10mol/L"},{"dep":["nk"],"text":"säure"}]	0	0	0	DE	\N	f	\N	\N	\N	0
3008688c-3f79-11eb-8736-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	190	t	["Wofür steht nochmal HX und X-\\n"]	0	f	f	\N	2022-10-24 14:06:08.011864	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
013c6b16-3f79-11eb-b334-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	191	t	["im Blut zB \\n"]	0	f	f	\N	2022-10-24 14:06:08.017642	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
3f116394-3f77-11eb-b334-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	192	t	["Machen wir auch Supersäuren?\\n"]	0	f	f	\N	2022-10-24 14:06:08.023951	f	[]	[{"dep":["oa"],"text":"Supersäuren"}]	0	0	0	DE	\N	f	\N	\N	\N	0
e5a52008-3f7a-11eb-b250-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	193	t	["Bis zum nächsten Mal danke!\\n"]	0	f	f	\N	2022-10-24 14:06:08.030022	f	[]	[{"dep":["nk"],"text":"Mal"}]	0	0	0	DE	\N	f	\N	\N	\N	0
ef9a1870-3f7a-11eb-9840-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	194	t	["Frohe Weihnachten!\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.036247	f	[]	[{"dep":["ROOT"],"text":"Frohe Weihnachten"}]	0	0	0	DE	\N	f	\N	\N	\N	0
f306c18e-3f7a-11eb-9840-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	195	t	["Dankeschön, Sie auch!\\n"]	0	f	f	\N	2022-10-24 14:06:08.042754	f	[]	[{"dep":["ROOT"],"text":"Dankeschön"}]	0	0	0	DE	\N	f	\N	\N	\N	0
0e11ca7c-3f77-11eb-9840-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	196	t	["Gibt es negative pH-Werte?\\n"]	0	f	f	\N	2022-10-24 14:06:08.049369	f	[]	[{"dep":["oa"],"text":"pH-Werte"}]	0	0	0	DE	\N	f	\N	\N	\N	0
a32ca9c0-53e4-11eb-86eb-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	197	t	["bitte nochmal Seite 3 zeigen \\n"]	0	f	f	\N	2022-10-24 14:06:08.055671	f	[]	[{"dep":["oa"],"text":"Seite"}]	0	0	0	DE	\N	f	\N	\N	\N	0
d2f3c182-53e6-11eb-86eb-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	198	t	["Vielleicht noch etwas mehr wasser dazugeben?\\n"]	0	f	f	\N	2022-10-24 14:06:08.061872	f	[]	[{"dep":["oa"],"text":"wasser"}]	0	0	0	DE	\N	f	\N	\N	\N	0
54698a08-32e0-11eb-9d68-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	199	t	["Bedeutet „alltagstauglich“ auch klausurtauglich ? \\n"]	0	f	f	\N	2022-10-24 14:06:08.067779	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
313c139c-5573-11eb-b3c0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	230	t	["Wäre es in der Klausur egal ob wir Gruppe 13 oder 3. Hauptgründe schreiben ?\\n"]	0	f	f	\N	2022-10-24 14:06:08.289735	f	[]	[{"dep":["nk"],"text":"Klausur"},{"dep":["pd"],"text":"Gruppe 13"}]	0	0	0	DE	\N	f	\N	\N	\N	0
5608f800-32de-11eb-9d68-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	200	t	["Wenn man einer der Produkte \\"wegnimmt\\". würde sich das chem. Gleichgewicht zu den Edukten verschieben, oder müssen alle Produkte entnommen werden?\\n"]	0	f	f	\N	2022-10-24 14:06:08.075128	f	[]	[{"dep":["ag","sb"],"text":"Produkt"},{"dep":["sb"],"text":"chem"},{"dep":["oa"],"text":"Gleichgewicht"}]	0	0	0	DE	\N	f	\N	\N	\N	0
7a09417a-556f-11eb-b524-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	201	t	["es rauscht so sehr dass man sie nicht versteht\\n"]	0	f	f	\N	2022-10-24 14:06:08.081158	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
aebe997c-5571-11eb-b524-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	202	t	["Die Rauschmeldungen sind immer zeitverzögert, bisher hatte sich das in der Zwischenzeit imma selbst erledigt\\n"]	0	f	f	\N	2022-10-24 14:06:08.087614	f	[]	[{"dep":["nk"],"text":"Zwischenzeit"}]	0	0	0	DE	\N	f	\N	\N	\N	0
7326827e-5960-11eb-8009-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	203	t	["Steht dort ein einhalbe Bindung , ich kann die Zahl unter dem Bruchstrich nicht gut erkennen\\n"]	0	f	f	\N	2022-10-24 14:06:08.09476	f	[]	[{"dep":["sb"],"text":"Bindung"},{"dep":["oa"],"text":"Zahl"},{"dep":["nk"],"text":"Bruchstrich"}]	2	2	0	DE	\N	f	\N	\N	\N	0
84d348e4-5574-11eb-afe1-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	204	t	["Könnte man auch einen Katalysator verwenden? \\n"]	0	f	f	\N	2022-10-24 14:06:08.10085	f	[]	[{"dep":["oa"],"text":"Katalysator"}]	0	0	0	DE	\N	f	\N	\N	\N	0
5c638972-5575-11eb-bd00-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	205	t	["Gibt es nicht das Problem dass dabei freie Radikale entstehen? Oder ist das hier irrelevant \\n"]	0	f	f	\N	2022-10-24 14:06:08.110595	f	[]	[{"dep":["oa"],"text":"Problem"},{"dep":["sb"],"text":"Radikaler"}]	0	0	0	DE	\N	f	\N	\N	\N	0
c6221216-5967-11eb-90b8-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	206	t	["macht man die Reaktion \\"nach Destillation\\" direkt mit dem gasförmigen HSiCl3 oder muss es vorher wieder runterkühlen, sodass es fest (s) ist?\\n"]	0	f	f	\N	2022-10-24 14:06:08.119028	f	[]	[{"dep":["oa"],"text":"Reaktion"},{"dep":["nk"],"text":"Destillation"},{"dep":["nk"],"text":"HSiCl3"}]	0	0	0	DE	\N	f	\N	\N	\N	0
e8cbed04-5571-11eb-b9f3-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	207	t	["Woher weiß man wie viele negative und positive Ladungen es bei Apatit gibt ?\\n"]	0	f	f	\N	2022-10-24 14:06:08.125966	f	[]	[{"dep":["mo"],"text":"Ladung"},{"dep":["nk"],"text":"Apatit"}]	0	0	0	DE	\N	f	\N	\N	\N	0
b246b0f2-5571-11eb-b9f3-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	208	t	["Können sie nochmal erklären, warum es einfach negativ ist \\n"]	0	f	f	\N	2022-10-24 14:06:08.13232	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
46c7e0a8-5968-11eb-9300-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	209	t	["steht da Züchtung von Eiskristallen oder was anderes?\\n"]	0	f	f	\N	2022-10-24 14:06:08.138657	f	[]	[{"dep":["sb"],"text":"Züchtung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
35883ac8-32e8-11eb-856c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	210	t	["Katalysatoren hinzugeben\\n"]	0	f	f	\N	2022-10-24 14:06:08.144601	f	[]	[{"dep":["sb"],"text":"Katalysator"}]	1	1	0	DE	\N	f	\N	\N	\N	0
4690a652-5af7-11eb-8009-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	211	t	["ja\\n"]	0	f	f	\N	2022-10-24 14:06:08.150273	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
f2af9e16-5af2-11eb-8009-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	212	t	["bzw. ist handelt es sich bei der Gerüststruktur auch um einen Kristall?\\n"]	0	f	f	\N	2022-10-24 14:06:08.156458	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
d7953262-5af2-11eb-a037-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	213	t	["Könnte man die Gerüststruktur auch als Kristallstruktur bezeichnen?\\n"]	0	f	f	\N	2022-10-24 14:06:08.162797	f	[]	[{"dep":["nk"],"text":"Kristallstruktur"}]	0	0	0	DE	\N	f	\N	\N	\N	0
58d65fca-53e8-11eb-86eb-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	214	t	["bitte nochmal das ende von blatt 6\\n"]	0	f	f	\N	2022-10-24 14:06:08.168843	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
d6973f94-5ee0-11eb-911e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	215	t	["Heißt das, das bei härterem Silikon mehrere kürzere Ketten vorhanden sind, als bei flüssigerem Silikon ? (Damit mehr  Kettenenden vorhanden sind) Oder sollen wir in der Klausur sagen, dass die Viskosität mit steigender Anzahl an Verzweigungen abnimmt?\\n"]	0	f	f	\N	2022-10-24 14:06:08.17767	f	[]	[{"dep":["nk"],"text":"Silikon"},{"dep":["sb"],"text":"Kette"},{"dep":["sb"],"text":"Kettenenden"},{"dep":["nk"],"text":"Klausur"},{"dep":["sb"],"text":"Viskosität"},{"dep":["nk"],"text":"Anzahl"}]	0	0	0	DE	\N	f	\N	\N	\N	0
8de31b16-39f5-11eb-8b4a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	216	t	["Blatt 8 bitte nochmal zeigen :)\\n"]	0	f	f	\N	2022-10-24 14:06:08.183868	f	[]	[{"dep":["ROOT"],"text":"Blatt"}]	0	0	0	DE	\N	f	\N	\N	\N	0
07af0026-5ee1-11eb-baab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	217	t	["Gibt ja Frauen die bin ihren silikoneinlagerungen Depressionen bekommen, reagiert dann ein Stoff aus dem Silikon mit den Hormonen ?\\n"]	0	f	f	\N	2022-10-24 14:06:08.191364	f	[]	[{"dep":["oa"],"text":"Frau"},{"dep":["oa"],"text":"Depression"},{"dep":["sb"],"text":"Stoff"},{"dep":["nk"],"text":"Silikon"},{"dep":["nk"],"text":"Hormon"}]	0	0	0	DE	\N	f	\N	\N	\N	0
fbfdb9a2-5ee5-11eb-9141-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	218	t	["Ich meinte elementares Vorkommen von Arsen, sorry\\n"]	0	f	f	\N	2022-10-24 14:06:08.197733	f	[]	[{"dep":["oa"],"text":"Vorkommen"},{"dep":["nk"],"text":"Arsen"}]	0	0	0	DE	\N	f	\N	\N	\N	0
f4a59432-5ee9-11eb-9862-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	219	t	["Wie wirkt sich eine halbe Bindung auf die reaktivitöt aus? \\n\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.203856	f	[]	[{"dep":["sb"],"text":"Bindung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
e8d1d040-5ee8-11eb-9862-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	220	t	["was steht nach sdp. ?\\n"]	0	f	f	\N	2022-10-24 14:06:08.217361	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
34589984-65f3-11eb-ab68-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	221	t	["Können Sie gerade nochmal erklären,was mit allen S-O-Bindungen sind gleich, gemeint ist ?\\n"]	0	f	f	\N	2022-10-24 14:06:08.226864	f	[]	[{"dep":["nk"],"text":"S-O-Bindungen"}]	0	0	0	DE	\N	f	\N	\N	\N	0
2393c4b8-5570-11eb-99a1-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	222	t	["Wann ist Vorlesungsende vor der Klausur ? \\n"]	0	f	f	\N	2022-10-24 14:06:08.234614	f	[]	[{"dep":["nk"],"text":"Klausur"}]	0	0	0	DE	\N	f	\N	\N	\N	0
5e9039b8-5960-11eb-90b8-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	223	t	["Auf Olat wurden 2 Videos von einem anderen Prof hochgeladen Länge 1,5h vermutlich ein Fehler beim Upload dann?\\n"]	0	f	f	\N	2022-10-24 14:06:08.242455	f	[]	[{"dep":["nk"],"text":"Olat"},{"dep":["nk"],"text":"Videos"},{"dep":["nk"],"text":"Prof"},{"dep":["sb"],"text":"Länge"},{"dep":["sb"],"text":"Fehler"},{"dep":["nk"],"text":"Upload"}]	0	0	0	DE	\N	f	\N	\N	\N	0
502a5434-556f-11eb-a9ac-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	224	t	["sie rauschen sehr stark\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.248868	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
022bd670-5eea-11eb-baab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	225	t	["@2008 denke mal für Deuterium (Isotop von Wasserstoff)\\n"]	0	f	f	\N	2022-10-24 14:06:08.258363	f	[]	[{"dep":["nk"],"text":"Deuterium"},{"dep":["par"],"text":"Isotop"},{"dep":["nk"],"text":"Wasserstoff"}]	0	0	0	DE	\N	f	\N	\N	\N	0
d9e25520-5572-11eb-a9ac-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	226	t	["Wie kommt die neue benennung der Hauptgruppen zu stande   \\n"]	0	f	f	\N	2022-10-24 14:06:08.264895	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
60d5f726-5960-11eb-90b8-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	227	t	["Bei Ihren E-Lectures ist ein anderer Prof zu sehen \\n"]	0	f	f	\N	2022-10-24 14:06:08.270974	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
e0e2db0c-5571-11eb-b3c0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	228	t	["Warum Apatit einfach negativ ist \\n"]	0	f	f	\N	2022-10-24 14:06:08.277133	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
2e3c42de-5578-11eb-a9ac-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	229	t	["können sie das Blatt nochmal hochschieben?\\n"]	0	f	f	\N	2022-10-24 14:06:08.283295	f	[]	[{"dep":["oa"],"text":"Blatt"}]	0	0	0	DE	\N	f	\N	\N	\N	0
760f6428-556f-11eb-b3c0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	231	t	["Ja sie rauschen extrem \\n"]	0	f	f	\N	2022-10-24 14:06:08.295798	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
d723a322-5570-11eb-b3c0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	232	t	["Ist Rattengift in hoher Konzentration dann auch für den Menschen tödlich ?\\n"]	0	f	f	\N	2022-10-24 14:06:08.302818	f	[]	[{"dep":["sb"],"text":"Rattengift"},{"dep":["nk"],"text":"Konzentration"},{"dep":["nk"],"text":"Mensch"}]	0	0	0	DE	\N	f	\N	\N	\N	0
746c5b28-2d68-11eb-9ff0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	233	t	["Danke und bis MIttwoch! :)\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.309321	f	[]	[{"dep":["ROOT"],"text":"Danke"},{"dep":["nk"],"text":"MIttwoch"}]	0	0	0	DE	\N	f	\N	\N	\N	0
c7b19202-2d64-11eb-8525-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	234	t	["Und Pfeil nach unten ein Niederschlag aus festen Stoffen?\\n"]	0	f	f	\N	2022-10-24 14:06:08.316185	f	[]	[{"dep":["sb"],"text":"Pfeil"},{"dep":["ROOT"],"text":"Niederschlag"},{"dep":["nk"],"text":"Stoff"}]	0	0	0	DE	\N	f	\N	\N	\N	0
325498c4-2d61-11eb-8525-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	235	t	["die Stoffmenge stehr im Nenner, n hat die Einheit mol und deswegen steht da 1/mol, was mol-1 ist\\n"]	0	f	f	\N	2022-10-24 14:06:08.326339	f	[]	[{"dep":["sb"],"text":"Stoffmenge"},{"dep":["nk"],"text":"Nenner"},{"dep":["oa"],"text":"Einheit"},{"dep":["sb"],"text":"mol-1"}]	0	0	0	DE	\N	f	\N	\N	\N	0
6cfda956-2ef7-11eb-bb4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	236	t	["was steht im Index von dc/dt in der letzten Zeile?\\n"]	0	f	f	\N	2022-10-24 14:06:08.332421	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
6faebd68-2efa-11eb-a71c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	237	t	["Können sie das Prinzip des kleinstens Zwangs nochmal auflegen biite\\n"]	0	f	f	\N	2022-10-24 14:06:08.338903	f	[]	[{"dep":["oa"],"text":"Prinzip"},{"dep":["ag"],"text":"Zwang"}]	0	0	0	DE	\N	f	\N	\N	\N	0
c101a5d6-2efa-11eb-a71c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	238	t	["bye bye\\n"]	0	f	f	\N	2022-10-24 14:06:08.344849	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
0e9c3246-32e0-11eb-b45e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	239	t	["Das live video hängt \\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.350498	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
03fd6a9e-32e0-11eb-b45e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	240	t	["Eisen\\n"]	0	f	f	\N	2022-10-24 14:06:08.356463	f	[]	[{"dep":["ROOT"],"text":"Eisen"}]	0	0	0	DE	\N	f	\N	\N	\N	0
069c1406-32e2-11eb-b45e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	241	t	["Der Reaktionspfeil\\n\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.36319	f	[]	[{"dep":["ROOT"],"text":"Reaktionspfeil"}]	0	0	0	DE	\N	f	\N	\N	\N	0
60db7748-3479-11eb-b7c6-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	242	t	["78\\n"]	0	f	f	\N	2022-10-24 14:06:08.368731	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
4ba36dee-3475-11eb-b7c6-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	243	t	["delta G = 0 wurde vorgegeben?\\n"]	0	f	f	\N	2022-10-24 14:06:08.374837	f	[]	[{"dep":["ROOT"],"text":"delta G"}]	0	0	0	DE	\N	f	\N	\N	\N	0
a178fc50-385f-11eb-a58e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	244	t	["Warum schreibt man an das O-Atom nicht 2delta-, wenn jedes H-Atom jeweils eine Partialladung von +1delta haben und Wasser nach außen neutral geladen ist?\\n"]	0	f	f	\N	2022-10-24 14:06:08.382469	f	[]	[{"dep":["nk"],"text":"O-Atom"},{"dep":["sb"],"text":"H-Atom"},{"dep":["oa"],"text":"Partialladung"},{"dep":["oa"],"text":"Wasser"}]	0	0	0	DE	\N	f	\N	\N	\N	0
4b0fc3a4-3863-11eb-a58e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	245	t	["Gibt es eine Dampfdruckkurve oder gibt es zwei (eine für Eis und eine für Wasser)?\\n"]	0	f	f	\N	2022-10-24 14:06:08.389328	f	[]	[{"dep":["oa"],"text":"Dampfdruckkurve"},{"dep":["nk"],"text":"Eis"},{"dep":["nk"],"text":"Wasser"}]	0	0	0	DE	\N	f	\N	\N	\N	0
87d00e58-39f7-11eb-8b4a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	246	t	["Die die Umfrage hat bei mir schon aufgehört als ich drauf geklickt hab \\n"]	0	f	f	\N	2022-10-24 14:06:08.396399	f	[]	[{"dep":["sb"],"text":"Umfrage"},{"dep":["nk"],"text":"mir"}]	0	0	0	DE	\N	f	\N	\N	\N	0
76b486c2-39f1-11eb-addd-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	247	t	["Woran sieht man, dass es auch von recht nach links reagiert \\n"]	0	f	f	\N	2022-10-24 14:06:08.403426	f	[]	[{"dep":["mo"],"text":"Woran"},{"dep":["nk"],"text":"recht"}]	0	0	0	DE	\N	f	\N	\N	\N	0
82486c2c-39f8-11eb-8b4a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	248	t	["Warum genau nimmt man Ne ?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.420549	f	[]	[{"dep":["mo"],"text":"Warum"}]	0	0	0	DE	\N	f	\N	\N	\N	0
2753a03a-39f2-11eb-8b4a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	249	t	["Das Geräusch ist trotz der Pause noch zu hören\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.431506	f	[]	[{"dep":["sb"],"text":"Geräusch"},{"dep":["nk"],"text":"Pause"}]	0	0	0	DE	\N	f	\N	\N	\N	0
ee8e2c4a-39f9-11eb-8b4a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	250	t	["Bei dem Beispiel auf Seite 11 haben sie die elektronenkonfigurationen von den beiden Stoffen der von Ne angepasst um die Ladung zu bekommen. Aber warum nimmt man hier Ne ? \\n"]	0	f	f	\N	2022-10-24 14:06:08.440917	f	[]	[{"dep":["nk"],"text":"Beispiel"},{"dep":["nk"],"text":"Seite"},{"dep":["nk"],"text":"Stoff"},{"dep":["nk"],"text":"Ladung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
0ed507e0-3f71-11eb-a2fc-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	251	t	["Gibt der pOH wert die C(OH-) an ?\\n"]	0	f	f	\N	2022-10-24 14:06:08.447072	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
795e2f28-3f77-11eb-b250-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	252	t	["-1\\n"]	0	f	f	\N	2022-10-24 14:06:08.453299	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
2743079a-3f77-11eb-b250-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	253	t	["Wie berechnet man den pH-Wert von mittelstarken Säuren?\\n"]	0	f	f	\N	2022-10-24 14:06:08.460393	f	[]	[{"dep":["oa"],"text":"pH-Wert"},{"dep":["nk"],"text":"Säure"}]	0	0	0	DE	\N	f	\N	\N	\N	0
6d35daa2-3f77-11eb-a2fc-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	254	t	["Ok, danke, rechnerisch war es mir zwar klar, nur sagt man ja, der pH-Wert ginge nur von 0-14 (laut Vorwissen)...\\n"]	0	f	f	\N	2022-10-24 14:06:08.467532	f	[]	[{"dep":["sb"],"text":"pH-Wert"}]	0	0	0	DE	\N	f	\N	\N	\N	0
b5d72036-3f77-11eb-b250-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	255	t	["0 bis 14, sorry\\n"]	0	f	f	\N	2022-10-24 14:06:08.473926	f	[]	[{"dep":["npadvmod"],"text":"bis"}]	0	0	0	EN	\N	f	\N	\N	\N	0
2289f09c-3f77-11eb-b250-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	256	t	["was ist mit mittelstarken Säuren? Wie ist da die pH-Wert Berechnung?\\n"]	0	f	f	\N	2022-10-24 14:06:08.481191	f	[]	[{"dep":["nk"],"text":"Säure"},{"dep":["sb"],"text":"pH-Wert Berechnung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
13fe5f64-3f7b-11eb-8f4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	257	t	["Vielen Dank, Ihnen auch!\\n"]	0	f	f	\N	2022-10-24 14:06:08.48732	f	[]	[{"dep":["ROOT"],"text":"Dank"}]	0	0	0	DE	\N	f	\N	\N	\N	0
f91a96d6-3f7a-11eb-8f4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	258	t	["Frohe Weihnachten und einen guten Rutsch. :)\\n"]	0	f	f	\N	2022-10-24 14:06:08.494342	f	[]	[{"dep":["ROOT"],"text":"Frohe Weihnachten"},{"dep":["cj"],"text":"Rutsch"}]	0	0	0	DE	\N	f	\N	\N	\N	0
e12e2364-3f78-11eb-8f4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	259	t	["wo finden Puffersysteme ihre Anwendung?\\n"]	0	f	f	\N	2022-10-24 14:06:08.500752	f	[]	[{"dep":["oa"],"text":"Anwendung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
92b86a24-5edf-11eb-ada1-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	260	t	["warum reagiert das silikon nicht mit dem H2O im Körper?\\n"]	0	f	f	\N	2022-10-24 14:06:08.507181	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
4acc0a3c-5961-11eb-9300-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	261	t	["Die große Oberfläche von aktivkohle hängt mit dern vielen schwachen Bindungen zusammen, richtig? \\n"]	0	f	f	\N	2022-10-24 14:06:08.514597	f	[]	[{"dep":["sb"],"text":"Oberfläche"},{"dep":["nk"],"text":"aktivkohle"},{"dep":["nk"],"text":"dern"},{"dep":["nk"],"text":"Bindung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
040d185c-5961-11eb-90b8-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	262	t	["Ich meine gesichtsmasken in Beauty Produkten, ups \\n"]	0	f	f	\N	2022-10-24 14:06:08.520918	f	[]	[{"dep":["oa"],"text":"ups"}]	0	0	0	DE	\N	f	\N	\N	\N	0
9b269002-5af5-11eb-a1c0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	263	t	["Seite neu Laden und wieder reingehen dann klappt alles\\n"]	0	f	f	\N	2022-10-24 14:06:08.527712	f	[]	[{"dep":["ROOT"],"text":"Seite"},{"dep":["sb"],"text":"Laden"}]	0	0	0	DE	\N	f	\N	\N	\N	0
49976c00-5af7-11eb-a1c0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	264	t	["Ich verstehe sie, alles ist normal\\n"]	0	f	f	\N	2022-10-24 14:06:08.534048	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
30d98aa4-5ee9-11eb-a89d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	265	t	["Ammoniak wäre doch eine geeignete Verbindung, um zu zeigen, dass ein Teilchen sowohl als Base als auch als Säure reagieren kann, richtig?\\n"]	0	f	f	\N	2022-10-24 14:06:08.54255	f	[]	[{"dep":["sb"],"text":"Ammoniak"},{"dep":["pd"],"text":"Verbindung"},{"dep":["sb"],"text":"Teilchen"},{"dep":["nk"],"text":"Base"},{"dep":["nk"],"text":"Säure"}]	0	0	0	DE	\N	f	\N	\N	\N	0
e7b4649e-5afa-11eb-a058-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	266	t	["Also hängt die Summenformel sozusagen von der Betrachtungsebene ab ?\\n"]	0	f	f	\N	2022-10-24 14:06:08.549282	f	[]	[{"dep":["sb"],"text":"Summenformel"}]	0	0	0	DE	\N	f	\N	\N	\N	0
11c007c6-53de-11eb-91de-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	267	t	["ja stimmt, musste aktualisieren dann ging es\\n"]	0	f	f	\N	2022-10-24 14:06:08.555684	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
c014592e-5967-11eb-80ec-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	268	t	["Kann man diese Methode gleichzeitig zur Wasserstoffgewinnung nutzen?\\n"]	0	f	f	\N	2022-10-24 14:06:08.562602	f	[]	[{"dep":["oa"],"text":"Methode"}]	0	0	0	DE	\N	f	\N	\N	\N	0
3f766ff8-5577-11eb-a341-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	269	t	["Ich kanns bis in mein Zimmer riechen\\n"]	0	f	f	\N	2022-10-24 14:06:08.570234	f	[]	[{"dep":["nk"],"text":"Zimmer"}]	6	7	1	DE	\N	f	\N	\N	\N	0
48f6c348-5572-11eb-b9f3-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	270	t	["Ich geh jetzt erstmal Zähne putzen\\n"]	0	f	f	\N	2022-10-24 14:06:08.576621	f	[]	[{"dep":["oa"],"text":"Zahn"}]	3	4	1	DE	\N	f	\N	\N	\N	0
9ac6894e-557a-11eb-a341-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	271	t	["Was bedeutet essenziell/ nicht essenziell ? \\n"]	0	f	f	\N	2022-10-24 14:06:08.582983	f	[]	[{"dep":["oa"],"text":"essenziell/"}]	0	1	1	DE	\N	f	\N	\N	\N	0
29c6f2e0-5572-11eb-8566-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	272	t	["Die Chemie Vorlesung ist by heart <3 xD\\n"]	0	f	f	\N	2022-10-24 14:06:08.589247	f	[]	[]	5	6	1	AUTO	\N	f	\N	\N	\N	0
2a559d84-5576-11eb-bd00-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	273	t	["Warum breáucht man nochmal 2 chemische Zünder beim Thermit-VErfahren?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.595566	f	[]	[]	0	1	1	AUTO	\N	f	\N	\N	\N	0
788b404a-557a-11eb-bd00-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	274	t	["Gibt es einen Grund, warum Flevorium nicht in der Tabelle vorkommt? Ist wahrscheinlich für uns nicht relevant oder?\\n"]	0	f	f	\N	2022-10-24 14:06:08.602621	f	[]	[{"dep":["oa"],"text":"Grund"},{"dep":["nk"],"text":"Tabelle"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
d7e090ea-65f6-11eb-a63b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	275	t	["Woher wissen wie die EN von einem Element? In Ihrem PSE wieß ich nicht, wo ich sie finde.\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.627693	f	[]	[{"dep":["nk"],"text":"Element"},{"dep":["nk"],"text":"PSE"}]	0	0	0	DE	\N	f	\N	\N	\N	0
f626cf6a-2d61-11eb-8525-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	276	t	["u ist die atomare masseneinheit die relative atommasse ist Ar (das r ist tiefer gestellt)\\n"]	0	f	f	\N	2022-10-24 14:06:08.640691	f	[]	[{"dep":["pd"],"text":"masseneinheit"},{"dep":["pd"],"text":"atommasse"}]	2	2	0	DE	\N	f	\N	\N	\N	0
24af2a34-2d5e-11eb-95ba-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	277	t	["Wie kann man so toll sein 😂\\n"]	0	f	f	\N	2022-10-24 14:06:08.648489	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
a50bb794-6074-11eb-848c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	278	t	["Was bedeutet denn, dass CL angeregt ist und wodurch kommt es dazu\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.655253	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
c44c8b68-6071-11eb-960c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	279	t	["Wissen sie schon wann wir uns für das Praktikum eintragen können Bzw wann es stattfinden wird ?\\n"]	0	f	f	\N	2022-10-24 14:06:08.662041	f	[]	[{"dep":["nk"],"text":"Praktikum"},{"dep":["oa"],"text":"Bzw"}]	0	0	0	DE	\N	f	\N	\N	\N	0
0bb81fd4-6073-11eb-a2a1-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	280	t	["Aber ich dachte NH3 ist nicht schädlich für die Pflanzen, weil sie den selbst aufnehmen \\n"]	0	f	f	\N	2022-10-24 14:06:08.669572	f	[]	[{"dep":["sb"],"text":"NH3"},{"dep":["nk"],"text":"Pflanze"}]	0	0	0	DE	\N	f	\N	\N	\N	0
67e01064-6073-11eb-a259-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	281	t	["Was meinen Sie mit 'Derivat der' ?\\n"]	0	f	f	\N	2022-10-24 14:06:08.676169	f	[]	[{"dep":["nk"],"text":"Derivat"}]	0	0	0	DE	\N	f	\N	\N	\N	0
cf287928-6073-11eb-bf90-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	282	t	["Ist das der selbe Ostwald, wie beim Ostwald'schen Verdünnungsgesetz?\\n"]	0	f	f	\N	2022-10-24 14:06:08.682509	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
11b21dce-6071-11eb-848c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	283	t	["Heute aber ohne H-wort \\n"]	0	f	f	\N	2022-10-24 14:06:08.688651	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
18033804-6074-11eb-8009-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	284	t	["Was genau davon ist das Königswasser ?\\n"]	0	f	f	\N	2022-10-24 14:06:08.698355	f	[]	[{"dep":["sb"],"text":"Königswasser"}]	0	0	0	DE	\N	f	\N	\N	\N	0
8cc8e3d0-607b-11eb-848c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	285	t	["War es nicht Liebig, der den (flüssigen) Dünger \\"erfunden\\" hat?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.705538	f	[]	[{"dep":["oa"],"text":"Dünger"}]	0	0	0	DE	\N	f	\N	\N	\N	0
d66d83ce-6463-11eb-b113-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	286	t	["Was ist denn eine Pechblende? Wurde unter dem Punkt Polonium erwähnt....\\n"]	0	f	f	\N	2022-10-24 14:06:08.713106	f	[]	[{"dep":["pd"],"text":"Pechblende"},{"dep":["nk"],"text":"Punkt"},{"dep":["nk"],"text":"Polonium"}]	0	0	0	DE	\N	f	\N	\N	\N	0
dbf3411c-2eee-11eb-a952-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	287	t	["Geht es schon los? bei mir steht es wir noch auf den Start gewartet \\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.72883	f	[]	[{"dep":["nk"],"text":"mir"},{"dep":["nk"],"text":"Start"}]	0	0	0	DE	\N	f	\N	\N	\N	0
399f64c8-2ef2-11eb-a952-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	288	t	["4\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.738758	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
97b9d246-6075-11eb-a259-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	289	t	["Der Unterschied zwischen einem normalen CL radikal  und einem angeregten CL ist, dass letzteres das velnzelektron auf einer höheren Ebene hat\\n"]	0	f	f	\N	2022-10-24 14:06:08.747019	f	[]	[{"dep":["sb"],"text":"Unterschied"},{"dep":["nk"],"text":"Ebene"}]	0	0	0	DE	\N	f	\N	\N	\N	0
69b3df96-2ef1-11eb-a952-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	290	t	["2\\n"]	0	f	f	\N	2022-10-24 14:06:08.753063	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
5f963806-2ef1-11eb-87ab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	291	t	["@8417 su kannst such beim Codenamen AAC NaWi 2020 schreiben\\n"]	0	f	f	\N	2022-10-24 14:06:08.759729	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
623d2bc8-2ef1-11eb-87ab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	292	t	["2\\n"]	0	f	f	\N	2022-10-24 14:06:08.76578	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
c4670386-607a-11eb-a2a1-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	293	t	["Warum ist die Neigung zur Bildung von Doppelbindungen bei der Phosphorsäure niedriger?\\n"]	0	f	f	\N	2022-10-24 14:06:08.773832	f	[]	[{"dep":["mo"],"text":"Warum"},{"dep":["sb"],"text":"Neigung"},{"dep":["nk"],"text":"Bildung"},{"dep":["nk"],"text":"Doppelbindungen"},{"dep":["nk"],"text":"Phosphorsäure"}]	0	0	0	DE	\N	f	\N	\N	\N	0
347204c4-2ef2-11eb-b394-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	294	t	["Der Kameramann zeigt Sie...\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.780298	f	[]	[{"dep":["sb"],"text":"Kameramann"}]	0	0	0	DE	\N	f	\N	\N	\N	0
674c7aa0-2ef2-11eb-b394-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	295	t	["4\\n"]	0	f	f	\N	2022-10-24 14:06:08.789338	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
341943c4-2ef3-11eb-b394-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	296	t	["5 1/2\\n"]	0	f	f	\N	2022-10-24 14:06:08.795252	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
c3743dc6-607b-11eb-bf90-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	297	t	["Ich dachte eine Doppelbindung sei wegen einer sterischen Hinderung nicht möglich\\n"]	0	f	f	\N	2022-10-24 14:06:08.802038	f	[]	[{"dep":["sb"],"text":"Doppelbindung"},{"dep":["nk"],"text":"Hinderung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
b1e02ff2-607b-11eb-8009-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	298	t	["Warum liegt bei der Phosphorsäure jetzt eine Doppelbindung vor?\\n"]	0	f	f	\N	2022-10-24 14:06:08.809272	f	[]	[{"dep":["mo"],"text":"Warum"},{"dep":["nk"],"text":"Phosphorsäure"},{"dep":["sb"],"text":"Doppelbindung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
896432dc-32de-11eb-a122-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	299	t	["Um beim Gleichggewicht die Produktseite zu begünstigen, kann man mehr Edukte hinzugeben und Produkte abführen. \\n"]	0	f	f	\N	2022-10-24 14:06:08.816174	f	[]	[{"dep":["oa"],"text":"Produkt"}]	0	0	0	DE	\N	f	\N	\N	\N	0
d40c3ff2-32e1-11eb-a122-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	300	t	["Warum steht H Ende nicht rechts vom Pfeil?\\n"]	0	f	f	\N	2022-10-24 14:06:08.822431	f	[]	[{"dep":["nk"],"text":"Pfeil"}]	0	0	0	DE	\N	f	\N	\N	\N	0
8bfc7202-32de-11eb-856c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	301	t	["Die 0.1 mol, die dann vom H20 w\\"wegfallen\\" kommen doch einfach dadurch, dass 0.1mol reagiert haben. Dann schaut man wieviele Teilchen das von der insg zeilchenkonz wegnehmen würde oder? \\n"]	0	f	f	\N	2022-10-24 14:06:08.830228	f	[]	[{"dep":["ROOT"],"text":"mol"},{"dep":["pnc"],"text":"H20"},{"dep":["oa"],"text":"Teilchen"}]	0	0	0	DE	\N	f	\N	\N	\N	0
ea1bb5fa-32de-11eb-856c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	302	t	["Veränderung des Drucks\\n"]	0	f	f	\N	2022-10-24 14:06:08.836785	f	[]	[{"dep":["ROOT"],"text":"Veränderung"},{"dep":["ag"],"text":"Drucks"}]	0	0	0	DE	\N	f	\N	\N	\N	0
79532438-32df-11eb-856c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	303	t	["Katalysator um die aktivierungsenergie zu senken\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.844036	f	[]	[{"dep":["ROOT"],"text":"Katalysator"},{"dep":["nk"],"text":"aktivierungsenergie"}]	0	0	0	DE	\N	f	\N	\N	\N	0
a80e7cba-32e8-11eb-9d68-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	304	t	["Dankeeee Sie auch !!\\n"]	0	f	f	\N	2022-10-24 14:06:08.850137	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
ab5629b8-32de-11eb-970c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	305	t	["Dann werden mehr H+ Ionen gebildet \\n"]	0	f	f	\N	2022-10-24 14:06:08.856011	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
8f69cf36-32e1-11eb-856c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	306	t	["können Sie das mit der P Gleichung nochmal erklären\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.862361	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
62314308-32de-11eb-803e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	307	t	["Eins reicht \\n"]	0	f	f	\N	2022-10-24 14:06:08.868486	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
a886d524-32df-11eb-803e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	308	t	["senkt aber die Temperatur die benötigt wird\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.875216	f	[]	[{"dep":["sb"],"text":"Temperatur"}]	0	0	0	DE	\N	f	\N	\N	\N	0
e1cec406-32dd-11eb-958e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	309	t	["Aber widerspricht das nicht eigentlich dem MWG ?\\n"]	0	f	f	\N	2022-10-24 14:06:08.88205	f	[]	[{"dep":["da"],"text":"MWG"}]	0	0	0	DE	\N	f	\N	\N	\N	0
55c5e2cc-32de-11eb-803e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	310	t	["Man kann doch auch Cl2 einfach wegnehmen und dann wird es ebenfalls mehr produziert oder?\\n"]	0	f	f	\N	2022-10-24 14:06:08.888847	f	[]	[{"dep":["sb"],"text":"Cl2"}]	0	0	0	DE	\N	f	\N	\N	\N	0
dc73e00e-32e2-11eb-803e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	311	t	["Wo findet man die Energien ?\\n"]	0	f	f	\N	2022-10-24 14:06:08.895132	f	[]	[{"dep":["oa"],"text":"Energie"}]	0	0	0	DE	\N	f	\N	\N	\N	0
a28ebba4-32e5-11eb-803e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	312	t	["Ist dann die Reaktion von ganz am Anfang mit Stickstoff, auch im chemischen Gleichgewicht, weil beide Reaktionen in beide Richtungen parallel stattfinden ?\\n"]	0	f	f	\N	2022-10-24 14:06:08.902781	f	[]	[{"dep":["sb"],"text":"Reaktion"},{"dep":["nk"],"text":"Anfang"},{"dep":["nk"],"text":"Stickstoff"},{"dep":["nk"],"text":"Gleichgewicht"},{"dep":["nk"],"text":"Richtung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
cd067cc2-32e6-11eb-a122-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	313	t	["könne sie die reaktion bitte nochmal zeigen\\n"]	0	f	f	\N	2022-10-24 14:06:08.909258	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
3c873c3e-3478-11eb-a957-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	314	t	["Was steht bei der dritten R Gleichung \\n"]	0	f	f	\N	2022-10-24 14:06:08.915892	f	[]	[{"dep":["nk"],"text":"dritten R Gleichung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
a2cece68-39f9-11eb-b6ec-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	315	t	["Man verwendet Neon bei der Elektronenkonfiguration als Abkürzung, da dafür immer das Edelgas aus der vorigen Periode verwendet wird. (Falls sich die Frage eben auf das Element in den eckigen Klammern bei der Elektronenkonfiguration bezieht)\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.925222	f	[]	[{"dep":["oa"],"text":"Neon"},{"dep":["nk"],"text":"Elektronenkonfiguration"},{"dep":["nk"],"text":"Abkürzung"},{"dep":["sb"],"text":"Edelgas"},{"dep":["nk"],"text":"Periode"},{"dep":["sb"],"text":"Frage"},{"dep":["nk"],"text":"Element"},{"dep":["nk"],"text":"Klammer"}]	0	0	0	DE	\N	f	\N	\N	\N	0
3325119c-39fb-11eb-bb0b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	316	t	["Du kannst dir die Vorlesungs-Mitschrift im Nachhinein nochmal angucken.\\n"]	0	f	f	\N	2022-10-24 14:06:08.932304	f	[]	[{"dep":["oa"],"text":"Vorlesungs-Mitschrift"}]	0	0	0	DE	\N	f	\N	\N	\N	0
253a9da8-39f2-11eb-bb0b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	317	t	["Auch wenn sie nicht reden \\n"]	0	f	f	\N	2022-10-24 14:06:08.938645	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
49241ae0-39f8-11eb-8bb0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	318	t	["man kann den kgV nutzen\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.944753	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
1aa928a6-39fb-11eb-8bb0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	319	t	["Können Sie bitte nächstes mal etwas langsamer machen? Heute waren Sie  sehr schnell. Bin kaum hinterher gekommen mit dem Schreiben... :(\\n"]	0	f	f	\N	2022-10-24 14:06:08.951646	f	[]	[{"dep":["mo"],"text":"mal"},{"dep":["nk"],"text":"Schreiben"}]	0	0	0	DE	\N	f	\N	\N	\N	0
9f9e0028-3865-11eb-9fde-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	320	t	["Herr buchsbaum ist wieder lustig unterwegs \\n"]	0	f	f	\N	2022-10-24 14:06:08.958554	f	[]	[{"dep":["sb"],"text":"Herr"}]	0	0	0	DE	\N	f	\N	\N	\N	0
8a586f4e-3862-11eb-9fde-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	321	t	["Schmelzpunkt\\n"]	0	f	f	\N	2022-10-24 14:06:08.964915	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
861d1614-3862-11eb-9fde-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	322	t	["Schmelzpunkt \\n"]	0	f	f	\N	2022-10-24 14:06:08.97108	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
30d99dfc-3868-11eb-b27c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	323	t	["Bilden sich so auch hydrathüllen? In der Biologie ist das ja sehr wichtig \\n"]	0	f	f	\N	2022-10-24 14:06:08.978297	f	[]	[{"dep":["nk"],"text":"Biologie"}]	0	0	0	DE	\N	f	\N	\N	\N	0
75154fc8-5ee4-11eb-b352-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	324	t	["Können Konzentrationen von Gasen wie H2 auch im MWG als konstat angesehen werden?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.986318	f	[]	[{"dep":["sb"],"text":"Konzentration"},{"dep":["nk"],"text":"Gas"},{"dep":["nk"],"text":"MWG"}]	0	0	0	DE	\N	f	\N	\N	\N	0
d094de0c-39f7-11eb-addd-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	325	t	["geht Edelgaskonfiguration?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:08.992501	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
c0def0f8-3f77-11eb-a2fc-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	326	t	["thx\\n"]	0	f	f	\N	2022-10-24 14:06:08.99862	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
c1268b7c-3f7a-11eb-9f30-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	327	t	["Geil!!\\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.004752	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
c4068a2e-3f78-11eb-ba23-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	328	t	["Das ist die Seite 98\\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.010908	f	[]	[{"dep":["pd"],"text":"Seite"}]	0	0	0	DE	\N	f	\N	\N	\N	0
7fbfc666-2efa-11eb-ac45-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	494	t	["Nochmal glrichgewuxhtslage und Beeinflussung Ziegen \\n"]	0	f	f	\N	2022-10-24 14:06:10.237473	f	[]	[]	2	2	0	DE	\N	f	\N	\N	\N	0
23e1979c-3f72-11eb-ba23-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	329	t	["aber wenn der pH 1 ist muss dann OH nicht 13 sein, Bzw c nicht 10^-13\\n"]	0	f	f	\N	2022-10-24 14:06:09.017815	f	[]	[{"dep":["sb"],"text":"pH 1"},{"dep":["sb"],"text":"muss"}]	0	0	0	DE	\N	f	\N	\N	\N	0
0cb10116-2ef1-11eb-ac45-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	330	t	["9\\n"]	0	f	f	\N	2022-10-24 14:06:09.023917	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
552883a0-2ef2-11eb-bb4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	331	t	["5\\n"]	0	f	f	\N	2022-10-24 14:06:09.030121	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
efd81048-2ef0-11eb-ac45-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	332	t	["24\\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.036284	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
df06a3ec-2d65-11eb-a175-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	333	t	["Mam braucht zusätzliche Infos zu wissen welcher Pfeil größer wird (wie Energie oder so) für Gleichgewicht?\\n"]	0	f	f	\N	2022-10-24 14:06:09.044206	f	[]	[{"dep":["sb"],"text":"Mam"},{"dep":["oa"],"text":"Info"},{"dep":["sb"],"text":"Pfeil"},{"dep":["cc"],"text":"Energie"},{"dep":["nk"],"text":"Gleichgewicht"}]	0	0	0	DE	\N	f	\N	\N	\N	0
db9deff6-2d5d-11eb-9e65-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	334	t	["Ja, das funktioniert mit der Suche, danke!\\n"]	0	f	f	\N	2022-10-24 14:06:09.050642	f	[]	[{"dep":["nk"],"text":"Suche"}]	0	0	0	DE	\N	f	\N	\N	\N	0
807eb678-3474-11eb-aab8-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	335	t	["Zum Gleichgewicht hatte ich eine Frage im Forum gestellt, mit Grafiken die das vllt etwas verdeutlichen, denke ich mal. \\n"]	0	f	f	\N	2022-10-24 14:06:09.058566	f	[]	[{"dep":["nk"],"text":"Gleichgewicht"},{"dep":["oa"],"text":"Frage"},{"dep":["nk"],"text":"Forum"},{"dep":["nk"],"text":"Grafik"}]	0	0	0	DE	\N	f	\N	\N	\N	0
3345935e-2d67-11eb-a175-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	336	t	["Sie müssen wieder auf den Beamer umschalten.\\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.065441	f	[]	[{"dep":["nk"],"text":"Beamer"}]	0	0	0	DE	\N	f	\N	\N	\N	0
8a8eeeba-2d64-11eb-9e65-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	337	t	["@2624 hängt dann wahrscheinlich von Mediasite ab, überlastet wahrscheinlivh :/\\n"]	0	f	f	\N	2022-10-24 14:06:09.072409	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
795837c2-5af1-11eb-9300-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	338	t	["Warum steht unten in dem Diagramm nochmal sp2 und nicht sp3? Ich dachte sp2 wäre nur bei Doppelbindungen\\n"]	0	f	f	\N	2022-10-24 14:06:09.079819	f	[]	[{"dep":["mo"],"text":"Warum"},{"dep":["nk"],"text":"Diagramm"},{"dep":["sb"],"text":"sp2"},{"dep":["cj"],"text":"sp3"},{"dep":["nk"],"text":"Doppelbindungen"}]	0	0	0	DE	\N	f	\N	\N	\N	0
18349f62-5961-11eb-aa3a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	339	t	["Kann Aktivkohle teerartig werden ? \\n"]	0	f	f	\N	2022-10-24 14:06:09.086847	f	[]	[{"dep":["sb"],"text":"Aktivkohle"}]	0	0	0	DE	\N	f	\N	\N	\N	0
9906df32-5967-11eb-830d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	340	t	["Warum hat reines Si keinen Si-Gehalt von 100%?\\n"]	0	f	f	\N	2022-10-24 14:06:09.093466	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
e98a307c-5966-11eb-830d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	341	t	["Ja das Internet hing mal wieder. Jetzt läuft es wieder.\\n"]	0	f	f	\N	2022-10-24 14:06:09.100317	f	[]	[{"dep":["sb"],"text":"Internet"}]	0	0	0	DE	\N	f	\N	\N	\N	0
34f1a27e-5968-11eb-a1c0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	342	t	["Was kann man n denn nach der Destillation alles abziehenweil der Siedepunkt niedriger als von Si ist \\n"]	0	f	f	\N	2022-10-24 14:06:09.107226	f	[]	[{"dep":["nk"],"text":"Destillation"},{"dep":["ag"],"text":"Siedepunkt"}]	0	0	0	DE	\N	f	\N	\N	\N	0
f838c1d4-32e6-11eb-85f9-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	343	t	["die mit dc ..\\n"]	0	f	f	\N	2022-10-24 14:06:09.113947	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
db1510f6-5967-11eb-a037-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	344	t	["Welchen Aggregatzustand hat das HSiCl3 bei der Reaktion mit H2?\\n"]	0	f	f	\N	2022-10-24 14:06:09.120909	f	[]	[{"dep":["sb"],"text":"HSiCl3"},{"dep":["nk"],"text":"Reaktion"}]	0	0	0	DE	\N	f	\N	\N	\N	0
138adab8-3471-11eb-aab8-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	345	t	["Könnten Sie bitte Blatt 2 nochmal zeigen?\\n"]	0	f	f	\N	2022-10-24 14:06:09.127503	f	[]	[{"dep":["sb"],"text":"Blatt"}]	0	0	0	DE	\N	f	\N	\N	\N	0
e3d9562c-32e1-11eb-a122-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	346	t	["Was ist bei einer endothermen Reaktion \\n"]	0	f	f	\N	2022-10-24 14:06:09.134299	f	[]	[{"dep":["nk"],"text":"Reaktion"}]	0	0	0	DE	\N	f	\N	\N	\N	0
64a7b52a-32e0-11eb-85f9-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	347	t	["Wärme ist eine transportform von Energie \\n"]	0	f	f	\N	2022-10-24 14:06:09.141217	f	[]	[{"dep":["sb"],"text":"Wärme"},{"dep":["nk"],"text":"Energie"}]	0	0	0	DE	\N	f	\N	\N	\N	0
00a0e130-32e2-11eb-85f9-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	348	t	["Er meint das größer Zeichen \\n"]	0	f	f	\N	2022-10-24 14:06:09.147928	f	[]	[{"dep":["oa"],"text":"Zeichen"}]	0	0	0	DE	\N	f	\N	\N	\N	0
dda23e70-32e3-11eb-85f9-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	349	t	["Woher wissen wir die enthalpien von (2) und (3) sind die dann schon gegeben oder woher wissen wir die? \\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.154696	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
09b59b02-5962-11eb-80ec-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	350	t	["Sie haben bei 4.1.3 aufgeschrieben, dass Aktivkohle eine niedrige, statt große Oberfläche hat\\n"]	0	f	f	\N	2022-10-24 14:06:09.161824	f	[]	[{"dep":["sb"],"text":"Aktivkohle"},{"dep":["oa"],"text":"Oberfläche"}]	0	0	0	DE	\N	f	\N	\N	\N	0
c36575ae-5962-11eb-80ec-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	351	t	["Können sie das Blatt davor nochmal zeigen?\\n"]	0	f	f	\N	2022-10-24 14:06:09.16833	f	[]	[{"dep":["sb"],"text":"Blatt"}]	0	0	0	DE	\N	f	\N	\N	\N	0
3af1ba2a-32e8-11eb-803e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	352	t	["können sie die abbilding des komplexes nochmal zeigen\\n"]	0	f	f	\N	2022-10-24 14:06:09.174889	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
96d6cacc-3ddf-11eb-ab04-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	353	t	["Kann ich nicht die Rückreaktion direkt bilden und dann das Vorzeichen der Enthalpie ändern ?\\n"]	0	f	f	\N	2022-10-24 14:06:09.182129	f	[]	[{"dep":["oa"],"text":"Rückreaktion"},{"dep":["oa"],"text":"Vorzeichen"},{"dep":["ag"],"text":"Enthalpie"}]	0	0	0	DE	\N	f	\N	\N	\N	0
94b42614-5af3-11eb-830d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	354	t	["Sollen wir in der Klausur auch die Keil-Strich-Schreibweise verwenden ?\\n"]	0	f	f	\N	2022-10-24 14:06:09.189322	f	[]	[{"dep":["nk"],"text":"Klausur"},{"dep":["oa"],"text":"Keil-Strich-Schreibweise"}]	0	0	0	DE	\N	f	\N	\N	\N	0
3e2db57c-5af7-11eb-aa3a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	355	t	["ja\\n"]	0	f	f	\N	2022-10-24 14:06:09.195861	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
df9181ec-5af1-11eb-a058-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	356	t	["Wann genau kommt es zu einer hybridieseirung?\\n"]	0	f	f	\N	2022-10-24 14:06:09.202356	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
5c643fc2-5af5-11eb-8593-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	357	t	["geht gar kein video mehr bei mir \\n"]	0	f	f	\N	2022-10-24 14:06:09.209103	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
824bb062-5afa-11eb-8593-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	358	t	["Was ist denn das gegenion🤔\\n"]	0	f	f	\N	2022-10-24 14:06:09.215664	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
a56a82d6-5ee1-11eb-ba5a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	359	t	["Was steht hinter technisches Silikon? Trennung in der Kettenlänge?\\n"]	0	f	f	\N	2022-10-24 14:06:09.222855	f	[]	[{"dep":["nk"],"text":"Silikon"},{"dep":["ROOT"],"text":"Trennung"},{"dep":["nk"],"text":"Kettenlänge"}]	0	0	0	DE	\N	f	\N	\N	\N	0
4493449a-5af7-11eb-830d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	360	t	["Ich verstehe Sie gut \\n"]	0	f	f	\N	2022-10-24 14:06:09.229282	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
b96cc894-5af3-11eb-830d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	361	t	["Wie erkennen wir ob es eine räumliche Anordnung gibt ? Also wann wir sie auch so zeichnen müssen \\n"]	0	f	f	\N	2022-10-24 14:06:09.236584	f	[]	[{"dep":["oa"],"text":"Anordnung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
7d5849a4-5ee1-11eb-9567-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	362	t	["Wie stellt man denn eine genaue Kettenlänge sicher ?\\n"]	0	f	f	\N	2022-10-24 14:06:09.243425	f	[]	[{"dep":["oa"],"text":"Kettenlänge"}]	0	0	0	DE	\N	f	\N	\N	\N	0
53acd954-3866-11eb-9cd2-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	363	t	["Können sie das Diagramm nochmal zeigen bitte?\\n"]	0	f	f	\N	2022-10-24 14:06:09.250463	f	[]	[{"dep":["oa"],"text":"Diagramm"}]	0	0	0	DE	\N	f	\N	\N	\N	0
47b38a6c-3de3-11eb-b142-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	364	t	["Duktilität oder Duktivität?\\n"]	0	f	f	\N	2022-10-24 14:06:09.256799	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
387645b8-39fa-11eb-af88-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	365	t	["weil [Ne] günstiger ist!!!!!!!\\n"]	0	f	f	\N	2022-10-24 14:06:09.26347	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
dba30f64-39fa-11eb-af88-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	366	t	["Vielen lieben Dank für ihre Geduld und auch ein liebes Dankeschön an Frank Schaun für die funktionierende Übertragung!!\\n"]	0	f	f	\N	2022-10-24 14:06:09.271279	f	[]	[{"dep":["oa"],"text":"Dank"},{"dep":["nk"],"text":"Geduld"},{"dep":["cj"],"text":"Dankeschön"},{"dep":["nk"],"text":"Übertragung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
dd48ee8e-2ef0-11eb-b394-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	367	t	["0\\n"]	0	f	f	\N	2022-10-24 14:06:09.277576	f	[]	[]	2	2	0	AUTO	\N	f	\N	\N	\N	0
810fdc90-0f09-11ed-a6b1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	11	t	["9/143 + 105/143\\n"]	0	f	f	\N	2022-10-24 14:06:09.283688	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
c79da046-2d62-11eb-95ba-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	368	t	["https://www.tu-chemnitz.de/physik/PGP/files/Allgemeines/Rundungsregeln.pdf\\n\\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.289162	f	[]	[]	2	2	0	AUTO	\N	f	\N	\N	\N	0
9ee08568-5af5-11eb-90b8-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	369	t	["Einfach nochmal neu laden dann geht es wieder\\n"]	0	f	f	\N	2022-10-24 14:06:09.29619	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
5187e444-5af7-11eb-90b8-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	370	t	["Ich verstehe sie gut\\n\\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.30265	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
4a3447aa-5af7-11eb-a058-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	371	t	["Ich wiederhole, drück doch einfach F5\\n\\nIch höre alles gut btw\\n"]	0	f	f	\N	2022-10-24 14:06:09.309416	f	[]	[]	0	0	0	DE	\N	f	\N	\N	\N	0
56bcd526-5577-11eb-afe1-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	372	t	["Heller als vieler Leute Zukunft \\n"]	0	f	f	\N	2022-10-24 14:06:09.316272	f	[]	[{"dep":["ROOT"],"text":"Heller"}]	2	4	2	DE	\N	f	\N	\N	\N	0
41d002de-6074-11eb-bf90-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	373	t	["würden die beiden Chlor-Radikale nicht direkt miteinander zu molekularem Chlor reagieren? Die Radikale haben ja ein starkes Bestreben Elektronen zu bekommen und dann könnten sie sich ja zusammen tun...\\n"]	0	f	f	\N	2022-10-24 14:06:09.324164	f	[]	[{"dep":["sb"],"text":"Chlor-Radikale"},{"dep":["nk"],"text":"Chlor"},{"dep":["sb"],"text":"Radikaler"}]	0	0	0	DE	\N	f	\N	\N	\N	0
91454eda-347a-11eb-a2c7-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	374	t	["Wofür stehen q und r nochmal beim Dipolmoment? \\n"]	0	f	f	\N	2022-10-24 14:06:09.33097	f	[]	[{"dep":["nk"],"text":"Dipolmoment"}]	1	1	0	DE	\N	f	\N	\N	\N	0
df6f2a68-347a-11eb-b7c6-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	375	t	["Wäre Wasser ein permanenter Dipol? \\n"]	0	f	f	\N	2022-10-24 14:06:09.337692	f	[]	[{"dep":["sb"],"text":"Wasser"},{"dep":["pd"],"text":"Dipol"}]	1	1	0	DE	\N	f	\N	\N	\N	0
05c901a8-32e0-11eb-a122-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	376	t	["wartet doch einfach mal\\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.343997	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
3affdfb0-5af7-11eb-80ec-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	377	t	["Man versteht sie sehr gut\\n"]	0	f	f	\N	2022-10-24 14:06:09.350242	f	[]	[]	3	3	0	AUTO	\N	f	\N	\N	\N	0
1feb0982-39f7-11eb-a94d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	378	t	["Wir haben keine Frage gesehen und als Sie diese eben gesagt haben, war die Zeit schon rum\\n"]	0	f	f	\N	2022-10-24 14:06:09.35714	f	[]	[{"dep":["oa"],"text":"Frage"},{"dep":["sb"],"text":"Zeit"}]	1	1	0	DE	\N	f	\N	\N	\N	0
1fa48e36-2d67-11eb-8525-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	379	t	["müssen noch umschalten\\n\\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.363758	f	[]	[]	-2	0	2	AUTO	\N	f	\N	\N	\N	0
c0baf71a-2ef2-11eb-9ede-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	380	t	["4 = 3 mal 1/2= 3 und 8 mal 1/8=1\\n"]	0	f	f	\N	2022-10-24 14:06:09.37018	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
ffd05626-2d61-11eb-b524-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	381	t	["ist es wichtig aufwelche nachkommer stelle man die molare masse angibt?\\n"]	0	f	f	\N	2022-10-24 14:06:09.37743	f	[]	[]	-1	0	1	AUTO	\N	f	\N	\N	\N	0
e6180980-2d5d-11eb-95ba-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	382	t	["Funktioniert \\n"]	0	f	f	\N	2022-10-24 14:06:09.383579	f	[]	[]	-1	0	1	DE	\N	f	\N	\N	\N	0
24832606-2d67-11eb-9e65-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	383	t	["Sie müssen umschalten \\n"]	0	f	f	\N	2022-10-24 14:06:09.389805	f	[]	[]	-1	0	1	AUTO	\N	f	\N	\N	\N	0
7d19681c-2d61-11eb-9e65-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	384	t	["bitte nochmal Blatt 5 zeigen \\n"]	0	f	f	\N	2022-10-24 14:06:09.396809	f	[]	[{"dep":["sb"],"text":"Blatt"}]	0	1	1	DE	\N	f	\N	\N	\N	0
f758a32e-2d5f-11eb-9ff0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	385	t	["Bitte zurück aufs Blatt schalten \\n"]	0	f	f	\N	2022-10-24 14:06:09.403438	f	[]	[{"dep":["nk"],"text":"Blatt"}]	-4	2	6	DE	\N	f	\N	\N	\N	0
edd92158-2d63-11eb-b9da-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	386	t	["Also können wir in der Klausur auch mit Brüchen in der Reaktionsgleichung arbeiten?\\n"]	0	f	f	\N	2022-10-24 14:06:09.41084	f	[]	[{"dep":["nk"],"text":"Klausur"},{"dep":["nk"],"text":"Reaktionsgleichung"}]	0	1	1	DE	\N	f	\N	\N	\N	0
01f1c28c-53e1-11eb-b8bb-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	387	t	["Lassen Sie soch ihren Kameramann versuchdn, die katusche zu wechseln \\n"]	0	f	f	\N	2022-10-24 14:06:09.417836	f	[]	[]	7	8	1	AUTO	\N	f	\N	\N	\N	0
9ae69a02-53dd-11eb-b8bb-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	388	t	["moin moin. ton ist irgendwie nicht synchron\\n"]	0	f	f	\N	2022-10-24 14:06:09.424089	f	[]	[]	-3	0	3	AUTO	\N	f	\N	\N	\N	0
803240d8-3ddc-11eb-bae4-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	389	t	["Kürzeste Vorlesung aller Zeiten :D\\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.430943	f	[]	[{"dep":["ROOT"],"text":"Vorlesung"},{"dep":["ag"],"text":"Zeit"}]	6	7	1	DE	\N	f	\N	\N	\N	0
14b90800-32e2-11eb-9d68-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	390	t	["gehört H ende nicht auf die seite der Produkte?\\n"]	0	f	f	\N	2022-10-24 14:06:09.437803	f	[]	[]	-3	1	4	AUTO	\N	f	\N	\N	\N	0
09fb1f9e-3de5-11eb-b142-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	391	t	["wieso werde ich die ganze zeit vom stream rausgeworfen?\\n"]	0	f	f	\N	2022-10-24 14:06:09.444834	f	[]	[]	-4	0	4	AUTO	\N	f	\N	\N	\N	0
df536b9a-39f5-11eb-addd-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	392	t	["Also bei Präsenz würde es grad sehr witzig werden XD\\n"]	0	f	f	\N	2022-10-24 14:06:09.451793	f	[]	[{"dep":["nk"],"text":"Präsenz"},{"dep":["sb"],"text":"grad"}]	0	1	1	DE	\N	f	\N	\N	\N	0
1125b928-3de0-11eb-8f4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	393	t	["Frage expliziter gestellt: kommt die stoffchemie noch in der ersten Klausur vor?\\n"]	0	f	f	\N	2022-10-24 14:06:09.458728	f	[]	[{"dep":["mo"],"text":"Frage"},{"dep":["nk"],"text":"Klausur"}]	-5	0	5	DE	\N	f	\N	\N	\N	0
4f08334c-39f3-11eb-8b4a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	394	t	["bitte blatt 5 nochmal \\n"]	0	f	f	\N	2022-10-24 14:06:09.465158	f	[]	[]	-1	0	1	AUTO	\N	f	\N	\N	\N	0
90d0c5c8-3de0-11eb-871d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	395	t	["Haben Sie Empfehlungen für Übungsbücher ?\\n"]	0	f	f	\N	2022-10-24 14:06:09.471969	f	[]	[{"dep":["oa"],"text":"Empfehlung"}]	-4	0	4	DE	\N	f	\N	\N	\N	0
8a83f13c-3de4-11eb-871d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	396	t	["können sie die vorherige folie nochmal zeigen\\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.478613	f	[]	[]	-3	0	3	AUTO	\N	f	\N	\N	\N	0
0c8e7dd8-2ef8-11eb-a952-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	568	t	["fehlen nicht die hochzahlen ?\\n"]	0	f	f	\N	2022-10-24 14:06:10.774525	f	[]	[]	-3	0	3	AUTO	\N	f	\N	\N	\N	0
3073e6d0-3f73-11eb-8736-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	397	t	["Bei einer vollständigen Reaktion der Säure: Muss d der pH-Wert immer null sein? Alle anderen pH werte charakterisieren die Säure als schwach (Schwächer)?\\n"]	0	f	f	\N	2022-10-24 14:06:09.486496	f	[]	[{"dep":["nk"],"text":"Reaktion"},{"dep":["oa","ag"],"text":"Säure"},{"dep":["pnc"],"text":"Muss"},{"dep":["ROOT"],"text":"pH-Wert"}]	-1	1	2	DE	\N	f	\N	\N	\N	0
2f8ca934-3f71-11eb-bbdb-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	398	t	["Vllt liegt es am fehlenden Kaffe, aber hätte man nicht einfach sagen können, wenn die Summe 10^-14 ist und man weiß man hat einen pH von 10 kann man die ^-10 nicht einfach abziehen ? Ohne die rechengleichung\\n"]	0	f	f	\N	2022-10-24 14:06:09.495168	f	[]	[{"dep":["mo"],"text":"Vllt"},{"dep":["nk"],"text":"Kaffe"},{"dep":["sb"],"text":"Summe"}]	0	1	1	DE	\N	f	\N	\N	\N	0
a8ff048a-3f74-11eb-b334-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	399	t	["Würde sich der pH-Wert auch dann ändern, wenn man einfach Salze, wie z. B. NaCl in Wasser löst, weil das Anion doch H+ durch seine freien Elektronenpaare theoretisch aufnehmen könnte\\n"]	0	f	f	\N	2022-10-24 14:06:09.503509	f	[]	[{"dep":["sb"],"text":"pH-Wert"},{"dep":["oa"],"text":"Salz"},{"dep":["nk"],"text":"Wasser"},{"dep":["sb"],"text":"Anion"}]	-1	1	2	DE	\N	f	\N	\N	\N	0
320f5140-3f74-11eb-8736-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	400	t	["Wenn eine Säure aber nicht vollständig dissoziiert, ist das chemische Gleichgewicht trotzdem gegeben, weil Produkte = Edukte sind ?\\n"]	0	f	f	\N	2022-10-24 14:06:09.511431	f	[]	[{"dep":["sb"],"text":"Säure"},{"dep":["sb"],"text":"Gleichgewicht"},{"dep":["mo"],"text":"Produkt"}]	0	1	1	DE	\N	f	\N	\N	\N	0
257e07b4-3f74-11eb-8736-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	401	t	["Gibt Sinn, die EN von F ist am höchsten, die von von I am geringsten. FH ist am schwächsten, da die Protonen (H+) durch die hohe EN stärker angezogen werden, als bei IH, bei welchem die Protonen leichter abgegeben werden.\\n"]	0	f	f	\N	2022-10-24 14:06:09.519453	f	[]	[{"dep":["oa"],"text":"Sinn"},{"dep":["sb"],"text":"Protonen"}]	0	2	2	DE	\N	f	\N	\N	\N	0
f4a44146-2ef0-11eb-87ab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	402	t	["1\\n"]	0	f	f	\N	2022-10-24 14:06:09.525578	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
cfa52248-5af1-11eb-80ec-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	403	t	["die 3 sieht aus wie eine 2 glaub ich \\n"]	0	f	f	\N	2022-10-24 14:06:09.532627	f	[]	[]	-1	0	1	AUTO	\N	f	\N	\N	\N	0
10834314-5afa-11eb-a058-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	404	t	["Schlagzeile: Buchsbaum deckt von der uni vertuschten Mord im uni Keller auf\\n"]	0	f	f	\N	2022-10-24 14:06:09.540523	f	[]	[{"dep":["ROOT"],"text":"Schlagzeile"},{"dep":["sb"],"text":"Buchsbaum"},{"dep":["nk"],"text":"uni"},{"dep":["oa"],"text":"Mord"}]	1	2	1	DE	\N	f	\N	\N	\N	0
4a8eb55a-5af7-11eb-a037-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	405	t	["wir hören sie, verstehen ist was anderes\\n"]	0	f	f	\N	2022-10-24 14:06:09.547336	f	[]	[]	7	8	1	AUTO	\N	f	\N	\N	\N	0
fc4e4dba-5af6-11eb-8593-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	406	t	["Es hängt einfach komplett , ich versteh gar nichts mehr\\n"]	0	f	f	\N	2022-10-24 14:06:09.553898	f	[]	[]	-4	0	4	AUTO	\N	f	\N	\N	\N	0
3d2c52f4-5aee-11eb-90b8-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	407	t	["Bei mir ist der Live stream nur schwarz , hat er schon angefangen ?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.560748	f	[]	[]	-1	1	2	AUTO	\N	f	\N	\N	\N	0
bf1218f6-5af5-11eb-9300-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	408	t	["Wie ist der Dipolmoment definiert und was genau \\"macht\\" dieser?\\n"]	0	f	f	\N	2022-10-24 14:06:09.567823	f	[]	[{"dep":["sb"],"text":"Dipolmoment"}]	0	1	1	DE	\N	f	\N	\N	\N	0
05fec5e0-5af9-11eb-a1c0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	409	t	["Ich versteh nicht woher die -12 bei den Ordnungszahlen kommt dass es am Ende negativ ist \\n"]	0	f	f	\N	2022-10-24 14:06:09.575035	f	[]	[{"dep":["nk"],"text":"Ordnungszahlen"},{"dep":["nk"],"text":"Ende"}]	-5	1	6	DE	\N	f	\N	\N	\N	0
57fef89c-5ee1-11eb-ba5a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	410	t	["Nein, nix Hormone\\n"]	0	f	f	\N	2022-10-24 14:06:09.581632	f	[]	[]	0	1	1	AUTO	\N	f	\N	\N	\N	0
42ecf60a-5af6-11eb-a058-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	411	t	["Wir sind auch Nerds :(\\n"]	0	f	f	\N	2022-10-24 14:06:09.58811	f	[]	[{"dep":["pd"],"text":"Nerds :("}]	0	2	2	DE	\N	f	\N	\N	\N	0
ad91fd72-5af9-11eb-a1c0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	412	t	["Die Lösung ist eine geheime Vorlesung, wo wir uns im Keller der Uni treffen und dort, natürlich Corona-konform, von Dr. Buchsbaum lernen\\n"]	0	f	f	\N	2022-10-24 14:06:09.613689	f	[]	[{"dep":["sb"],"text":"Lösung"},{"dep":["pd"],"text":"Vorlesung"},{"dep":["nk"],"text":"Keller"},{"dep":["ag"],"text":"Uni"},{"dep":["par"],"text":"Corona-konform"}]	1	2	1	DE	\N	f	\N	\N	\N	0
d16878e0-5edf-11eb-9862-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	413	t	["Bezeichnet \\"Müller-Rochow-Synthese\\" nur den Reaktionsschritt, bei der die einzelnen Monomere verknüpft werden zu einem Disiloxan oder Polysiloxan, oder bezeichnet der Name den kompletten Bildungsprozess, anfangend bei der Reaktion von Silicium mit CH3Cl?\\n"]	0	f	f	\N	2022-10-24 14:06:09.632251	f	[]	[{"dep":["sb"],"text":"Müller-Rochow-Synthese"},{"dep":["sb"],"text":"Monomere"},{"dep":["nk"],"text":"Disiloxan"},{"dep":["cj"],"text":"Polysiloxan"},{"dep":["sb"],"text":"Name"},{"dep":["nk"],"text":"Reaktion"},{"dep":["nk"],"text":"Silicium"},{"dep":["nk"],"text":"CH3Cl"}]	0	1	1	DE	\N	f	\N	\N	\N	0
851c104e-5ee1-11eb-ba5a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	414	t	["Sind Silikon-Brotdosen unbedenklich? Also lösen sich keine Teilchen, die man, dann mit dem Brot aufnimmt.\\n"]	0	f	f	\N	2022-10-24 14:06:09.652278	f	[]	[{"dep":["sb"],"text":"Silikon-Brotdosen"},{"dep":["sb"],"text":"Teilchen"},{"dep":["nk"],"text":"Brot"}]	-2	0	2	DE	\N	f	\N	\N	\N	0
b58665f6-5edf-11eb-baab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	415	t	["Könnten sie bitte nochmal Blatt 2 und 3 zeigen?\\n"]	0	f	f	\N	2022-10-24 14:06:09.663046	f	[]	[{"dep":["sb"],"text":"Blatt"}]	-2	0	2	DE	\N	f	\N	\N	\N	0
d46df8dc-32e6-11eb-9d68-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	416	t	["Schneemann vibes lol\\n"]	0	f	f	\N	2022-10-24 14:06:09.670942	f	[]	[]	9	9	0	AUTO	\N	f	\N	\N	\N	0
c29281a4-3ddc-11eb-8c34-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	417	t	["Bildschirm schwarz und ton weg aber trotzdem guten Morgen \\n"]	0	f	f	\N	2022-10-24 14:06:09.678608	f	[]	[{"dep":["cj"],"text":"ton"},{"dep":["ROOT"],"text":"Morgen"}]	10	10	0	DE	\N	f	\N	\N	\N	0
5a18d954-39f7-11eb-addd-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	418	t	["Herr Buchsbaum wie mussten alle raten :)\\n"]	0	f	f	\N	2022-10-24 14:06:09.686106	f	[]	[{"dep":["cc"],"text":"mussten"},{"dep":["nk"],"text":"raten"}]	1	1	0	DE	\N	f	\N	\N	\N	0
85e86120-39ee-11eb-93b0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	419	t	["Moin Meister\\n"]	0	f	f	\N	2022-10-24 14:06:09.692672	f	[]	[]	2	2	0	DE	\N	f	\N	\N	\N	0
e6467b08-2d5d-11eb-bc8d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	420	t	["ja jetzt klappts\\n"]	0	f	f	\N	2022-10-24 14:06:09.699346	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
b30480b0-2d61-11eb-95ba-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	421	t	["Was ist nochmal u ? Relative Atommasse?\\n"]	0	f	f	\N	2022-10-24 14:06:09.70582	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
c16e486e-2d63-11eb-a175-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	422	t	["@3630 H O N Cl Br I F liegen natürluch molekular vor, alle anderen meines wissens nach atomar\\n"]	0	f	f	\N	2022-10-24 14:06:09.712767	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
627eeb74-2d68-11eb-bc8d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	423	t	["tschüss\\n"]	0	f	f	\N	2022-10-24 14:06:09.719487	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
877e2066-2d68-11eb-9ff0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	424	t	["ja unter kursmaterial\\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.726233	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
72d43baa-2d63-11eb-b9da-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	425	t	["wenn ich wie in dem bsp mit 21 mol H + 1mol O rechne muss da ein = hin oder ein reaktionspfeil? ist ja eine Rechnung die eine Reaktion beschreibt \\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.733702	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
c9ae453c-2d64-11eb-8525-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	426	t	["Wieso liegt Natrium jetzt nicht Molekular vor?\\n"]	0	f	f	\N	2022-10-24 14:06:09.74165	f	[]	[{"dep":["sb"],"text":"Natrium"},{"dep":["mo"],"text":"Molekular"}]	1	1	0	DE	\N	f	\N	\N	\N	0
3c34b426-2eee-11eb-a71c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	427	t	["Guten Morgen :-)\\n"]	0	f	f	\N	2022-10-24 14:06:09.748359	f	[]	[{"dep":["ROOT"],"text":"Morgen"}]	0	1	1	DE	\N	f	\N	\N	\N	0
1ba727a6-2d5f-11eb-9ff0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	428	t	["Wie weiß man, in welcher Reihenfolge man die Atome in einer Verbindunh schreiben soll?\\n"]	0	f	f	\N	2022-10-24 14:06:09.755582	f	[]	[{"dep":["nk"],"text":"Reihenfolge"},{"dep":["oa"],"text":"Atome"}]	3	3	0	DE	\N	f	\N	\N	\N	0
a774ebd0-2ef2-11eb-931a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	429	t	["Innenzentriert ist es wenn die Kugel in der Mitte weiß ist \\n"]	0	f	f	\N	2022-10-24 14:06:09.762455	f	[]	[{"dep":["sb"],"text":"Kugel"},{"dep":["nk"],"text":"Mitte"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
142dfd36-2ef1-11eb-87ab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	430	t	["1000\\n"]	0	f	f	\N	2022-10-24 14:06:09.768791	f	[]	[]	0	1	1	AUTO	\N	f	\N	\N	\N	0
ceca691e-2ef0-11eb-ac45-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	431	t	["Keine\\n"]	0	f	f	\N	2022-10-24 14:06:09.775591	f	[]	[]	3	4	1	AUTO	\N	f	\N	\N	\N	0
3fa470ca-2ef7-11eb-ac45-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	432	t	["ich glaube du meinst le chattelier xD\\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.782618	f	[]	[]	2	4	2	AUTO	\N	f	\N	\N	\N	0
460f0b34-2ef5-11eb-931a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	433	t	["Es hängt...Das video\\n"]	0	f	f	\N	2022-10-24 14:06:09.789108	f	[]	[]	0	1	1	AUTO	\N	f	\N	\N	\N	0
327dea0c-2d67-11eb-9e65-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	434	t	["Bitteeeee umschalten \\n"]	0	f	f	\N	2022-10-24 14:06:09.79591	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
1da3bf0a-2d60-11eb-8811-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	435	t	["War von Anfang an richtig so\\n"]	0	f	f	\N	2022-10-24 14:06:09.802534	f	[]	[{"dep":["nk"],"text":"Anfang"}]	2	2	0	DE	\N	f	\N	\N	\N	0
5fb8ca22-2d68-11eb-8811-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	436	t	["Tschühüüüs\\n"]	0	f	f	\N	2022-10-24 14:06:09.808824	f	[]	[]	2	2	0	AUTO	\N	f	\N	\N	\N	0
af1daa20-2d62-11eb-9e65-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	437	t	["@Antwort 42: nicht jeder ist in Physik I\\n"]	0	f	f	\N	2022-10-24 14:06:09.815549	f	[]	[{"dep":["nk"],"text":"Physik I"}]	2	2	0	DE	\N	f	\N	\N	\N	0
0f6b25bc-2d61-11eb-a175-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	438	t	["Der Radius nimmt ab, weil auch der Kern ja größer wird und durch die Elektronen näher an sich ziehen kann (Antwort auf Frage 27)\\n"]	0	f	f	\N	2022-10-24 14:06:09.823584	f	[]	[{"dep":["sb"],"text":"Radius"},{"dep":["sb"],"text":"Kern"},{"dep":["nk"],"text":"Elektron"},{"dep":["oa"],"text":"Antwort"},{"dep":["nk"],"text":"Frage"}]	2	2	0	DE	\N	f	\N	\N	\N	0
cd3383ce-2ef0-11eb-9ede-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	439	t	["8\\n"]	0	f	f	\N	2022-10-24 14:06:09.829854	f	[]	[]	2	2	0	AUTO	\N	f	\N	\N	\N	0
9d94de58-2d63-11eb-95ba-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	440	t	["Wie weiß man, ob ein Element natürlich atomar oder molekular vorliegt?\\n"]	0	f	f	\N	2022-10-24 14:06:09.836775	f	[]	[{"dep":["sb"],"text":"Element"},{"dep":["cj"],"text":"molekular"}]	5	5	0	DE	\N	f	\N	\N	\N	0
c4c1cbe2-2efa-11eb-9ede-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	441	t	["klopf klopf \\n"]	0	f	f	\N	2022-10-24 14:06:09.843097	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
e5291bba-2ef0-11eb-b394-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	442	t	["keine weil die Kugeln außerhalb der Begrenzung liegen\\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.850102	f	[]	[{"dep":["sb"],"text":"Kugeln"},{"dep":["nk"],"text":"Begrenzung"}]	2	2	0	DE	\N	f	\N	\N	\N	0
03992f92-2eef-11eb-87ab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	443	t	["Es sind 2 für heute drin ...Das untere ist live\\n"]	0	f	f	\N	2022-10-24 14:06:09.856997	f	[]	[]	2	2	0	DE	\N	f	\N	\N	\N	0
3f631c66-5af7-11eb-80ec-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	444	t	["bei mir ist alles normal\\n"]	0	f	f	\N	2022-10-24 14:06:09.863574	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
322da826-2ef2-11eb-a778-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	445	t	["4 = Freibier\\n"]	0	f	f	\N	2022-10-24 14:06:09.869966	f	[]	[{"dep":["ROOT"],"text":"Freibier"}]	1	1	0	DE	\N	f	\N	\N	\N	0
e33c3f6a-2ef2-11eb-a778-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	446	t	["Können Sie nochmal erklären wie die versch Namen waren als in der Mitte rot bzw weiß war \\n"]	0	f	f	\N	2022-10-24 14:06:09.877406	f	[]	[{"dep":["sb"],"text":"Name"},{"dep":["nk"],"text":"Mitte"},{"dep":["pnc"],"text":"bzw"}]	1	1	0	DE	\N	f	\N	\N	\N	0
5fe5070c-2ef5-11eb-a952-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	447	t	["Be mir funktioniert\\n"]	0	f	f	\N	2022-10-24 14:06:09.883771	f	[]	[]	2	2	0	AUTO	\N	f	\N	\N	\N	0
5d063d7a-2ef6-11eb-ac45-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	448	t	["Oder man nutzt ein Katalysator\\n"]	0	f	f	\N	2022-10-24 14:06:09.89059	f	[]	[{"dep":["oa"],"text":"Katalysator"}]	1	1	0	DE	\N	f	\N	\N	\N	0
dd39ed62-2ef0-11eb-a71c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	449	t	["Keine \\n"]	0	f	f	\N	2022-10-24 14:06:09.900607	f	[]	[]	2	2	0	AUTO	\N	f	\N	\N	\N	0
c325ef16-2efa-11eb-87ab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	450	t	["danke, gleichfalls\\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.910264	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
63073e98-2ef3-11eb-9ede-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	451	t	["Könnten Sie den Würfel mal drehen und nochmal zeigen wie genau er aussieht? \\n"]	0	f	f	\N	2022-10-24 14:06:09.920196	f	[]	[{"dep":["oa"],"text":"Würfel"}]	1	1	0	DE	\N	f	\N	\N	\N	0
15771fae-32e0-11eb-958e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	452	t	["Bei mir hat der Stream gestoppt und da steht \\"Medienfehler\\"\\n"]	0	f	f	\N	2022-10-24 14:06:09.930464	f	[]	[{"dep":["sb"],"text":"Stream"}]	1	1	0	DE	\N	f	\N	\N	\N	0
cf0662ea-2ef8-11eb-8a62-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	453	t	["Nochmal erklären warum im exponentenndie stöchiometire steht bitte\\n"]	0	f	f	\N	2022-10-24 14:06:09.940204	f	[]	[]	2	2	0	AUTO	\N	f	\N	\N	\N	0
d119bade-32de-11eb-85f9-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	454	t	["Da winkt doch die Ammoniak Synthese :)\\n"]	0	f	f	\N	2022-10-24 14:06:09.946952	f	[]	[{"dep":["sb"],"text":"Ammoniak Synthese :)"}]	1	1	0	DE	\N	f	\N	\N	\N	0
fa9f9f46-32e2-11eb-85f9-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	455	t	["Die kann man in Tabellen ablesen und dann ausrechnen\\n"]	0	f	f	\N	2022-10-24 14:06:09.954137	f	[]	[{"dep":["nk"],"text":"Tabellen"}]	1	1	0	DE	\N	f	\N	\N	\N	0
0d9ea4d0-32e2-11eb-9d68-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	456	t	["die Ungleichung lässt sich ja drehen. wichtiger ist was sie aussagt\\n"]	0	f	f	\N	2022-10-24 14:06:09.961331	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
67033fae-5966-11eb-80ec-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	457	t	["Sie haben noch nicht auf Ihre Mitschrift umgeschaltet\\n"]	0	f	f	\N	2022-10-24 14:06:09.96855	f	[]	[{"dep":["nk"],"text":"Mitschrift"}]	-4	0	4	DE	\N	f	\N	\N	\N	0
949afdd0-5966-11eb-a058-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	458	t	["Ich sehe immer noch den Film\\n"]	0	f	f	\N	2022-10-24 14:06:09.9762	f	[]	[{"dep":["oa"],"text":"Film"}]	-4	0	4	DE	\N	f	\N	\N	\N	0
165af72e-32e0-11eb-a122-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	459	t	["neuladen\\n\\n"]	0	f	f	\N	2022-10-24 14:06:09.982833	f	[]	[]	2	2	0	AUTO	\N	f	\N	\N	\N	0
1a8402d2-32e0-11eb-9d68-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	460	t	["Ihr müsst die Seite neu laden\\n"]	0	f	f	\N	2022-10-24 14:06:09.989789	f	[]	[{"dep":["oa"],"text":"Seite"}]	2	2	0	DE	\N	f	\N	\N	\N	0
bb3fcc14-32dc-11eb-a122-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	461	t	["Guuuuten Moorgen Heeerr (Dr.) Buchsbaum\\n"]	0	f	f	\N	2022-10-24 14:06:09.99669	f	[]	[]	4	4	0	AUTO	\N	f	\N	\N	\N	0
26942664-53e8-11eb-8edf-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	462	t	["fehlt bei dem O vom Sulfat oben rechts ein freies Elektronenpaar oder soll das so?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.004147	f	[]	[{"dep":["nk"],"text":"Sulfat"},{"dep":["oa"],"text":"Elektronenpaar"}]	1	1	0	DE	\N	f	\N	\N	\N	0
7bc133b0-2efa-11eb-a778-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	569	t	["SOrry wie macht wasser konz. kte?\\n"]	0	f	f	\N	2022-10-24 14:06:10.781667	f	[]	[]	-2	0	2	AUTO	\N	f	\N	\N	\N	0
25483150-32e7-11eb-a122-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	463	t	["Können Sie die Reaktion auf Blatt 7 auch noch mal erklären bitte \\n"]	0	f	f	\N	2022-10-24 14:06:10.011784	f	[]	[{"dep":["oa"],"text":"Reaktion"},{"dep":["nk"],"text":"Blatt"}]	1	1	0	DE	\N	f	\N	\N	\N	0
e90522da-347a-11eb-b7c6-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	464	t	["bei der Formel zu p, V und T welches Zeichen steht zwischen n und RT?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.019096	f	[]	[{"dep":["nk"],"text":"Formel"},{"dep":["sb"],"text":"Zeichen"}]	1	1	0	DE	\N	f	\N	\N	\N	0
e51c43ba-53dd-11eb-b15f-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	465	t	["Nein, alles ist synchron. Aktualisier am besten deinen Browser\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.026393	f	[]	[{"dep":["pd"],"text":"synchron"},{"dep":["oa"],"text":"Browser"}]	6	6	0	DE	\N	f	\N	\N	\N	0
7c55bbe6-346f-11eb-8332-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	466	t	["Skript ist auf Olat unter Vorlesungsunterlagen\\n"]	0	f	f	\N	2022-10-24 14:06:10.033434	f	[]	[{"dep":["sb"],"text":"Skript"},{"dep":["nk"],"text":"Olat"}]	2	2	0	DE	\N	f	\N	\N	\N	0
ddb75812-347a-11eb-b7c6-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	467	t	["ist ein induzierter Dipol auch ein temporärer Dipol?\\n"]	0	f	f	\N	2022-10-24 14:06:10.04052	f	[]	[{"dep":["pd","sb"],"text":"Dipol"}]	1	1	0	DE	\N	f	\N	\N	\N	0
af0b371a-5575-11eb-bd00-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	468	t	["@4513 zur Gewinnung von Aluminium\\n"]	0	f	f	\N	2022-10-24 14:06:10.047736	f	[]	[{"dep":["nk"],"text":"Gewinnung"},{"dep":["nk"],"text":"Aluminium"}]	1	1	0	DE	\N	f	\N	\N	\N	0
ee88ec7a-3473-11eb-b477-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	469	t	["@7561: t1/2 ist einfach die Zeit, nach der noch die Hälfte des Stoffs da ist. In dem Beispiel, die Zeit, nach der nur noch halb so viele Studierende da sind.\\n"]	0	f	f	\N	2022-10-24 14:06:10.056282	f	[]	[{"dep":["mo"],"text":"t1/2"},{"dep":["pd","app"],"text":"Zeit"},{"dep":["sb"],"text":"Hälfte"},{"dep":["ag"],"text":"Stoff"},{"dep":["nk"],"text":"Beispiel"},{"dep":["sb"],"text":"Studierende"}]	1	1	0	DE	\N	f	\N	\N	\N	0
9248691e-32e7-11eb-803e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	470	t	["einbildung ist auch eine bildung\\n"]	0	f	f	\N	2022-10-24 14:06:10.063144	f	[]	[]	2	2	0	AUTO	\N	f	\N	\N	\N	0
1c3e593e-3479-11eb-8332-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	471	t	["Aus Mortimer 11. Auflage: \\"Ein hypothetisches Gas, das unter allen Bedingungen die Gleichung exakt erfüllt, nennt man ein ideales Gas.\\"\\n"]	0	f	f	\N	2022-10-24 14:06:10.071134	f	[]	[{"dep":["nk"],"text":"Auflage"},{"dep":["oa"],"text":"Gas"},{"dep":["nk"],"text":"Bedingung"},{"dep":["oa"],"text":"Gleichung"}]	1	1	0	DE	\N	f	\N	\N	\N	0
1b502bf8-5579-11eb-afe1-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	472	t	["Laden Sie auch noch ein Skript zum Stoffchemie-Teil hoch? \\n"]	0	f	f	\N	2022-10-24 14:06:10.078697	f	[]	[{"dep":["ROOT"],"text":"Laden"},{"dep":["sb"],"text":"Skript"},{"dep":["nk"],"text":"Stoffchemie-Teil"}]	-8	0	8	DE	\N	f	\N	\N	\N	0
72e17462-5ee8-11eb-9862-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	473	t	["haben Sie 5.1 übersprungen?\\n"]	0	f	f	\N	2022-10-24 14:06:10.086122	f	[]	[]	-5	0	5	AUTO	\N	f	\N	\N	\N	0
13540100-5ee2-11eb-911e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	474	t	["Wie entsteht die Hülle von Silikon?\\n"]	0	f	f	\N	2022-10-24 14:06:10.093707	f	[]	[{"dep":["sb"],"text":"Hülle"},{"dep":["nk"],"text":"Silikon"}]	0	1	1	DE	\N	f	\N	\N	\N	0
b034928e-53e1-11eb-86eb-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	475	t	["Wir haben Sie trotzdem lieb, Maestro\\n"]	0	f	f	\N	2022-10-24 14:06:10.101015	f	[]	[{"dep":["oa"],"text":"Maestro"}]	7	7	0	DE	\N	f	\N	\N	\N	0
d240f03e-32e4-11eb-856c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	476	t	["Ich breche heute echt zusammen :D\\n"]	0	f	f	\N	2022-10-24 14:06:10.107626	f	[]	[]	8	8	0	DE	\N	f	\N	\N	\N	0
5c355968-53e9-11eb-b8bb-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	477	t	["Danke und bis Mittwoch ! \\n"]	0	f	f	\N	2022-10-24 14:06:10.114684	f	[]	[{"dep":["ROOT"],"text":"Danke"},{"dep":["nk"],"text":"Mittwoch"}]	1	1	0	DE	\N	f	\N	\N	\N	0
ec0aba06-6078-11eb-960c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	478	t	["Ist Weißer Phosphor nicht auch im Krieg eingesetzt worden?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.121973	f	[]	[{"dep":["sb"],"text":"Weißer Phosphor"},{"dep":["nk"],"text":"Krieg"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
b832a8b4-6074-11eb-a259-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	479	t	["Schalten Sie bitte um?\\n"]	0	f	f	\N	2022-10-24 14:06:10.128921	f	[]	[]	-1	0	1	AUTO	\N	f	\N	\N	\N	0
a5d1b48e-6075-11eb-a2a1-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	480	t	["kurze frage: können Sie in die Evaluation einsehen? also was jeder einzelne eingegeben hat?\\n"]	0	f	f	\N	2022-10-24 14:06:10.136421	f	[]	[]	-3	0	3	AUTO	\N	f	\N	\N	\N	0
5ca81ddc-607c-11eb-8009-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	481	t	["Weshalb sind auf Seite 10 bei der linken mesomeren grenzfomrel die elektronenepaare des o beide auf einer Seite \\n"]	0	f	f	\N	2022-10-24 14:06:10.143976	f	[]	[{"dep":["nk"],"text":"Seite"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
5a03d758-6075-11eb-960c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	482	t	["War das nicht auch im Libanon so ? Bei der Explosion \\n"]	0	f	f	\N	2022-10-24 14:06:10.151025	f	[]	[{"dep":["nk"],"text":"Libanon"},{"dep":["nk"],"text":"Explosion"}]	0	1	1	DE	\N	f	\N	\N	\N	0
7e85a92a-607b-11eb-a259-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	483	t	["Guten Morgen, wo bekommt man denn am Besten flüssigen Stickstoff zu einem guten Preis her?\\n"]	0	f	f	\N	2022-10-24 14:06:10.159187	f	[]	[{"dep":["mo"],"text":"Guten Morgen"},{"dep":["nk"],"text":"Besten"},{"dep":["oa"],"text":"Stickstoff"},{"dep":["nk"],"text":"Preis"}]	-2	0	2	DE	\N	f	\N	\N	\N	0
0f8722e2-6076-11eb-bed5-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	484	t	["Haben wir 5.5 übersprungen oder bin ich einfach nicht mitgekommen?\\n"]	0	f	f	\N	2022-10-24 14:06:10.166251	f	[]	[]	-4	0	4	AUTO	\N	f	\N	\N	\N	0
dcaefa16-53e1-11eb-86eb-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	485	t	["Grün/Gelb?\\n"]	0	f	f	\N	2022-10-24 14:06:10.172983	f	[]	[{"dep":["ROOT"],"text":"Grün/Gelb"}]	2	2	0	DE	\N	f	\N	\N	\N	0
ded1fc4e-53e1-11eb-b8bb-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	486	t	["grün oder?\\n"]	0	f	f	\N	2022-10-24 14:06:10.179673	f	[]	[]	2	2	0	AUTO	\N	f	\N	\N	\N	0
6e873a8a-53e1-11eb-b15f-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	487	t	["f an alle die mittwoch geschrieben haben\\n"]	0	f	f	\N	2022-10-24 14:06:10.186555	f	[]	[]	7	7	0	AUTO	\N	f	\N	\N	\N	0
09937550-53e4-11eb-b15f-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	488	t	["jetzt fragt man sich ob Herr Buchsbaum als Kind auch mit Lego und Bausteinen oder mit Calcitkristallen gespielt hat \\n"]	0	f	f	\N	2022-10-24 14:06:10.194405	f	[]	[{"dep":["sb"],"text":"Herr"},{"dep":["nk"],"text":"Buchsbaum"},{"dep":["nk"],"text":"Kind"},{"dep":["nk"],"text":"Lego"}]	13	13	0	DE	\N	f	\N	\N	\N	0
c94290a8-3868-11eb-8548-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	489	t	["Danke \\n"]	0	f	f	\N	2022-10-24 14:06:10.200861	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
aa1982d0-39ef-11eb-8bb0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	490	t	["bitte passen Sie auf Ihr Mikro auf, irgendwas verursacht unangenehme Geräusche\\n"]	0	f	f	\N	2022-10-24 14:06:10.208269	f	[]	[{"dep":["nk"],"text":"Mikro"},{"dep":["oa"],"text":"Geräusche"}]	2	2	0	DE	\N	f	\N	\N	\N	0
c80e288c-39f8-11eb-831c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	491	t	["Das Rauschen hat übrigens aufgehört! :)\\n"]	0	f	f	\N	2022-10-24 14:06:10.215543	f	[]	[{"dep":["sb"],"text":"Rauschen"}]	1	1	0	DE	\N	f	\N	\N	\N	0
013c930c-3de4-11eb-871d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	492	t	["Sind gerostete Metalle dann gar keine Metalle mehr? Wenn man auf diese mit einem Hammer schlägt brechen sie ja auch, glanz haben diebisch nicht mehr richtig.\\n"]	0	f	f	\N	2022-10-24 14:06:10.223856	f	[]	[{"dep":["sb"],"text":"Metalle"},{"dep":["pd"],"text":"Metall"},{"dep":["nk"],"text":"Hammer"},{"dep":["sb"],"text":"glanz"}]	1	1	0	DE	\N	f	\N	\N	\N	0
3da85206-5af7-11eb-9300-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	493	t	["Ja, ich verstehe Sie\\n"]	0	f	f	\N	2022-10-24 14:06:10.230692	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
e5e921b4-32e0-11eb-ae7b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	495	t	["Man muss schon ehrlich sagen, die AAC Vorlesungen vom Dr. Buchsbaum sind die wildesten \\n"]	0	f	f	\N	2022-10-24 14:06:10.245231	f	[]	[{"dep":["sb"],"text":"AAC"},{"dep":["nk"],"text":"Vorlesungen"}]	9	9	0	DE	\N	f	\N	\N	\N	0
ca0f20ac-53e6-11eb-8d89-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	496	t	["Viel hilft viel (?)\\n"]	0	f	f	\N	2022-10-24 14:06:10.252206	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
c0d88730-53e6-11eb-b92a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	497	t	["Montag halt \\n"]	0	f	f	\N	2022-10-24 14:06:10.259158	f	[]	[]	3	3	0	AUTO	\N	f	\N	\N	\N	0
fed364f6-53e6-11eb-9afc-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	498	t	["CaO hat keine Angst vor Ihnen sie müssen ihre Galluskontakte spielen lassen \\n"]	0	f	f	\N	2022-10-24 14:06:10.266799	f	[]	[{"dep":["sb"],"text":"CaO"},{"dep":["oa"],"text":"Angst"}]	4	4	0	DE	\N	f	\N	\N	\N	0
387e3e18-5579-11eb-b524-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	499	t	["sind  die gase die entstehen nicht giftig?\\n"]	0	f	f	\N	2022-10-24 14:06:10.273731	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
e67f1b98-53e6-11eb-b8bb-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	500	t	["Man gebe dem Mann ein paar Calcit-Kristalle damit er wieder glücklich wird\\n"]	0	f	f	\N	2022-10-24 14:06:10.28127	f	[]	[{"dep":["da"],"text":"Mann"},{"dep":["oa"],"text":"Calcit-Kristalle"}]	6	6	0	DE	\N	f	\N	\N	\N	0
5dbb9892-2d65-11eb-a175-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	501	t	["Wie können die Ionen hydratisiert sein, wenn es auf der Eduktseite nur 2 mol H2o gibt und es somit keine H2o Moleküle zur hydratisierung übrig bleiben?\\n"]	0	f	f	\N	2022-10-24 14:06:10.289503	f	[]	[{"dep":["sb"],"text":"Ionen"},{"dep":["nk"],"text":"hydratisierung"}]	0	0	0	DE	\N	f	\N	\N	\N	0
de569296-32e6-11eb-803e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	502	t	["Können sie Blatt 7 Kurz nochmal zeigen?\\n"]	0	f	f	\N	2022-10-24 14:06:10.296809	f	[]	[{"dep":["sb"],"text":"Blatt"}]	0	0	0	DE	\N	f	\N	\N	\N	0
736ee86e-3ddd-11eb-9b23-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	503	t	["Da Blatt und sie sind nicht synchron \\n"]	0	f	f	\N	2022-10-24 14:06:10.304198	f	[]	[{"dep":["sb"],"text":"Blatt"},{"dep":["pd"],"text":"synchron"}]	-2	0	2	DE	\N	f	\N	\N	\N	0
76b9e988-3478-11eb-a957-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	504	t	["Welche Kohäsionskräfte wirken zwischen Gasteilchen?\\nVdW-Kräfte?\\n"]	0	f	f	\N	2022-10-24 14:06:10.31154	f	[]	[]	-3	0	3	AUTO	\N	f	\N	\N	\N	0
f68f3cbe-3de0-11eb-bae4-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	505	t	["Lewis-Formeln = \\"sharing is caring\\"\\n"]	0	f	f	\N	2022-10-24 14:06:10.318599	f	[]	[{"dep":["nsubj"],"text":"sharing"}]	2	3	1	EN	\N	f	\N	\N	\N	0
07133af6-39f7-11eb-831c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	506	t	["es gibt nur buchstaben als antwortmöglichkeit\\n"]	0	f	f	\N	2022-10-24 14:06:10.325706	f	[]	[]	3	3	0	AUTO	\N	f	\N	\N	\N	0
42855490-2ef1-11eb-a952-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	507	t	["Können sie nochmal den Qrcode zeigen?\\n"]	0	f	f	\N	2022-10-24 14:06:10.332659	f	[]	[{"dep":["oa"],"text":"Qrcode"}]	2	2	0	DE	\N	f	\N	\N	\N	0
5e281242-2efa-11eb-bb4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	508	t	["Oh wei, oh wei und schon ist der Spaß wieder vorbei\\n"]	0	f	f	\N	2022-10-24 14:06:10.339758	f	[]	[{"dep":["sb"],"text":"Spaß"}]	1	1	0	DE	\N	f	\N	\N	\N	0
2460ff04-2d67-11eb-af1e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	509	t	["Bitte umschalten ;O\\n"]	0	f	f	\N	2022-10-24 14:06:10.346529	f	[]	[]	2	2	0	DE	\N	f	\N	\N	\N	0
102f0b1a-32e0-11eb-a122-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	510	t	["Bei mir ist ein Medienerror aufgetreten und die Vorlesung geht nicht mehr\\n"]	0	f	f	\N	2022-10-24 14:06:10.35387	f	[]	[{"dep":["sb"],"text":"Vorlesung"}]	1	1	0	DE	\N	f	\N	\N	\N	0
14fe470e-2ef4-11eb-bb4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	511	t	["Ich liebe diesen Mann xD\\n"]	0	f	f	\N	2022-10-24 14:06:10.360851	f	[]	[{"dep":["oa"],"text":"Mann"}]	10	10	0	DE	\N	f	\N	\N	\N	0
98902350-32df-11eb-b45e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	512	t	["Katalysatoren beeinflussen das ggw NICHT\\n"]	0	f	f	\N	2022-10-24 14:06:10.368008	f	[]	[{"dep":["sb"],"text":"Katalysator"}]	2	2	0	DE	\N	f	\N	\N	\N	0
77d0feae-3475-11eb-a957-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	513	t	["deltaG = 0 ggw\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.374644	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
8ae0b7d2-32e5-11eb-958e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	514	t	["Blatt 6 nochmal Ziegen bitte \\n"]	0	f	f	\N	2022-10-24 14:06:10.381827	f	[]	[{"dep":["ROOT"],"text":"Blatt"},{"dep":["ROOT"],"text":"Ziege"}]	8	8	0	DE	\N	f	\N	\N	\N	0
a1a6d53e-32e8-11eb-9d68-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	515	t	["Vielen Dank Maestro Buchsbaum\\nich/wir hatten heute wieder große Freude\\n"]	0	f	f	\N	2022-10-24 14:06:10.389359	f	[]	[{"dep":["nk"],"text":"ich/wir"},{"dep":["oa"],"text":"Freude"}]	12	12	0	DE	\N	f	\N	\N	\N	0
45b37aaa-39f2-11eb-addd-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	516	t	["Klingt so wie wenn man ein Kabel nicht richtig stecken hat \\n"]	0	f	f	\N	2022-10-24 14:06:10.39683	f	[]	[{"dep":["oa"],"text":"Kabel"}]	1	1	0	DE	\N	f	\N	\N	\N	0
64c68450-53e7-11eb-92d9-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	517	t	["Wenn wir grade von Kristallen reden, warum bleibt der Faden am Eisblock kleben wenn man Salz hinzugibt ?\\n"]	0	f	f	\N	2022-10-24 14:06:10.404778	f	[]	[{"dep":["nk"],"text":"Kristallen"},{"dep":["sb"],"text":"Faden"},{"dep":["nk"],"text":"Eisblock"},{"dep":["oa"],"text":"Salz"}]	1	1	0	DE	\N	f	\N	\N	\N	0
13991998-5960-11eb-830d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	518	t	["Was hat es eigentlich mit diesen Videos auf sich die heute hochgeladen wurden?\\n"]	0	f	f	\N	2022-10-24 14:06:10.412239	f	[]	[{"dep":["nk"],"text":"Videos"}]	2	2	0	DE	\N	f	\N	\N	\N	0
b755afaa-3f76-11eb-8f4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	519	t	["pH ist 2.37\\n"]	0	f	f	\N	2022-10-24 14:06:10.419467	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
d5bc4cfc-5960-11eb-aa3a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	520	t	["Ist aktivkohle nicht auch in vielen Gesichtsmasken? Wieso eigentlich?\\n"]	0	f	f	\N	2022-10-24 14:06:10.426572	f	[]	[]	1	1	0	DE	\N	f	\N	\N	\N	0
4e0fd510-3f72-11eb-a2fc-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	521	t	["sie haben auf das Nebenblatt ph 1 geschrieben aber mit ph 0 gerechnet und erklärt\\n"]	0	f	f	\N	2022-10-24 14:06:10.434135	f	[]	[{"dep":["nk"],"text":"Nebenblatt"},{"dep":["mo"],"text":"ph 1"}]	1	1	0	DE	\N	f	\N	\N	\N	0
eb38f300-3f7a-11eb-9840-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	522	t	["Schöne Zeit!!\\n"]	0	f	f	\N	2022-10-24 14:06:10.440808	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
e6c04850-3f7a-11eb-929d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	523	t	["Frohe Weihnachten und guten Rutsch!!\\n"]	0	f	f	\N	2022-10-24 14:06:10.448262	f	[]	[{"dep":["ROOT"],"text":"Frohe Weihnachten"},{"dep":["cj"],"text":"Rutsch"}]	3	3	0	DE	\N	f	\N	\N	\N	0
572ced2c-3f72-11eb-9f30-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	524	t	["Sie haben ganz oben links statt ph = 0  Ph= 1 geschrieben..\\n"]	0	f	f	\N	2022-10-24 14:06:10.455668	f	[]	[{"dep":["ROOT"],"text":"Ph="}]	3	3	0	DE	\N	f	\N	\N	\N	0
ee616f26-3f7a-11eb-b250-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	525	t	["Danke, für sie auch alles Gute!\\n"]	0	f	f	\N	2022-10-24 14:06:10.462926	f	[]	[{"dep":["ROOT"],"text":"Danke"},{"dep":["app"],"text":"Gute"}]	2	2	0	DE	\N	f	\N	\N	\N	0
3d74cb50-5964-11eb-a058-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	526	t	["Sie sind sehr schnell\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.469784	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
c2bff65c-5966-11eb-aa3a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	527	t	["Bei mir ist es umgeschaltet\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.476595	f	[]	[]	4	4	0	AUTO	\N	f	\N	\N	\N	0
93448652-32e4-11eb-803e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	528	t	["Die Fragen sind heute aber anders lost Leute\\n"]	0	f	f	\N	2022-10-24 14:06:10.48399	f	[]	[{"dep":["sb"],"text":"Frage"},{"dep":["oa"],"text":"Leute"}]	19	19	0	DE	\N	f	\N	\N	\N	0
e726ffc2-556f-11eb-afe1-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	529	t	["Jetzt rauscht es nicht mehr :)\\n"]	0	f	f	\N	2022-10-24 14:06:10.491018	f	[]	[]	2	2	0	AUTO	\N	f	\N	\N	\N	0
0d3d4fc6-eb2a-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	238	t	["n*p\\n"]	0	f	f	\N	2022-10-24 14:06:14.132343	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
5fb4a7a4-5571-11eb-a341-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	530	t	["Das Rauschen ist weider weg ^^\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.498262	f	[]	[{"dep":["sb"],"text":"Rauschen"}]	1	1	0	DE	\N	f	\N	\N	\N	0
8c0b4e2e-5572-11eb-b9f3-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	531	t	["sie müssen umschalten\\n"]	0	f	f	\N	2022-10-24 14:06:10.505051	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
f2379d8e-5571-11eb-99a1-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	532	t	["müssen wir diese seite für die klausur by heart können?\\n"]	0	f	f	\N	2022-10-24 14:06:10.512392	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
31053e90-5577-11eb-b9f3-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	533	t	["echt geiler scheiss XD\\n"]	0	f	f	\N	2022-10-24 14:06:10.519248	f	[]	[]	5	5	0	AUTO	\N	f	\N	\N	\N	0
d4aecce8-5575-11eb-a341-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	534	t	["Man könnte meinen es wäre Montag \\n"]	0	f	f	\N	2022-10-24 14:06:10.526351	f	[]	[{"dep":["pd"],"text":"Montag"}]	5	5	0	DE	\N	f	\N	\N	\N	0
99148b3a-5572-11eb-b3c0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	535	t	["Man sieht den Rubin nicht weil sie nicht umgeschaltet haben \\n"]	0	f	f	\N	2022-10-24 14:06:10.533985	f	[]	[{"dep":["oa"],"text":"Rubin"}]	8	8	0	DE	\N	f	\N	\N	\N	0
9b7a56f6-5af5-11eb-a058-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	536	t	["Schon mal probiert F5 zu drücken?\\n"]	0	f	f	\N	2022-10-24 14:06:10.541233	f	[]	[]	1	1	0	DE	\N	f	\N	\N	\N	0
3c73fd10-5af5-11eb-8009-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	537	t	["präsentation  beendet ?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.548137	f	[]	[]	5	5	0	AUTO	\N	f	\N	\N	\N	0
f38292e6-6077-11eb-848c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	538	t	["Habe nicht ganz verstanden warum man jetzt nicht die Doppel Bindungen benutzt :(\\n"]	0	f	f	\N	2022-10-24 14:06:10.555463	f	[]	[]	2	2	0	DE	\N	f	\N	\N	\N	0
f92d2654-607a-11eb-8009-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	539	t	["könnten Sie irgendwann erklären warum cola mit mentos  so exploiv reagiert? \\n"]	0	f	f	\N	2022-10-24 14:06:10.562552	f	[]	[]	1	1	0	AUTO	\N	f	\N	\N	\N	0
1f142e9e-2d5e-11eb-8811-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	540	t	["Wieso nimmt der Atomradius in der Periode durch zunehmende Protonenzahl ab?\\nDie Elektronenzahl steigt proportional ja auch?\\n"]	0	f	f	\N	2022-10-24 14:06:10.5707	f	[]	[{"dep":["sb"],"text":"Atomradius"},{"dep":["nk"],"text":"Periode"},{"dep":["nk"],"text":"Protonenzahl"}]	0	1	1	DE	\N	f	\N	\N	\N	0
321ad5d2-2d5f-11eb-95ba-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	541	t	["Ist O2 (2 soll unten stehen) ein Molekül oder ein Atom? Weil Sie hatten bei Daltons Postulaten gesagt, dass es keine Verbindung ist.\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.579053	f	[]	[{"dep":["sb"],"text":"Molekül"},{"dep":["cj"],"text":"Atom"},{"dep":["pd"],"text":"Verbindung"}]	-1	1	2	DE	\N	f	\N	\N	\N	0
e67246ca-2d5d-11eb-8525-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	542	t	["jetzt bin ich drin (:\\n\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.585764	f	[]	[]	0	2	2	AUTO	\N	f	\N	\N	\N	0
f62a1b88-2d5d-11eb-9e65-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	543	t	["wenn wir uns aus den Gruppen austragen, bekommen wir dann heute noch den Link für Zoom?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.593663	f	[]	[{"dep":["nk"],"text":"Gruppe"},{"dep":["oa"],"text":"Link"},{"dep":["nk"],"text":"Zoom"}]	1	2	1	DE	\N	f	\N	\N	\N	0
efef8a50-2d5d-11eb-9e65-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	544	t	["funktioniert \\n"]	0	f	f	\N	2022-10-24 14:06:10.600276	f	[]	[]	-1	0	1	AUTO	\N	f	\N	\N	\N	0
8868fbf0-2d62-11eb-95ba-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	545	t	["Auf signifikante Stellen Runden wie es in Physik I vorgetragen wurde kann als Faustregel genutzt werden\\n"]	0	f	f	\N	2022-10-24 14:06:10.608055	f	[]	[{"dep":["nk"],"text":"Physik I"},{"dep":["nk"],"text":"Faustregel"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
16cddc10-2d60-11eb-bc8d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	546	t	["Man sieht das Blatt\\n"]	0	f	f	\N	2022-10-24 14:06:10.615381	f	[]	[{"dep":["oa"],"text":"Blatt"}]	0	1	1	DE	\N	f	\N	\N	\N	0
13e36bb4-2d60-11eb-9ff0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	547	t	["Ivh kann Blatt sehen\\n"]	0	f	f	\N	2022-10-24 14:06:10.622394	f	[]	[]	1	2	1	AUTO	\N	f	\N	\N	\N	0
2f20a736-2d5e-11eb-a175-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	548	t	["Können sie das Blatt bisschen runter schieben bitte \\n"]	0	f	f	\N	2022-10-24 14:06:10.629846	f	[]	[{"dep":["oa"],"text":"Blatt"},{"dep":["oc"],"text":"bisschen"}]	2	3	1	DE	\N	f	\N	\N	\N	0
c46449f0-2d5f-11eb-b524-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	549	t	["Welches element ist Su im PSE?\\n\\n\\n\\n\\n\\nDas Element der Überraschung (suprise) :D\\n"]	0	f	f	\N	2022-10-24 14:06:10.637453	f	[]	[]	1	4	3	AUTO	\N	f	\N	\N	\N	0
27c1ec62-2d67-11eb-b524-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	550	t	["Umschalten \\n"]	0	f	f	\N	2022-10-24 14:06:10.644354	f	[]	[]	-1	0	1	DE	\N	f	\N	\N	\N	0
691b0d26-2efa-11eb-bb4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	551	t	["Tipp topp\\n"]	0	f	f	\N	2022-10-24 14:06:10.651422	f	[]	[]	0	1	1	AUTO	\N	f	\N	\N	\N	0
a74a83f2-2ef9-11eb-8a62-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	552	t	["im Skript steht das MWG 5.2 war und Beeinflussung des Gleichgewichts 5.3 ist\\n"]	0	f	f	\N	2022-10-24 14:06:10.659618	f	[]	[{"dep":["nk"],"text":"Skript"},{"dep":["sb"],"text":"MWG"},{"dep":["sb"],"text":"Beeinflussung"},{"dep":["ag"],"text":"Gleichgewicht"}]	-2	0	2	DE	\N	f	\N	\N	\N	0
43e3c6ae-2ef7-11eb-8a62-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	553	t	["Kurz mal vom ganzen Kurs hops genommen xD\\n"]	0	f	f	\N	2022-10-24 14:06:10.66664	f	[]	[]	10	11	1	AUTO	\N	f	\N	\N	\N	0
70b96aee-2ef2-11eb-87ab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	554	t	["Jeweils 4 rote und  weiße\\n"]	0	f	f	\N	2022-10-24 14:06:10.673508	f	[]	[]	1	2	1	AUTO	\N	f	\N	\N	\N	0
9d983798-2ef2-11eb-a71c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	555	t	["rot\\n"]	0	f	f	\N	2022-10-24 14:06:10.680571	f	[]	[]	0	1	1	AUTO	\N	f	\N	\N	\N	0
94328640-2ef2-11eb-9ede-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	556	t	["Ist das obere in der Mitte zur Kamera gezeigte rot oder weiß \\n"]	0	f	f	\N	2022-10-24 14:06:10.688212	f	[]	[{"dep":["nk"],"text":"Mitte"},{"dep":["nk"],"text":"Kamera"}]	1	2	1	DE	\N	f	\N	\N	\N	0
fd5ad770-2eee-11eb-931a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	557	t	["Über was hat er uns informiert? Internet hing :/ \\n"]	0	f	f	\N	2022-10-24 14:06:10.69574	f	[]	[{"dep":["sb"],"text":"Internet"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
870c16ca-2ef2-11eb-931a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	558	t	["Leudis ihr sollt das nicht hier machen\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.70307	f	[]	[]	0	1	1	DE	\N	f	\N	\N	\N	0
14c26c1e-2ef1-11eb-8a62-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	559	t	["1\\n"]	0	f	f	\N	2022-10-24 14:06:10.709748	f	[]	[]	-1	0	1	AUTO	\N	f	\N	\N	\N	0
3c7458f2-2ef7-11eb-b394-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	560	t	["Das Prinzip von Chatelier? \\n"]	0	f	f	\N	2022-10-24 14:06:10.716812	f	[]	[]	-1	2	3	AUTO	\N	f	\N	\N	\N	0
37c87d70-2ef1-11eb-87ab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	561	t	["8\\n"]	0	f	f	\N	2022-10-24 14:06:10.723671	f	[]	[]	-2	0	2	AUTO	\N	f	\N	\N	\N	0
8bea4fe0-2ef2-11eb-ac45-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	562	t	["wie war das nochmal mit dem innenzentiert\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.730913	f	[]	[]	1	2	1	AUTO	\N	f	\N	\N	\N	0
a59cd286-2ef8-11eb-931a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	563	t	["Ist bei eich auch alles grad down?\\n"]	0	f	f	\N	2022-10-24 14:06:10.737991	f	[]	[]	0	3	3	AUTO	\N	f	\N	\N	\N	0
724a5fde-2ef8-11eb-9ede-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	564	t	["Hin durch rück wenn es hilft denk an dreisatz\\n"]	0	f	f	\N	2022-10-24 14:06:10.745281	f	[]	[]	-1	0	1	AUTO	\N	f	\N	\N	\N	0
114ddb58-2ef7-11eb-a778-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	565	t	["Pfff\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.752227	f	[]	[]	-1	1	2	AUTO	\N	f	\N	\N	\N	0
5fc4c578-2efa-11eb-9ede-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	566	t	["ist die Reaktion Ende oder exotherm\\n"]	0	f	f	\N	2022-10-24 14:06:10.759228	f	[]	[]	0	1	1	DE	\N	f	\N	\N	\N	0
ea7677b0-2ef6-11eb-931a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	567	t	["Spoiler: Man entzieht der Reaktion seine Produkte, sodass mehr Edukte nachsynthetisiert werden.\\n"]	0	f	f	\N	2022-10-24 14:06:10.767481	f	[]	[{"dep":["ROOT"],"text":"Spoiler"},{"dep":["da"],"text":"Reaktion"},{"dep":["oa"],"text":"Produkt"}]	-1	1	2	DE	\N	f	\N	\N	\N	0
68e4f556-2efa-11eb-a952-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	570	t	["Können sie nochmal erklären wieso von der relativen Atommasse zu molaren Masse kommt? \\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.789585	f	[]	[{"dep":["nk"],"text":"Atommasse"},{"dep":["nk"],"text":"Masse"}]	-2	0	2	DE	\N	f	\N	\N	\N	0
4c405616-32e2-11eb-803e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	571	t	["Ist wohl schon Zeit für einen Jägermeister.\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.797526	f	[]	[{"dep":["sb"],"text":"Zeit"},{"dep":["nk"],"text":"Jägermeister"}]	19	20	1	DE	\N	f	\N	\N	\N	0
af6b8a26-2efa-11eb-ac45-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	572	t	["@8417 fühle mich gemobbt haha\\n"]	0	f	f	\N	2022-10-24 14:06:10.804595	f	[]	[{"dep":["oa"],"text":"haha"}]	-3	0	3	DE	\N	f	\N	\N	\N	0
d21d4520-3475-11eb-aab8-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	573	t	["Also Phase= Aggregationszustand?\\n"]	0	f	f	\N	2022-10-24 14:06:10.811801	f	[]	[]	-1	0	1	AUTO	\N	f	\N	\N	\N	0
8c24cbd6-2efa-11eb-ac45-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	574	t	["Der Doppelspoiler Kommentar hat einfach dir negativste Bewertung im ganzen Raum xD \\n"]	0	f	f	\N	2022-10-24 14:06:10.819673	f	[]	[{"dep":["sb"],"text":"Kommentar"},{"dep":["oa"],"text":"Bewertung"},{"dep":["nk"],"text":"Raum"}]	0	3	3	DE	\N	f	\N	\N	\N	0
9337b70c-3de7-11eb-8f4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	575	t	["legen Sie wert darauf, dass wir hinter die Ionen \\"(aq)\\" schreiben? Denn die Ionen sind ja von einer Hydrathülle umgeben.\\n"]	0	f	f	\N	2022-10-24 14:06:10.828052	f	[]	[{"dep":["nk","sb"],"text":"Ionen"},{"dep":["nk"],"text":"Hydrathülle"}]	0	1	1	DE	\N	f	\N	\N	\N	0
e855f956-39fa-11eb-8bb0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	576	t	["Bye bye Maestro\\n"]	0	f	f	\N	2022-10-24 14:06:10.834993	f	[]	[]	0	1	1	AUTO	\N	f	\N	\N	\N	0
210642b6-32de-11eb-856c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	577	t	["ich glaube Sie haben gesagt h20 reagiert mit 1mol cl2. es waren aber doch nur 0,1 mol, oder ?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.842172	f	[]	[]	-2	0	2	AUTO	\N	f	\N	\N	\N	0
966d7262-3479-11eb-a957-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	578	t	["können sie das komplette besipiel sichtbar machen, also das Blatt\\n"]	0	f	f	\N	2022-10-24 14:06:10.860758	f	[]	[{"dep":["app"],"text":"Blatt"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
b25ce4fc-3475-11eb-bb87-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	579	t	["Gleichgewicht = Q=K und deltaG=0\\n"]	0	f	f	\N	2022-10-24 14:06:10.872679	f	[]	[{"dep":["ROOT"],"text":"Gleichgewicht"},{"dep":["cj"],"text":"deltaG=0"}]	0	1	1	DE	\N	f	\N	\N	\N	0
5ad7cdf0-53dd-11eb-b15f-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	580	t	["Bei der nerntschen Gleichung müsste es + 0,059 V/z sein oder? In ihrer Mitschrift hatten sie - geschrieben\\n"]	0	f	f	\N	2022-10-24 14:06:10.883818	f	[]	[{"dep":["nk"],"text":"Gleichung"},{"dep":["pd"],"text":"V/z"},{"dep":["nk"],"text":"Mitschrift"}]	-4	0	4	DE	\N	f	\N	\N	\N	0
d7e98fb6-32dd-11eb-970c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	581	t	["Warum ist es 55,41 dann und nicht 54,51?\\n"]	0	f	f	\N	2022-10-24 14:06:10.89382	f	[]	[{"dep":["mo"],"text":"Warum"}]	-4	0	4	DE	\N	f	\N	\N	\N	0
5567edc4-2ef7-11eb-a952-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	582	t	["Ja meinte le chatelier, verwechsle immer die franzosen haha\\n"]	0	f	f	\N	2022-10-24 14:06:10.903303	f	[]	[]	-8	2	10	AUTO	\N	f	\N	\N	\N	0
b4e0b424-3475-11eb-a2c7-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	583	t	["Und Plasma?\\n"]	0	f	f	\N	2022-10-24 14:06:10.91268	f	[]	[]	1	2	1	AUTO	\N	f	\N	\N	\N	0
75f32380-32e7-11eb-85f9-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	584	t	["Jetzt hab ich Lust auf Gummibärchen \\n"]	0	f	f	\N	2022-10-24 14:06:10.922441	f	[]	[{"dep":["oa"],"text":"Lust"},{"dep":["nk"],"text":"Gummibärchen"}]	6	7	1	DE	\N	f	\N	\N	\N	0
ac4cf5ec-39ee-11eb-8bb0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	585	t	["Man kann sich schon seit einiger Zeit auf QIS für die Klausur anmelden. Allerdings ist im QIS als Datum für die Klausur der 02.03. und nicht der 01.03. eingetragen. Welches Datum ist nun zutreffend?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:10.934109	f	[]	[{"dep":["nk"],"text":"Zeit"},{"dep":["nk"],"text":"QIS"},{"dep":["nk"],"text":"Klausur"},{"dep":["nk","sb"],"text":"Datum"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
daa9e150-39f0-11eb-b6ec-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	586	t	["die Bezeichnung hydrophil könnte auch passen :)\\n"]	0	f	f	\N	2022-10-24 14:06:10.94444	f	[]	[{"dep":["sb"],"text":"Bezeichnung"},{"dep":["nk"],"text":"hydrophil"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
a644c4a0-53dd-11eb-9c3d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	587	t	["Guten Morgen, gibt es für den Teil Stoffchemie auch ein Skript?\\n"]	0	f	f	\N	2022-10-24 14:06:10.955649	f	[]	[{"dep":["mo"],"text":"Guten Morgen"},{"dep":["nk"],"text":"Teil"},{"dep":["oa"],"text":"Skript"}]	0	1	1	DE	\N	f	\N	\N	\N	0
662e443e-32e7-11eb-85f9-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	588	t	["Wären doch alle lehrenden Personen so symphatisch. :D\\n"]	0	f	f	\N	2022-10-24 14:06:10.9632	f	[]	[{"dep":["sb"],"text":"Person"}]	0	1	1	DE	\N	f	\N	\N	\N	0
5819da96-346f-11eb-aab8-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	589	t	["Bei Qis sieht man auch die Klausurtermine, anstatt den Herrn zu fragen, kann man da auch mal reinschauen\\n"]	0	f	f	\N	2022-10-24 14:06:10.972932	f	[]	[{"dep":["nk"],"text":"Qis"},{"dep":["oa"],"text":"Herrn"}]	0	1	1	DE	\N	f	\N	\N	\N	0
762042ca-3ddc-11eb-9b23-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	590	t	["es rauscht oder der ton ist ganz weg \\n"]	0	f	f	\N	2022-10-24 14:06:10.980283	f	[]	[]	5	6	1	AUTO	\N	f	\N	\N	\N	0
4ddea350-3de7-11eb-8c34-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	591	t	["Nochmal Benennung s1 s2 b1 b2 erklären. Bitte \\n"]	0	f	f	\N	2022-10-24 14:06:10.993046	f	[]	[{"dep":["ROOT"],"text":"Bitte"}]	0	1	1	DE	\N	f	\N	\N	\N	0
319360c6-32e5-11eb-970c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	592	t	["Laut Mortimer hat der heutige Dr. Buchsbaum recht mit der Entropie\\n"]	0	f	f	\N	2022-10-24 14:06:11.001498	f	[]	[{"dep":["nk"],"text":"Mortimer"},{"dep":["nk"],"text":"Entropie"}]	7	8	1	DE	\N	f	\N	\N	\N	0
7a22d79c-346e-11eb-8452-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	593	t	["Guten Morgen :)\\n"]	0	f	f	\N	2022-10-24 14:06:11.009037	f	[]	[{"dep":["ROOT"],"text":"Guten Morgen :)"}]	0	1	1	DE	\N	f	\N	\N	\N	0
13d9a6c2-346f-11eb-b477-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	594	t	["Gibt es bereits Informationen, wann die Vorlesungsklausur stattfinden wird? \\n"]	0	f	f	\N	2022-10-24 14:06:11.017535	f	[]	[{"dep":["oa"],"text":"Information"}]	-3	0	3	DE	\N	f	\N	\N	\N	0
c4fb5378-53dd-11eb-9afc-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	595	t	["ihr Bildschirm der Dokumentenkamera ist ca. 30 Sekunden versetzt zu dem was Sie sagen\\n"]	0	f	f	\N	2022-10-24 14:06:11.02569	f	[]	[]	-4	0	4	AUTO	\N	f	\N	\N	\N	0
764595f2-32e7-11eb-958e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	596	t	["sind wir gebildet??\\n"]	0	f	f	\N	2022-10-24 14:06:11.036825	f	[]	[]	1	2	1	AUTO	\N	f	\N	\N	\N	0
6ccd68d8-3de6-11eb-9b23-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	597	t	["warum steht bei der letzten reaktion plus 1/2 H2 hoch 1\\n"]	0	f	f	\N	2022-10-24 14:06:11.045421	f	[]	[{"dep":["mo"],"text":"warum"},{"dep":["nk"],"text":"reaktion"},{"dep":["mnr"],"text":"plus 1/2 H2"}]	1	2	1	DE	\N	f	\N	\N	\N	0
ca4485f8-3de8-11eb-871d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	598	t	["Woher bekommen wir das vorlesungsskript für die stoffchemie?\\n"]	0	f	f	\N	2022-10-24 14:06:11.05317	f	[]	[]	1	2	1	AUTO	\N	f	\N	\N	\N	0
45a6237c-5af5-11eb-8593-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	599	t	["Bei mir ist sie auch beendet, rip\\n"]	0	f	f	\N	2022-10-24 14:06:11.061475	f	[]	[{"dep":["cj"],"text":"rip"}]	1	2	1	DE	\N	f	\N	\N	\N	0
f6cec00c-3479-11eb-bb87-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	600	t	["Man weiß einfach, dass Stickstoff in der Luft zu einem Anteil von 0,78 gegeben ist\\n"]	0	f	f	\N	2022-10-24 14:06:11.07014	f	[]	[{"dep":["sb"],"text":"Stickstoff"},{"dep":["nk"],"text":"Luft"},{"dep":["nk"],"text":"Anteil"}]	0	1	1	DE	\N	f	\N	\N	\N	0
76eb707a-3de7-11eb-8f4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	601	t	["Wieso ist (im roten Kasten) die Rede von Protonen wenn wir h+ meinen? Wäre h+ nicht ein kation und kein Proton ?\\n"]	0	f	f	\N	2022-10-24 14:06:11.079179	f	[]	[]	-1	2	3	AUTO	\N	f	\N	\N	\N	0
351bbbea-3de8-11eb-8f4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	602	t	["In welchem Zusammenhang stehen S1 und B1 nochmal? Aus der Säure wird die Base?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:11.08943	f	[]	[{"dep":["nk"],"text":"Zusammenhang"},{"dep":["nk"],"text":"Säure"},{"dep":["sb"],"text":"Base"}]	0	1	1	DE	\N	f	\N	\N	\N	0
92cc803c-3864-11eb-9cd2-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	603	t	["Sie haben 7.5 übersprungen\\n"]	0	f	f	\N	2022-10-24 14:06:11.098954	f	[]	[]	-1	0	1	AUTO	\N	f	\N	\N	\N	0
df6ded02-32e4-11eb-a5e3-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	604	t	["Im Skript steht je größer die Unordnung, desto kleiner die Entropie. Sie hatten gerade aber größer geschrieben, was stimmt denn jetzt?\\n"]	0	f	f	\N	2022-10-24 14:06:11.107443	f	[]	[{"dep":["nk"],"text":"Skript"},{"dep":["sb"],"text":"Unordnung"},{"dep":["sb"],"text":"Entropie"}]	1	2	1	DE	\N	f	\N	\N	\N	0
e33b4ee8-3479-11eb-a2c7-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	605	t	["wir haben phi doch nicht gegeben oder, wie haben sie dann gerechnet\\n\\n"]	0	f	f	\N	2022-10-24 14:06:11.115357	f	[]	[{"dep":["oa"],"text":"phi"}]	-2	0	2	DE	\N	f	\N	\N	\N	0
5933c360-32e4-11eb-b45e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	606	t	["Passiert das auch in der Realität oder ist das nur ein math. Werkzeug? Ich meine wenn man C dann CO dann O2 bekommt man ein gemisch CO u CO2?\\n"]	0	f	f	\N	2022-10-24 14:06:11.124193	f	[]	[{"dep":["nk"],"text":"Realität"},{"dep":["pd"],"text":"math"},{"dep":["ROOT"],"text":"Werkzeug"},{"dep":["oa"],"text":"CO2"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
033ffb0e-32df-11eb-ae7b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	607	t	["Druck erhöhen, weil auf der rechten Seite nur 2 Mol sind und links 3\\n"]	0	f	f	\N	2022-10-24 14:06:11.132561	f	[]	[{"dep":["oa"],"text":"Druck"},{"dep":["nk"],"text":"Seite"},{"dep":["sb"],"text":"Mol"}]	-3	1	4	DE	\N	f	\N	\N	\N	0
5260b2e6-32e4-11eb-85f9-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	608	t	["Dürfen wir uns wie in Physik, für die Klausur ein DIN A4 Blatt mit Notizen mitnehmen ?\\n"]	0	f	f	\N	2022-10-24 14:06:11.141867	f	[]	[{"dep":["nk"],"text":"Physik"},{"dep":["nk"],"text":"Klausur"},{"dep":["oa"],"text":"DIN A4"},{"dep":["nk"],"text":"Blatt"},{"dep":["nk"],"text":"Notiz"}]	-4	0	4	DE	\N	f	\N	\N	\N	0
0f3df380-53e0-11eb-9c3d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	609	t	["es rauscht \\n"]	0	f	f	\N	2022-10-24 14:06:11.149892	f	[]	[]	-3	1	4	AUTO	\N	f	\N	\N	\N	0
cb07ef98-385c-11eb-b55d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	610	t	["Warum genau benötigt auch eine exogene kurzzeitig ativierungsenergue\\n"]	0	f	f	\N	2022-10-24 14:06:11.15776	f	[]	[{"dep":["mo"],"text":"Warum"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
ed9e3f7a-3862-11eb-b27c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	611	t	["was ist mit der kurve zwischen tripelpunkt und sdp ?\\n"]	0	f	f	\N	2022-10-24 14:06:11.165403	f	[]	[]	0	1	1	AUTO	\N	f	\N	\N	\N	0
9b8eb71a-53e1-11eb-9c3d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	612	t	["Also bei mir ist es immer noch sticht synchron, auch nach mehrmaligem Browserneustart\\n"]	0	f	f	\N	2022-10-24 14:06:11.173319	f	[]	[{"dep":["nk"],"text":"mir"},{"dep":["nk"],"text":"synchron"}]	-1	1	2	DE	\N	f	\N	\N	\N	0
212ec78c-53e4-11eb-9c3d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	613	t	["Was Genua ist die Struktur, welche so aussieht wie eine Lewis formel\\n"]	0	f	f	\N	2022-10-24 14:06:11.182044	f	[]	[{"dep":["sb"],"text":"Genua"},{"dep":["pd"],"text":"Struktur"},{"dep":["mo"],"text":"Lewis"}]	-2	0	2	DE	\N	f	\N	\N	\N	0
b78231ee-385d-11eb-831c-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	614	t	["Bei einem Gas sind die einzelnen Moleküle ja nicht direkt aneinander sondern haben einen Abstand. Was befindet sich dann dazwischen? Luftmoleküle? \\n"]	0	f	f	\N	2022-10-24 14:06:11.190619	f	[]	[{"dep":["nk"],"text":"Gas"},{"dep":["sb"],"text":"Molekül"},{"dep":["oa"],"text":"Abstand"}]	-1	1	2	DE	\N	f	\N	\N	\N	0
d5ad1d7a-2ef6-11eb-8a62-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	615	t	["Warum schreiben Sie nicht beitseitig, es ist besser für die Umwelt :)\\n"]	0	f	f	\N	2022-10-24 14:06:11.198542	f	[]	[{"dep":["nk"],"text":"Umwelt"}]	-19	2	21	DE	\N	f	\N	\N	\N	0
59541b5e-39f3-11eb-bb0b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	616	t	["Das war ein Bisschen schnell, können sie bitte nochmal das vorige Blatt zeigen ?\\n"]	0	f	f	\N	2022-10-24 14:06:11.206822	f	[]	[]	-1	0	1	AUTO	\N	f	\N	\N	\N	0
03357b5c-2ef7-11eb-ac45-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	617	t	["Doppelspoiler: man nennt es Prinzip von Lavoisier ;)\\n"]	0	f	f	\N	2022-10-24 14:06:11.21459	f	[]	[{"dep":["oa"],"text":"Prinzip"},{"dep":["nk"],"text":"Lavoisier"}]	-36	1	37	DE	\N	f	\N	\N	\N	0
cb7bc8b2-3868-11eb-97a5-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	618	t	["Ciao Chef \\n"]	0	f	f	\N	2022-10-24 14:06:11.22284	f	[]	[]	0	1	1	AUTO	\N	f	\N	\N	\N	0
a557e446-3ddf-11eb-ab04-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	619	t	["Wann wird die Stoffchemie durchgenommen?\\n"]	0	f	f	\N	2022-10-24 14:06:11.230743	f	[]	[]	-4	0	4	DE	\N	f	\N	\N	\N	0
7f0a5300-3de4-11eb-8f4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	620	t	["blatt 7 bitte nochmal kurz zeigen\\n"]	0	f	f	\N	2022-10-24 14:06:11.239052	f	[]	[{"dep":["ROOT"],"text":"blatt"}]	-3	0	3	DE	\N	f	\N	\N	\N	0
ac5da4e2-39f7-11eb-af88-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	621	t	["hängt bei noch jemandem der live stream?\\n"]	0	f	f	\N	2022-10-24 14:06:11.246942	f	[]	[]	-1	0	1	AUTO	\N	f	\N	\N	\N	0
df1e2606-39f0-11eb-af88-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	622	t	["Silbernitrat wird für den Halogennachweis verwendet.\\n Es fällt das schwer lösliche Silberchlorid aus.\\n"]	0	f	f	\N	2022-10-24 14:06:11.256635	f	[]	[{"dep":["sb"],"text":"Silbernitrat"},{"dep":["sb"],"text":"Silberchlorid"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
3367c640-39f6-11eb-a94d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	623	t	["Nächstes Semester könnten Sie zur Veranschaulichung Melonen zur Vorlesung mitbringen, Honigmelonen, Wassermelonen etc. XD\\n\\n"]	0	f	f	\N	2022-10-24 14:06:11.265846	f	[]	[{"dep":["sb"],"text":"Semester"},{"dep":["nk"],"text":"Vorlesung"},{"dep":["cj"],"text":"Honigmelonen"}]	-2	0	2	DE	\N	f	\N	\N	\N	0
e247bc0e-3de0-11eb-acc5-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	624	t	["Was ist ein Radikal ?\\n"]	0	f	f	\N	2022-10-24 14:06:11.273508	f	[]	[{"dep":["pd"],"text":"Radikal"}]	-4	0	4	DE	\N	f	\N	\N	\N	0
9f28e6a6-3de4-11eb-8f4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	625	t	["und das mit silber nochmal erkären\\n\\n"]	0	f	f	\N	2022-10-24 14:06:11.281306	f	[]	[]	0	1	1	AUTO	\N	f	\N	\N	\N	0
9b356752-3ddc-11eb-8f4b-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	626	t	["Der stream ist abgebrochen\\n"]	0	f	f	\N	2022-10-24 14:06:11.289794	f	[]	[]	2	3	1	AUTO	\N	f	\N	\N	\N	0
080422f0-3dde-11eb-9b23-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	627	t	["Sie sind mind. 20sek hinterher beim Screen \\n"]	0	f	f	\N	2022-10-24 14:06:11.297759	f	[]	[{"dep":["nk"],"text":"Screen"}]	-5	0	5	DE	\N	f	\N	\N	\N	0
7bed3b1e-39f8-11eb-a94d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	628	t	["Wieso verwendet man die Summenformel P5O10 statt PO5? Die verbindung kam soweit ich weiß in der Übung vor. eigentlich sucht man ja das kleinste gemeinsame vielfache...\\n"]	0	f	f	\N	2022-10-24 14:06:11.306809	f	[]	[{"dep":["sb"],"text":"verbindung"},{"dep":["nk"],"text":"Übung"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
c27dc55a-3de8-11eb-b97a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	629	t	["klopf klopf\\n\\n"]	0	f	f	\N	2022-10-24 14:06:11.313774	f	[]	[]	2	3	1	AUTO	\N	f	\N	\N	\N	0
5f07aec4-5af7-11eb-90b8-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	630	t	["Die frag.jetzt seite hängt leider en bisschen sehr\\n"]	0	f	f	\N	2022-10-24 14:06:11.321197	f	[]	[]	-1	0	1	AUTO	\N	f	\N	\N	\N	0
d0f5e0fe-3de8-11eb-b97a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	631	t	["Vielen Dank und adios \\n"]	0	f	f	\N	2022-10-24 14:06:11.328996	f	[]	[]	0	1	1	AUTO	\N	f	\N	\N	\N	0
1d295e66-5af3-11eb-a1c0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	632	t	["Die Keile symbolisieren also die hybridisierung? \\n"]	0	f	f	\N	2022-10-24 14:06:11.336761	f	[]	[{"dep":["oa"],"text":"hybridisierung"}]	-2	0	2	DE	\N	f	\N	\N	\N	0
0bfb7d32-eb2e-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	233	t	["p-value\\n"]	0	f	f	\N	2022-10-24 14:06:14.095741	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-13 16:29:39.657	0
d26df9c8-5961-11eb-aa3a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	633	t	["Glaube ihr Ton ist zum Bild versetzt \\n"]	0	f	f	\N	2022-10-24 14:06:11.344752	f	[]	[{"dep":["ROOT"],"text":"Glaube"},{"dep":["sb"],"text":"Ton"},{"dep":["nk"],"text":"Bild"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
5a8f6286-5af9-11eb-a058-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	634	t	["Die Verzweiflung wird von Vorlesung zu Vorlesung größer\\n"]	0	f	f	\N	2022-10-24 14:06:11.352753	f	[]	[{"dep":["sb"],"text":"Verzweiflung"},{"dep":["nk"],"text":"Vorlesung"}]	2	3	1	DE	\N	f	\N	\N	\N	0
5ae923de-2eee-11eb-9ede-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	635	t	["Guten Morgen Maestro Buchsbaum\\n\\n"]	0	f	f	\N	2022-10-24 14:06:11.359922	f	[]	[{"dep":["ROOT"],"text":"Guten Morgen"}]	18	20	2	DE	\N	f	\N	\N	\N	0
e6100f9c-595d-11eb-830d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	636	t	["Guten Morgen, es wäre super, wenn Sie nach dem Start des Streams nochmal 30 Sekunden warten könnten, weil es des öfteren noch kurz lädt. So würde mann dann sicher auch ihre ersten Sätze mitbekommen  :) \\n"]	0	f	f	\N	2022-10-24 14:06:11.369931	f	[]	[{"dep":["ROOT"],"text":"Guten Morgen"},{"dep":["nk"],"text":"Start"},{"dep":["ag"],"text":"Streams"},{"dep":["oa"],"text":"Sekunde"},{"dep":["sb"],"text":"mann"},{"dep":["oa"],"text":"Satz"}]	0	1	1	DE	\N	f	\N	\N	\N	0
78f5eaec-595f-11eb-830d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	637	t	["In dem einen Kohlenstoffring sind drei doppelbindungen und in den anderen nur zwei. Soll das so sein ? \\n"]	0	f	f	\N	2022-10-24 14:06:11.377525	f	[]	[]	-1	0	1	DE	\N	f	\N	\N	\N	0
11d1b83a-3f71-11eb-a2fc-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	638	t	["Wir sind da, keine Sorge :)\\n"]	0	f	f	\N	2022-10-24 14:06:11.385374	f	[]	[{"dep":["cj"],"text":"Sorge"}]	1	2	1	DE	\N	f	\N	\N	\N	0
3c695644-3f75-11eb-b250-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	639	t	["bei pH wirds immer logaritmisiert\\n"]	0	f	f	\N	2022-10-24 14:06:11.393279	f	[]	[]	0	1	1	AUTO	\N	f	\N	\N	\N	0
24946c2e-3f71-11eb-a2fc-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	640	t	["Wie entstehen z.b. bei einer HCl-Lösung die OH- - Ionen? Also wenn man dabei den pOH-Wert bestimmen soll..\\n"]	0	f	f	\N	2022-10-24 14:06:11.401049	f	[]	[]	0	1	1	AUTO	\N	f	\N	\N	\N	0
bd7fafca-5966-11eb-a1c0-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	641	t	["Ich kann sie sehen und auch wieder hören.\\n"]	0	f	f	\N	2022-10-24 14:06:11.408679	f	[]	[]	-1	0	1	AUTO	\N	f	\N	\N	\N	0
3a80ad12-557a-11eb-a341-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	642	t	["habe ich richtig verstanden, dass die elektrische LEitfähigkeit mit höherem Durchmesser eines Darhtes zunimmt? Wenn ja, warum?\\n"]	0	f	f	\N	2022-10-24 14:06:11.416969	f	[]	[{"dep":["sb"],"text":"LEitfähigkeit"},{"dep":["nk"],"text":"Durchmesser"}]	0	1	1	DE	\N	f	\N	\N	\N	0
33abce80-5571-11eb-9bad-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	643	t	["Sie rauschen wieder \\n"]	0	f	f	\N	2022-10-24 14:06:11.42445	f	[]	[]	-3	0	3	AUTO	\N	f	\N	\N	\N	0
38be0186-5571-11eb-bd00-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	644	t	["Oh Oh, das Rauschen is wieder da\\n"]	0	f	f	\N	2022-10-24 14:06:11.431665	f	[]	[]	-4	0	4	AUTO	\N	f	\N	\N	\N	0
bd7c120a-5af8-11eb-a058-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	645	t	["mist meine oxidationszahlen :D\\n"]	0	f	f	\N	2022-10-24 14:06:11.439053	f	[]	[]	-2	0	2	AUTO	\N	f	\N	\N	\N	0
59f6984a-5af8-11eb-8593-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	646	t	["Die Frage kommt leider ein bisschen spät, weil die seite hing, aber nochmal zum quarz, wenn man mechanischen druck auf den quarz ausübt, wird er dann auch sichtbar an den druckstellen \\"kleiner\\"? aslo würde man das sehen?\\n"]	0	f	f	\N	2022-10-24 14:06:11.44907	f	[]	[{"dep":["sb"],"text":"Frage"},{"dep":["sb"],"text":"bisschen"},{"dep":["uc"],"text":"seite"},{"dep":["nk"],"text":"quarz"},{"dep":["oa"],"text":"druck"},{"dep":["pd"],"text":"klein"},{"dep":["mo"],"text":"aslo"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
077ea3f2-5afb-11eb-a058-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	647	t	["werden daraus nicht brustvergrößerungen gemacht?\\n"]	0	f	f	\N	2022-10-24 14:06:11.456616	f	[]	[]	1	3	2	AUTO	\N	f	\N	\N	\N	0
934cef22-5af8-11eb-9300-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	648	t	["könnten sie bitte die ordnungszahlen nochmal nennen bei beiden stoffen \\n"]	0	f	f	\N	2022-10-24 14:06:11.464128	f	[]	[]	-2	0	2	AUTO	\N	f	\N	\N	\N	0
9cc15024-5ee1-11eb-a331-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	649	t	["Silikonfreies Shampoo ist besser für die haare, da silikonhaltige Shampoos eine Schicht über die haare legen (diese zwar damit sehr schön weich machen) aber die haare darunter kaputt gehen\\n"]	0	f	f	\N	2022-10-24 14:06:11.473018	f	[]	[{"dep":["nk","cj"],"text":"haare"},{"dep":["oa"],"text":"Schicht"}]	-1	2	3	DE	\N	f	\N	\N	\N	0
0f020b86-5ee4-11eb-911e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	650	t	["kommt es mir nur so vor oder fühlen sich volle akkus schwerer an als leere akkus?\\n"]	0	f	f	\N	2022-10-24 14:06:11.480945	f	[]	[{"dep":["nk"],"text":"akkus"}]	0	1	1	DE	\N	f	\N	\N	\N	0
5a8e65e8-5ee1-11eb-a89d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	651	t	["warum sind die meisten Shampoos etc. silikonfrei wenn es doch eigentlich physiologisch unbedenklich ist?\\n"]	0	f	f	\N	2022-10-24 14:06:11.488824	f	[]	[]	0	1	1	DE	\N	f	\N	\N	\N	0
7b82addc-5ee5-11eb-ba5a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	652	t	["Was wäre denn ein Beispiel für \\"elementares Vorkommen\\" ?\\n"]	0	f	f	\N	2022-10-24 14:06:11.496761	f	[]	[{"dep":["pd"],"text":"Beispiel"},{"dep":["nk"],"text":"Vorkommen"}]	0	1	1	DE	\N	f	\N	\N	\N	0
a489a58c-2d5d-11eb-9e65-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	653	t	["ich glaube dieser QR Code funktioniert nicht, ich habe einfach den Raumname AAC NaWi 2020 eingegeben und ich glaube ich bin die einzige Person hier. vielleicht können sie das nochmal für alle sagen \\n"]	0	f	f	\N	2022-10-24 14:06:11.506213	f	[]	[{"dep":["da"],"text":"QR Code"},{"dep":["oa"],"text":"Raumname"},{"dep":["pnc"],"text":"AAC"},{"dep":["nk"],"text":"NaWi"},{"dep":["pd"],"text":"Person"}]	33	37	4	DE	\N	f	\N	\N	\N	0
76b17b56-5ee2-11eb-ada1-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	654	t	["Oder Blei im Trinkwasser sehr alter Gebäude\\n"]	0	f	f	\N	2022-10-24 14:06:11.514141	f	[]	[{"dep":["nk"],"text":"Blei"},{"dep":["nk"],"text":"Trinkwasser"},{"dep":["ROOT"],"text":"Gebäude"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
d839da2c-5ee1-11eb-a331-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	655	t	["Es gibt ein sehr schönes Video über Silikone in Shampoos auf dem Kanal von maiLab auf Youtube\\n\\n"]	0	f	f	\N	2022-10-24 14:06:11.525405	f	[]	[{"dep":["oa"],"text":"Video"},{"dep":["nk"],"text":"Silikon"},{"dep":["nk"],"text":"Kanal"},{"dep":["nk"],"text":"maiLab"},{"dep":["nk"],"text":"Youtube"}]	1	2	1	DE	\N	f	\N	\N	\N	0
199c1cde-5ee5-11eb-9862-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	656	t	["Weil es ein Feststoff ist\\n\\n"]	0	f	f	\N	2022-10-24 14:06:11.532682	f	[]	[{"dep":["pd"],"text":"Feststoff"}]	-2	0	2	DE	\N	f	\N	\N	\N	0
fddb3e1a-5ee6-11eb-911e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	657	t	["Ich wäre dafür etwas in das flüssige Stickstoff reinzuwerfen\\n"]	0	f	f	\N	2022-10-24 14:06:11.540619	f	[]	[{"dep":["nk"],"text":"Stickstoff"}]	3	4	1	DE	\N	f	\N	\N	\N	0
34cecda6-5ee7-11eb-9862-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	658	t	["Ist flüssiger Stickstoff auch beim Sushi in den Bechern für einen coolen Effekt ? Schadet uns das nicht ? Oder unserem Essen \\n"]	0	f	f	\N	2022-10-24 14:06:11.549641	f	[]	[{"dep":["sb"],"text":"Stickstoff"},{"dep":["nk"],"text":"Sushi"},{"dep":["nk"],"text":"Becher"},{"dep":["nk"],"text":"Effekt"},{"dep":["ROOT"],"text":"Essen"}]	0	1	1	DE	\N	f	\N	\N	\N	0
c58fad5e-5ee4-11eb-9141-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	659	t	["Für was steht nochmal das Z in der Nernstschen Gleichung?\\n"]	0	f	f	\N	2022-10-24 14:06:11.557515	f	[]	[{"dep":["nk"],"text":"Gleichung"}]	1	2	1	DE	\N	f	\N	\N	\N	0
55f5f7ae-5ee5-11eb-b352-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	660	t	["Kriegen wir auch mal weißen phosphor im brennenden Zustand zu sehen?:)\\n"]	0	f	f	\N	2022-10-24 14:06:11.565395	f	[]	[{"dep":["oa"],"text":"phosphor"},{"dep":["nk"],"text":"Zustand"}]	0	1	1	DE	\N	f	\N	\N	\N	0
f7e56b9a-5ee4-11eb-9567-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	661	t	["Warum ist nochmal die Konzentration von PbSO4 konstant? Mein Internet ist angeschmiert \\n"]	0	f	f	\N	2022-10-24 14:06:11.573945	f	[]	[{"dep":["mo"],"text":"Warum"},{"dep":["sb"],"text":"Konzentration"},{"dep":["nk"],"text":"PbSO4"},{"dep":["sb"],"text":"Internet"}]	0	2	2	DE	\N	f	\N	\N	\N	0
46b707f4-5ee7-11eb-ba5a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	662	t	["sind übrigens fast -200 grad celcius falls es jmd interessiert\\n"]	0	f	f	\N	2022-10-24 14:06:11.581841	f	[]	[]	0	1	1	AUTO	\N	f	\N	\N	\N	0
b69b1a04-5ee4-11eb-ba5a-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	663	t	["sie haben einmal ln vergessen\\n"]	0	f	f	\N	2022-10-24 14:06:11.599502	f	[]	[]	-1	0	1	AUTO	\N	f	\N	\N	\N	0
d1844888-5ee7-11eb-9141-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	664	t	["Gibt es überhaupt ein Skript für die Stoffchemie ? \\n"]	0	f	f	\N	2022-10-24 14:06:11.613641	f	[]	[{"dep":["oa"],"text":"Skript"}]	-4	1	5	DE	\N	f	\N	\N	\N	0
7ce01214-5ee5-11eb-baab-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	665	t	["wer auch immer alle fragen nen daumen runter gibt, ist ein hs\\n"]	0	f	f	\N	2022-10-24 14:06:11.623116	f	[]	[]	-6	4	10	AUTO	\N	f	\N	\N	\N	0
b7a58878-5ee7-11eb-a89d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	666	t	["Wo können wir denn die Tabellen zu den Gruppen finden? Ich habe die im Skript nicht gesehen \\n"]	0	f	f	\N	2022-10-24 14:06:11.631748	f	[]	[{"dep":["oa"],"text":"Tabelle"},{"dep":["nk"],"text":"Gruppe"},{"dep":["nk"],"text":"Skript"}]	-1	0	1	DE	\N	f	\N	\N	\N	0
232739a2-32e7-11eb-803e-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	667	t	["Herr Buchsbaum lernt Jugendsprache, allgemeine Verzweiflung über Kommentare und dazu ein paar Flachwitze. Die Stunde ist der Hammer\\n"]	0	f	f	\N	2022-10-24 14:06:11.640864	f	[]	[{"dep":["oa"],"text":"Jugendsprache"},{"dep":["oa"],"text":"Verzweiflung"},{"dep":["nk"],"text":"Kommentar"},{"dep":["sb"],"text":"Stunde"},{"dep":["pd"],"text":"Hammer"}]	21	23	2	DE	\N	f	\N	\N	\N	0
8f3117b4-2d68-11eb-bc8d-0242ac140003	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	668	t	["ok, vielen Dank\\n\\n"]	0	f	f	\N	2022-10-24 14:06:11.648516	f	[]	[]	0	0	0	AUTO	\N	f	\N	\N	\N	0
e5e22472-a959-11ec-87e6-0242ac120007	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	53/1	t	["Tut er! Hat er aber auch gesagt.\\nGrüße\\nChristian Buchsbaum\\n"]	0	f	f	\N	2022-10-24 14:06:11.65675	f	[]	[]	0	0	0	AUTO	\N	f	\N	7cc53470-2d68-11eb-b524-0242ac140003	\N	1
c84b6d4c-c4b1-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	12	t	["Which version of R should we download?\\n"]	1	f	f	\N	2022-10-24 14:06:11.673351	f	[]	[{"dep":["dobj"],"text":"version"}]	0	0	0	AUTO		f	\N	\N	\N	0
542b2268-c4ac-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	13	t	["Where do we have exercise? (I mean the room of exercise \\n"]	1	f	f	\N	2022-10-24 14:06:11.684635	f	[]	[{"dep":["pobj","dobj"],"text":"exercise"},{"dep":["dobj"],"text":"room"}]	0	0	0	AUTO		f	\N	\N	2022-04-28 20:03:39.708	0
5b8b4206-c491-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	14	t	["Will the course be recorded and update to moodle or lecturio?\\n\\n"]	1	f	f	\N	2022-10-24 14:06:11.690588	f	[]	[{"dep":["nsubjpass"],"text":"course"},{"dep":["pobj"],"text":"moodle"}]	1	1	0	AUTO		f	\N	\N	\N	0
bdf457e6-c4b1-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	15	t	["what is the enviroment?\\n"]	1	f	f	\N	2022-10-24 14:06:11.700709	f	[]	[{"dep":["nsubj"],"text":"environment"}]	0	0	0	AUTO		f	\N	\N	\N	0
f7b65fde-c4a1-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	14/1	t	["no\\n"]	0	f	f	\N	2022-10-24 14:06:11.706366	f	[]	[]	0	0	0	AUTO		f	\N	5b8b4206-c491-11ec-9e13-0242ac190004	2022-04-25 14:14:18.009	1
1be3c5ee-c52a-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	16	t	["Where exactly can we sign up for our preferred tutorial?\\n"]	0	f	f	\N	2022-10-24 14:06:11.71242	f	[]	[{"dep":["pobj"],"text":"tutorial"}]	0	0	0	AUTO		f	\N	\N	\N	0
ef3b1638-c4a1-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	14/2	t	["no\\n"]	0	f	f	\N	2022-10-24 14:06:11.71792	f	[]	[]	0	0	0	AUTO		f	\N	5b8b4206-c491-11ec-9e13-0242ac190004	\N	1
0da867b0-c4ac-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	17	t	["What’s the average grade in this course approximately \\n"]	0	f	f	\N	2022-10-24 14:06:11.724322	f	[]	[{"dep":["npadvmod"],"text":"grade"},{"dep":["pobj"],"text":"course"}]	0	0	0	AUTO		f	\N	\N	2022-04-25 20:12:14.134	0
3f1f66a4-c4ac-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	18	t	["But isn’t there an average grade every year.\\n"]	0	f	f	\N	2022-10-24 14:06:11.733129	f	[]	[{"dep":["attr"],"text":"grade"},{"dep":["npadvmod"],"text":"every year"}]	0	0	0	AUTO		f	\N	\N	2022-04-25 20:12:19.361	0
eee39628-c4ac-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	19	t	["Is R somewhat similar to matlab? \\n"]	1	f	f	\N	2022-10-24 14:06:11.739296	f	[]	[{"dep":["pobj"],"text":"matlab"}]	0	0	0	AUTO		f	\N	\N	2022-04-28 20:03:45.851	0
ed1c8fe4-c4ab-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	20	t	["Is the content comparable / the same as in the last summer semester when it was online? \\n\\n"]	0	f	f	\N	2022-10-24 14:06:11.751312	f	[]	[{"dep":["nsubj"],"text":"content"},{"dep":["det"],"text":"the last summer"},{"dep":["pobj"],"text":"semester"}]	1	1	0	AUTO		f	\N	\N	2022-04-25 20:12:46.122	0
49326990-c4aa-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	21	t	["can U upload any online lectures from past years\\n"]	0	f	f	\N	2022-10-24 14:06:11.757612	f	[]	[{"dep":["dobj"],"text":"lecture"},{"dep":["pobj"],"text":"past years"}]	5	5	0	AUTO		f	\N	\N	2022-05-03 14:09:17.394	0
91cb91b6-c4b1-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	22	t	["where did the data for the pressure plot come from?\\n"]	1	f	f	\N	2022-10-24 14:06:11.763508	f	[]	[{"dep":["nsubj"],"text":"datum"},{"dep":["pobj"],"text":"pressure plot"}]	0	0	0	AUTO		f	\N	\N	2022-05-03 14:09:03.432	0
0c39a12c-c4b2-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	23	t	["are the same contents in the morning and evening class?\\n"]	1	f	f	\N	2022-10-24 14:06:11.769684	f	[]	[{"dep":["attr"],"text":"content"},{"dep":["det"],"text":"the morning"},{"dep":["conj"],"text":"evening class"}]	0	0	0	AUTO		f	\N	\N	\N	0
a8a83872-c4ab-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	24	t	["Online exercises are in high demand!\\n"]	0	f	f	\N	2022-10-24 14:06:11.775888	f	[]	[{"dep":["nsubj"],"text":"exercise"},{"dep":["pobj"],"text":"demand"}]	7	7	0	AUTO		f	\N	\N	\N	0
d1d099ce-c4ab-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	25	t	["Is R relevant for the exam?\\n"]	1	f	f	\N	2022-10-24 14:06:11.781732	f	[]	[{"dep":["pobj"],"text":"exam"}]	1	1	0	AUTO		f	\N	\N	\N	0
141f77dc-c4ac-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	17/1	t	["okay! :)\\n"]	0	f	f	\N	2022-10-24 14:06:11.789998	f	[]	[]	0	0	0	AUTO		f	\N	0da867b0-c4ac-11ec-84b5-0242ac190004	2022-04-25 15:26:44.411	1
798c69a6-c491-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	26	t	["Can you recommend to use Pycharm for R development?\\n"]	1	f	f	\N	2022-10-24 14:06:11.796468	f	[]	[{"dep":["dobj"],"text":"Pycharm"},{"dep":["pobj"],"text":"r development"}]	0	0	0	AUTO		f	\N	\N	2022-04-28 20:02:58.422	0
cc7d59da-c4ab-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	27	t	["For those working from home: When supposed to discuss ideas in the lecture, will we get breakout rooms in zoom?\\n"]	1	f	f	\N	2022-10-24 14:06:11.803165	f	[]	[{"dep":["pobj"],"text":"home"},{"dep":["dobj"],"text":"idea"},{"dep":["pobj"],"text":"lecture"},{"dep":["pobj"],"text":"zoom"}]	0	0	0	AUTO		f	\N	\N	2022-04-28 20:02:30.256	0
f2a16fc0-c4ab-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	28	t	["Can Frag.Jetzt also manage follow-up questions?\\n"]	1	f	f	\N	2022-10-24 14:06:11.809235	f	[]	[{"dep":["nsubj"],"text":"Jetzt"},{"dep":["dobj"],"text":"question"}]	0	0	0	AUTO		f	\N	\N	2022-04-28 20:03:28.599	0
d8148470-c4b1-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	12/1	t	["Newest one that’s not a beta\\n"]	1	f	f	\N	2022-10-24 14:06:11.826422	f	[]	[{"dep":["ROOT"],"text":"one"},{"dep":["attr"],"text":"beta"}]	0	0	0	AUTO		f	\N	c84b6d4c-c4b1-11ec-b02a-0242ac190004	\N	1
10ba72e4-c7b4-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	53/1	t	["Apparently it’s too much of an effort for them…\\n"]	0	f	f	\N	2022-10-24 14:06:12.151658	f	[]	[{"dep":["pobj"],"text":"effort"}]	0	0	0	AUTO		f	\N	dd46d670-c7b1-11ec-a5cb-0242ac190004	\N	1
87e11690-c4a1-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	26/1	t	["I can't personally recommend it, due to lack of experience. But there are of course options to use it in combination with R \\nhttps://www.jetbrains.com/help/pycharm/r-plugin-support.html\\n\\n"]	0	f	f	\N	2022-10-24 14:06:11.838885	f	[]	[{"dep":["pobj"],"text":"lack"},{"dep":["pobj"],"text":"experience"},{"dep":["pobj"],"text":"combination"},{"dep":["conj"],"text":"https://www.jetbrains.com/help/pycharm/r-plugin-support.html"}]	0	0	0	AUTO		f	\N	798c69a6-c491-11ec-84b5-0242ac190004	2022-04-25 14:11:59.484	1
a6301890-c4aa-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	29	t	["Will be the lecture the whole semester streamed or only in the beginning?\\n"]	1	f	f	\N	2022-10-24 14:06:11.846232	f	[]	[{"dep":["attr"],"text":"lecture"},{"dep":["nsubj"],"text":"semester"},{"dep":["pobj"],"text":"beginning"}]	2	2	0	AUTO		f	\N	\N	2022-04-28 20:02:51.769	0
c0f3e1ec-c4ab-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	30	t	["Are exercisesessions on Monday and Friday the same\\n"]	1	f	f	\N	2022-10-24 14:06:11.852288	f	[]	[{"dep":["pobj"],"text":"Monday"},{"dep":["conj"],"text":"Friday"}]	0	0	0	AUTO		f	\N	\N	\N	0
5cc76684-c4ac-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	31	t	["How many % usually pass the exam?\\n"]	0	f	f	\N	2022-10-24 14:06:11.858065	f	[]	[{"dep":["dobj"],"text":"exam"}]	0	1	1	AUTO		f	\N	\N	2022-04-25 20:05:39.666	0
d08124f6-c4ad-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	32	t	["Just FYI: the audio quality is really terrible\\n"]	1	f	f	\N	2022-10-24 14:06:11.864043	f	[]	[{"dep":["ROOT"],"text":"fyi"},{"dep":["nsubj"],"text":"quality"}]	2	2	0	AUTO		f	\N	\N	\N	0
b0b23e4a-c4b1-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	33	t	["Yes. It’s really hard to understand the audio\\n"]	0	f	f	\N	2022-10-24 14:06:11.870448	f	[]	[{"dep":["dobj"],"text":"audio"}]	1	1	0	AUTO		f	\N	\N	2022-04-25 19:59:21.432	0
4b108d68-c4d3-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	19/1	t	["I would guess, that if you are familiar with matlab, you should have no problem learning R. At least that was true for me\\n"]	0	f	f	\N	2022-10-24 14:06:11.877251	f	[]	[{"dep":["pobj"],"text":"matlab"},{"dep":["dobj"],"text":"problem"}]	0	0	0	AUTO		f	\N	eee39628-c4ac-11ec-84b5-0242ac190004	\N	1
9bac8948-c4d3-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	13/1	t	["the rooms are 0220, N1070, N1090 (maybe also N1095, if needed)\\n"]	0	f	f	\N	2022-10-24 14:06:11.883619	f	[]	[{"dep":["nsubj"],"text":"room"}]	0	0	0	AUTO		f	\N	542b2268-c4ac-11ec-a5cb-0242ac190004	\N	1
b7afa77e-c4d3-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	13/2	t	["0220, N1070, N1090 (maybe also N1095)\\n"]	0	f	f	\N	2022-10-24 14:06:11.889709	f	[]	[]	0	0	0	AUTO		f	\N	542b2268-c4ac-11ec-a5cb-0242ac190004	2022-04-25 20:10:21.558	1
47f10bc0-c4ac-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	34	t	["Is the content covered this semester comparable to / the same as in the summer semester 2021 (when it was online?) \\n"]	1	f	f	\N	2022-10-24 14:06:11.896426	f	[]	[{"dep":["nsubj"],"text":"content"},{"dep":["dobj"],"text":"semester"},{"dep":["pobj"],"text":"the summer semester 2021"}]	0	0	0	AUTO		f	\N	\N	\N	0
3e056174-c4d4-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	28/1	t	["you can write a reply to another reply. just try to reply to this answer\\n"]	0	f	f	\N	2022-10-24 14:06:11.90281	f	[]	[{"dep":["pobj","dobj"],"text":"reply"},{"dep":["pobj"],"text":"answer"}]	0	0	0	AUTO		f	\N	f2a16fc0-c4ab-11ec-90ff-0242ac190004	\N	1
48a790e8-c4d4-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	25/1	t	["yes\\n"]	0	f	f	\N	2022-10-24 14:06:11.908453	f	[]	[]	0	0	0	AUTO		f	\N	d1d099ce-c4ab-11ec-a6d5-0242ac190004	\N	1
e247cf06-c4d4-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	27/1	t	["this semester takes place on campus and is just supplemented by digital formats. therefore, we will not offer the opportunity to discuss during lecture time something in breakout rooms. you will just be able to ask questions through frag.jetzt. additionally we can also try how it works, if you ask questions live on zoom. \\n"]	0	f	f	\N	2022-10-24 14:06:11.917905	f	[]	[{"dep":["nsubj"],"text":"semester"},{"dep":["dobj"],"text":"place"},{"dep":["pobj"],"text":"campus"},{"dep":["pobj"],"text":"digital formats"},{"dep":["dobj"],"text":"opportunity"},{"dep":["dobj"],"text":"question"},{"dep":["pobj"],"text":"zoom"}]	0	0	0	AUTO		f	\N	cc7d59da-c4ab-11ec-90ff-0242ac190004	\N	1
1dec3286-c4d5-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	29/1	t	["if it works out fine for both sides, we might stream the lecture throughout the semester. but we can't guarantee it\\n"]	0	f	f	\N	2022-10-24 14:06:11.924521	f	[]	[{"dep":["pobj"],"text":"side"},{"dep":["dobj"],"text":"lecture"},{"dep":["pobj"],"text":"semester"}]	0	0	0	AUTO		f	\N	a6301890-c4aa-11ec-a5fc-0242ac190004	\N	1
f3832ed6-c4d5-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	26/2	t	["I haven't tried it, so I can't say. but it should work \\n",{"attributes":{"link":"https://www.jetbrains.com/help/pycharm/r-plugin-support.html"},"insert":"https://www.jetbrains.com/help/pycharm/r-plugin-support.html"},"\\n\\n"]	0	f	f	\N	2022-10-24 14:06:11.931403	f	[]	[{"dep":["ROOT"],"text":"https://www.jetbrains.com/help/pycharm/r-plugin-support.html"}]	0	0	0	AUTO		f	\N	798c69a6-c491-11ec-84b5-0242ac190004	\N	1
9f54f67e-ca0b-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	35	t	["What is the difference between Blocking and Clustering?\\n"]	0	f	f	\N	2022-10-24 14:06:11.936991	f	[]	[{"dep":["nsubj"],"text":"difference"},{"dep":["pobj"],"text":"Blocking and Clustering"}]	0	0	0	AUTO		f	\N	\N	\N	0
9f97c920-c4d1-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	23/1	t	["both courses cover the same topics, but they will not be exactly synchronised. so within one day, there might be small differences \\n"]	0	f	f	\N	2022-10-24 14:06:11.944459	f	[]	[{"dep":["nsubj"],"text":"course"},{"dep":["dobj"],"text":"topic"},{"dep":["pobj"],"text":"one day"},{"dep":["attr"],"text":"difference"}]	0	0	0	AUTO		f	\N	0c39a12c-c4b2-11ec-a6d5-0242ac190004	\N	1
cf35855a-c48b-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	36	t	["Are the exercise classes online?\\n"]	0	f	f	\N	2022-10-24 14:06:11.950593	f	[]	[]	7	7	0	AUTO	Peter	f	\N	\N	2022-05-03 14:09:43.994	0
c059ae94-c4d1-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	12/2	t	["you download the latest version, so 4.2. but a slightly older version 4.x is also fine\\n"]	0	f	f	\N	2022-10-24 14:06:11.956944	f	[]	[{"dep":["nsubj","dobj"],"text":"version"},{"dep":["nsubj"],"text":"4.x"}]	0	0	0	AUTO		f	\N	c84b6d4c-c4b1-11ec-b02a-0242ac190004	\N	1
15c75de0-c4d2-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	15/1	t	["the environment contains all objects, which you create within your R session. I e.g. generated the object \\nx <- 1:10\\nwhich is a vector of length 10, containing the numbers 1 to 10. This object was then part of the environment of my R session\\n"]	0	f	f	\N	2022-10-24 14:06:11.964966	f	[]	[{"dep":["pobj","nsubj"],"text":"environment"},{"dep":["nsubj","dobj"],"text":"object"},{"dep":["pobj"],"text":"r session"},{"dep":["attr"],"text":"vector"},{"dep":["pobj"],"text":"length"},{"dep":["dobj"],"text":"number"},{"dep":["attr"],"text":"part"}]	0	0	0	AUTO		f	\N	bdf457e6-c4b1-11ec-a5cb-0242ac190004	\N	1
043f3b64-c4d3-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	32/1	t	["we have to check this next monday. fridays we have to use a different setup in room 2300. unfortunately i don't expect it to work better the one from today ",{"emoji":"pensive"},"\\n"]	0	f	f	\N	2022-10-24 14:06:11.980067	f	[]	[{"dep":["dobj"],"text":"this next monday"},{"dep":["ROOT"],"text":"fridays"},{"dep":["dobj"],"text":"setup"},{"dep":["pobj"],"text":"room"},{"dep":["pobj"],"text":"one"},{"dep":["pobj"],"text":"today"}]	0	0	0	AUTO		f	\N	d08124f6-c4ad-11ec-a5fc-0242ac190004	\N	1
acd7668a-c7b1-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	55	t	["NO STREAM\\n"]	0	f	f	\N	2022-10-24 14:06:12.157389	f	[]	[{"dep":["ROOT"],"text":"stream"}]	14	14	0	AUTO		f	\N	\N	2022-05-03 07:18:50.088	0
9731d54a-c7b1-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	56	t	["Please record the lectures and upload it!!!\\n"]	0	f	f	\N	2022-10-24 14:06:12.163819	f	[]	[{"dep":["dobj"],"text":"lecture"}]	23	23	0	AUTO		f	\N	\N	2022-05-03 07:17:56.429	0
98404220-c539-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	37	t	[{"attributes":{"bold":true},"insert":"The link for tutorials doesnt work:"},"\\nBitte entschuldigen Sie aber ...\\n\\ndie dynamische HTML - Seitenerstellung konnte aufgrund folgenden Fehlers :\\nLV Ansicht\\n3 Anzeige\\nORA-20630: Sie haben keine Berechtigung zur Änderung von Beschreibungsdaten der LV \\"0000003774 Exercises for Statistics for Business Administration [MA9712]\\" (nr=950604054).\\n\\nnicht durchgeführt werden.\\nFehler-Referenznummer: 1277173666\\n\\n"]	0	f	f	Technical problem	2022-10-24 14:06:11.988239	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
385ad2fc-c567-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	38	t	["Will the currently uploaded exercises \\"starwars.rmd\\" and \\"hotels.rmd\\" be graded?\\n"]	0	f	f	homework	2022-10-24 14:06:11.994126	f	[]	[{"dep":["nmod"],"text":"exercise"}]	1	1	0	AUTO		f	\N	\N	\N	0
f67b6230-c556-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	37/1	t	[{"attributes":{"link":"https://campus.tum.de/tumonline/ee/ui/ca2/app/desktop/#/slc.tm.cp/student/courses/950604054?$ctx=design=ca;lang=de&$scrollTo=toc_overview"},"insert":"https://campus.tum.de/tumonline/ee/ui/ca2/app/desktop/#/slc.tm.cp/student/courses/950604054?$ctx=design=ca;lang=de&$scrollTo=toc_overview"},"\\n"]	1	f	f	\N	2022-10-24 14:06:12.003724	f	[]	[{"dep":["ROOT"],"text":"https://campus.tum.de/tumonline/ee/ui/ca2/app/desktop/#/slc.tm.cp/student/courses/950604054?$ctx=design=ca;lang=de&$scrollTo=toc_overview"}]	0	0	0	AUTO		f	\N	98404220-c539-11ec-8ca6-0242ac190004	\N	1
8249545c-c78c-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	39	t	["What is the difference between stata and cluster?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:12.010507	f	[]	[{"dep":["nsubj"],"text":"difference"},{"dep":["pobj"],"text":"stata"},{"dep":["conj"],"text":"cluster"}]	0	0	0	AUTO		f	\N	\N	\N	0
ab3101b0-c7b1-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	40	t	["Honestly, I find this quite disrespectful. Sitting at home with covid.. what are you supposed to do? \\n"]	0	f	f	\N	2022-10-24 14:06:12.017574	f	[]	[{"dep":["pobj"],"text":"home"},{"dep":["pobj"],"text":"covid"}]	21	21	0	AUTO		f	\N	\N	\N	0
0cc53e44-c557-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	16/1	t	[{"attributes":{"link":"https://campus.tum.de/tumonline/ee/ui/ca2/app/desktop/#/slc.tm.cp/student/courses/950604054?$ctx=design=ca;lang=de&$scrollTo=toc_overview"},"insert":"https://campus.tum.de/tumonline/ee/ui/ca2/app/desktop/#/slc.tm.cp/student/courses/950604054?$ctx=design=ca;lang=de&$scrollTo=toc_overview"},"\\n"]	1	f	f	\N	2022-10-24 14:06:12.026149	f	[]	[{"dep":["ROOT"],"text":"https://campus.tum.de/tumonline/ee/ui/ca2/app/desktop/#/slc.tm.cp/student/courses/950604054?$ctx=design=ca;lang=de&$scrollTo=toc_overview"}]	0	0	0	AUTO		f	\N	1be3c5ee-c52a-11ec-b02a-0242ac190004	\N	1
2b4b40dc-c707-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	38/1	t	["no. we said, that those homework problems will be specially marked. \\n\\nyou will actually work on the exercises from the two markdown files during the exercise class sessions\\n"]	0	f	f	\N	2022-10-24 14:06:12.046731	f	[]	[{"dep":["pobj"],"text":"exercise"},{"dep":["nummod"],"text":"two"},{"dep":["pobj"],"text":"file"}]	0	0	0	AUTO		f	\N	385ad2fc-c567-11ec-b02a-0242ac190004	\N	1
0b476cee-c490-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	41	t	["Could you please upload today's slides to Moodle?\\n"]	1	f	f	\N	2022-10-24 14:06:12.05367	f	[]	[{"dep":["poss"],"text":"today"},{"dep":["dobj"],"text":"slide"},{"dep":["pobj"],"text":"Moodle"}]	2	2	0	AUTO		f	\N	\N	2022-04-28 20:03:10.794	0
5e8d95d4-c7b2-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	42	t	["Could you please inform us on how you will be providing the students of the failed stream with the contents of this lecture? Recording, additional lecture, etc.? \\n"]	0	f	f	\N	2022-10-24 14:06:12.061248	f	[]	[{"dep":["dobj"],"text":"student"},{"dep":["pobj"],"text":"stream"},{"dep":["pobj"],"text":"content"},{"dep":["conj","pobj"],"text":"lecture"},{"dep":["ROOT"],"text":"recording"}]	18	18	0	AUTO		f	\N	\N	2022-05-03 10:41:07.88	0
ced94be8-c7ae-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	43	t	["Could you please make the screen more readable? It is too bright. Thank you!\\n"]	0	f	f	\N	2022-10-24 14:06:12.067244	f	[]	[{"dep":["nsubj"],"text":"screen"}]	0	0	0	AUTO		f	\N	\N	2022-05-03 07:07:14.008	0
a6459764-c4b0-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	44	t	["Is it not possible to upload the lecture on lecturio or somewhere else. It’s a lot to take in and having no way to revisit the lecture makes it very difficult to follow…\\n"]	1	f	f	\N	2022-10-24 14:06:12.074149	f	[]	[{"dep":["dobj"],"text":"lecture"},{"dep":["attr"],"text":"lot"},{"dep":["dobj"],"text":"way"}]	4	4	0	AUTO		f	\N	\N	\N	0
a158e34c-c7b1-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	45	t	["THE STREAM IS DOWN!\\n"]	0	f	f	\N	2022-10-24 14:06:12.085863	f	[]	[{"dep":["nsubj"],"text":"stream"}]	23	23	0	AUTO		f	\N	\N	2022-05-03 07:18:43.47	0
7c1571f4-c7b1-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	46	t	["Hello! Is there any chance you could stream to twitch? or TUM live?? like all the profs do?\\n"]	0	f	f	\N	2022-10-24 14:06:12.092375	f	[]	[{"dep":["attr"],"text":"chance"},{"dep":["nsubj"],"text":"profs"}]	14	14	0	AUTO		f	\N	\N	\N	0
b76f9fae-c7b1-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	47	t	["honestly it’s so disrespectful toward us sitting at home, just continuing the lecture without us and not even recording it!!! Shame on you!\\n"]	0	f	f	\N	2022-10-24 14:06:12.098907	f	[]	[{"dep":["pobj"],"text":"home"},{"dep":["dobj"],"text":"lecture"},{"dep":["ROOT"],"text":"shame"}]	24	24	0	AUTO		f	\N	\N	\N	0
7a8c024c-c7ae-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	48	t	["Would you recommend using Pycharm vor R development?\\n"]	0	f	f	\N	2022-10-24 14:06:12.104394	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-05-02 20:39:53.277	0
e246e26e-c7b1-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	49	t	["No Stream!!!\\n\\n"]	0	f	f	\N	2022-10-24 14:06:12.110181	f	[]	[{"dep":["ROOT"],"text":"Stream"}]	13	13	0	AUTO		f	\N	\N	2022-05-03 11:53:45.762	0
f6aed65c-c7b2-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	50	t	["Seriously! a lot of people took you word, that you would make a stream, for granted. Streaming at least the audio should be possible (due to lower bandwidth). If you care about your students, please don't let us down.\\n"]	0	f	f	\N	2022-10-24 14:06:12.119061	f	[]	[{"dep":["nsubj"],"text":"lot"},{"dep":["pobj"],"text":"people"},{"dep":["dobj"],"text":"word"},{"dep":["dobj"],"text":"stream"},{"dep":["nsubj"],"text":"audio"},{"dep":["pobj"],"text":"bandwidth"},{"dep":["pobj"],"text":"student"}]	16	16	0	AUTO		f	\N	\N	2022-05-03 11:55:42.735	0
7c41efa8-c7b2-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	51	t	["Please put some effort into bringing the stream back online, even if its just the audio! Here are 150 People, desperate that they have been just ditched!\\n"]	0	f	f	\N	2022-10-24 14:06:12.12661	f	[]	[{"dep":["dobj"],"text":"effort"},{"dep":["dobj"],"text":"stream"},{"dep":["advcl"],"text":"audio"},{"dep":["nsubj"],"text":"People"}]	26	26	0	AUTO		f	\N	\N	2022-05-03 11:55:34.771	0
cba4a5c8-c7b1-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	52	t	["not everyone can attend the classes maybe due to illness or family stuff.\\n\\n"]	0	f	f	\N	2022-10-24 14:06:12.133244	f	[]	[{"dep":["dobj"],"text":"class"},{"dep":["pobj"],"text":"illness"},{"dep":["conj"],"text":"family stuff"}]	18	18	0	AUTO		f	\N	\N	2022-05-03 10:47:32.397	0
dd46d670-c7b1-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	53	t	["Can we at least get a recording of the lecture?\\n"]	0	f	f	\N	2022-10-24 14:06:12.1392	f	[]	[{"dep":["dobj"],"text":"recording"},{"dep":["pobj"],"text":"lecture"}]	21	21	0	AUTO		f	\N	\N	\N	0
6ca02e9a-c78d-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	54	t	["Could you tell us the solution to the quiz on page 44?\\n"]	0	f	f	\N	2022-10-24 14:06:12.145622	f	[]	[{"dep":["dobj"],"text":"solution"},{"dep":["pobj"],"text":"quiz"},{"dep":["pobj"],"text":"page 44"}]	1	2	1	AUTO		f	\N	\N	\N	0
ad37bc88-c7b1-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	57	t	["This is extremely unfair for the people that cant join because of quarantine or other stuff\\n"]	0	f	f	\N	2022-10-24 14:06:12.179372	f	[]	[{"dep":["pobj"],"text":"people"},{"dep":["pobj"],"text":"quarantine"},{"dep":["conj"],"text":"stuff"}]	25	25	0	AUTO		f	\N	\N	2022-05-03 11:54:17.972	0
b1a140a8-c7ae-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	58	t	["What does it mean when a numerical variable makes a jump? aka making it a discrete variable?\\n"]	0	f	f	\N	2022-10-24 14:06:12.188928	f	[]	[{"dep":["ccomp","nsubj"],"text":"variable"},{"dep":["dobj"],"text":"jump"}]	0	0	0	AUTO		f	\N	\N	2022-05-02 20:40:06.886	0
2e545624-c7b1-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	59	t	["Wäre es möglich die Zoom Session zu starten ?\\n"]	0	f	f	\N	2022-10-24 14:06:12.195728	f	[]	[]	5	5	0	AUTO		f	\N	\N	2022-05-03 07:07:24.248	0
c038c5f2-c7b1-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	60	t	["STREAM RIP\\n"]	0	f	f	\N	2022-10-24 14:06:12.201415	f	[]	[]	13	13	0	AUTO		f	\N	\N	2022-05-03 10:47:23.095	0
b2b056be-ca29-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	61	t	["Are exercise sessions on Monday and Friday the same?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:12.207615	f	[]	[{"dep":["pobj"],"text":"Monday"},{"dep":["conj"],"text":"Friday"}]	0	0	0	AUTO		f	\N	\N	\N	0
0f4c1e54-ca2e-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	62	t	["what does alpha do\\n\\n"]	0	f	f	\N	2022-10-24 14:06:12.213223	f	[]	[{"dep":["nsubj"],"text":"alpha"}]	0	0	0	AUTO		f	\N	\N	\N	0
a4513132-ca66-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	63	t	["Where does the professor get all the datasets?  \\ne.g. the \\"penguin-dataset\\".\\nlibrary(palmerpenguins)\\nglimpse(penguins)\\nTy in advance \\nEmanuel\\n \\n\\n"]	0	f	f	\N	2022-10-24 14:06:12.221585	f	[{"text":"dataset","dep":["nsubj","dobj"]},{"text":"penguin","dep":["compound"]}]	[{"dep":["nsubj"],"text":"professor"},{"dep":["nsubj","dobj"],"text":"dataset"},{"dep":["compound"],"text":"penguin"},{"dep":["appos"],"text":"library(palmerpenguin"},{"dep":["ROOT"],"text":"glimpse(penguin"},{"dep":["pobj"],"text":"advance"},{"dep":["ROOT"],"text":"Emanuel"}]	0	0	0	AUTO		f	\N	\N	\N	0
1adcc862-cab1-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	46/1	t	["I guess, that you are in the wrong frag.jetzt room\\n\\nregarding TUMlive, just have a look at ",{"attributes":{"link":"https://live.rbg.tum.de/about"},"insert":"https://live.rbg.tum.de/about"}," and also all other options, who need a stable network connection, won't work, if there is a problem with the network. \\n\\nbut the network problem is hopefully solved\\n\\n"]	0	f	f	\N	2022-10-24 14:06:12.231786	f	[]	[{"dep":["dobj"],"text":"look"},{"dep":["pobj"],"text":"https://live.rbg.tum.de"},{"dep":["nsubj"],"text":"option"},{"dep":["dobj"],"text":"network connection"},{"dep":["attr"],"text":"problem"},{"dep":["pobj"],"text":"network"},{"dep":["nsubjpass"],"text":"network problem"}]	0	0	0	AUTO	Stephan	f	\N	7c1571f4-c7b1-11ec-84b5-0242ac190004	\N	1
2dfbd916-ca2e-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	62/1	t	["Change the transparency\\n"]	0	f	f	\N	2022-10-24 14:06:12.237632	f	[]	[{"dep":["dobj"],"text":"transparency"}]	0	0	0	AUTO		f	\N	0f4c1e54-ca2e-11ec-a5cb-0242ac190004	2022-05-02 15:40:29.106	1
fd0a2a46-ca0b-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	64	t	["Is it possible to reopen the registration for the \\"Exercises for Statistics for Business Administration\\" Course? I missed the time frame to sign up\\n"]	0	f	f	\N	2022-10-24 14:06:12.245574	f	[{"text":"exercise","dep":["pobj"]},{"text":"registration","dep":["dobj"]}]	[{"dep":["dobj"],"text":"registration"},{"dep":["pobj"],"text":"exercise"},{"dep":["pobj"],"text":"Statistics"},{"dep":["pobj"],"text":"Business Administration"},{"dep":["npadvmod"],"text":"course"},{"dep":["dobj"],"text":"time frame"}]	1	1	0	AUTO		f	\N	\N	\N	0
dc4de44c-cd0c-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	65	t	["how to submit the written homework?\\n"]	0	f	f	\N	2022-10-24 14:06:12.256614	f	[]	[{"dep":["dobj"],"text":"homework"}]	0	0	0	AUTO		f	\N	\N	\N	0
f1d85534-ca2a-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	66	t	["Does R chooses the Colour?\\n"]	0	f	f	\N	2022-10-24 14:06:12.262641	f	[]	[{"dep":["dobj"],"text":"color"}]	0	0	0	AUTO		f	\N	\N	\N	0
f4af685c-c7ae-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	39/1	t	["In a stratum, all entities have to be similar to some degree (clustering groups arbitrarily), and you only select a random subset of the elements in each stratum (in clustering, you look at all entities in the cluster); also, you're always looking at all strata; (while by clustering, you can also select only some clusters).... right?\\n"]	1	f	f	\N	2022-10-24 14:06:12.27174	f	[]	[{"dep":["pobj"],"text":"stratum"},{"dep":["pobj","nsubj"],"text":"entity"},{"dep":["pobj"],"text":"degree"},{"dep":["dobj"],"text":"group"},{"dep":["dobj"],"text":"subset"},{"dep":["pobj"],"text":"element"},{"dep":["pobj","dobj"],"text":"cluster"},{"dep":["dobj"],"text":"strata"}]	0	0	0	AUTO		f	\N	8249545c-c78c-11ec-b02a-0242ac190004	\N	1
65dfbcc6-ca2e-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	67	t	["What's with the ~, as opposed to =?\\n"]	0	f	f	\N	2022-10-24 14:06:12.277356	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
3aa56fba-ca2e-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	62/2	t	["Change the transparancy \\n"]	0	f	f	\N	2022-10-24 14:06:12.283525	f	[]	[{"dep":["dobj"],"text":"transparency"}]	0	0	0	AUTO		f	\N	0f4c1e54-ca2e-11ec-a5cb-0242ac190004	2022-05-02 15:40:52.402	1
ba56f6b6-ca2e-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	68	t	["can you repeat what does the + mean?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:12.289105	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
fd7a8734-ca2c-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	69	t	["für was stand 'geom_point' nochmal?\\n"]	0	f	f	\N	2022-10-24 14:06:12.295062	f	[]	[{"dep":["nsubj"],"text":"für"},{"dep":["nmod"],"text":"stand"}]	0	0	0	AUTO		f	\N	\N	\N	0
281f2368-ca2e-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	70	t	["By \\"Global\\", you mean in the scope of the plot, not like a global variable, right?\\n"]	0	f	f	\N	2022-10-24 14:06:12.301826	f	[]	[{"dep":["pobj"],"text":"Global"},{"dep":["pobj"],"text":"scope"},{"dep":["pobj"],"text":"plot"},{"dep":["pobj"],"text":"variable"}]	0	0	0	AUTO		f	\N	\N	\N	0
fe5c0588-c7b1-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	71	t	["I loved statistics until this point, but this ist just really really bad\\n"]	0	f	f	\N	2022-10-24 14:06:12.30869	f	[]	[{"dep":["dobj"],"text":"statistic"},{"dep":["pobj"],"text":"point"},{"dep":["conj"],"text":"ist"}]	8	8	0	AUTO		f	\N	\N	2022-05-03 11:54:37.021	0
e96cc8d8-c7b1-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	72	t	["PLEASE REQUIRE RECORDED LECTURES\\nalmost every prof does this\\n\\nplease be human to your students\\n"]	0	f	f	\N	2022-10-24 14:06:12.315348	f	[]	[{"dep":["dobj"],"text":"lecture"},{"dep":["nsubj"],"text":"prof"},{"dep":["pobj"],"text":"student"}]	23	23	0	AUTO		f	\N	\N	2022-05-03 11:53:54.824	0
094df704-ca2c-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	73	t	["If the legend corresponds to shapes, and not to colours : should we write labs(shape = \\"Species\\")\\n"]	1	f	f	\N	2022-10-24 14:06:12.322464	f	[]	[{"dep":["nsubj"],"text":"legend"},{"dep":["pobj"],"text":"shape"},{"dep":["conj"],"text":"colour"},{"dep":["dobj"],"text":"labs(shape"},{"dep":["ROOT"],"text":"specie"}]	0	0	0	AUTO		f	\N	\N	\N	0
d10f6afa-cad8-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	2/1	t	["we need to define a mapping from the variables to aesthetics (like x variable, color, shape, ...) of the plot. \\n"]	0	f	f	\N	2022-10-24 14:06:12.330456	f	[]	[{"dep":["dobj"],"text":"mapping"},{"dep":["pobj"],"text":"variable"},{"dep":["pobj"],"text":"aesthetic"},{"dep":["conj"],"text":"color"},{"dep":["conj"],"text":"shape"},{"dep":["pobj"],"text":"plot"}]	0	0	0	AUTO		f	\N	9a77e77c-ca2b-11ec-90ff-0242ac190004	2022-05-03 12:02:13.391	1
bfbe87e4-cfa7-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	100	t	["what exactly makes the se = FALSE in the geom_smooth?\\n"]	0	f	f	\N	2022-10-24 14:06:12.689422	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-05-09 16:31:01.916	0
2ff449b8-cada-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	2/2	t	["we need to define a mapping from the variables to aesthetics (like x variable, color, shape, ...) of the plot. this mapping is done with the aes() function\\n"]	0	f	f	\N	2022-10-24 14:06:12.338256	f	[]	[{"dep":["nsubjpass","dobj"],"text":"mapping"},{"dep":["pobj"],"text":"variable"},{"dep":["pobj"],"text":"aesthetic"},{"dep":["conj"],"text":"color"},{"dep":["conj"],"text":"shape"},{"dep":["pobj"],"text":"plot"},{"dep":["pobj"],"text":"aes"},{"dep":["appos"],"text":"function"}]	0	0	0	AUTO		f	\N	9a77e77c-ca2b-11ec-90ff-0242ac190004	\N	1
be1abc6a-c7b3-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	74	t	["This is what we call an \\"excellence university\\"!\\n"]	0	f	f	\N	2022-10-24 14:06:12.344369	f	[]	[{"dep":["oprd"],"text":"excellence university"}]	1	3	2	AUTO		f	\N	\N	2022-05-03 11:56:11.008	0
ac40699e-ca57-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	39/1/1	t	["yes, you are right about strata containing similar cases, while cluster are supposed to be rather heterogeneous.\\nsampling several of the clusters and not considering all of them, is also a major part of cluster sampling\\n"]	0	f	f	\N	2022-10-24 14:06:12.35245	f	[]	[{"dep":["pobj"],"text":"strata"},{"dep":["dobj"],"text":"case"},{"dep":["nsubjpass","pobj"],"text":"cluster"},{"dep":["attr"],"text":"part"},{"dep":["pobj"],"text":"cluster sampling"}]	0	0	0	AUTO		f	\N	f4af685c-c7ae-11ec-a5cb-0242ac190004	\N	2
e403e5b8-ca57-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	54/1	t	["There are 2 explanatory variables (light and noise), 1 blocking variable (gender), and 1 response variable (exam performance)\\n\\n\\n"]	0	f	f	\N	2022-10-24 14:06:12.359936	f	[]	[{"dep":["conj","attr"],"text":"variable"},{"dep":["appos"],"text":"light"},{"dep":["conj"],"text":"noise"},{"dep":["appos"],"text":"gender"},{"dep":["conj"],"text":"response variable"},{"dep":["appos"],"text":"exam performance"}]	0	0	0	AUTO		f	\N	6ca02e9a-c78d-11ec-87c8-0242ac190004	\N	1
b7097d46-ca27-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	75	t	["Are exercise sessions on Monday and Friday the same?\\n"]	0	f	f	\N	2022-10-24 14:06:12.372195	f	[]	[{"dep":["pobj"],"text":"Monday"},{"dep":["conj"],"text":"Friday"}]	0	0	0	AUTO		f	\N	\N	2022-05-03 11:59:35.29	0
8514887e-cad8-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	61/1	t	["yes\\n"]	0	f	f	\N	2022-10-24 14:06:12.377797	f	[]	[]	0	0	0	AUTO		f	\N	b2b056be-ca29-11ec-a5cb-0242ac190004	\N	1
bb4599dc-cd04-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	76	t	["On slide 14: Why didn't you say hotel as the first argument in the arrange function?\\n"]	0	f	f	\N	2022-10-24 14:06:12.384354	f	[]	[{"dep":["pobj"],"text":"slide"},{"dep":["dobj"],"text":"hotel"},{"dep":["amod"],"text":"first"},{"dep":["pobj"],"text":"argument"}]	0	0	0	AUTO		f	\N	\N	2022-05-06 07:28:33.562	0
03566626-cafc-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	77	t	["While going over the readings regarding Data visualization i tried installing the nycflights13 package, but I got an error saying that there was no package found, although the dplyr package was installed\\n"]	0	f	f	\N	2022-10-24 14:06:12.392416	f	[]	[{"dep":["pobj"],"text":"reading"},{"dep":["compound"],"text":"Data"},{"dep":["pobj"],"text":"visualization"},{"dep":["dobj"],"text":"error"},{"dep":["attr"],"text":"package"}]	0	0	0	AUTO		f	\N	\N	2022-05-03 17:12:06.827	0
1c290c88-cadb-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	70/1	t	["by global I mean, that a global dataset or mapping (defined in ggplot()) can be used \\"globally\\" by all other components/functions of your ggplot2 command. if a mapping is defined just locally (inside a geom function) it can just be used for that function\\n"]	0	f	f	\N	2022-10-24 14:06:12.401382	f	[]	[{"dep":["nsubjpass"],"text":"dataset"},{"dep":["nsubjpass","conj"],"text":"mapping"},{"dep":["pobj"],"text":"ggplot"},{"dep":["nmod"],"text":"component"},{"dep":["pobj"],"text":"function"}]	0	0	0	AUTO		f	\N	281f2368-ca2e-11ec-9e13-0242ac190004	\N	1
6aa76538-cad8-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	64/1	t	["you can write an email to haug@tum.de and tell me your preferred group (which can't be C, since it is already full)\\n"]	0	f	f	\N	2022-10-24 14:06:12.408292	f	[]	[{"dep":["dobj"],"text":"email"},{"dep":["pobj"],"text":"haug@tum.de"},{"dep":["dobj"],"text":"group"}]	0	0	0	AUTO		f	\N	fd0a2a46-ca0b-11ec-87c8-0242ac190004	\N	1
a97ecaa6-cada-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	62/3	t	["the value of alpha defines the degree of transparency in your plot. the larger the value, the less transparent. see\\n",{"attributes":{"link":"https://ggplot2.tidyverse.org/reference/scale_alpha.html"},"insert":"https://ggplot2.tidyverse.org/reference/scale_alpha.html"},"\\nfor details\\n"]	0	f	f	\N	2022-10-24 14:06:12.4168	f	[]	[{"dep":["nsubj","ROOT"],"text":"value"},{"dep":["pobj"],"text":"alpha"},{"dep":["dobj"],"text":"degree"},{"dep":["pobj"],"text":"transparency"},{"dep":["pobj"],"text":"plot"},{"dep":["pobj"],"text":"detail"}]	0	0	0	AUTO		f	\N	0f4c1e54-ca2e-11ec-a5cb-0242ac190004	\N	1
30ddea0c-cadd-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	63/1	t	["the penguins dataset e.g. is contained in the palmerpenguins R package. this means, you need to first install the package\\n\\ninstall.packages(palmerpenguins)\\n\\nand then you have to load the package\\n\\nlibrary(palmerpenguins)\\n\\nafter that, you are able to access the penguins dataset\\n"]	0	f	f	\N	2022-10-24 14:06:12.424935	f	[]	[{"dep":["nsubjpass","dobj"],"text":"penguin"},{"dep":["pobj"],"text":"palmerpenguin r package"},{"dep":["dobj"],"text":"package"},{"dep":["punct"],"text":"library(palmerpenguin"}]	0	0	0	AUTO		f	\N	a4513132-ca66-11ec-a5fc-0242ac190004	\N	1
9371d1a2-ca2d-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	69/1	t	["To show your data as points and not as a line\\n"]	1	f	f	\N	2022-10-24 14:06:12.431986	f	[]	[{"dep":["dobj"],"text":"datum"},{"dep":["pobj"],"text":"point"},{"dep":["pobj"],"text":"line"}]	0	0	0	AUTO		f	\N	fd7a8734-ca2c-11ec-9e13-0242ac190004	\N	1
53c52244-cadb-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	67/1	t	["= is an assignment. in R it is used to assign values to arguments of function.\\n\\n~ is used as a formula notation. it defines a left hand side (everything left of ~) and right hand side\\n"]	0	f	f	\N	2022-10-24 14:06:12.444084	f	[]	[{"dep":["attr"],"text":"assignment"},{"dep":["dobj"],"text":"value"},{"dep":["pobj"],"text":"argument"},{"dep":["pobj"],"text":"function"},{"dep":["conj","dobj"],"text":"hand side"}]	0	0	0	AUTO		f	\N	65dfbcc6-ca2e-11ec-90ff-0242ac190004	\N	1
a0716400-cada-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	78	t	["What is the difference between a stratum and a cluster\\n"]	0	f	f	\N	2022-10-24 14:06:12.450263	f	[]	[{"dep":["nsubj"],"text":"difference"},{"dep":["pobj"],"text":"stratum"},{"dep":["conj"],"text":"cluster"}]	0	0	0	AUTO		f	\N	\N	\N	0
dd4f8446-cd06-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	79	t	["can someone please post of the link to the data set here? Thx\\n\\n"]	0	f	f	\N	2022-10-24 14:06:12.456337	f	[]	[{"dep":["pobj"],"text":"link"},{"dep":["pobj"],"text":"data"}]	0	0	0	AUTO		f	\N	\N	2022-05-06 07:28:20.351	0
e565d972-cadc-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	68/1	t	["the + adds together the different parts/layers of a ggplot2 command. so, I can have e.g. these two parts\\nggplot(data = penguins), mapping = aes(x = bill_length_mm, y=bill_depth_mm))\\ngeom_point()\\neach one alone is not able to produce a plot with content. but if you combine them with a +\\n\\nggplot(data = penguins), mapping = aes(x = bill_length_mm, y=bill_depth_mm)) +\\n\\tgeom_point()\\n\\nyou get a nice scatterplot\\n\\n"]	0	f	f	\N	2022-10-24 14:06:12.472761	f	[]	[{"dep":["dobj","nmod"],"text":"part"},{"dep":["dobj"],"text":"layer"},{"dep":["pobj"],"text":"example"},{"dep":["nummod"],"text":"two"},{"dep":["nummod"],"text":"ggplot(data"},{"dep":["pobj","appos"],"text":"penguin"},{"dep":["intj","advcl"],"text":"mapping"},{"dep":["ROOT"],"text":"aes(x"},{"dep":["dobj"],"text":"plot"},{"dep":["pobj"],"text":"content"},{"dep":["dobj"],"text":"scatter plot"}]	0	0	0	AUTO		f	\N	ba56f6b6-ca2e-11ec-8ca6-0242ac190004	\N	1
fe0e7e74-cd03-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	80	t	["Could you please show us how to download the hotel dataset so we can also try these operations? Would be very helpful :)\\n"]	0	f	f	\N	2022-10-24 14:06:12.49626	f	[]	[{"dep":["compound"],"text":"hotel"},{"dep":["dobj"],"text":"operation"}]	0	0	0	AUTO		f	\N	\N	2022-05-06 07:28:40.111	0
24794a48-cd05-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	81	t	["What is the difference between \\"+\\" and \\"%>%\\" ?\\n"]	0	f	f	\N	2022-10-24 14:06:12.50295	f	[]	[{"dep":["nsubj"],"text":"difference"}]	0	0	0	AUTO		f	\N	\N	2022-05-06 06:26:41.06	0
2a54a25c-cd03-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	82	t	["whats csv?\\n"]	0	f	f	\N	2022-10-24 14:06:12.508769	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-05-06 07:28:44.859	0
676aa372-cd0c-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	83	t	["Which form of exam do we have? Multiple-choice or writing exam?\\n"]	0	f	f	\N	2022-10-24 14:06:12.514928	f	[]	[{"dep":["dobj"],"text":"form"},{"dep":["pobj"],"text":"exam"},{"dep":["nmod"],"text":"choice"}]	0	0	0	AUTO		f	\N	\N	\N	0
4bbb1854-cd03-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	82/1	t	["csv is a file format and short for \\"comma-separated values\\"\\n"]	0	f	f	\N	2022-10-24 14:06:12.521711	f	[]	[{"dep":["nsubj"],"text":"csv"},{"dep":["attr"],"text":"file format"},{"dep":["pobj"],"text":"value"}]	0	0	0	AUTO		f	\N	2a54a25c-cd03-11ec-9e13-0242ac190004	\N	1
564644ca-cd0c-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	84	t	["Do you expect us to do all the tasks from the R labs including the readings? It seems to be pretty much for one week this time\\n\\n"]	0	f	f	\N	2022-10-24 14:06:12.540659	f	[]	[{"dep":["dobj"],"text":"task"},{"dep":["pobj"],"text":"r lab"},{"dep":["pobj"],"text":"reading"},{"dep":["pobj"],"text":"one week"},{"dep":["npadvmod"],"text":"time"}]	0	0	0	AUTO		f	\N	\N	2022-05-06 07:25:27.098	0
3ceaaae4-cd0b-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	85	t	["what does adr stand for in mean(adr)?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:12.546954	f	[]	[{"dep":["nsubj"],"text":"adr"}]	0	0	0	AUTO		f	\N	\N	2022-05-06 07:25:56.043	0
6614d85a-cd0a-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	86	t	["I also want to know which exercises we should do, because it is not clear to me.\\n"]	0	f	f	\N	2022-10-24 14:06:12.553043	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-05-06 07:26:09.003	0
a3eaf7a6-cd0d-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	83/1	t	["written exam\\n"]	0	f	f	\N	2022-10-24 14:06:12.559184	f	[]	[{"dep":["ROOT"],"text":"exam"}]	0	0	0	AUTO		f	\N	676aa372-cd0c-11ec-9e13-0242ac190004	\N	1
40130bb6-cd07-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	87	t	["Can you please explain the select function on slide 31\\n"]	0	f	f	\N	2022-10-24 14:06:12.566045	f	[]	[{"dep":["dobj"],"text":"function"},{"dep":["pobj"],"text":"slide"}]	0	0	0	AUTO		f	\N	\N	2022-05-06 07:28:14.22	0
ef9c5662-cd04-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	88	t	["Why do we need to pass the hotel dataset to the select function when it is am Argument in the select function?\\n"]	0	f	f	\N	2022-10-24 14:06:12.573007	f	[]	[{"dep":["dobj"],"text":"hotel"},{"dep":["pobj"],"text":"function"},{"dep":["attr"],"text":"Argument"}]	0	0	0	AUTO		f	\N	\N	2022-05-06 07:28:27.013	0
01a16042-cd36-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	89	t	["Can I go to Course A on Monday and to Course B on Friday or are they asynchronous?\\n"]	0	f	f	\N	2022-10-24 14:06:12.579887	f	[]	[{"dep":["pobj"],"text":"course a"},{"dep":["pobj"],"text":"Monday"},{"dep":["pobj"],"text":"Course B"},{"dep":["pobj"],"text":"Friday"}]	0	0	0	AUTO		f	\N	\N	\N	0
7b219fd6-cd09-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	90	t	["I have a question regarding the homework. For this week we need to work on a quiz but there are also 2 other exercises from the book marked as homework. So my question is how to submit these exercises \\nnd whether you should register for the exercise sessions to be eligible for the bonus.\\n"]	0	f	f	\N	2022-10-24 14:06:12.58952	f	[{"text":"bonus","dep":["pobj"]},{"text":"exercise","dep":["dobj","attr"]},{"text":"homework","dep":["pobj"]},{"text":"question","dep":["nsubj","dobj"]}]	[{"dep":["nsubj","dobj"],"text":"question"},{"dep":["pobj"],"text":"homework"},{"dep":["pobj"],"text":"this week"},{"dep":["pobj"],"text":"quiz"},{"dep":["dobj","attr"],"text":"exercise"},{"dep":["pobj"],"text":"book"},{"dep":["pobj"],"text":"bonus"}]	0	0	0	AUTO		f	\N	\N	\N	0
95e50bba-cd0d-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	65/1	t	["in this week there is no written homework, which you can submit. the first homework is a quiz, which you can do on mooodle. the other two, you do on your own and then compare our solution to yours\\n"]	0	f	f	\N	2022-10-24 14:06:12.597421	f	[]	[{"dep":["pobj"],"text":"this week"},{"dep":["nsubj","attr"],"text":"homework"},{"dep":["attr"],"text":"quiz"},{"dep":["npadvmod"],"text":"two"},{"dep":["dobj"],"text":"solution"}]	0	0	0	AUTO		f	\N	dc4de44c-cd0c-11ec-8ca6-0242ac190004	\N	1
4cdadfe4-cd08-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	91	t	["Can you give more information when should we do these exercises\\n"]	0	f	f	\N	2022-10-24 14:06:12.608759	f	[]	[{"dep":["dobj"],"text":"information"},{"dep":["dobj"],"text":"exercise"}]	0	0	0	AUTO		f	\N	\N	2022-05-06 07:28:07.406	0
18372932-cd30-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	63/1/1	t	["Okay, noted. Thank you!\\n"]	0	f	f	\N	2022-10-24 14:06:12.614573	f	[]	[]	0	0	0	AUTO		f	\N	30ddea0c-cadd-11ec-a971-0242ac190004	\N	2
9a5f8434-cd37-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	92	t	["Can select() function be used before filter() function?\\n"]	0	f	f	\N	2022-10-24 14:06:12.62111	f	[]	[{"dep":["nsubjpass","pobj"],"text":"function"},{"dep":["nmod"],"text":"filter"}]	0	0	0	AUTO		f	\N	\N	2022-05-06 12:27:11.452	0
cb42f356-cd37-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	93	t	["Why don't we have  with the function arrange without the specification \\"desc\\" any number in the column of baby  ?\\n"]	0	f	f	\N	2022-10-24 14:06:12.628567	f	[]	[{"dep":["nmod"],"text":"specification"},{"dep":["pobj"],"text":"desc"},{"dep":["appos"],"text":"number"},{"dep":["pobj"],"text":"column"},{"dep":["pobj"],"text":"baby"}]	0	0	0	AUTO		f	\N	\N	2022-05-09 12:32:51.122	0
9d2c2492-cd0a-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	94	t	["It isn't clear to me how this count (hotel, little_ones) works or what exactly it shows. And in the first row, if dbl is 0, how can de proportion be 0.619?\\n"]	0	f	f	\N	2022-10-24 14:06:12.643534	f	[]	[{"dep":["nsubj"],"text":"count"},{"dep":["nmod"],"text":"hotel"},{"dep":["pobj"],"text":"the first row"},{"dep":["nsubj"],"text":"dbl"},{"dep":["nsubj"],"text":"proportion"}]	0	0	0	AUTO		f	\N	\N	2022-05-06 07:26:02.846	0
0ca24b34-cd0c-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	95	t	["Will we get a solution for the Homework problems?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:12.655091	f	[]	[{"dep":["dobj"],"text":"solution"},{"dep":["compound"],"text":"Homework"},{"dep":["pobj"],"text":"problem"}]	0	0	0	AUTO		f	\N	\N	\N	0
023110d2-cd38-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	96	t	["what can function distinct() do?\\n"]	0	f	f	\N	2022-10-24 14:06:12.663485	f	[]	[{"dep":["amod"],"text":"distinct"},{"dep":["nsubj"],"text":"function"}]	0	0	0	AUTO		f	\N	\N	\N	0
915790d2-cd34-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	97	t	["Will you offer Q & As for using R? Or specific sessions were we can get direct help with coding?\\n"]	0	f	f	\N	2022-10-24 14:06:12.670986	f	[]	[{"dep":["nsubj"],"text":"session"},{"dep":["dobj"],"text":"help"},{"dep":["pobj"],"text":"coding"}]	1	1	0	AUTO		f	\N	\N	\N	0
be9b39d6-cd0c-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	98	t	["Will we have to do R code in the exam?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:12.677087	f	[]	[{"dep":["dobj"],"text":"r code"},{"dep":["pobj"],"text":"exam"}]	2	2	0	AUTO		f	\N	\N	\N	0
b2384134-cd39-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	99	t	["How can you add an extra row to make the code look more aesthetic?\\n"]	0	f	f	\N	2022-10-24 14:06:12.683374	f	[]	[{"dep":["dobj"],"text":"row"},{"dep":["nsubj"],"text":"code"}]	0	0	0	AUTO		f	\N	\N	\N	0
a175557c-cfa9-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	101	t	["why do we write aes(y=y)? \\n"]	0	f	f	\N	2022-10-24 14:06:12.695492	f	[]	[{"dep":["intj"],"text":"aes(y"}]	0	0	0	AUTO		f	\N	\N	2022-05-09 16:30:56.8	0
5e6205a2-cfac-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	102	t	["Is this weeks quiz about last weeks lectures?\\n"]	0	f	f	\N	2022-10-24 14:06:12.701287	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
89f26716-cfac-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	103	t	["can you upload the solution of the quiz\\n"]	2	f	f	\N	2022-10-24 14:06:12.707502	f	[]	[{"dep":["dobj"],"text":"solution"},{"dep":["pobj"],"text":"quiz"}]	0	0	0	AUTO		f	\N	\N	\N	0
45d68a40-cfab-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	104	t	["what n() means in the slide 11?\\n"]	0	f	f	\N	2022-10-24 14:06:12.713546	f	[]	[{"dep":["pobj"],"text":"slide"}]	0	0	0	AUTO		f	\N	\N	2022-05-09 16:30:50.882	0
f19793be-cfb1-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	105	t	["what is the answer of quiz?\\nmean>median?\\n"]	0	f	f	\N	2022-10-24 14:06:12.720006	f	[]	[{"dep":["nsubj"],"text":"answer"},{"dep":["pobj"],"text":"quiz"},{"dep":["dobj"],"text":"median"}]	0	0	0	AUTO		f	\N	\N	\N	0
c17b71bc-cfae-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	106	t	["I'm not quite sure if I understand the IQR correctly.\\nIs the IQR equivalent to the median, Q2?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:12.731641	f	[{"text":"iqr","dep":["nsubj","dobj"]}]	[{"dep":["nsubj","dobj"],"text":"iqr"},{"dep":["pobj"],"text":"median"}]	0	0	0	AUTO		f	\N	\N	2022-05-09 16:30:33.216	0
07387af8-cf94-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	89/1	t	["last week they have been asynchronous from Monday to Friday, and synchronous again after the lecture on Friday. this may happen quite often.\\n"]	0	f	f	\N	2022-10-24 14:06:12.739051	f	[]	[{"dep":["npadvmod"],"text":"last week"},{"dep":["pobj"],"text":"Monday to Friday"},{"dep":["pobj"],"text":"lecture"},{"dep":["pobj"],"text":"Friday"}]	0	0	0	AUTO		f	\N	01a16042-cd36-11ec-a971-0242ac190004	\N	1
8f99fa52-cf94-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	96/1	t	["it picks the unique rows of a dataframe. the arguments (after specifying the dataset) of distinct() define, which combinations of variables are considered, when the unique combinations are selected.\\n"]	0	f	f	\N	2022-10-24 14:06:12.747595	f	[]	[{"dep":["dobj"],"text":"row"},{"dep":["pobj"],"text":"dataframe"},{"dep":["ROOT"],"text":"argument"},{"dep":["dobj"],"text":"dataset"},{"dep":["amod"],"text":"distinct"},{"dep":["pobj"],"text":"define"},{"dep":["nsubjpass"],"text":"combination"},{"dep":["pobj"],"text":"variable"}]	0	0	0	AUTO		f	\N	023110d2-cd38-11ec-a5fc-0242ac190004	\N	1
cb16907c-cf94-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	99/1	t	["I`m not sure about your question. but with dplyr and the pipe operator, you could e.g. make a new line (hitting enter) after each pipe operator.\\n"]	0	f	f	\N	2022-10-24 14:06:12.75456	f	[]	[{"dep":["ROOT"],"text":"I`m"},{"dep":["pobj"],"text":"question"},{"dep":["pobj"],"text":"dplyr"},{"dep":["dobj"],"text":"line"},{"dep":["appos"],"text":"enter"}]	0	0	0	AUTO		f	\N	b2384134-cd39-11ec-8ca6-0242ac190004	\N	1
b3b38bb8-cf8c-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	107	t	["so we only talk about ‘mean’ when it come to dot plots?\\n"]	0	f	f	\N	2022-10-24 14:06:12.768044	f	[]	[{"dep":["pobj"],"text":"dot plot"}]	0	0	0	AUTO		f	\N	\N	2022-05-09 16:31:08.837	0
9dfd38e4-cf8e-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	108	t	["should we have to respect this order … fill …bins…colour ?\\n"]	0	f	f	\N	2022-10-24 14:06:12.774685	f	[]	[{"dep":["dobj"],"text":"order"},{"dep":["nmod"],"text":"fill"},{"dep":["dobj"],"text":"bins"},{"dep":["appos"],"text":"colour"}]	0	0	0	AUTO		f	\N	\N	\N	0
b9e77312-cf93-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	97/1	t	["this week and next week you will work on R problems in the exercise sessions. so, you will get help from the TA. \\nalso the RStudio primers provide feedback, hints\\n"]	0	f	f	\N	2022-10-24 14:06:12.783043	f	[]	[{"dep":["npadvmod"],"text":"this week"},{"dep":["npadvmod"],"text":"next week"},{"dep":["dobj"],"text":"help"},{"dep":["dobj"],"text":"feedback"},{"dep":["ROOT"],"text":"hint"}]	0	0	0	AUTO		f	\N	915790d2-cd34-11ec-8ca6-0242ac190004	\N	1
de8b2816-cd32-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	109	t	["We don't have to hand in the assignments, do we?\\n"]	0	f	f	\N	2022-10-24 14:06:12.788922	f	[]	[{"dep":["dobj"],"text":"assignment"}]	0	0	0	AUTO		f	\N	\N	2022-05-09 12:28:39.017	0
f031753a-cd36-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	110	t	["Is there a start_with method or something similiar for filter()\\n"]	0	f	f	\N	2022-10-24 14:06:12.795692	f	[]	[{"dep":["attr"],"text":"method"},{"dep":["pobj"],"text":"filter"}]	0	0	0	AUTO		f	\N	\N	2022-05-09 12:30:45.962	0
2049c772-cfb2-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	111	t	["can you explain the box plot again, thanks\\n"]	0	f	f	\N	2022-10-24 14:06:12.802538	f	[]	[{"dep":["dobj"],"text":"box plot"},{"dep":["npadvmod"],"text":"thank"}]	0	0	0	AUTO		f	\N	\N	\N	0
5a5575d8-cfb2-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	112	t	["Is the exam multiple/single-choice?\\n"]	0	f	f	\N	2022-10-24 14:06:12.808703	f	[]	[{"dep":["nmod"],"text":"exam"},{"dep":["nsubj"],"text":"choice"}]	1	1	0	AUTO		f	\N	\N	\N	0
ac9f125c-cfb4-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	105/1	t	["no, since the data is left skewed the mean is shifted to the left and hence it is smaller than the median\\n"]	0	f	f	\N	2022-10-24 14:06:12.815647	f	[]	[{"dep":["nsubjpass"],"text":"data"},{"dep":["nsubjpass"],"text":"mean"},{"dep":["pobj"],"text":"left"},{"dep":["pobj"],"text":"median"}]	0	0	0	AUTO		f	\N	f19793be-cfb1-11ec-8ca6-0242ac190004	\N	1
d860e6a0-cfa9-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	113	t	["what is the difference between cases and a sample\\n"]	0	f	f	\N	2022-10-24 14:06:12.821517	f	[]	[{"dep":["nsubj"],"text":"difference"},{"dep":["pobj"],"text":"case"},{"dep":["conj"],"text":"sample"}]	0	0	0	AUTO		f	\N	\N	\N	0
534ee8e2-cfac-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	114	t	["Where can we find the nobel.csv file we need for this weeks homework? Because it is not in the \\"Data files, R markdown files and previous problem sets\\" folder on Moodle\\n"]	0	f	f	\N	2022-10-24 14:06:12.835012	f	[]	[{"dep":["pobj"],"text":"homework"},{"dep":["pobj"],"text":"data file"},{"dep":["conj"],"text":"r markdown file"},{"dep":["conj"],"text":"problem set"},{"dep":["dobj"],"text":"folder"},{"dep":["pobj"],"text":"Moodle"}]	0	0	0	AUTO		f	\N	\N	2022-05-09 16:48:56.299	0
952b3f08-cfb2-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	115	t	["what do the points in the box plot diagram mean?\\n"]	0	f	f	\N	2022-10-24 14:06:12.841593	f	[]	[{"dep":["dobj"],"text":"point"},{"dep":["pobj"],"text":"box plot diagram"}]	0	0	0	AUTO		f	\N	\N	\N	0
a6586f12-cfb7-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	116	t	["the csv file needed for the homework seems to be missing\\n"]	0	f	f	\N	2022-10-24 14:06:12.848367	f	[]	[{"dep":["nsubj"],"text":"csv file"},{"dep":["pobj"],"text":"homework"}]	2	2	0	AUTO		f	\N	\N	2022-05-12 10:06:31.6	0
d3882174-06aa-11ed-bbd1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	117	t	["2 * pt(t-value)\\n"]	0	f	f	\N	2022-10-24 14:06:12.854146	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-18 15:06:35.701	0
8d2c8a2a-daaa-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	118	t	["What stat_function and data.frame do?\\n"]	0	f	f	\N	2022-10-24 14:06:12.865454	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-05-24 07:12:01.266	0
e50f7fdc-f0b1-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	119	t	["Why we reject the null hypothesis on slide 28?\\n"]	0	f	f	\N	2022-10-24 14:06:12.877371	f	[]	[{"dep":["dobj"],"text":"hypothesis"},{"dep":["pobj"],"text":"slide"}]	0	0	0	AUTO		f	\N	\N	2022-06-27 13:53:07.291	0
5e6d70d2-eb30-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	234	t	["can you explain very short again why we chose 0.85 and not 0.80 i was away shortly\\n"]	0	f	f	\N	2022-10-24 14:06:14.105281	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-13 16:31:29.801	0
a02d923a-f0b3-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	120	t	["In the quiz on side 29 why does it say \\"at least 0.545\\" and not the value of 0.867 we computed on slide 26? Isn't the p value computed by looking at how likely it is to observe values further away from the 0 than 0.867 in both directions if the null is true?\\n"]	0	f	f	\N	2022-10-24 14:06:12.886888	f	[]	[{"dep":["pobj"],"text":"quiz"},{"dep":["pobj"],"text":"side"},{"dep":["xcomp"],"text":"at least 0.545"},{"dep":["conj","dobj"],"text":"value"},{"dep":["pobj"],"text":"slide"},{"dep":["nsubj"],"text":"p value"},{"dep":["pobj"],"text":"0 than 0.867"},{"dep":["pobj"],"text":"direction"},{"dep":["nsubj"],"text":"null"}]	0	0	0	AUTO		f	\N	\N	\N	0
aae006d8-f0ac-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	121	t	["Why is 10-1\\n"]	0	f	f	\N	2022-10-24 14:06:12.893357	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-27 13:51:36.388	0
f18ae49e-012e-11ed-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	122	t	["a\\n"]	0	f	f	\N	2022-10-24 14:06:12.900197	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-11 16:41:48.114	0
fb1bb1d8-0128-11ed-938f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	123	t	["How many points are needed for the grade bonus?\\n"]	0	f	f	\N	2022-10-24 14:06:12.906453	f	[]	[{"dep":["nsubjpass"],"text":"point"}]	0	0	0	EN		f	\N	\N	2022-07-11 16:42:12.511	0
01bb3aa6-0096-11ed-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	124	t	["I think there's a mistake in the solution of the last exercise. You argued in the text that C>B>A but then said that C>B and A>B. Maybe you could take another look on it. Excuse me if Im wrong.\\n"]	0	f	f	\N	2022-10-24 14:06:12.919325	f	[]	[{"dep":["attr"],"text":"mistake"},{"dep":["pobj"],"text":"solution"},{"dep":["pobj"],"text":"exercise"},{"dep":["pobj"],"text":"text"},{"dep":["ccomp"],"text":"C > b"},{"dep":["dobj"],"text":"look"},{"dep":["advmod"],"text":"wrong"}]	0	0	0	EN		f	\N	\N	\N	0
edb17e7e-012d-11ed-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	125	t	["The graph you have on slide 26 is different from ours I think, the column poverty and female_house are changed \\n"]	0	f	f	\N	2022-10-24 14:06:12.925817	f	[]	[{"dep":["nsubj"],"text":"graph"},{"dep":["pobj"],"text":"slide"},{"dep":["pobj"],"text":"our"}]	0	0	0	AUTO		f	\N	\N	2022-07-11 16:41:54.746	0
00eb772c-d287-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	126	t	["Where can I find the R file for the Exercise?\\n"]	0	f	f	\N	2022-10-24 14:06:12.932889	f	[]	[{"dep":["dobj"],"text":"r file"},{"dep":["pobj"],"text":"exercise"}]	0	0	0	AUTO		f	\N	\N	2022-05-13 07:51:43.585	0
0a9b403c-d28b-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	127	t	["What does nrow(df) mean?\\n"]	0	f	f	\N	2022-10-24 14:06:12.939135	f	[]	[{"dep":["nsubj"],"text":"nrow(df"}]	0	0	0	AUTO		f	\N	\N	2022-05-13 07:51:38.314	0
d975af54-040f-11ed-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	128	t	["But I used the other exercises always in R and not with the calculator since I dont know how to compute it by hand with the calculator\\n"]	0	f	f	\N	2022-10-24 14:06:12.951003	f	[]	[{"dep":["dobj"],"text":"exercise"},{"dep":["pobj"],"text":"calculator"},{"dep":["pobj"],"text":"hand"}]	0	0	0	EN		f	\N	\N	2022-07-15 08:30:19.145	0
7be011ac-040c-11ed-9520-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	129	t	["Can you maybe show what a multiple linear regression matrix look like\\n"]	0	f	f	\N	2022-10-24 14:06:12.970654	f	[]	[]	0	0	0	EN		f	\N	\N	2022-07-15 08:45:14.514	0
cc51c614-d28c-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	130	t	["Do we have to know all these functions, commands, data sets etc. by heart for the exam and courseDo we have to know all these functions, commands, data sets etc. by heart for the exam and course? It is quite a bit which we just learned in two weeks\\n"]	0	f	f	\N	2022-10-24 14:06:12.979802	f	[]	[{"dep":["dobj"],"text":"function"},{"dep":["conj"],"text":"command"},{"dep":["compound"],"text":"data set"},{"dep":["pobj"],"text":"heart"},{"dep":["pobj"],"text":"exam"},{"dep":["conj"],"text":"course"},{"dep":["attr"],"text":"bit"},{"dep":["pobj"],"text":"two weeks"}]	0	0	0	AUTO		f	\N	\N	2022-05-13 07:17:57.426	0
557dedc8-d28d-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	131	t	["Wouldn't you use 2 sigma for the standard deviation or would you rather choose something like 1%?\\n"]	0	f	f	\N	2022-10-24 14:06:12.986178	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
8fb8272a-d28c-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	132	t	["do we have to understand/reproduce code like this simulation or is that just an example?\\n"]	0	f	f	\N	2022-10-24 14:06:12.993114	f	[]	[{"dep":["dobj"],"text":"code"},{"dep":["pobj"],"text":"simulation"},{"dep":["attr"],"text":"example"}]	0	0	0	AUTO		f	\N	\N	2022-05-13 07:52:05.43	0
a62d51a0-d28d-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	133	t	["If you didn't pass the last moodle test, is there a way to retake it or open it up again?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:12.999415	f	[]	[{"dep":["attr"],"text":"way"}]	0	0	0	AUTO		f	\N	\N	\N	0
b2175468-d533-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	134	t	["\\n I feel like you're ignoring most of the questions on here\\n"]	0	f	f	\N	2022-10-24 14:06:13.005431	f	[]	[{"dep":["pobj"],"text":"question"}]	0	1	1	AUTO		f	\N	\N	\N	0
a81eafc6-d806-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	135	t	["Don't we have 13 hearts in a 52-deck, hence probability 13/52?\\n"]	0	f	f	\N	2022-10-24 14:06:13.011353	f	[]	[{"dep":["dobj"],"text":"heart"},{"dep":["pobj"],"text":"probability"}]	0	0	0	AUTO		f	\N	\N	\N	0
1c37880c-1181-11ed-a930-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	136	t	["For those, who weren´t able to participate the last lecture. Because of an exam, for example. Is it possible to get the solution from the exercises and maybe some hints what you were talking about? \\n\\nThank you! \\n"]	0	f	f	\N	2022-10-24 14:06:13.019941	f	[{"text":"exam","dep":["pobj"]},{"text":"lecture","dep":["dobj"]}]	[{"dep":["dobj"],"text":"lecture"},{"dep":["pobj"],"text":"exam"},{"dep":["pobj"],"text":"example"},{"dep":["dobj"],"text":"solution"},{"dep":["pobj"],"text":"exercise"},{"dep":["conj"],"text":"hint"}]	1	1	0	EN		f	\N	\N	\N	0
db452170-d28c-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	137	t	["Do we have to know all these functions, commands, data sets etc. by heart for the exam and course? It is quite a bit which we just learned in two weeks\\n\\n"]	0	f	f	\N	2022-10-24 14:06:13.027984	f	[]	[{"dep":["dobj"],"text":"function"},{"dep":["conj"],"text":"command"},{"dep":["compound"],"text":"data set"},{"dep":["pobj"],"text":"heart"},{"dep":["pobj"],"text":"exam"},{"dep":["conj"],"text":"course"},{"dep":["attr"],"text":"bit"},{"dep":["pobj"],"text":"two weeks"}]	0	0	0	AUTO		f	\N	\N	\N	0
adda69ac-d28c-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	138	t	["Do we have to know all these functions, commands, data sets etc. by heart for the exam and course? It is quite a bite which we just learned in two weeks\\n"]	0	f	f	\N	2022-10-24 14:06:13.036366	f	[]	[{"dep":["dobj"],"text":"function"},{"dep":["conj"],"text":"command"},{"dep":["compound"],"text":"data set"},{"dep":["pobj"],"text":"heart"},{"dep":["pobj"],"text":"exam"},{"dep":["conj"],"text":"course"},{"dep":["attr"],"text":"bite"},{"dep":["pobj"],"text":"two weeks"}]	0	0	0	AUTO		f	\N	\N	2022-05-13 07:17:33.126	0
fc5a2c3c-d2b1-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	139	t	["Why is a bar plot better than a pie chart? \\n"]	0	f	f	\N	2022-10-24 14:06:13.042352	f	[]	[{"dep":["nsubj"],"text":"bar plot"},{"dep":["pobj"],"text":"pie chart"}]	0	0	0	AUTO		f	\N	\N	\N	0
ed88f23a-d28c-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	140	t	["is 3 out of 100 enough for we to unvalid the null hypothesis? \\n"]	0	f	f	\N	2022-10-24 14:06:13.049201	f	[]	[{"dep":["dobj"],"text":"hypothesis"}]	0	0	0	AUTO		f	\N	\N	2022-05-13 07:53:03.201	0
0b916f6a-ee02-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	235	t	["Can you explain one more time slide 14/23? Why do we use pnorm?\\n"]	0	f	f	\N	2022-10-24 14:06:14.112529	f	[]	[{"dep":["nummod"],"text":"one"}]	0	0	0	AUTO		f	\N	\N	2022-06-17 08:36:03.446	0
f79e0616-d291-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	131/1	t	["I'm really not sure about that question. You refer to a measure of variation (standard deviation) and the significance level of a test. I would say that these are two different things.\\n"]	0	f	f	\N	2022-10-24 14:06:13.057962	f	[]	[{"dep":["pobj"],"text":"question"},{"dep":["pobj"],"text":"measure"},{"dep":["pobj"],"text":"variation"},{"dep":["appos"],"text":"deviation"},{"dep":["conj"],"text":"significance level"},{"dep":["pobj"],"text":"test"},{"dep":["nummod"],"text":"two"},{"dep":["attr"],"text":"thing"}]	0	0	0	AUTO		f	\N	557dedc8-d28d-11ec-a6d5-0242ac190004	\N	1
511cae36-d292-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	133/1	t	["at some later time, I will combine questions from all the previous quizzes in a quiz, which does not count for the grade bonus. in this quiz we will update the questions from time to time, which has the consequence that all previous attempts will be deleted. but since this quiz will not count for the grade bonus, this is also not really a problem.\\n"]	0	f	f	\N	2022-10-24 14:06:13.067944	f	[]	[{"dep":["pobj"],"text":"time"},{"dep":["dobj"],"text":"question"},{"dep":["pobj","nsubj"],"text":"quiz"},{"dep":["dobj"],"text":"consequence"},{"dep":["nsubjpass"],"text":"attempt"},{"dep":["attr"],"text":"problem"}]	0	0	0	AUTO		f	\N	a62d51a0-d28d-11ec-87c8-0242ac190004	\N	1
7d753fd0-d28c-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	127/1	t	["try ?nrow() in R\\n\\nnrow(x): nrow and ncol return the number of rows or columns present in x\\n"]	0	f	f	\N	2022-10-24 14:06:13.074749	f	[]	[{"dep":["punct"],"text":"nrow(x"},{"dep":["dobj"],"text":"number"},{"dep":["pobj"],"text":"row"},{"dep":["conj"],"text":"column"}]	0	0	0	AUTO		f	\N	0a9b403c-d28b-11ec-a971-0242ac190004	\N	1
091cae3a-d286-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	141	t	["can you please explain again why some bars in a mosaic diagram are Wider or thinner\\n"]	0	f	f	\N	2022-10-24 14:06:13.081953	f	[]	[{"dep":["nsubj"],"text":"bar"},{"dep":["compound"],"text":"mosaic"},{"dep":["pobj"],"text":"diagram"},{"dep":["attr"],"text":"Wider"}]	0	0	0	AUTO		f	\N	\N	2022-05-13 07:51:50.629	0
638eb1ea-d2f1-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	142	t	["Can you explain me what \\"group = 1\\" stands for? I am speaking about slide 5 from the presentation \\"summarizing categorical data\\"\\n"]	0	f	f	\N	2022-10-24 14:06:13.089336	f	[]	[{"dep":["nmod"],"text":"group"},{"dep":["pobj"],"text":"slide"},{"dep":["pobj"],"text":"presentation"},{"dep":["dobj"],"text":"datum"}]	0	0	0	AUTO		f	\N	\N	\N	0
2acea012-d293-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	4/1	t	["yes, we used the simulations to test the null hypothesis.\\n\\nthe data is from our sample. for this sample we computed, that there is quite a difference between the promotion proportion of male and female applications. but the question then is, can this difference be generalised to the whole population, saying that there is gender discrimination, or not. this we tried to answer with a hypothesis test.\\n"]	0	f	f	\N	2022-10-24 14:06:13.099852	f	[]	[{"dep":["dobj"],"text":"simulation"},{"dep":["dobj"],"text":"hypothesis"},{"dep":["nsubj"],"text":"data"},{"dep":["pobj"],"text":"sample"},{"dep":["nsubjpass","attr"],"text":"difference"},{"dep":["pobj"],"text":"application"},{"dep":["nsubj"],"text":"question"},{"dep":["pobj"],"text":"population"},{"dep":["attr"],"text":"gender discrimination"},{"dep":["pobj"],"text":"hypothesis test"}]	0	0	0	AUTO		f	\N	c85ef5ee-d28d-11ec-b02a-0242ac190004	\N	1
a2924c9a-d2b4-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	143	t	["Is that even representative without any control group which doesn't show any gender? \\n"]	0	f	f	\N	2022-10-24 14:06:13.10613	f	[]	[{"dep":["pobj"],"text":"control group"},{"dep":["dobj"],"text":"gender"}]	0	0	0	AUTO		f	\N	\N	\N	0
73171de8-d533-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	144	t	["Im confused. Slide 36 says P(C|+) say = 0.12. On your solution for the next problem it said the same expression equals 0.52\\n"]	0	f	f	\N	2022-10-24 14:06:13.114102	f	[]	[{"dep":["nsubj"],"text":"slide"},{"dep":["pobj"],"text":"solution"},{"dep":["pobj"],"text":"problem"},{"dep":["nsubj"],"text":"expression"}]	2	2	0	AUTO		f	\N	\N	\N	0
cde0a5ce-d505-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	142/1	t	[{"attributes":{"link":"https://chat.tum.de/group/jWM2AZso4ps2PMzfD?msg=TKC53hjsA8A33Dr2z"},"insert":"https://chat.tum.de/group/jWM2AZso4ps2PMzfD?msg=TKC53hjsA8A33Dr2z"},"\\n"]	0	f	f	\N	2022-10-24 14:06:13.128592	f	[]	[]	0	0	0	AUTO		f	\N	638eb1ea-d2f1-11ec-a5fc-0242ac190004	\N	1
4c7e8ed8-06ab-11ed-bbd1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	145	t	["0.05\\n"]	0	f	f	\N	2022-10-24 14:06:13.139543	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-18 15:07:14.039	0
2cf8ba0a-d548-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	144/1	t	["the difference is, that on slide 36 the woman had one positive test result whereas on slide 38 she already had to positive test results. having two positive test results will of course increase the probability of having cancer\\n"]	0	f	f	\N	2022-10-24 14:06:13.148767	f	[]	[{"dep":["nsubj"],"text":"difference"},{"dep":["pobj"],"text":"slide"},{"dep":["nsubj"],"text":"woman"},{"dep":["nummod"],"text":"one"},{"dep":["pobj","nsubj","dobj"],"text":"test result"},{"dep":["nummod"],"text":"two"},{"dep":["pobj"],"text":"course"},{"dep":["dobj"],"text":"probability"},{"dep":["dobj"],"text":"cancer"}]	0	0	0	AUTO		f	\N	73171de8-d533-11ec-b02a-0242ac190004	\N	1
c1156f8c-d528-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	146	t	["roll a DICE*\\n"]	0	f	f	\N	2022-10-24 14:06:13.154173	f	[]	[]	-4	0	4	AUTO		f	\N	\N	2022-05-16 15:22:02.003	0
97c10dc0-d533-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	147	t	[" I feel like you're ignoring most of the questions on here\\n"]	0	f	f	\N	2022-10-24 14:06:13.160353	f	[]	[{"dep":["pobj"],"text":"question"}]	0	0	0	AUTO		f	\N	\N	2022-05-16 16:17:10.404	0
6a4c1994-d532-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	148	t	["Page 36 doesn‘t really make sense. „Patient HAD cancer“ means they had it in the past. „Patient DOES NOT have cancer“ means they do not have it in the present. Two facts that aren‘t necessarily opposites. Why do we compare them?\\n"]	0	f	f	\N	2022-10-24 14:06:13.179915	f	[]	[{"dep":["nsubj"],"text":"page"},{"dep":["dobj"],"text":"sense"},{"dep":["ROOT"],"text":"„ Patient"},{"dep":["nsubj","dobj"],"text":"cancer"},{"dep":["pobj"],"text":"past"},{"dep":["nsubj"],"text":"Patient"},{"dep":["pobj"],"text":"present"},{"dep":["nummod"],"text":"Two"},{"dep":["ROOT"],"text":"fact"}]	0	0	0	AUTO		f	\N	\N	\N	0
629fe8f0-d547-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	148/1	t	["correct, this is a typo\\n"]	0	f	f	\N	2022-10-24 14:06:13.190949	f	[]	[{"dep":["attr"],"text":"typo"}]	0	0	0	AUTO		f	\N	6a4c1994-d532-11ec-a5cb-0242ac190004	\N	1
98e62eec-d533-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	149	t	["why does the following calculation fail:\\n\\nprobability of 2 false positives = 0.1^2 => Woman has cancer with probability of 1 - 0.01 =  0.99\\n\\nbecause we assume that test1 and test2 are independent although they might not be?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:13.198682	f	[]	[{"dep":["pobj","appos"],"text":"probability"},{"dep":["pobj"],"text":"positive"},{"dep":["nsubj"],"text":"woman"},{"dep":["dobj"],"text":"cancer"},{"dep":["nsubj"],"text":"test1"}]	0	0	0	AUTO		f	\N	\N	\N	0
01c2615a-d52b-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	150	t	["What does \\"they agree with their parents' political views\\" mean in this context?\\n"]	0	f	f	\N	2022-10-24 14:06:13.206151	f	[]	[{"dep":["poss"],"text":"parent"},{"dep":["pobj"],"text":"view"},{"dep":["pobj"],"text":"context"}]	0	0	0	AUTO		f	\N	\N	2022-05-16 20:22:50.743	0
a30816d4-d52d-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	151	t	["what is the difference between disjoint and indipendent?\\n"]	0	f	f	\N	2022-10-24 14:06:13.213129	f	[]	[{"dep":["nsubj"],"text":"difference"},{"dep":["pobj"],"text":"disjoint"},{"dep":["conj"],"text":"indifferent"}]	1	1	0	AUTO		f	\N	\N	2022-05-16 15:47:49.74	0
ad8b3df6-d547-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	134/1	t	["i don't think so. the last two questions e.g. were posted at 18:15. so no wonder i did not see them before the end of the lecture.\\n\\n"]	0	f	f	\N	2022-10-24 14:06:13.22145	f	[]	[{"dep":["nummod"],"text":"two"},{"dep":["nsubjpass"],"text":"question"},{"dep":["ROOT"],"text":"wonder"},{"dep":["pobj"],"text":"end"},{"dep":["pobj"],"text":"lecture"}]	1	1	0	AUTO		f	\N	b2175468-d533-11ec-a971-0242ac190004	\N	1
16c1a7ba-d511-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	152	t	["What is the difference between Independence and disjoint?\\n"]	0	f	f	\N	2022-10-24 14:06:13.227744	f	[]	[{"dep":["nsubj"],"text":"difference"},{"dep":["pobj"],"text":"independence"},{"dep":["conj"],"text":"disjoint"}]	1	1	0	AUTO		f	\N	\N	\N	0
b49d597c-d52d-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	153	t	["I think he didn't mean the gun-ownership quiz, but the one before...\\n"]	0	f	f	\N	2022-10-24 14:06:13.234978	f	[]	[{"dep":["compound"],"text":"gun"},{"dep":["conj"],"text":"one"}]	0	0	0	AUTO		f	\N	\N	2022-05-16 20:22:31.561	0
ace4ca06-d52f-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	154	t	["What is the answer to the quiz on Slide 13?\\n"]	0	f	f	\N	2022-10-24 14:06:13.241376	f	[]	[{"dep":["nsubj"],"text":"answer"},{"dep":["pobj"],"text":"quiz"}]	1	1	0	AUTO		f	\N	\N	\N	0
1ea79594-d50b-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	155	t	["how many points do we need for the bonus in the end \\n"]	0	f	f	\N	2022-10-24 14:06:13.24802	f	[]	[{"dep":["dobj"],"text":"point"},{"dep":["pobj"],"text":"bonus"},{"dep":["pobj"],"text":"end"}]	0	0	0	AUTO		f	\N	\N	\N	0
d1c46c06-d51f-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	152/1	t	["you need to open the correct frag.jetzt room:\\n\\nkey for Course A: sba-2022a; key for Course B: sba-2022b\\n\\nif you want an answer during the lecture.\\n\\n\\nthink about rolling a die. then the events A={1,3,5} and B={2,4,6} are disjoint. but \\n\\n",{"formula":"P(A|B)=0"},"\\n\\nsince if you know that the outcome was even, then the probability of an odd number is 0, and therefore A and B are not independent.\\n"]	0	f	f	\N	2022-10-24 14:06:13.256412	f	[]	[]	0	0	0	AUTO		f	\N	16c1a7ba-d511-11ec-a5fc-0242ac190004	\N	1
816f8f08-d52c-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	156	t	["What's the solution to the Republican/Democrat quiz now? I didn't quite catch it (in Zoom)\\n"]	0	f	f	\N	2022-10-24 14:06:13.263424	f	[]	[{"dep":["nsubj"],"text":"solution"},{"dep":["nmod"],"text":"Republican"},{"dep":["compound"],"text":"Democrat"},{"dep":["pobj"],"text":"quiz"},{"dep":["pobj"],"text":"Zoom"}]	1	1	0	AUTO		f	\N	\N	2022-05-16 20:22:44.501	0
b0cbfc2c-d52d-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	157	t	["Can you explain again the result of the quiz on slide 16?\\n"]	0	f	f	\N	2022-10-24 14:06:13.270419	f	[]	[{"dep":["dobj"],"text":"result"},{"dep":["pobj"],"text":"quiz"},{"dep":["pobj"],"text":"slide"}]	0	0	0	AUTO		f	\N	\N	2022-05-16 20:22:36.656	0
928a3914-d806-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	158	t	["Could you please show the solution of slide 38 once again? (If you want, also at the end of the zoom meeting)\\n"]	0	f	f	\N	2022-10-24 14:06:13.277909	f	[]	[{"dep":["dobj"],"text":"solution"},{"dep":["pobj"],"text":"slide"},{"dep":["pobj"],"text":"end"},{"dep":["pobj"],"text":"zoom meeting"}]	1	1	0	AUTO		f	\N	\N	\N	0
bf0a6990-d802-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	159	t	["excuse me, the audio in zoom is quite rough, maybe you could check your microphone.  thank you very much\\n"]	0	f	f	\N	2022-10-24 14:06:13.285209	f	[]	[{"dep":["nsubj"],"text":"audio"},{"dep":["pobj"],"text":"zoom"},{"dep":["dobj"],"text":"microphone"}]	3	3	0	AUTO		f	\N	\N	2022-05-20 08:46:11.738	0
19e70aa6-d80a-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	135/1	t	["yes, but if it is an ace we win more. therefore, the ace is considered in the second case\\n"]	0	f	f	\N	2022-10-24 14:06:13.292187	f	[]	[{"dep":["nsubjpass","attr"],"text":"ace"},{"dep":["amod"],"text":"second"},{"dep":["pobj"],"text":"case"}]	0	0	0	AUTO		f	\N	a81eafc6-d806-11ec-9e13-0242ac190004	\N	1
13df2b0c-d805-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	160	t	["audio seems better, thank you\\n"]	0	f	f	\N	2022-10-24 14:06:13.297803	f	[]	[{"dep":["nsubj"],"text":"audio"}]	0	0	0	AUTO		f	\N	\N	2022-05-20 06:22:38.828	0
47c2cb16-d80c-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	161	t	["Could you please state the changes which you make on the slides when you upload it again? That would make it a lot easier for us to see the changes.\\n"]	0	f	f	\N	2022-10-24 14:06:13.304876	f	[]	[{"dep":["dobj"],"text":"change"},{"dep":["pobj"],"text":"slide"},{"dep":["npadvmod"],"text":"lot"}]	0	0	0	AUTO		f	\N	\N	\N	0
a746ec78-d80e-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	162	t	["0\\n"]	1	f	f	\N	2022-10-24 14:06:13.310678	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
0d559f36-d81a-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	158/1	t	["the solution can be computed using a tree diagram, or just by applying Bayes' theorem. let's try using Bayes' theorem\\n\\n",{"formula":"\\\\begin{aligned} P(inf | +) &= \\\\frac{P(inf \\\\cap +)}{P(+)}  = \\\\frac{P(+|inf) P(inf)}{P(+|sus) P(sus)+ P(+|inf) P(inf)+P(+|red) P(rec)}\\\\\\\\ & = \\\\frac{0.99 \\\\cdot 0.1 }{0.05 \\\\cdot 0.6 + 0.99 \\\\cdot 0.1 + 0.35 \\\\cdot 0.3}\\\\\\\\ &= \\\\frac{0.099}{0.03 + 0.099 + 0.105} \\\\approx 0.423  \\\\end{aligned}"},"\\n"]	0	f	f	\N	2022-10-24 14:06:13.32054	f	[]	[{"dep":["nsubjpass"],"text":"solution"},{"dep":["dobj"],"text":"tree diagram"},{"dep":["poss"],"text":"Bayes"},{"dep":["dobj"],"text":"theorem"}]	0	0	0	AUTO		f	\N	928a3914-d806-11ec-9e13-0242ac190004	\N	1
c5491f0a-d806-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	135/2	t	["Ok, thanks\\n\\n"]	0	f	f	\N	2022-10-24 14:06:13.326016	f	[]	[{"dep":["ROOT"],"text":"thank"}]	0	0	0	AUTO		f	\N	a81eafc6-d806-11ec-9e13-0242ac190004	\N	1
cc614800-d804-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	163	t	["My zoom meeting only displays a white screen, but i get audio\\nAny suggestions for solving this ?\\n"]	0	f	f	\N	2022-10-24 14:06:13.332569	f	[]	[{"dep":["nsubj"],"text":"zoom meeting"},{"dep":["dobj"],"text":"screen"},{"dep":["ROOT"],"text":"suggestion"}]	0	0	0	AUTO		f	\N	\N	2022-05-20 08:46:06.1	0
384d98be-d807-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	164	t	["What is 0.81 here? Can you explain that. Thank you!\\n\\n"]	0	f	f	\N	2022-10-24 14:06:13.338928	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-05-20 06:52:53.559	0
bdec2498-d818-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	158/2	t	["the solution can be computed using a tree diagram, or just by applying Bayes' theorem. let's try using Bayes' theorem\\n\\n",{"formula":"\\\\begin{aligned} P(inf | +) & = \\\\frac{P(inf \\\\cap +)}{P(+)} = \\\\frac{P(+ |inf) P(inf)}{P(+ |sus) P(sus)+ P(+ |inf) P(inf)  + P(+ |rec) P(rec)} \\\\\\\\ & =  \\\\frac{0.099}{0.03 + 0.099 + 0.105} \\\\approx 0.423\\\\end{aligned} "},"\\n\\nwhere we used the probabilities $P(+|inf)=0.99$ \\n"]	0	f	f	\N	2022-10-24 14:06:13.349033	f	[]	[{"dep":["nsubjpass"],"text":"solution"},{"dep":["dobj"],"text":"tree diagram"},{"dep":["poss"],"text":"Bayes"},{"dep":["dobj"],"text":"theorem"},{"dep":["dobj"],"text":"probability"}]	0	0	0	AUTO		f	\N	928a3914-d806-11ec-9e13-0242ac190004	2022-05-20 08:51:42.013	1
15b96d8c-d82f-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	165	t	["What is the difference between Independence and disjoint?\\n"]	0	f	f	\N	2022-10-24 14:06:13.356098	f	[]	[{"dep":["nsubj"],"text":"difference"},{"dep":["pobj"],"text":"independence"},{"dep":["conj"],"text":"disjoint"}]	0	0	0	AUTO		f	\N	\N	2022-05-24 07:12:23.221	0
71749e88-d831-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	166	t	["could ou guys be quiet please\\n"]	0	f	f	\N	2022-10-24 14:06:13.362514	f	[]	[{"dep":["ROOT"],"text":"could"},{"dep":["amod"],"text":"please"}]	0	0	0	AUTO		f	\N	\N	2022-05-24 07:12:18.24	0
59805068-dd83-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	183	t	["Could you upload the slides of part II of this week's lecture?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:13.553894	f	[]	[{"dep":["dobj"],"text":"slide"},{"dep":["compound"],"text":"part"},{"dep":["poss"],"text":"week"},{"dep":["pobj"],"text":"lecture"}]	0	0	0	AUTO		f	\N	\N	2022-05-27 07:46:10.062	0
05ed0352-daad-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	167	t	["Shouldn't we be able to effectively compare distributions of the same kind with a Z-Score (and not just normal distributions)?\\n"]	0	f	f	\N	2022-10-24 14:06:13.370009	f	[]	[{"dep":["npadvmod","dobj"],"text":"distribution"},{"dep":["pobj"],"text":"kind"},{"dep":["pobj"],"text":"score"}]	1	1	0	AUTO		f	\N	\N	2022-05-24 07:11:55.77	0
0d70000a-daaf-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	168	t	["Whats the difference between a propability function and a density function?\\n"]	0	f	f	\N	2022-10-24 14:06:13.377177	f	[]	[{"dep":["nsubj"],"text":"difference"},{"dep":["conj"],"text":"density function"}]	0	0	0	AUTO		f	\N	\N	2022-05-24 07:11:47.73	0
178fef84-dab2-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	169	t	["Can we use R during the exam and will the exam be open book?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:13.383532	f	[]	[{"dep":["pobj","nsubj"],"text":"exam"},{"dep":["attr"],"text":"book"}]	0	0	0	AUTO		f	\N	\N	\N	0
97a3758e-db33-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	169/1	t	["no. it's an exam using pen and paper. it will be no open book exam, but you will get a cheat sheet before the end of the lecture time.\\n"]	0	f	f	\N	2022-10-24 14:06:13.40675	f	[]	[{"dep":["attr"],"text":"exam"},{"dep":["dobj"],"text":"pen"},{"dep":["conj"],"text":"paper"},{"dep":["attr"],"text":"book exam"},{"dep":["dobj"],"text":"cheat sheet"},{"dep":["pobj"],"text":"end"},{"dep":["pobj"],"text":"time"}]	0	0	0	AUTO		f	\N	178fef84-dab2-11ec-a971-0242ac190004	\N	1
91f59154-dab0-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	170	t	["how we get 93,12\\n"]	0	f	f	\N	2022-10-24 14:06:13.415924	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-05-24 07:17:10.117	0
df6e9868-dab0-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	171	t	["Is pnorm ((36.2 - 36) / 0.11) - 0.0343795 also a valid method?\\n"]	0	f	f	\N	2022-10-24 14:06:13.432527	f	[]	[{"dep":["attr"],"text":"method"}]	0	0	0	AUTO		f	\N	\N	\N	0
36adc60a-dae1-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	172	t	["Can you please explain how you came up with the solution of -0.1 on page 15 of probability part II (lecture of last Friday)?\\n"]	0	f	f	\N	2022-10-24 14:06:13.440727	f	[]	[{"dep":["pobj"],"text":"solution"},{"dep":["pobj"],"text":"page"},{"dep":["pobj"],"text":"probability part ii"},{"dep":["appos"],"text":"lecture"},{"dep":["pobj"],"text":"last Friday"}]	0	0	0	AUTO		f	\N	\N	\N	0
7d78c486-eb30-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	173	t	["What is the difference between standard error and standard diviation\\n"]	0	f	f	\N	2022-10-24 14:06:13.44754	f	[]	[{"dep":["nsubj"],"text":"difference"},{"dep":["pobj"],"text":"error"}]	0	0	0	AUTO		f	\N	\N	\N	0
dac632e4-dab0-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	174	t	["What do you mean exactly, with \\"we used the Z-Score to apply the normal distribution\\"?\\n"]	0	f	f	\N	2022-10-24 14:06:13.46161	f	[]	[{"dep":["dobj"],"text":"the Z - Score"},{"dep":["dobj"],"text":"distribution"}]	1	1	0	AUTO		f	\N	\N	\N	0
5b0d1d70-daaf-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	175	t	["Why would we need the Z-Score in the Ketchup example? (Since we're not comparing any distribution with another...?)\\n"]	0	f	f	\N	2022-10-24 14:06:13.473643	f	[]	[{"dep":["dobj"],"text":"the Z - Score"},{"dep":["compound"],"text":"Ketchup"},{"dep":["pobj"],"text":"example"},{"dep":["dobj"],"text":"distribution"}]	1	1	0	AUTO		f	\N	\N	\N	0
6cfa9990-daaf-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	176	t	["Why we have here the mean is zero and sd equals to one.?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:13.480362	f	[]	[{"dep":["nsubj"],"text":"mean"},{"dep":["attr"],"text":"zero"}]	0	0	0	AUTO		f	\N	\N	\N	0
efa4c256-db33-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	172/1	t	["Remember, in this game we either win 500 (red and ace of clubs) or nothing. Let $X$ be the profit with values 495 and -5. Then we get\\n\\n",{"formula":"\\\\begin{aligned}  E(X) & = 495 P(X = 495) - 5 P(X = -5) \\\\\\\\  & = 495 \\\\cdot \\\\frac{26}{52} \\\\cdot \\\\frac{1}{51} - 5\\\\cdot (1- \\\\frac{26}{52} \\\\cdot \\\\frac{1}{51}) = -0.1  \\\\end{aligned}"},"\\n\\n"]	0	f	f	\N	2022-10-24 14:06:13.489543	f	[]	[{"dep":["pobj"],"text":"game"},{"dep":["dobj"],"text":"red"},{"dep":["conj"],"text":"ace"},{"dep":["pobj"],"text":"club"},{"dep":["attr"],"text":"profit"},{"dep":["pobj"],"text":"value"}]	0	0	0	AUTO		f	\N	36adc60a-dae1-11ec-90ff-0242ac190004	\N	1
2f22c21a-daa9-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	177	t	["Can you guys please start recording the lectures, its just not fair because we don't  even get past lectures\\n"]	0	f	f	\N	2022-10-24 14:06:13.496474	f	[]	[{"dep":["nsubj"],"text":"guy"},{"dep":["pobj","dobj"],"text":"lecture"}]	0	0	0	AUTO		f	\N	\N	2022-05-24 07:12:11.735	0
7077d310-db33-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	171/1	t	["the value [object Object] is the z score of the upper limit 36.2. if you now compute \\n\\npnorm( (36.2- 36) / 0.11)\\n\\nyou are computing the probability\\n\\n",{"formula":"P(\\\\text{amount of ketchup at most 36.2}) = \\\\int_{-\\\\infty}^{0.2/0.11} f(x)dx"},"\\n\\nusing the density f(x) from the standard normal distribution. but the probability of passing the test is of course\\n\\n",{"formula":"P(\\\\text{amount of ketchup between 35.8 and 36.2}) = \\\\int_{-\\\\infty}^{0.2/0.11} f(x)dx - \\\\int_{-\\\\infty}^{-0.2/0.11} f(x)dx = \\\\int_{-0.2/0.11}^{0.2/0.11} f(x)dx"},"\\n\\nsince we have ",{"formula":"\\\\int_{-\\\\infty}^{-0.2/0.11} f(x)dx = 0.0343795"},", your code is perfectly fine\\n\\n"]	0	f	f	\N	2022-10-24 14:06:13.5092	f	[]	[{"dep":["nsubj"],"text":"value"},{"dep":["appos"],"text":"object Object"},{"dep":["attr"],"text":"z score"},{"dep":["pobj"],"text":"limit"},{"dep":["nsubj","dobj"],"text":"probability"},{"dep":["compound"],"text":"density"},{"dep":["pobj"],"text":"distribution"},{"dep":["dobj"],"text":"test"},{"dep":["pobj"],"text":"course"},{"dep":["nsubj"],"text":"code"}]	0	0	0	AUTO		f	\N	df6e9868-dab0-11ec-90ff-0242ac190004	\N	1
f2b7d67e-dd84-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	178	t	["What means in pages 23 aes(heights) in ggplot(df, aes(heights)) ?\\n"]	0	f	f	\N	2022-10-24 14:06:13.515897	f	[]	[{"dep":["nsubj","appos"],"text":"aes(heights"},{"dep":["pobj"],"text":"ggplot(df"},{"dep":["pobj"],"text":"page"}]	0	0	0	AUTO		f	\N	\N	2022-05-27 06:20:40.282	0
3373ce4e-dd89-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	179	t	["could you explain slide 26 again?\\n"]	0	f	f	\N	2022-10-24 14:06:13.521903	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
49440d54-dd86-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	180	t	["Concerning bin widths. I feel like there are no clear criteria on how to choose them, at least for this course. For example, you talked about precision but in the homework you opted for the larger value of binwidth. I understand there is a tradeoff between precision and readability, but for me personally I thought the value of 1000 was more precise.\\n"]	0	f	f	\N	2022-10-24 14:06:13.53254	f	[]	[{"dep":["compound"],"text":"bin"},{"dep":["pobj"],"text":"width"},{"dep":["attr"],"text":"criterion"},{"dep":["pobj"],"text":"course"},{"dep":["pobj"],"text":"example"},{"dep":["pobj"],"text":"precision"},{"dep":["pobj"],"text":"homework"},{"dep":["pobj","nsubj"],"text":"value"},{"dep":["attr"],"text":"tradeoff"},{"dep":["conj"],"text":"readability"}]	0	0	0	AUTO		f	\N	\N	2022-05-27 07:45:23.092	0
9ce68254-dd89-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	181	t	["How do we derive the variance for a geometric distribution?\\n"]	0	f	f	\N	2022-10-24 14:06:13.539531	f	[]	[{"dep":["dobj"],"text":"variance"},{"dep":["pobj"],"text":"distribution"}]	0	0	0	AUTO		f	\N	\N	2022-05-27 07:45:09.765	0
f151c1de-dd82-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	182	t	["Can you upload the answer of ECP for week4\\n"]	0	f	f	\N	2022-10-24 14:06:13.54651	f	[]	[{"dep":["dobj"],"text":"answer"},{"dep":["pobj"],"text":"ecp"}]	0	0	0	AUTO		f	\N	\N	2022-05-27 06:05:17.125	0
863fbbac-eb2f-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	236	t	["can you explain again why we chose 0.85 and not 0.80?\\n"]	0	f	f	\N	2022-10-24 14:06:14.119233	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-13 16:29:57.676	0
e81c6ae4-dd85-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	184	t	["Could you please explain the page 23 ggplot(df, aes(heights)), the heights is x or?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:13.561086	f	[]	[{"dep":["dobj"],"text":"page"},{"dep":["appos"],"text":"ggplot(df"},{"dep":["appos"],"text":"aes(heights"},{"dep":["nsubj"],"text":"height"}]	0	0	0	AUTO		f	\N	\N	2022-05-27 07:45:29.694	0
7f8aa8ea-dd82-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	185	t	["During the first/second lecture we discussed different types of variables. I want to know is numerical discrete(contineous) variable the same as discrete(contineous) random variable and if there is a difference, can you please explain it to me?\\n"]	0	f	f	\N	2022-10-24 14:06:13.570243	f	[]	[{"dep":["amod"],"text":"first"},{"dep":["amod"],"text":"second"},{"dep":["pobj"],"text":"lecture"},{"dep":["dobj"],"text":"type"},{"dep":["ccomp","pobj","ROOT"],"text":"variable"},{"dep":["attr"],"text":"difference"}]	0	0	0	AUTO		f	\N	\N	2022-05-27 07:46:23.361	0
0fb0e612-dd85-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	186	t	["sorry but please mute yourself when you are coughing in the zoom calll, thank you <3 \\n"]	0	f	f	\N	2022-10-24 14:06:13.57701	f	[]	[]	1	1	0	AUTO		f	\N	\N	2022-05-27 07:45:54.343	0
c514cf5c-dd97-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	179/1	t	["I made an update of the slide, including now also the histogram of the data. but let's try to explain again the shapes\\n\\nright skew: a normal distribution has now lower endpoint, hence it would \\"expect\\" also values smaller than 0 (the value could be different for some other right skewed data), indicated by the values in the left tail of the normal probability plot being above the line. on the other hand, the normal distribution would not expect as large values as contained in the data. therefore, the points in the right tail are way above the values on the line.\\n\\nleft skew: here, the normal distribution would not expect to see values that far to the left of the center (of the distribution). since the values, in the left tail of the data, are smaller than expected, they are below the line. on the other hand, the values in the right tail are not that far away from the center of the distribution, at least not as far as a normal distribution would expect. hence, those values are also below the line.\\n\\nshort tails: the left tail of the data distribution increases slower than the tail of the normal distribution (since the distribution is much more concentrated around the mean). hence, the quantiles (their values) increase much slower than for a normal distribution (in the left tail). but this changes, once we come closer to the center of the distribution. there the quantiles of the data increase much faster. in the right tail, this is then again reversed. but for this particular data example, this is not so prominent. hence, it is also not so obvious to see an s-shape curve.\\n\\nlong tails: here one can combine the explanation from left-skew (in the left tail) and right-skew (in the right tail). since we can combine left- and right-skew, we have again a symmetric distribution.\\n"]	0	f	f	\N	2022-10-24 14:06:13.605975	f	[]	[{"dep":["dobj"],"text":"update"},{"dep":["pobj"],"text":"slide"},{"dep":["ccomp"],"text":"histogram"},{"dep":["pobj"],"text":"datum"},{"dep":["dobj"],"text":"shape"},{"dep":["pobj","nsubj"],"text":"skew"},{"dep":["pobj","nsubj","dobj"],"text":"distribution"},{"dep":["dobj"],"text":"endpoint"},{"dep":["appos","pobj","nsubj","dobj"],"text":"value"},{"dep":["pobj","dep","appos","ROOT"],"text":"tail"},{"dep":["pobj"],"text":"probability plot"},{"dep":["pobj"],"text":"line"},{"dep":["pobj"],"text":"hand"},{"dep":["nsubj"],"text":"point"},{"dep":["advmod"],"text":"way"},{"dep":["pobj"],"text":"left"},{"dep":["pobj"],"text":"center"},{"dep":["compound"],"text":"data distribution"},{"dep":["pobj"],"text":"mean"},{"dep":["nsubj"],"text":"quantile"},{"dep":["dobj"],"text":"shape curve"},{"dep":["dobj"],"text":"explanation"},{"dep":["amod"],"text":"right"},{"dep":["dobj"],"text":"left-"}]	0	0	0	AUTO		f	\N	3373ce4e-dd89-11ec-a5fc-0242ac190004	\N	1
371e7bf2-dd8e-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	187	t	["why do we subtract / add the standard deviation twice?\\n"]	0	f	f	\N	2022-10-24 14:06:13.611844	f	[]	[{"dep":["dobj"],"text":"deviation"}]	0	0	0	AUTO		f	\N	\N	\N	0
00e4bec8-dea3-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	188	t	["Hello, I have a question regarding exercise 1.1: why is upscale bathroom equipment an ordinal variable?\\n"]	0	f	f	\N	2022-10-24 14:06:13.618697	f	[]	[{"dep":["dobj"],"text":"question"},{"dep":["pobj"],"text":"exercise"},{"dep":["nsubj"],"text":"variable"}]	0	0	0	AUTO		f	\N	\N	\N	0
2b2740a0-dd83-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	189	t	["Can you upload the solution of ECP for week 4\\n"]	0	f	f	\N	2022-10-24 14:06:13.625731	f	[]	[{"dep":["dobj"],"text":"solution"},{"dep":["pobj"],"text":"ecp"},{"dep":["pobj"],"text":"week 4"}]	0	0	0	AUTO		f	\N	\N	2022-05-27 07:46:14.529	0
d516e782-dd83-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	190	t	["Can you explain how to find the proper / good bin width please\\n"]	0	f	f	\N	2022-10-24 14:06:13.632403	f	[{"text":"histogram","dep":["ROOT"]}]	[]	0	0	0	AUTO		f	\N	\N	2022-05-27 07:46:05.299	0
0157b2f8-dd8f-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	191	t	["Can you explain the first factor in rbinom()?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:13.638805	f	[]	[{"dep":["amod"],"text":"first"},{"dep":["dobj"],"text":"factor"}]	0	0	0	AUTO		f	\N	\N	\N	0
8d7b59d2-e00b-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	192	t	["Can someone mute Jasmin in the Zoom session? Audio is getting messed up\\n"]	0	f	f	\N	2022-10-24 14:06:13.647235	f	[{"text":"Audio","dep":["nsubjpass"]},{"text":"Zoom","dep":["compound"]}]	[{"dep":["dobj"],"text":"Jasmin"},{"dep":["compound"],"text":"Zoom"},{"dep":["pobj"],"text":"session"},{"dep":["nsubjpass"],"text":"Audio"}]	1	1	0	AUTO		f	\N	\N	2022-05-30 16:32:15.322	0
7a93c148-dd85-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	193	t	["Could you explain the difference between the short and long tail plots? They seem very similar to me.\\n"]	0	f	f	\N	2022-10-24 14:06:13.654195	f	[]	[{"dep":["dobj"],"text":"difference"},{"dep":["pobj"],"text":"tail plot"}]	0	0	0	AUTO		f	\N	\N	2022-05-27 07:45:38.228	0
2fdf0a64-dd89-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	194	t	["What is the difference between binomial distribution and geometric distribution?\\n"]	0	f	f	\N	2022-10-24 14:06:13.661157	f	[]	[{"dep":["nsubj"],"text":"difference"},{"dep":["conj","pobj"],"text":"distribution"}]	0	0	0	AUTO		f	\N	\N	2022-05-27 06:54:51.114	0
75b88572-dd90-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	187/1	t	["we defined unusual observations to be 2 standard deviations away from the mean, see slide 45\\n"]	0	f	f	\N	2022-10-24 14:06:13.668856	f	[]	[{"dep":["dobj"],"text":"observation"},{"dep":["npadvmod"],"text":"deviation"},{"dep":["pobj"],"text":"mean"},{"dep":["dobj"],"text":"slide"}]	0	0	0	AUTO		f	\N	371e7bf2-dd8e-11ec-a6d5-0242ac190004	\N	1
fb24cfe8-ee04-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	195	t	["If we write mu=0 or we dont write mu , it is the same?\\n"]	0	f	f	\N	2022-10-24 14:06:13.680453	f	[]	[{"dep":["punct"],"text":"mu=0"}]	0	0	0	AUTO		f	\N	\N	2022-06-17 08:35:42.39	0
5d16bdf6-ee0a-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	196	t	["c\\n"]	0	f	f	\N	2022-10-24 14:06:13.688642	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-17 08:34:55.107	0
e1d62426-dd90-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	191/1	t	["if you take a look at the help of rbinom it says\\n\\nrbinom(n, size, prob)\\n\\nthis is now a bit confusing, since in our definition we called the size n an the prob argument is the success probability p. \\n\\nbut when you take a look at the description of the arguments, it says that n is number of observations. so, we are generating n=300 observations from a binomial distribution with parameters size = 30 and success probability prob=0.4\\n\\n"]	0	f	f	\N	2022-10-24 14:06:13.70319	f	[]	[{"dep":["dobj"],"text":"look"},{"dep":["pobj"],"text":"help"},{"dep":["nmod"],"text":"size"},{"dep":["dobj"],"text":"prob"},{"dep":["npadvmod"],"text":"bit"},{"dep":["pobj"],"text":"definition"},{"dep":["attr"],"text":"success probability p."},{"dep":["pobj"],"text":"description"},{"dep":["pobj"],"text":"argument"},{"dep":["attr"],"text":"number"},{"dep":["pobj","dobj"],"text":"observation"},{"dep":["nummod"],"text":"n=300"},{"dep":["pobj"],"text":"distribution"},{"dep":["pobj"],"text":"parameter size"},{"dep":["conj"],"text":"success probability"},{"dep":["ROOT"],"text":"prob=0.4"}]	0	0	0	AUTO		f	\N	0157b2f8-dd8f-11ec-a5cb-0242ac190004	\N	1
ab0de5a6-eb27-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	237	t	["The exercise solution from last week was not uploaded.\\n"]	0	f	f	\N	2022-10-24 14:06:14.126116	f	[]	[{"dep":["pobj"],"text":"last week"}]	0	0	0	AUTO		f	\N	\N	2022-06-13 16:29:20.196	0
65213486-e022-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	188/1	t	["the description of the variable (upscale) already implies an ordering. therefore, we could (objectively) argue, that it is not just nominal, but also ordinal\\n"]	0	f	f	\N	2022-10-24 14:06:13.711237	f	[]	[{"dep":["nsubj"],"text":"description"},{"dep":["amod"],"text":"variable"},{"dep":["pobj"],"text":"upscale"},{"dep":["dobj"],"text":"ordering"}]	0	0	0	AUTO		f	\N	00e4bec8-dea3-11ec-a971-0242ac190004	\N	1
246a56d6-e016-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	197	t	["could you again show and explain the solution for the quiz/practice on slide 37 and 38 from last week where a women with cancer wants to get tested a second time? thanks\\n"]	0	f	f	\N	2022-10-24 14:06:13.725476	f	[]	[{"dep":["dobj"],"text":"solution"},{"dep":["nmod"],"text":"quiz"},{"dep":["pobj"],"text":"practice"},{"dep":["pobj"],"text":"slide"},{"dep":["pobj"],"text":"last week"},{"dep":["nsubj"],"text":"woman"},{"dep":["pobj"],"text":"cancer"},{"dep":["amod"],"text":"second"},{"dep":["npadvmod"],"text":"time"},{"dep":["ROOT"],"text":"thank"}]	0	0	0	AUTO		f	\N	\N	\N	0
da25f78a-e027-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	198	t	["Question about the homework peer review 1: what happens if I do the homework but don't correct other's homework? Do I get 0 points for the whole homework?\\n"]	0	f	f	\N	2022-10-24 14:06:13.733602	f	[]	[{"dep":["ROOT"],"text":"question"},{"dep":["pobj"],"text":"homework peer review"},{"dep":["pobj","dobj"],"text":"homework"},{"dep":["dobj"],"text":"point"}]	-1	0	1	AUTO		f	\N	\N	\N	0
a99745dc-e028-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	199	t	["Where are we computing all of the different probabilities again? (If we weren't to approximate it)\\n"]	0	f	f	\N	2022-10-24 14:06:13.749854	f	[]	[{"dep":["pobj"],"text":"probability"}]	1	1	0	AUTO		f	\N	\N	2022-05-30 16:31:14.689	0
d56e0a76-e02b-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	200	t	["Is not the variance of constant(parameter) equal to zero?\\n"]	0	f	f	\N	2022-10-24 14:06:13.75719	f	[]	[{"dep":["attr"],"text":"variance"},{"dep":["pobj"],"text":"constant(parameter"},{"dep":["pobj"],"text":"zero"}]	0	0	0	AUTO		f	\N	\N	2022-05-30 16:30:07.196	0
5d64b210-e02a-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	201	t	["Can, in theory, the k-th success already be observed at an earlier trial (> n), or does it always have to be exactly on the n-th trial?\\n"]	0	f	f	\N	2022-10-24 14:06:13.764833	f	[]	[{"dep":["pobj"],"text":"theory"},{"dep":["pobj"],"text":"trial"}]	1	1	0	AUTO		f	\N	\N	\N	0
2b3a569a-e02b-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	202	t	["Could you please repeat when do we use pnbinom and when dnbinom?\\n"]	0	f	f	\N	2022-10-24 14:06:13.779659	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-05-30 16:30:12.859	0
512ae4ee-e032-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	203	t	["Is the n the number of balls or observations ?\\nwhy we compute that again, we knew it already\\n\\n"]	0	f	f	\N	2022-10-24 14:06:13.786526	f	[]	[{"dep":["ROOT"],"text":"number"},{"dep":["pobj"],"text":"ball"},{"dep":["conj"],"text":"observation"}]	0	0	0	AUTO		f	\N	\N	\N	0
80980b76-e02d-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	204	t	["Can you repeat the what is size in dnbinom and pbinom?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:13.792878	f	[]	[{"dep":["nsubj"],"text":"size"}]	0	0	0	AUTO		f	\N	\N	\N	0
0641e9a6-e036-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	198/1	t	["no. but you are not able to get the 5 points for grading\\n"]	0	f	f	\N	2022-10-24 14:06:13.800102	f	[]	[{"dep":["dobj"],"text":"point"}]	0	0	0	AUTO		f	\N	da25f78a-e027-11ec-a971-0242ac190004	\N	1
15a3d4b8-e0ae-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	205	t	["Hi, sorry for this inconvenience, but does anybody know who I should consult in case of a credit transfer in this module?\\n"]	0	f	f	\N	2022-10-24 14:06:13.806986	f	[]	[{"dep":["pobj"],"text":"inconvenience"},{"dep":["pobj"],"text":"case"},{"dep":["pobj"],"text":"credit transfer"},{"dep":["pobj"],"text":"module"}]	0	0	0	AUTO		f	\N	\N	\N	0
968b3540-e035-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	204/1	t	["for the binomial distribution the size argument in the corresponding R functions is the number of trials we consider, so n\\n\\nfor the negative binomial size is the number of successes we are interested in, so k\\n"]	0	f	f	\N	2022-10-24 14:06:13.815585	f	[]	[{"dep":["pobj"],"text":"distribution"},{"dep":["pobj"],"text":"r function"},{"dep":["attr"],"text":"number"},{"dep":["pobj"],"text":"trial"},{"dep":["pobj"],"text":"success"}]	0	0	0	AUTO		f	\N	80980b76-e02d-11ec-9e13-0242ac190004	\N	1
bfcd91f2-e308-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	206	t	["Can you repeat again what seed does\\n"]	0	f	f	\N	2022-10-24 14:06:13.821287	f	[]	[{"dep":["nsubj"],"text":"seed"}]	0	0	0	AUTO		f	\N	\N	2022-06-03 08:02:10.113	0
64b21d9a-e30f-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	207	t	["Thanks, have a nice weekend too\\n"]	0	f	f	\N	2022-10-24 14:06:13.827728	f	[]	[{"dep":["npadvmod"],"text":"thank"},{"dep":["dobj"],"text":"a nice weekend"}]	0	0	0	AUTO		f	\N	\N	\N	0
a5a7837c-e309-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	206/1	t	["a seed is used to initialize a pseudorandom number generator\\n"]	0	f	f	\N	2022-10-24 14:06:13.84231	f	[]	[{"dep":["nsubjpass"],"text":"seed"},{"dep":["dobj"],"text":"number generator"}]	0	0	0	AUTO		f	\N	bfcd91f2-e308-11ec-8ca6-0242ac190004	\N	1
8244b57a-e30a-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	208	t	["why is the sample distribution unknown\\n"]	0	f	f	\N	2022-10-24 14:06:13.847743	f	[]	[{"dep":["nsubj"],"text":"sample distribution"}]	0	0	0	AUTO		f	\N	\N	\N	0
1095717e-e30c-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	209	t	["I have question regarding slide 43 : Aren't we excluding the value which is located exactly on mean - 2*sd?\\n"]	0	f	f	\N	2022-10-24 14:06:13.855224	f	[]	[{"dep":["dobj"],"text":"question"},{"dep":["pobj"],"text":"slide"},{"dep":["dobj"],"text":"value"},{"dep":["compound"],"text":"mean"},{"dep":["pobj"],"text":"2*sd"}]	0	0	0	AUTO		f	\N	\N	2022-06-03 07:57:31.685	0
b3fd2d16-e307-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	210	t	["What is the difference between ggplot and visualize?\\n"]	0	f	f	\N	2022-10-24 14:06:13.862274	f	[]	[{"dep":["nsubj"],"text":"difference"},{"dep":["pobj"],"text":"ggplot"},{"dep":["conj"],"text":"visualize"}]	0	0	0	AUTO		f	\N	\N	2022-06-03 08:02:14.855	0
7b6f6b42-e309-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	211	t	["set seed(123) means 123 times of same sample?\\n"]	0	f	f	\N	2022-10-24 14:06:13.869435	f	[]	[{"dep":["nsubj"],"text":"seed(123"},{"dep":["dobj"],"text":"time"},{"dep":["pobj"],"text":"sample"}]	0	0	0	AUTO		f	\N	\N	2022-06-03 08:02:05.327	0
fef1fc82-e304-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	212	t	["Can you please upload the answer of tutorial this week?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:13.876427	f	[]	[{"dep":["dobj"],"text":"answer"},{"dep":["pobj"],"text":"tutorial"},{"dep":["npadvmod"],"text":"this week"}]	0	0	0	AUTO		f	\N	\N	2022-06-03 08:02:46.635	0
f1fcf124-e30b-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	213	t	["Is it possible to determine the smallest possible confidence interval for a given value of alpha?\\n"]	0	f	f	\N	2022-10-24 14:06:13.883627	f	[]	[{"dep":["dobj"],"text":"confidence interval"},{"dep":["pobj"],"text":"value"},{"dep":["pobj"],"text":"alpha"}]	0	0	0	AUTO		f	\N	\N	2022-06-03 07:57:38.426	0
89088c7c-e307-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	214	t	["so stat = \\"prop\\" is another way to tell R to calculate the mean?\\n"]	0	f	f	\N	2022-10-24 14:06:13.890353	f	[]	[{"dep":["nsubj"],"text":"prop"},{"dep":["attr"],"text":"way"},{"dep":["dobj"],"text":"mean"}]	0	0	0	AUTO		f	\N	\N	2022-06-03 08:02:22.232	0
62ff8d00-eb36-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	219/1	t	["the p-value is equal to\\n\\npnorm(.85, mean = 0.8, sd = 0.0154, lower.tail = FALSE)\\n## [1] 0.0005836477\\n\\n"]	0	f	f	\N	2022-10-24 14:06:14.084511	f	[]	[{"dep":["nsubj"],"text":"value"}]	0	0	0	AUTO		f	\N	7f8961a8-eb31-11ec-b02a-0242ac190004	\N	1
6c08ef70-e313-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	208/1	t	["in practice the distribution of our sample ",{"formula":"X_1,\\\\dots,X_n"}," is unknown. often one assumes (we will also do this at some later point) a probability distribution for the sample, but the concrete parameter values for that probability distribution are not known, and hence the distribution is not fully known.\\n\\nif we now compute the sample statistic ",{"formula":"T(X)"},", we can't know the distribution (the sampling distribution) of ",{"formula":"T(X)"}," since we do not know the distribution of the sample  ",{"formula":"X = (X_1,\\\\dots,X_n)"},"\\n"]	0	f	f	\N	2022-10-24 14:06:13.902259	f	[]	[{"dep":["pobj"],"text":"practice"},{"dep":["nsubjpass","nsubj","dobj"],"text":"distribution"},{"dep":["pobj"],"text":"sample"},{"dep":["pobj"],"text":"point"},{"dep":["pobj","ROOT"],"text":"probability distribution"},{"dep":["nsubjpass"],"text":"parameter value"},{"dep":["dobj"],"text":"sample statistic"},{"dep":["appos"],"text":"sampling distribution"}]	0	0	0	AUTO		f	\N	8244b57a-e30a-11ec-a971-0242ac190004	\N	1
762bd296-e300-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	205/1	t	[{"attributes":{"link":"https://www.math.cit.tum.de/statistics/lehre/anerkennung-von-lv-ma9712/"},"insert":"https://www.math.cit.tum.de/statistics/lehre/anerkennung-von-lv-ma9712/"},"\\n"]	0	f	f	\N	2022-10-24 14:06:13.908674	f	[]	[]	0	0	0	AUTO		f	\N	15a3d4b8-e0ae-11ec-b02a-0242ac190004	\N	1
e7b5d24e-e306-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	215	t	["audio and internet connections are fine so far for me, the tracking camera is a great feature.\\n\\n"]	1	f	f	\N	2022-10-24 14:06:13.915347	f	[]	[{"dep":["nmod"],"text":"audio"},{"dep":["conj"],"text":"internet connection"},{"dep":["nsubj"],"text":"tracking camera"},{"dep":["attr"],"text":"feature"}]	1	1	0	AUTO		f	\N	\N	\N	0
fcf5656a-e30c-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	209/1	t	["slide 43\\n"]	0	f	f	\N	2022-10-24 14:06:13.921576	f	[]	[]	0	0	0	AUTO		f	\N	1095717e-e30c-11ec-a971-0242ac190004	\N	1
3d095a34-e30e-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	216	t	["what is the difference between type=\\"se\\" and \\"percentile\\"\\n"]	0	f	f	\N	2022-10-24 14:06:13.928037	f	[]	[{"dep":["nsubj"],"text":"difference"},{"dep":["pobj"],"text":"type=\\"se"},{"dep":["conj"],"text":"percentile"}]	0	0	0	AUTO		f	\N	\N	2022-06-03 07:57:00.549	0
eca5f44e-e30d-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	217	t	["How can I take only the lower boundary of per_ci? per_ci[1]?\\n"]	0	f	f	\N	2022-10-24 14:06:13.935085	f	[]	[{"dep":["pobj"],"text":"perci"},{"dep":["ROOT"],"text":"perci[1"}]	0	0	0	AUTO		f	\N	\N	\N	0
b153073c-e345-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	218	t	["Central limit theorem page 35:\\nIt is said that each trial is independent of each other and identically distributed. However, I do not understand as to why the standard deviation changes.\\nIf we think of a die, the roll of a die does not influence the result of the next roll, so it is independent. Also, the probability distribution for each side is 1/6, so it is identically distributed.\\nSo knowing that they are identically distributed why does the SD change with larger sample size change? Thanks\\n"]	0	f	f	\N	2022-10-24 14:06:13.946242	f	[]	[{"dep":["ROOT"],"text":"limit theorem page"},{"dep":["nsubj"],"text":"trial"},{"dep":["pobj"],"text":"die"},{"dep":["pobj","nsubj"],"text":"roll"},{"dep":["dobj"],"text":"result"},{"dep":["nsubj"],"text":"probability distribution"},{"dep":["pobj"],"text":"side"},{"dep":["pobj"],"text":"sample size change"},{"dep":["ROOT"],"text":"thank"}]	0	0	0	AUTO		f	\N	\N	\N	0
7f8961a8-eb31-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	219	t	["slide 14 the p-value which we receive is lower than ?\\n"]	0	f	f	\N	2022-10-24 14:06:13.952554	f	[]	[{"dep":["ROOT"],"text":"slide"},{"dep":["nsubj"],"text":"value"}]	0	0	0	AUTO		f	\N	\N	\N	0
b80e97b4-e686-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	220	t	["Can you explain the difference between sample distribution and bootstrap distribution?\\n"]	0	f	f	\N	2022-10-24 14:06:13.967427	f	[]	[{"dep":["dobj"],"text":"difference"},{"dep":["pobj"],"text":"sample distribution"}]	0	0	0	AUTO		f	\N	\N	\N	0
78746b92-e88e-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	221	t	["What again means point in the null distribution Workflow?\\n"]	0	f	f	\N	2022-10-24 14:06:13.974531	f	[]	[{"dep":["dobj"],"text":"point"},{"dep":["compound"],"text":"distribution"},{"dep":["pobj"],"text":"Workflow"}]	0	0	0	AUTO		f	\N	\N	\N	0
4bdab4c4-e889-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	222	t	["Can you also formulate type 2 error as \\"rejecting Ha even though Ha is true\\"?\\n"]	0	f	f	\N	2022-10-24 14:06:13.980811	f	[]	[{"dep":["nmod"],"text":"type"},{"dep":["dobj"],"text":"error"}]	0	0	0	AUTO		f	\N	\N	\N	0
0bafd872-e887-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	223	t	["I dont really understand why we get different results in the bootstrapping result because we always draw a new sample with size 100 from an original sample with size 100\\n"]	0	f	f	\N	2022-10-24 14:06:13.988238	f	[]	[{"dep":["dobj"],"text":"result"},{"dep":["pobj","dobj"],"text":"sample"},{"dep":["pobj"],"text":"size"}]	0	0	0	AUTO		f	\N	\N	2022-06-10 06:36:48.523	0
8ed9baea-eb2c-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	224	t	["where does the 1.96 come from on slide 11?\\n"]	0	f	f	\N	2022-10-24 14:06:13.994624	f	[]	[{"dep":["pobj"],"text":"slide"}]	0	0	0	AUTO		f	\N	\N	2022-06-13 16:29:33.235	0
8a8db996-eb31-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	173/1	t	[" just different wording\\n"]	0	f	f	\N	2022-10-24 14:06:14.000886	f	[]	[]	0	0	0	AUTO		f	\N	7d78c486-eb30-11ec-b02a-0242ac190004	\N	1
2fa06c9a-eb2c-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	225	t	["slide 10 why do we start with 0,85 +/-\\n"]	0	f	f	\N	2022-10-24 14:06:14.006627	f	[]	[{"dep":["ROOT"],"text":"folie"}]	0	0	0	AUTO		f	\N	\N	2022-06-13 15:20:47.181	0
3c30a54e-eb34-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	226	t	["can you explain very short again why we chose 0.85 and not 0.80 on slide 14\\n"]	0	f	f	\N	2022-10-24 14:06:14.012872	f	[]	[{"dep":["pobj"],"text":"slide"}]	0	0	0	AUTO		f	\N	\N	\N	0
dcd2d2dc-e88d-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	227	t	["is the null distribution just like the significance test from the right side?\\n"]	0	f	f	\N	2022-10-24 14:06:14.023989	f	[]	[{"dep":["attr"],"text":"distribution"},{"dep":["pobj"],"text":"significance test"},{"dep":["pobj"],"text":"side"}]	0	0	0	AUTO		f	\N	\N	\N	0
8cfb16f0-040f-11ed-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	228	t	["please don't forget to follow up on my quiz question\\n\\n"]	0	f	f	\N	2022-10-24 14:06:14.030528	f	[]	[]	0	0	0	EN		f	\N	\N	2022-07-15 08:25:26.016	0
8b706c28-e884-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	229	t	["can you please write on ipad or so, we can't see the blackboard through zoom . thank you\\n"]	0	f	f	\N	2022-10-24 14:06:14.037535	f	[]	[{"dep":["pobj"],"text":"ipad"},{"dep":["dobj"],"text":"board"},{"dep":["pobj"],"text":"zoom"},{"dep":["ROOT"],"text":"thank"}]	0	0	0	AUTO		f	\N	\N	\N	0
202098c2-eb30-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	230	t	["the question is if the data provides convincing evidence that more than 80% have a good intuition, so why is H0: p = 0.80 and not p>0.8?\\n"]	0	f	f	\N	2022-10-24 14:06:14.045325	f	[]	[{"dep":["nsubj"],"text":"question"},{"dep":["nsubj"],"text":"datum"},{"dep":["dobj"],"text":"evidence"},{"dep":["nsubj"],"text":"more than 80 %"},{"dep":["dobj"],"text":"intuition"}]	0	0	0	AUTO		f	\N	\N	\N	0
3eb13004-eb2f-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	231	t	["0.85\\n\\n"]	1	f	f	\N	2022-10-24 14:06:14.051725	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-13 16:29:51.694	0
02c38df2-eb35-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	226/1	t	["please\\n"]	0	f	f	\N	2022-10-24 14:06:14.057698	f	[]	[]	0	0	0	AUTO		f	\N	3c30a54e-eb34-11ec-8ca6-0242ac190004	\N	1
89115444-eb2f-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	232	t	["why pnorm of 0.85\\n"]	0	f	f	\N	2022-10-24 14:06:14.063067	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-13 16:30:04.549	0
8ecf7c1a-eb2c-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	239	t	["could you please explain again where the 1.96 came from? (slide 10)\\n"]	0	f	f	\N	2022-10-24 14:06:14.139293	f	[]	[{"dep":["ROOT"],"text":"slide"}]	0	0	0	AUTO		f	\N	\N	2022-06-13 16:29:27.484	0
5947de62-eb30-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	240	t	["Can you explain again the result sequence on slide 14?\\n"]	0	f	f	\N	2022-10-24 14:06:14.145861	f	[]	[{"dep":["pobj"],"text":"slide"}]	0	0	0	AUTO		f	\N	\N	\N	0
2d5236f6-eb2e-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	241	t	["alpha\\n"]	1	f	f	\N	2022-10-24 14:06:14.152265	f	[]	[]	1	1	0	AUTO		f	\N	\N	\N	0
454374d8-eb32-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	242	t	["why do we use 1,96 again?\\n"]	0	f	f	\N	2022-10-24 14:06:14.158226	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
adbc66e4-eb2f-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	243	t	["To what extend is R important for the exam?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:14.165197	f	[]	[{"dep":["csubj"],"text":"extend"},{"dep":["pobj"],"text":"exam"}]	0	0	0	AUTO		f	\N	\N	2022-06-13 16:30:11.567	0
71d103e2-ee0e-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	244	t	["Why is n = (r -1)*(c - 1)\\n"]	0	f	f	\N	2022-10-24 14:06:14.171853	f	[]	[{"dep":["compound"],"text":"r -1)*(c"}]	0	0	0	AUTO		f	\N	\N	2022-06-17 08:28:05.72	0
4ffe92ba-ee05-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	245	t	["Can we make get_ci of null_dist?\\n"]	0	f	f	\N	2022-10-24 14:06:14.182341	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-17 08:35:37.478	0
52e300de-ee04-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	246	t	["Should hypothesis testing only be with = and != , H0:p1=p2, H1:p1!=p2 or H1 can also be p1>p2, when it is specified that p1 is larger?\\n"]	0	f	f	\N	2022-10-24 14:06:14.189751	f	[]	[{"dep":["ROOT"],"text":"p1 = p2"},{"dep":["intj"],"text":"p1!=p2"}]	0	0	0	AUTO		f	\N	\N	2022-06-17 08:35:49.319	0
572bfd02-ee05-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	247	t	["How can be do a ohne-sided Test With r ? Do we Then use for example  direction = “greater”\\n"]	0	f	f	\N	2022-10-24 14:06:14.197154	f	[]	[{"dep":["npadvmod"],"text":"ohne"},{"dep":["dobj"],"text":"test"},{"dep":["pobj"],"text":"example"},{"dep":["dobj"],"text":"direction"}]	0	0	0	AUTO		f	\N	\N	2022-06-17 08:35:32.137	0
02baddc4-ee0a-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	248	t	["What shows the degree of freedom\\n"]	0	f	f	\N	2022-10-24 14:06:14.203902	f	[]	[{"dep":["dobj"],"text":"degree"},{"dep":["pobj"],"text":"freedom"}]	0	0	0	AUTO		f	\N	\N	2022-06-17 08:35:00.706	0
2ca3b414-ee0d-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	249	t	["When p1>p2 direction=greater?\\n"]	0	f	f	\N	2022-10-24 14:06:14.210164	f	[]	[{"dep":["nsubj"],"text":"p1 > p2 direction"}]	0	0	0	AUTO		f	\N	\N	2022-06-17 08:28:16.829	0
d8b5d28c-ee03-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	250	t	["How can we interpret this , when the confidence interval is between a negative and positive value?\\n"]	0	f	f	\N	2022-10-24 14:06:14.21727	f	[]	[{"dep":["nsubj"],"text":"confidence interval"},{"dep":["pobj"],"text":"value"}]	0	0	0	AUTO		f	\N	\N	2022-06-17 08:35:55.355	0
0b86735c-ee08-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	251	t	["When we can not make get_ci from null_dist should we do this from obs_diff?\\n"]	0	f	f	\N	2022-10-24 14:06:14.223568	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-17 08:35:10.249	0
37147fb0-ee0c-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	252	t	["can you repeat the page 21?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:14.23027	f	[]	[{"dep":["dobj"],"text":"page"}]	1	1	0	AUTO		f	\N	\N	2022-06-17 08:31:19.821	0
08d3afbe-ee06-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	253	t	["When we observe difference of means, can we write only m=0 in hypothesize or we should write mu=mean(…)\\n"]	0	f	f	\N	2022-10-24 14:06:14.238214	f	[]	[{"dep":["dobj"],"text":"difference"},{"dep":["pobj"],"text":"mean"},{"dep":["dobj"],"text":"m=0"},{"dep":["pobj"],"text":"hypothesize"},{"dep":["dobj"],"text":"mu ="}]	0	0	0	AUTO		f	\N	\N	2022-06-17 08:35:26.45	0
f9ba53f4-ee0d-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	254	t	["Is the direction always “greater” in the chi-square test?\\n"]	0	f	f	\N	2022-10-24 14:06:14.245008	f	[]	[{"dep":["nsubj"],"text":"direction"},{"dep":["pobj"],"text":"test"}]	0	0	0	AUTO		f	\N	\N	2022-06-17 08:28:10.316	0
95034bf2-ee06-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	255	t	["When we have H1:p1>p2, should we have by get_pvalue(…direction = „ one sided“ ) or again direction=„both“?\\n"]	0	f	f	\N	2022-10-24 14:06:14.252742	f	[]	[{"dep":["dep"],"text":"p1 > p2"},{"dep":["npadvmod"],"text":"direction"},{"dep":["nummod"],"text":"one"}]	0	0	0	AUTO		f	\N	\N	2022-06-17 08:35:15.064	0
dc893a98-ee08-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	256	t	["what are O and E in the formula for the chi square?\\n"]	0	f	f	\N	2022-10-24 14:06:14.259763	f	[]	[{"dep":["pobj"],"text":"formula"},{"dep":["pobj"],"text":"the chi square"}]	0	0	0	AUTO		f	\N	\N	2022-06-17 08:35:05.301	0
6ced7666-ee0a-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	257	t	["Should we use always by H0:p_1=p_2 or it can also be in H0: p_1!=p_2\\n"]	0	f	f	\N	2022-10-24 14:06:14.266714	f	[]	[{"dep":["ROOT"],"text":"p1!=p2"}]	0	0	0	AUTO		f	\N	\N	\N	0
b77a5802-ee0a-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	258	t	["I am confused with the p-value interpretation.\\n\\nassume H0 = no inconsistency (fair dice)\\nX^2 = 0 should mean observed and expected values are identical, right?\\n\\n> pchisq(0, df = 5, lower.tail = FALSE)\\n[1] 1\\n\\nSo large values favor H0 and small values facor HA, correct?\\n"]	0	f	f	\N	2022-10-24 14:06:14.275289	f	[]	[{"dep":["ROOT"],"text":"inconsistency"},{"dep":["appos"],"text":"dice"},{"dep":["nsubj"],"text":"x^2"},{"dep":["nsubj"],"text":"value"}]	0	0	0	AUTO		f	\N	\N	2022-06-17 07:00:05.231	0
563d4b7c-ee18-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	257/1	t	["you can never have a null hypothesis like ",{"formula":"H_0: p_1 \\\\ne p_2"},", because you don't know what the concrete difference is (that's the reason why you run the test). but you would need to know, since the null distribution would depend on the concrete value. and since you need to compute the null distribution to run the test, this approach is not feasible.\\n"]	0	f	f	\N	2022-10-24 14:06:14.285418	f	[]	[{"dep":["dobj"],"text":"hypothesis"},{"dep":["nsubj"],"text":"difference"},{"dep":["attr"],"text":"reason"},{"dep":["dobj"],"text":"test"},{"dep":["nsubj","dobj"],"text":"distribution"},{"dep":["pobj"],"text":"value"},{"dep":["nsubj"],"text":"approach"}]	0	0	0	AUTO		f	\N	6ced7666-ee0a-11ec-a971-0242ac190004	\N	1
e0f08678-ee0c-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	259	t	["sorry, am still confused about the interpretation of the p-value\\n\\nassume H0 = no inconsistency (fair dice)\\nX^2 = 0 should mean observed and expected values are identical, right?\\n\\n> pchisq(0, df = 5, lower.tail = FALSE)\\n[1] 1\\n\\n\\nSo large p-values favor H0 and small p-values favor HA, correct?\\n"]	0	f	f	\N	2022-10-24 14:06:14.293767	f	[]	[{"dep":["pobj"],"text":"interpretation"},{"dep":["conj","pobj","nsubj"],"text":"value"},{"dep":["acl"],"text":"assume"},{"dep":["ROOT"],"text":"inconsistency"},{"dep":["appos"],"text":"dice"},{"dep":["nsubj"],"text":"x^2"}]	0	0	0	AUTO		f	\N	\N	\N	0
6f349f76-ee0f-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	260	t	["What is the role of type=permute, can we do not write it or there will be a difference?\\n"]	0	f	f	\N	2022-10-24 14:06:14.301933	f	[]	[{"dep":["nsubj"],"text":"role"},{"dep":["pobj"],"text":"type"},{"dep":["relcl"],"text":"permute"},{"dep":["attr"],"text":"difference"}]	0	0	0	AUTO		f	\N	\N	2022-06-17 08:27:49.055	0
f1f1e298-eb35-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	242/1	t	["it's again a 95% confidence interval. hence, 1.96 is again the critical value\\n"]	0	f	f	\N	2022-10-24 14:06:14.30967	f	[]	[{"dep":["attr"],"text":"confidence interval"},{"dep":["attr"],"text":"value"}]	1	1	0	AUTO		f	\N	454374d8-eb32-11ec-a6d5-0242ac190004	\N	1
6bbb3db6-f62d-11ec-800b-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	287	t	["categorical ordinal\\n"]	0	f	f	\N	2022-10-24 14:06:14.511158	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
43aa331e-f0af-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	261	t	["What does the pValue for the two sided test tell us?\\n"]	0	f	f	\N	2022-10-24 14:06:14.316503	f	[]	[{"dep":["nsubj"],"text":"pValue"},{"dep":["nummod"],"text":"two"},{"dep":["pobj"],"text":"test"}]	1	1	0	AUTO		f	\N	\N	2022-06-27 13:52:12.428	0
01803234-f0b1-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	262	t	["The question was whether they the variables are independent or not. How did we jump from that to assuming their averages are the same? why not score_read = alpha *  score_write (with alpha != 1)?\\n"]	0	f	f	\N	2022-10-24 14:06:14.325048	f	[]	[{"dep":["nsubj"],"text":"question"},{"dep":["appos"],"text":"variable"},{"dep":["nsubj"],"text":"average"},{"dep":["pobj","ROOT"],"text":"alpha"}]	0	0	0	AUTO		f	\N	\N	2022-06-27 13:52:41.525	0
0e930668-f0b1-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	263	t	["Why 200\\n"]	0	f	f	\N	2022-10-24 14:06:14.331112	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
21013dd8-f0b1-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	264	t	["Can you explain when we should look at the t distribution and when at the normal distribution\\n"]	0	f	f	\N	2022-10-24 14:06:14.338456	f	[]	[{"dep":["pobj"],"text":"t distribution"},{"dep":["pobj"],"text":"distribution"}]	0	0	0	AUTO		f	\N	\N	2022-06-27 13:52:51.665	0
174d3a00-f0af-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	265	t	["Why do we use qt() and not qnorm()\\n"]	0	f	f	\N	2022-10-24 14:06:14.344619	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-27 13:52:05.231	0
4078cbf0-f0b0-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	266	t	["slide 22 how do we check independence\\n"]	0	f	f	\N	2022-10-24 14:06:14.351022	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-27 13:52:26.68	0
a0c768d8-f0ae-11ec-90ff-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	267	t	["What is the difference between „two-sided“ and „both“ in parameter „direction“ or they are the same?\\n"]	0	f	f	\N	2022-10-24 14:06:14.358735	f	[]	[{"dep":["nsubj"],"text":"difference"},{"dep":["nummod"],"text":"two"},{"dep":["amod"],"text":"parameter"},{"dep":["pobj"],"text":"direction"}]	0	0	0	AUTO		f	\N	\N	2022-06-27 13:51:49.931	0
dd2d049a-f0b3-11ec-a5cb-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	120/1	t	["0.545 was the mean of the differences. 0.867 is the value of the test statistic. in the interpretation we would refer to differences in the data, not the test statistic\\n"]	0	f	f	\N	2022-10-24 14:06:14.367396	f	[]	[{"dep":["attr"],"text":"mean"},{"dep":["pobj"],"text":"difference"},{"dep":["attr"],"text":"value"},{"dep":["conj","pobj"],"text":"test statistic"},{"dep":["pobj"],"text":"interpretation"},{"dep":["pobj"],"text":"datum"}]	1	1	0	AUTO		f	\N	a02d923a-f0b3-11ec-a971-0242ac190004	\N	1
28091a84-f0a1-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	259/1	t	["yes\\n"]	0	f	f	\N	2022-10-24 14:06:14.372877	f	[]	[]	0	0	0	AUTO		f	\N	e0f08678-ee0c-11ec-84b5-0242ac190004	\N	1
405bd768-f388-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	268	t	["The stream people can't really see the writings on the board \\n"]	0	f	f	\N	2022-10-24 14:06:14.37908	f	[]	[{"dep":["dobj"],"text":"writing"},{"dep":["pobj"],"text":"board"}]	1	1	0	AUTO		f	\N	\N	2022-06-27 13:50:28.361	0
b997d4ac-f387-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	269	t	["Would be helpful for those at home to check whether the camera follows you on the blackboard\\n"]	0	f	f	\N	2022-10-24 14:06:14.386789	f	[]	[{"dep":["pobj"],"text":"home"},{"dep":["nsubj"],"text":"camera"},{"dep":["pobj"],"text":"blackboard"}]	0	0	0	AUTO		f	\N	\N	2022-06-27 13:50:41.583	0
b29f97dc-f38e-11ec-a5fc-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	270	t	["Is standrad error=standard deviation\\n"]	0	f	f	\N	2022-10-24 14:06:14.393376	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-27 13:53:55.949	0
58121b54-f38a-11ec-8ca6-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	271	t	["What will be the difference when lower.tail=TRUE\\n"]	0	f	f	\N	2022-10-24 14:06:14.40017	f	[]	[{"dep":["attr"],"text":"difference"}]	0	0	0	AUTO		f	\N	\N	2022-06-27 13:53:30.201	0
270ba9fe-f389-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	272	t	["What is S1 and s2?\\n"]	0	f	f	\N	2022-10-24 14:06:14.406858	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-27 13:53:17.842	0
31d91dd4-f38f-11ec-a6d5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	273	t	["Why again we subtract -3 and devide with SE and then geht Z on slide 24?\\n"]	0	f	f	\N	2022-10-24 14:06:14.4137	f	[]	[{"dep":["pobj"],"text":"slide"}]	0	0	0	AUTO		f	\N	\N	\N	0
541c07cc-f389-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	274	t	["Why minus 0 on slide 38\\n"]	0	f	f	\N	2022-10-24 14:06:14.420631	f	[]	[{"dep":["pobj"],"text":"slide"}]	0	0	0	AUTO		f	\N	\N	2022-06-27 13:53:24.586	0
07a2fe64-f631-11ec-b507-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	275	t	["what are those \\"which\\" and \\"ncol\\" arguments in the autoplot function?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:14.427441	f	[]	[{"dep":["nsubj"],"text":"argument"}]	0	0	0	AUTO		f	\N	\N	2022-06-28 07:30:00.167	0
ebc3a000-f2ec-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	276	t	["i have an issue with exercise 2 in the rlabs \\"inference for categorical data\\":\\nI can't paste the code because of the character limit but it's the same as the solution.\\nIt shows the error: \\nError: A proportion is not well-defined for a multinomial categorical response variable (text_ind) and no explanatory variable.",{"attributes":{"blockquote":true},"insert":"\\n"},"I have tidyverse, openintro and infer installed and loaded\\n"]	0	f	f	\N	2022-10-24 14:06:14.43873	f	[]	[{"dep":["dobj"],"text":"issue"},{"dep":["pobj"],"text":"exercise"},{"dep":["pobj"],"text":"inference"},{"dep":["pobj"],"text":"datum"},{"dep":["dobj"],"text":"code"},{"dep":["pobj"],"text":"solution"},{"dep":["dobj","ROOT"],"text":"error"},{"dep":["nsubj"],"text":"proportion"},{"dep":["pobj"],"text":"response variable"},{"dep":["conj"],"text":"variable"},{"dep":["dobj"],"text":"tidyverse"},{"dep":["conj"],"text":"openintro"}]	2	2	0	AUTO		f	\N	\N	\N	0
ae659076-f62d-11ec-921c-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	277	t	["what is meant by the df of error?\\n"]	0	f	f	\N	2022-10-24 14:06:14.445082	f	[]	[{"dep":["pobj"],"text":"error"}]	0	0	0	AUTO		f	\N	\N	\N	0
e618e39a-f62f-11ec-921c-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	278	t	["what does the pt function stand for?\\n"]	0	f	f	\N	2022-10-24 14:06:14.452108	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-27 15:44:19.683	0
1c2bd16a-f633-11ec-921c-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	279	t	["P(A) * P(B)\\n"]	0	f	f	\N	2022-10-24 14:06:14.458552	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-28 07:29:35.561	0
a4ce7eb4-f633-11ec-921c-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	280	t	["Why we first multiplate with 3 and then /3 and in the end again *3\\n"]	0	f	f	\N	2022-10-24 14:06:14.465155	f	[]	[{"dep":["pobj"],"text":"end"}]	0	0	0	AUTO		f	\N	\N	\N	0
98530e56-f62f-11ec-810e-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	281	t	["d\\n"]	0	f	f	\N	2022-10-24 14:06:14.47111	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
9daad6fe-f62f-11ec-810e-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	282	t	["Why no 'antworte.jetzt' this time? It's quite funny;) \\n"]	0	f	f	\N	2022-10-24 14:06:14.477675	f	[]	[{"dep":["npadvmod"],"text":"time"}]	4	4	0	AUTO		f	\N	\N	\N	0
ea1f2f0a-f632-11ec-810e-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	283	t	["Events should be disjoint\\n"]	0	f	f	\N	2022-10-24 14:06:14.484598	f	[]	[{"dep":["nsubj"],"text":"event"},{"dep":["acomp"],"text":"disjoint"}]	0	0	0	AUTO		f	\N	\N	2022-06-28 07:29:46.428	0
872ec9ce-f38e-11ec-9e13-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	284	t	["What is -3.3319\\n"]	0	f	f	\N	2022-10-24 14:06:14.490828	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-27 13:53:36.648	0
bbf77b12-f62a-11ec-b19f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	285	t	["Couldn't we also just do k-1 T-Test (from last week) instead?\\n"]	0	f	f	\N	2022-10-24 14:06:14.498405	f	[]	[{"dep":["nummod"],"text":"k-1 T"},{"dep":["dobj"],"text":"Test"},{"dep":["pobj"],"text":"last week"}]	1	1	0	AUTO		f	\N	\N	\N	0
993f5f20-f62c-11ec-800b-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	286	t	["Can you repeat what are n and k\\n"]	0	f	f	\N	2022-10-24 14:06:14.505029	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
59a975d6-f630-11ec-b19f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	288	t	["How would the anova would Look if we Want to know Which mean Differs?\\n"]	0	f	f	\N	2022-10-24 14:06:14.518396	f	[]	[{"dep":["nsubj"],"text":"anova"},{"dep":["dobj"],"text":"differ"}]	0	0	0	AUTO		f	\N	\N	2022-06-28 07:30:35.012	0
74b418b2-f631-11ec-b19f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	289	t	["Can you explain Why we See that we have no constant variance?\\n"]	0	f	f	\N	2022-10-24 14:06:14.525288	f	[]	[{"dep":["dobj"],"text":"variance"}]	0	0	0	AUTO		f	\N	\N	2022-06-28 07:29:52.718	0
0bc3bcca-f633-11ec-800b-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	290	t	["It should be <= or == by Ph 0,1?\\n"]	0	f	f	\N	2022-10-24 14:06:14.53155	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-28 07:29:26.334	0
24d03850-f634-11ec-b19f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	291	t	["What was the correct answer on slide 30?\\n"]	0	f	f	\N	2022-10-24 14:06:14.538744	f	[]	[{"dep":["nsubj"],"text":"answer"},{"dep":["pobj"],"text":"slide"}]	0	0	0	AUTO		f	\N	\N	2022-06-27 16:16:33.054	0
0ca060d4-f0ad-11ec-b02a-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	292	t	["So we can use H0:p1=p2 and H1: p1>p2? It should not strongly be H1: p1!=p2?\\n"]	0	f	f	\N	2022-10-24 14:06:14.545599	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-27 13:51:43.149	0
82a4e49c-f62c-11ec-8f42-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	293	t	["Can you again explain the Quiz on Page 11?\\n"]	0	f	f	\N	2022-10-24 14:06:14.552285	f	[]	[{"dep":["dobj"],"text":"Quiz"},{"dep":["pobj"],"text":"Page"}]	0	0	0	AUTO		f	\N	\N	\N	0
fb6ef436-f0ae-11ec-84b5-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	294	t	["Can you explain how we should understand the degree of freedom\\n"]	0	f	f	\N	2022-10-24 14:06:14.563767	f	[]	[{"dep":["dobj"],"text":"degree"},{"dep":["pobj"],"text":"freedom"}]	0	0	0	AUTO		f	\N	\N	2022-06-27 13:51:58.186	0
8d1ae804-f0af-11ec-a971-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	295	t	["Why does the t.test or qt functions do not take the degree of freedom as an input? is there a default case?\\n"]	0	f	f	\N	2022-10-24 14:06:14.571763	f	[]	[{"dep":["nsubj"],"text":"function"},{"dep":["dobj"],"text":"degree"},{"dep":["pobj"],"text":"freedom"},{"dep":["pobj"],"text":"input"}]	0	0	0	AUTO		f	\N	\N	2022-06-27 13:52:18.24	0
dce16bf0-f62d-11ec-a563-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	296	t	["So SSG is basically the weighted mean differences of the overall mean?\\n"]	0	f	f	\N	2022-10-24 14:06:14.578736	f	[]	[{"dep":["nsubj"],"text":"ssg"},{"dep":["attr"],"text":"difference"},{"dep":["pobj"],"text":"mean"}]	0	0	0	AUTO		f	\N	\N	\N	0
ebb6f6fe-f632-11ec-a563-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	297	t	["They are all independent\\n"]	0	f	f	\N	2022-10-24 14:06:14.585763	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-28 07:29:41.038	0
ca82aafe-f0b1-11ec-87c8-0242ac190004	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	298	t	["We use independence when we have two different values and point only for one value?\\n"]	0	f	f	\N	2022-10-24 14:06:14.593765	f	[]	[{"dep":["dobj"],"text":"independence"},{"dep":["nummod"],"text":"two"},{"dep":["pobj","dobj"],"text":"value"},{"dep":["conj"],"text":"point"},{"dep":["nummod"],"text":"one"}]	0	0	0	AUTO		f	\N	\N	2022-06-27 13:52:59.927	0
c8e88660-f628-11ec-b507-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	299	t	["Excuse me I have a question; has anyone seen Godzilla (2014) ?\\n"]	0	f	f	\N	2022-10-24 14:06:14.600497	f	[]	[]	-1	0	1	AUTO		f	\N	\N	\N	0
0e2b3db0-f62b-11ec-b507-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	300	t	["Have these cases normal distribution?\\n"]	0	f	f	\N	2022-10-24 14:06:14.607771	f	[]	[{"dep":["ROOT"],"text":"case"},{"dep":["dobj"],"text":"distribution"}]	0	0	0	AUTO		f	\N	\N	\N	0
534a9b16-f62b-11ec-b507-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	301	t	["can we use anova for less than 3 comparisons, too?\\n"]	0	f	f	\N	2022-10-24 14:06:14.614677	f	[]	[{"dep":["dobj"],"text":"anova"},{"dep":["nummod"],"text":"less than 3"},{"dep":["pobj"],"text":"comparison"}]	0	0	0	AUTO		f	\N	\N	\N	0
e991d624-f62c-11ec-b507-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	302	t	["Is aov(aldrin ~ depth, data = wolf) equal to aov(depth ~ aldrin, data = wolf)?\\n"]	0	f	f	\N	2022-10-24 14:06:14.622757	f	[]	[{"dep":["nmod"],"text":"depth"},{"dep":["intj","nmod"],"text":"data"},{"dep":["npadvmod","ROOT"],"text":"wolf"},{"dep":["nmod"],"text":"aldrin"}]	1	1	0	AUTO		f	\N	\N	\N	0
973946f2-f62f-11ec-b507-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	303	t	["d\\n"]	0	f	f	\N	2022-10-24 14:06:14.628915	f	[]	[]	1	1	0	AUTO		f	\N	\N	\N	0
247854be-f62f-11ec-8cfa-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	304	t	["Is Y a parameter for the population and y for the sample?\\n"]	0	f	f	\N	2022-10-24 14:06:14.636312	f	[]	[{"dep":["attr"],"text":"parameter"},{"dep":["pobj"],"text":"population"},{"dep":["pobj"],"text":"sample"}]	0	0	0	AUTO		f	\N	\N	\N	0
6403d89c-f634-11ec-8cfa-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	305	t	["So what is a* in the Bonferroni correction?\\n"]	0	f	f	\N	2022-10-24 14:06:14.643021	f	[]	[{"dep":["compound"],"text":"Bonferroni"},{"dep":["pobj"],"text":"correction"}]	0	0	0	AUTO		f	\N	\N	\N	0
3a5752a8-f62f-11ec-8695-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	306	t	["how we calculate this p value by Hand?\\n"]	0	f	f	\N	2022-10-24 14:06:14.650258	f	[]	[{"dep":["dobj"],"text":"p value"},{"dep":["pobj"],"text":"Hand"}]	0	0	0	AUTO		f	\N	\N	\N	0
12c39f7a-f630-11ec-8695-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	307	t	["what does the pf function stand for and what is the first argument?\\n"]	0	f	f	\N	2022-10-24 14:06:14.65756	f	[]	[{"dep":["amod"],"text":"first"},{"dep":["nsubj"],"text":"argument"}]	0	0	0	AUTO		f	\N	\N	2022-06-28 07:30:40.95	0
ff53287c-f632-11ec-8695-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	308	t	["-P(H0 and H1) ...\\n"]	0	f	f	\N	2022-10-24 14:06:14.663923	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-06-28 07:29:31.312	0
6a4ce36a-f62f-11ec-903b-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	304/1	t	["Yes\\n"]	0	f	f	\N	2022-10-24 14:06:14.670533	f	[]	[]	0	0	0	AUTO		f	\N	247854be-f62f-11ec-8cfa-0242ac190008	\N	1
058acf40-f635-11ec-903b-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	280/1	t	["we multiple by 3 because we have three probabilities to add. in the end we want to compensate this, by choosing a significance level of ",{"formula":"\\\\alpha / 3"},"\\n"]	0	f	f	\N	2022-10-24 14:06:14.677777	f	[]	[{"dep":["nummod"],"text":"three"},{"dep":["dobj"],"text":"probability"},{"dep":["pobj"],"text":"end"},{"dep":["dobj"],"text":"significance level"}]	0	0	0	AUTO		f	\N	a4ce7eb4-f633-11ec-921c-0242ac190008	\N	1
fd332836-f6b3-11ec-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	305/1	t	[{"formula":"\\\\alpha / K"},"\\n"]	0	f	f	\N	2022-10-24 14:06:14.683583	f	[]	[]	0	0	0	AUTO		f	\N	6403d89c-f634-11ec-8cfa-0242ac190008	\N	1
8f436894-f907-11ec-b79a-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	309	t	["Slide 8: how can the y intercept be 73.11? (In the plot it's more around 17.5)  maybe it helps if you explain again how you got the 73.11\\n"]	0	f	f	\N	2022-10-24 14:06:14.690128	f	[]	[{"dep":["nsubj"],"text":"slide"},{"dep":["pobj"],"text":"plot"}]	0	0	0	AUTO		f	\N	\N	2022-07-01 07:41:59.467	0
4df71120-f906-11ec-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	310	t	["Could please explain the residual error Again\\n"]	0	f	f	\N	2022-10-24 14:06:14.696966	f	[]	[{"dep":["dobj"],"text":"error"}]	0	0	0	AUTO		f	\N	\N	2022-07-01 07:42:05.592	0
c2df86d6-f909-11ec-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	311	t	["page 13 quiz\\n\\nby exclusion:\\n- R needs to be in [-1,+1] -> exclude e\\n- slope is negative -> exclude a, d\\n- there is significant correlation: exclude c\\n\\nresult:\\n- b remains as only possible answer\\n"]	0	f	f	\N	2022-10-24 14:06:14.711205	f	[]	[{"dep":["ROOT"],"text":"page"},{"dep":["ROOT"],"text":"quiz"},{"dep":["pobj"],"text":"exclusion"},{"dep":["nsubj"],"text":"slope"},{"dep":["attr"],"text":"correlation"},{"dep":["ROOT"],"text":"result"},{"dep":["pobj"],"text":"answer"}]	0	0	0	AUTO		f	\N	\N	2022-07-01 07:41:16.155	0
9bdca614-f908-11ec-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	312	t	["correlation is standardized?\\n"]	0	f	f	\N	2022-10-24 14:06:14.723723	f	[]	[{"dep":["nsubjpass"],"text":"correlation"}]	0	0	0	AUTO		f	\N	\N	2022-07-01 07:41:54.658	0
756ff300-f90d-11ec-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	313	t	["my guess page 23: d\\n"]	0	f	f	\N	2022-10-24 14:06:14.73045	f	[]	[{"dep":["ROOT"],"text":"my guess"}]	0	0	0	AUTO		f	\N	\N	2022-07-01 07:41:02.668	0
834724a0-f90b-11ec-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	314	t	["slide 17 : should be x head?\\n"]	0	f	f	\N	2022-10-24 14:06:14.737364	f	[]	[{"dep":["ROOT"],"text":"slide"},{"dep":["attr"],"text":"head"}]	0	0	0	AUTO		f	\N	\N	2022-07-01 07:41:11.382	0
d5982de6-f904-11ec-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	282/1	t	["how can we use the poverty data in R? its only a list when i open it and i can not use the codes of the lecture\\n"]	0	f	f	»frag.jetzt«	2022-10-24 14:06:14.745568	f	[{"text":"code","dep":["dobj"]},{"text":"lecture","dep":["pobj"]},{"text":"list","dep":["ROOT"]}]	[{"dep":["ROOT"],"text":"list"},{"dep":["dobj"],"text":"code"},{"dep":["pobj"],"text":"lecture"}]	0	0	0	AUTO		f	\N	9daad6fe-f62f-11ec-810e-0242ac190008	\N	1
db4f19bc-f908-11ec-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	315	t	["how can we use the poverty dataset in R studio? It's yust a list when i open it and i can't use the code of the lecture\\n"]	0	f	f	\N	2022-10-24 14:06:14.752648	f	[]	[{"dep":["dobj"],"text":"poverty"},{"dep":["pobj"],"text":"r studio"},{"dep":["attr"],"text":"list"},{"dep":["dobj"],"text":"code"},{"dep":["pobj"],"text":"lecture"}]	0	0	0	AUTO		f	\N	\N	2022-07-01 07:41:49.641	0
a78e9a7c-f90c-11ec-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	316	t	["Can we use the cheatsheet in the exam? Is it possible that you upload it already now?\\n"]	0	f	f	\N	2022-10-24 14:06:14.759827	f	[]	[{"dep":["dobj"],"text":"cheatsheet"},{"dep":["pobj"],"text":"exam"}]	0	0	0	AUTO		f	\N	\N	\N	0
5f385a62-f905-11ec-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	317	t	["What does mean linear parameters? Is bo = 4 a linear parameter?\\n"]	0	f	f	\N	2022-10-24 14:06:14.766798	f	[]	[{"dep":["attr","dobj"],"text":"parameter"},{"dep":["nsubj"],"text":"bo ="}]	0	0	0	AUTO		f	\N	\N	\N	0
03e3e3d8-f906-11ec-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	317/1	t	["my take: think of it as line equation (geradengleichung) y=mx+b; y=b+mx -> y = b0+b1*x\\n\\nb0 and b1 are the coefficients of the linear equation\\n"]	1	f	f	\N	2022-10-24 14:06:14.776004	f	[]	[{"dep":["nsubj"],"text":"opinion"},{"dep":["pobj"],"text":"line equation y"},{"dep":["advmod"],"text":"mx+b"},{"dep":["intj"],"text":"b+mx"},{"dep":["ROOT"],"text":"b0+b1x"},{"dep":["attr"],"text":"coefficient"},{"dep":["pobj"],"text":"line equation"}]	0	0	0	AUTO		f	\N	5f385a62-f905-11ec-945f-0242ac190008	\N	1
0fc26c06-f906-11ec-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	318	t	["What is moderately strong?\\n"]	0	f	f	\N	2022-10-24 14:06:14.781451	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-01 07:42:09.861	0
7efa153e-fbad-11ec-9520-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	319	t	["What is the residual \\n"]	0	f	f	\N	2022-10-24 14:06:14.787874	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-08 05:39:21.516	0
0703dfc2-fbb4-11ec-bbd1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	320	t	["Do We need to compute this by hand in the Exam?\\n"]	0	f	f	\N	2022-10-24 14:06:14.796429	f	[]	[{"dep":["pobj"],"text":"hand"},{"dep":["pobj"],"text":"Exam"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 05:38:35.331	0
11d38f8a-fbad-11ec-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	321	t	["your audio is really bad on zoom, maybe your microphone needs to be relocated?\\n"]	0	f	f	\N	2022-10-24 14:06:14.803964	f	[]	[{"dep":["nsubj"],"text":"audio"},{"dep":["nsubj"],"text":"microphone"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 05:39:38.361	0
6891295a-fbac-11ec-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	322	t	["mute pls\\n"]	0	f	f	\N	2022-10-24 14:06:14.810359	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-08 05:39:57.383	0
ef8c469c-fbac-11ec-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	323	t	["The audio quality is pretty bad right now\\n"]	0	f	f	\N	2022-10-24 14:06:14.817255	f	[]	[{"dep":["nsubj"],"text":"quality"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 05:39:52.174	0
6681b61a-fbad-11ec-938f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	324	t	["Can you pls explain again Why the autolplot for the Anica model looks different?\\n"]	0	f	f	\N	2022-10-24 14:06:14.824524	f	[]	[{"dep":["compound"],"text":"Anica"},{"dep":["pobj"],"text":"model"}]	0	0	0	AUTO		f	\N	\N	\N	0
740ea22a-fbad-11ec-938f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	325	t	["yes much better\\n"]	0	f	f	\N	2022-10-24 14:06:14.830734	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-08 05:39:26.87	0
6f183718-fbad-11ec-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	324/1	t	["*Anova \\n"]	0	f	f	\N	2022-10-24 14:06:14.83739	f	[]	[]	0	0	0	AUTO		f	\N	6681b61a-fbad-11ec-938f-0242ac190008	\N	1
e40cb9ae-fbad-11ec-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	326	t	["for the audio please stay near your laptop otherwise we can't hear you poperly again\\n"]	0	f	f	\N	2022-10-24 14:06:14.843659	f	[]	[{"dep":["pobj"],"text":"audio"},{"dep":["pobj"],"text":"laptop"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 05:39:16.766	0
f1bedf74-fbac-11ec-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	327	t	["The audio for the livestream is really quiet and weird right now :(\\n"]	0	f	f	\N	2022-10-24 14:06:14.851244	f	[]	[{"dep":["nsubj"],"text":"audio"},{"dep":["pobj"],"text":"livestream"}]	2	2	0	AUTO		f	\N	\N	2022-07-08 05:39:47.062	0
c6d1adee-fbae-11ec-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	328	t	["can you reexplain what high leverage means?\\n"]	0	f	f	\N	2022-10-24 14:06:14.858262	f	[]	[{"dep":["nsubj"],"text":"leverage"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 05:39:09.024	0
c712b80c-fbb3-11ec-b79a-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	329	t	["How we cross out the 1/n-1, when we have it only for sx,y\\n"]	0	f	f	\N	2022-10-24 14:06:14.86538	f	[]	[{"dep":["dobj"],"text":"n-1"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 05:38:43.065	0
5e43f3a2-fbb0-11ec-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	330	t	["Can you explain again the residual Plot means and How ist works?\\n"]	0	f	f	\N	2022-10-24 14:06:14.872394	f	[]	[{"dep":["nsubj"],"text":"Plot"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 05:39:03.917	0
363334c6-fe18-11ec-9d78-0242ac190008	ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	669	t	["\\n"]	0	f	f	\N	2022-10-24 14:06:14.878653	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-07 17:14:13.28	0
323a80fa-fe85-11ec-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	331	t	["residual vs leverage: are observations with high leverage and high negative residuals also influential?\\n"]	0	f	f	\N	2022-10-24 14:06:14.887168	f	[]	[{"dep":["pobj"],"text":"leverage"},{"dep":["nsubj"],"text":"observation"},{"dep":["conj"],"text":"residual"}]	0	0	0	AUTO		f	\N	\N	\N	0
aaf304fe-fe85-11ec-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	332	t	["how do we workout the df in slide 62. where did the 27 come from\\n"]	0	f	f	\N	2022-10-24 14:06:14.893931	f	[]	[{"dep":["pobj"],"text":"slide"}]	1	1	0	AUTO		f	\N	\N	\N	0
1843e18e-fe88-11ec-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	333	t	["T=(b1-nullvalue)/SEb1; solve for b1\\n"]	1	f	f	\N	2022-10-24 14:06:14.900621	f	[]	[]	1	1	0	AUTO		f	\N	\N	\N	0
efadea12-fe8c-11ec-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	334	t	["we saw you ...\\n"]	0	f	f	\N	2022-10-24 14:06:14.907369	f	[]	[]	3	3	0	AUTO		f	\N	\N	2022-07-08 09:15:18.936	0
9ef994a4-fe8c-11ec-9520-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	335	t	["At the same volume, hardbacks are heavier\\n"]	0	f	f	\N	2022-10-24 14:06:14.919666	f	[]	[{"dep":["pobj"],"text":"volume"},{"dep":["nsubj"],"text":"hardback"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:15:41.698	0
bbb0680c-fe8c-11ec-9520-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	336	t	["same slopes, different intercepts\\n"]	0	f	f	\N	2022-10-24 14:06:14.926562	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:15:32.165	0
715fbebe-fe88-11ec-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	337	t	["4.01 + (-0.0883x) = 3.9983 solve for x\\n"]	1	f	f	\N	2022-10-24 14:06:14.944039	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
ef577e88-fe88-11ec-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	338	t	["p-value for one sided t-test \\n"]	0	f	f	\N	2022-10-24 14:06:14.951579	f	[]	[{"dep":["ROOT"],"text":"value"},{"dep":["nummod"],"text":"one"},{"dep":["pobj"],"text":"test"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:19:35.606	0
fe55a54e-fe89-11ec-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	339	t	["where do we find the slides we are just looking at?\\n"]	0	f	f	\N	2022-10-24 14:06:14.958242	f	[]	[{"dep":["dobj"],"text":"slide"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:19:21.621	0
a51c9a18-fe8a-11ec-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	340	t	["also back to b: calculating the slope b1 results in a negative value. is the answer good enough?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:14.965175	f	[]	[{"dep":["compound"],"text":"slope"},{"dep":["dobj"],"text":"result"},{"dep":["pobj"],"text":"value"},{"dep":["nsubj"],"text":"answer"}]	0	0	0	AUTO		f	\N	\N	\N	0
953ee172-fe8b-11ec-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	341	t	["c\\n"]	0	f	f	\N	2022-10-24 14:06:14.971994	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:16:22.622	0
1929d0f8-fe8e-11ec-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	342	t	["446\\n"]	0	f	f	\N	2022-10-24 14:06:14.978129	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:15:03.933	0
55d8581e-fe19-11ec-b79a-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	343	t	["Could you please explain/show again what lying horizontally away from the center of the cloud specifically means (topic of outliers being high leverage)? In which picture/graph/illustration do I need to look to find that out?\\n"]	0	f	f	\N	2022-10-24 14:06:14.987616	f	[]	[{"dep":["pobj"],"text":"center"},{"dep":["pobj"],"text":"cloud"},{"dep":["dobj"],"text":"topic"},{"dep":["nsubj"],"text":"outlier"},{"dep":["attr"],"text":"leverage"},{"dep":["nmod"],"text":"picture"},{"dep":["nmod"],"text":"graph"},{"dep":["pcomp"],"text":"illustration"}]	0	0	0	AUTO		f	\N	\N	\N	0
db9bbd76-fe85-11ec-b79a-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	332/1	t	["n = 27 (see slide 16)\\n"]	0	f	f	\N	2022-10-24 14:06:14.9943	f	[]	[]	1	1	0	AUTO		f	\N	aaf304fe-fe85-11ec-9d78-0242ac190008	\N	1
d5b54262-fe88-11ec-b79a-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	344	t	["It does provide convincing evidence that the relationship between those is ",{"attributes":{"bold":true},"insert":"negative"},"\\n"]	0	f	f	\N	2022-10-24 14:06:15.000841	f	[]	[{"dep":["dobj"],"text":"evidence"},{"dep":["nsubj"],"text":"relationship"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 06:43:05.598	0
4de25072-fe89-11ec-b79a-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	345	t	["without the absolute value\\n"]	0	f	f	\N	2022-10-24 14:06:15.007887	f	[]	[{"dep":["pobj"],"text":"value"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 06:43:50.749	0
c2a8ba98-fe8b-11ec-938f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	346	t	["d\\n"]	0	f	f	\N	2022-10-24 14:06:15.014257	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:16:18.763	0
7c1f424e-fe8c-11ec-938f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	347	t	["where do we find the info about books being over 10cm^3?\\n"]	0	f	f	\N	2022-10-24 14:06:15.021541	f	[]	[{"dep":["dobj"],"text":"info"},{"dep":["nsubj"],"text":"book"},{"dep":["pobj"],"text":"10cm^3"}]	1	1	0	AUTO		f	\N	\N	2022-07-08 09:15:48.324	0
3336e36e-fe8e-11ec-938f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	348	t	["444.68\\n"]	1	f	f	\N	2022-10-24 14:06:15.027805	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
a7cad7c0-fe8f-11ec-938f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	349	t	["All else being equal kids with…one point higher, have a 0.56147 higher Score.\\n"]	1	f	f	\N	2022-10-24 14:06:15.035427	f	[]	[{"dep":["attr"],"text":"kid"},{"dep":["nummod"],"text":"one"},{"dep":["npadvmod"],"text":"point"},{"dep":["dobj"],"text":"Score"}]	0	0	0	AUTO		f	\N	\N	\N	0
f03f8bc2-fe85-11ec-bbd1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	350	t	["Can you explain why we take 0.975 on slide 63\\n"]	0	f	f	\N	2022-10-24 14:06:15.042094	f	[]	[{"dep":["pobj"],"text":"slide"}]	0	0	0	AUTO		f	\N	\N	\N	0
e908f8d6-fe88-11ec-bbd1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	351	t	["reject H0 of no relation\\n"]	0	f	f	\N	2022-10-24 14:06:15.053815	f	[]	[{"dep":["pobj"],"text":"discard"},{"dep":["ROOT"],"text":"relationship"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:19:41.702	0
10031804-fe8e-11ec-bbd1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	352	t	["628.73\\n"]	0	f	f	\N	2022-10-24 14:06:15.060588	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:15:08.332	0
32da61e8-fe8e-11ec-bbd1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	353	t	["444.68\\n\\n"]	0	f	f	\N	2022-10-24 14:06:15.067108	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:14:59.403	0
069ba956-fe8f-11ec-bbd1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	354	t	["If I don't get the 0.3 grade bonus, am I still allowed to do the exam?\\n"]	0	f	f	\N	2022-10-24 14:06:15.073952	f	[]	[{"dep":["dobj"],"text":"exam"}]	0	0	0	AUTO		f	\N	\N	\N	0
656aa50e-fbb0-11ec-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	355	t	["Can you pls explain again Why the autolplot for the Anova model looks different?\\n"]	0	f	f	\N	2022-10-24 14:06:15.086194	f	[]	[{"dep":["compound"],"text":"Anova"},{"dep":["pobj"],"text":"model"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 05:38:58.614	0
abde0fa6-fe82-11ec-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	356	t	["can we start the class at 8:05 please? I need to get my breakfast\\nthank you \\n"]	0	f	f	\N	2022-10-24 14:06:15.093405	f	[]	[{"dep":["dobj"],"text":"class"},{"dep":["dobj"],"text":"breakfast"}]	1	1	0	AUTO		f	\N	\N	\N	0
f48f8c12-fe8b-11ec-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	357	t	["\\"One can show, that the R^2 value is equal to the square of the correlation\\ncoefficient in the simple linear model\\"\\n"]	1	f	f	\N	2022-10-24 14:06:15.101315	f	[]	[{"dep":["nsubj"],"text":"r^2 value"},{"dep":["pobj"],"text":"square"},{"dep":["pobj"],"text":"correlation"},{"dep":["acomp"],"text":"coefficient"},{"dep":["pobj"],"text":"model"}]	0	0	0	AUTO		f	\N	\N	\N	0
e83bcb58-fe8e-11ec-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	358	t	["can you explain again about the slope of cover in slide 14?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:15.114575	f	[]	[{"dep":["pobj"],"text":"slope"},{"dep":["pobj"],"text":"cover"},{"dep":["pobj"],"text":"slide"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:14:42.792	0
8f0e92d0-fe8f-11ec-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	359	t	["... have 0.56 IQ points more\\n\\n"]	0	f	f	\N	2022-10-24 14:06:15.12197	f	[]	[{"dep":["ROOT"],"text":"have"},{"dep":["amod"],"text":"more"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:12:46.073	0
b679fd04-fe86-11ec-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	360	t	["what is \\"Biological\\" in confint()?\\n"]	0	f	f	\N	2022-10-24 14:06:15.12844	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
dd7a54fc-fe87-11ec-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	361	t	["question for the peer review. In exercise  7 you wrote \\"[...] the length La of the interval can be shorter than the length Lb of the interval [...]\\". Yet you conlude that La > Lb. I am a little confused, could you maybe check it again?\\n"]	0	f	f	\N	2022-10-24 14:06:15.1371	f	[]	[{"dep":["ROOT"],"text":"question"},{"dep":["pobj"],"text":"peer review"},{"dep":["pobj"],"text":"exercise"},{"dep":["pobj"],"text":"interval"}]	0	0	0	AUTO		f	\N	\N	\N	0
6211d5a0-fe88-11ec-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	362	t	["y = intercept + slope * x, where y and x denote the averages\\n"]	0	f	f	\N	2022-10-24 14:06:15.144538	f	[]	[{"dep":["conj"],"text":"slope"},{"dep":["dobj"],"text":"average"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:20:25.932	0
cc01f9c6-fe89-11ec-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	363	t	["what was the answer to b) again?\\n"]	0	f	f	\N	2022-10-24 14:06:15.151464	f	[]	[{"dep":["nsubj"],"text":"answer"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:19:25.175	0
17173e12-fe8f-11ec-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	364	t	["What is the adjusted R squared?\\n"]	0	f	f	\N	2022-10-24 14:06:15.157791	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
93d603d4-fe8f-11ec-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	365	t	["If all else hold, kids with mothers whose IQs are one point highter have on average 0.56 higher test scores\\n"]	0	f	f	\N	2022-10-24 14:06:15.165585	f	[]	[{"dep":["ROOT"],"text":"kid"},{"dep":["pobj"],"text":"mother"},{"dep":["nummod"],"text":"one"},{"dep":["pobj"],"text":"test score"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:12:38.046	0
1dc47ac8-fe89-11ec-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	366	t	["Look at confidence interval \\n"]	0	f	f	\N	2022-10-24 14:06:15.172734	f	[]	[{"dep":["pobj"],"text":"confidence interval"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:19:30.046	0
c456ca06-fe8b-11ec-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	367	t	["what does the 0.8026 represent?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:15.179425	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:16:14.295	0
66ecd882-fe8c-11ec-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	368	t	["can you please explain why is b correct?\\n"]	0	f	f	\N	2022-10-24 14:06:15.186351	f	[]	[]	2	2	0	AUTO		f	\N	\N	2022-07-08 09:15:53.443	0
a6768e9e-fe8c-11ec-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	369	t	["We have a group difference\\n\\n"]	0	f	f	\N	2022-10-24 14:06:15.192963	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:15:36.335	0
4fc95e02-fe9e-11ec-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	354/1	t	["of course. it‘s a bonus, not a requirement \\n"]	0	f	f	\N	2022-10-24 14:06:15.199554	f	[]	[]	0	0	0	AUTO		f	\N	069ba956-fe8f-11ec-bbd1-0242ac190008	\N	1
28759d3e-fe85-11ec-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	370	t	["Could you please explain/show again what lying horizontally away from the center of the cloud specifically means (topic of outliers being high leverage)? In which picture/graph/illustration do I need to look to find that out?\\n"]	0	f	f	\N	2022-10-24 14:06:15.207955	f	[]	[{"dep":["pobj"],"text":"center"},{"dep":["pobj"],"text":"cloud"},{"dep":["dobj"],"text":"topic"},{"dep":["nsubj"],"text":"outlier"},{"dep":["attr"],"text":"leverage"},{"dep":["nmod"],"text":"picture"},{"dep":["nmod"],"text":"graph"},{"dep":["pcomp"],"text":"illustration"}]	0	0	0	AUTO		f	\N	\N	\N	0
21316cd6-fe87-11ec-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	371	t	["What does the points on the residual bis fitted Plot mean?\\n"]	0	f	f	\N	2022-10-24 14:06:15.215347	f	[]	[{"dep":["nsubj"],"text":"point"},{"dep":["pobj"],"text":"bis"},{"dep":["nsubj"],"text":"Plot"}]	0	0	0	AUTO		f	\N	\N	\N	0
4f951980-fe8a-11ec-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	372	t	["Why in b) is not the relationship negativ because of the negative sign of beauty?\\n"]	0	f	f	\N	2022-10-24 14:06:15.223046	f	[]	[{"dep":["attr"],"text":"relationship"},{"dep":["pobj"],"text":"sign"},{"dep":["pobj"],"text":"beauty"}]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:19:17.428	0
8f36ca5e-fe8d-11ec-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	373	t	["How do we know that x = 1 when we have a paper cover and not when we have a hard cover(slide 12)\\n"]	0	f	f	\N	2022-10-24 14:06:15.230378	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-08 09:15:14.435	0
a9b50f84-fe8e-11ec-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	374	t	["could you explain again why we deduct 184*1?\\n"]	0	f	f	\N	2022-10-24 14:06:15.238165	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
3e96587e-0132-11ed-bbd1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	375	t	["why *0.8 in slide 41\\n"]	0	f	f	\N	2022-10-24 14:06:15.24515	f	[]	[{"dep":["nummod"],"text":"0.8"},{"dep":["pobj"],"text":"slide"}]	0	0	0	EN		f	\N	\N	2022-07-11 16:41:25.828	0
1090787c-01ed-11ed-bbd1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	376	t	["About the homework: If \\"spring\\" were the reference variable (i.e., the lowest number in the data set that is 1), but the data set mentions that 1 is winter, how reasonable would it be to replace all 1s with \\"spring\\" instead of \\"winter\\" just because \\"spring\\" is defined as the reference variable?\\n"]	0	f	f	\N	2022-10-24 14:06:15.260466	f	[{"text":"homework","dep":["pobj"]}]	[{"dep":["pobj"],"text":"homework"},{"dep":["nsubjpass","pobj","nsubj"],"text":"spring"},{"dep":["appos"],"text":"number"},{"dep":["pobj"],"text":"data set"},{"dep":["nsubj"],"text":"data"},{"dep":["dobj"],"text":"mention"},{"dep":["pobj","attr"],"text":"winter"}]	0	0	0	EN		f	\N	\N	\N	0
0fea0a54-0130-11ed-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	377	t	["Decrease \\n"]	0	f	f	\N	2022-10-24 14:06:15.267268	f	[]	[{"dep":["ROOT"],"text":"Decrease"}]	0	0	0	EN		f	\N	\N	2022-07-11 16:41:37.725	0
c6b8d7a8-0129-11ed-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	378	t	["higher\\n"]	0	f	f	\N	2022-10-24 14:06:15.27372	f	[]	[]	0	0	0	EN		f	\N	\N	2022-07-11 16:42:06.24	0
0b3bf24c-0130-11ed-b79a-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	379	t	["decrease\\n\\n"]	0	f	f	\N	2022-10-24 14:06:15.281286	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-11 16:41:42.5	0
f768d63e-0131-11ed-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	380	t	["Will the RMSE formula be on the exam cheat sheet?\\n"]	0	f	f	\N	2022-10-24 14:06:15.288211	f	[]	[{"dep":["pobj"],"text":"exam cheat sheet"}]	0	0	0	EN		f	\N	\N	\N	0
41f9007a-0132-11ed-b79a-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	381	t	["sorry what was the solution again for the quiz on slide 8?\\n"]	0	f	f	\N	2022-10-24 14:06:15.295423	f	[]	[{"dep":["nsubj"],"text":"solution"},{"dep":["pobj"],"text":"quiz"},{"dep":["pobj"],"text":"slide"}]	0	0	0	EN		f	\N	\N	\N	0
d9185adc-0132-11ed-b79a-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	381/1	t	["a\\n\\n"]	2	f	f	\N	2022-10-24 14:06:15.302172	f	[]	[]	0	0	0	AUTO		f	\N	41f9007a-0132-11ed-b79a-0242ac190008	\N	1
897efee8-0134-11ed-b79a-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	382	t	["What is RSS\\n"]	0	f	f	\N	2022-10-24 14:06:15.307555	f	[]	[{"dep":["acomp"],"text":"rss"}]	0	0	0	AUTO		f	\N	\N	\N	0
c63e82ac-0406-11ed-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	383	t	["What's the difference between b0 and Beta0?\\n"]	0	f	f	\N	2022-10-24 14:06:15.319234	f	[]	[{"dep":["nsubj"],"text":"difference"}]	0	0	0	EN		f	\N	\N	2022-07-15 08:45:48.138	0
8a9f8708-0129-11ed-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	384	t	["For last week's quiz, in question 2, the mean from the question showed only 1 decimal place, but the standard solution somehow uses a mean with 2 decimal places, which leads to deviations of my answers to the standard solution and losing the points. Is there any way to compensate for it? \\n"]	0	f	f	\N	2022-10-24 14:06:15.338994	f	[]	[{"dep":["poss"],"text":"last week 's"},{"dep":["pobj"],"text":"quiz"},{"dep":["pobj"],"text":"question"},{"dep":["nsubj","dobj"],"text":"mean"},{"dep":["nummod"],"text":"only 1"},{"dep":["pobj","dobj"],"text":"place"},{"dep":["pobj","nsubj"],"text":"solution"},{"dep":["pobj"],"text":"deviation"},{"dep":["pobj"],"text":"answer"},{"dep":["dobj"],"text":"point"},{"dep":["attr"],"text":"way"}]	0	0	0	AUTO		f	\N	\N	2022-07-11 15:01:58.122	0
fa48054e-012e-11ed-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	385	t	["c\\n\\n"]	0	f	f	\N	2022-10-24 14:06:15.345963	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-11 15:35:48.437	0
192ed510-0408-11ed-b79a-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	386	t	["What is y_out \\n"]	0	f	f	\N	2022-10-24 14:06:15.35297	f	[]	[]	0	0	0	EN		f	\N	\N	2022-07-15 06:34:14.279	0
cc12a58a-0129-11ed-938f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	387	t	["If all else hold, if mother worked, child's score is 2.5... higher\\n"]	0	f	f	\N	2022-10-24 14:06:15.360706	f	[]	[{"dep":["nsubj"],"text":"mother"},{"dep":["poss"],"text":"child"},{"dep":["nsubj"],"text":"score"}]	0	0	0	AUTO		f	\N	\N	2022-07-11 16:42:02.007	0
960923e2-0133-11ed-938f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	388	t	["What is the ei in the Formulas?\\n"]	0	f	f	\N	2022-10-24 14:06:15.367819	f	[]	[{"dep":["pobj"],"text":"formula"}]	0	0	0	EN		f	\N	\N	2022-07-11 16:40:10.462	0
46b874b4-0138-11ed-938f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	382/1	t	["residual sum of squares\\n"]	0	f	f	\N	2022-10-24 14:06:15.374605	f	[]	[]	0	0	0	PT		f	\N	897efee8-0134-11ed-b79a-0242ac190008	\N	1
877e268e-040c-11ed-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	389	t	["Could you please upload the solutions to the test-exam? \\n"]	0	f	f	\N	2022-10-24 14:06:15.381577	f	[]	[{"dep":["dobj"],"text":"solution"},{"dep":["compound"],"text":"test"},{"dep":["pobj"],"text":"exam"}]	0	0	0	EN		f	\N	\N	2022-07-15 08:44:33.66	0
7f65803c-040c-11ed-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	390	t	["Do we get the solution to the mock exam? That would be very helpful to understand the steps to do. Thanks in advance\\n"]	0	f	f	\N	2022-10-24 14:06:15.390156	f	[]	[{"dep":["dobj"],"text":"solution"},{"dep":["pobj"],"text":"exam"},{"dep":["dobj"],"text":"step"},{"dep":["ROOT"],"text":"thank"},{"dep":["pobj"],"text":"advance"}]	0	0	0	EN		f	\N	\N	2022-07-15 08:44:52.843	0
9d0ed70a-0f10-11ed-a513-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	508	t	["virtual clapping\\n\\n"]	0	f	f	\N	2022-10-24 14:06:16.618358	f	[]	[]	0	0	0	IT		f	\N	\N	\N	0
b6bf7632-040c-11ed-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	129/1	t	["and how the leverage score is computed\\n"]	0	f	f	\N	2022-10-24 14:06:15.397145	f	[]	[{"dep":["nsubjpass"],"text":"leverage score"}]	0	0	0	EN		f	\N	7be011ac-040c-11ed-9520-0242ac190008	\N	1
46ff4bb8-040e-11ed-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	391	t	["Would the answer to the first question in the exam be to introduce a blocking variable? \\n"]	0	f	f	\N	2022-10-24 14:06:15.40385	f	[]	[{"dep":["nsubj"],"text":"answer"},{"dep":["amod"],"text":"first"},{"dep":["pobj"],"text":"question"},{"dep":["pobj"],"text":"exam"}]	0	0	0	EN		f	\N	\N	\N	0
2c2465a2-040f-11ed-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	392	t	["If I understand it right we don´t have to know how to solve e.g. a hypothesis test by hand with a calculator but just have to know how to read a given R table?\\n"]	0	f	f	\N	2022-10-24 14:06:15.411941	f	[]	[{"dep":["dobj"],"text":"hypothesis test"},{"dep":["pobj"],"text":"hand"},{"dep":["pobj"],"text":"calculator"}]	0	0	0	EN		f	\N	\N	\N	0
9ba639a4-0406-11ed-bbd1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	393	t	["Why do we choose model_int over model_select?\\n"]	0	f	f	\N	2022-10-24 14:06:15.427159	f	[]	[]	0	0	0	EN		f	\N	\N	2022-07-15 08:45:53.036	0
3573ea2e-040a-11ed-bbd1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	394	t	["What really are the fitted values?\\n"]	0	f	f	\N	2022-10-24 14:06:15.433566	f	[]	[{"dep":["nsubj"],"text":"value"}]	0	0	0	EN		f	\N	\N	\N	0
a1811540-0403-11ed-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	395	t	["I do not hear anything\\n"]	0	f	f	\N	2022-10-24 14:06:15.445577	f	[]	[]	0	0	0	EN		f	\N	\N	2022-07-15 08:46:42.286	0
bfc51618-0404-11ed-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	396	t	["What is the difference between forward and backward selection\\n"]	0	f	f	\N	2022-10-24 14:06:15.453005	f	[]	[{"dep":["nsubj"],"text":"difference"},{"dep":["pobj"],"text":"selection"}]	0	0	0	EN		f	\N	\N	\N	0
6170ef92-040e-11ed-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	397	t	["I think the slides of the last chapter are not complete on Moodle \\n"]	0	f	f	\N	2022-10-24 14:06:15.460206	f	[]	[{"dep":["nsubj"],"text":"slide"},{"dep":["pobj"],"text":"chapter"},{"dep":["pobj"],"text":"moodle"}]	0	0	0	EN		f	\N	\N	\N	0
3e0e2484-0404-11ed-938f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	398	t	["I am terribly sorry to bother you with question 5 from the recent quiz on numerical inference and Anova. Are the solutions for c, d, e really correct?\\n\\nThe following data from a ANOVA test was given:\\nlevels = 5\\nnumber of samples = 44631\\np-value = 0.1635\\nsignificance value = 0.05\\n\\nexample: \\nc) identify dfe: n-k = 44631-5 = 44626 => the solution states 6511.0324\\n\\nPlease respond in frag.jetzt if possible. Thank you very much.\\n"]	0	f	f	\N	2022-10-24 14:06:15.486123	f	[]	[{"dep":["pobj"],"text":"question"},{"dep":["pobj"],"text":"quiz"},{"dep":["pobj"],"text":"inference"},{"dep":["conj"],"text":"Anova"},{"dep":["nsubj"],"text":"solution"},{"dep":["nsubjpass"],"text":"data"},{"dep":["ROOT"],"text":"level"},{"dep":["appos"],"text":"number"},{"dep":["pobj"],"text":"sample"},{"dep":["ROOT"],"text":"value ="},{"dep":["ROOT"],"text":"0.05"},{"dep":["ROOT"],"text":"example"},{"dep":["dobj"],"text":"dfe"}]	0	0	0	EN		f	\N	\N	\N	0
6f48af2c-0451-11ed-938f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	399	t	["regarding the current homework, is there maybe a problem with the data set because for me all observations for the season variable are \\"NA\\" or did I make a mistake here? \\n"]	0	f	f	\N	2022-10-24 14:06:15.49521	f	[]	[{"dep":["pobj"],"text":"homework"},{"dep":["attr"],"text":"problem"},{"dep":["pobj"],"text":"data"},{"dep":["nsubj"],"text":"observation"},{"dep":["det"],"text":"the season"},{"dep":["pobj"],"text":"variable"},{"dep":["dobj"],"text":"mistake"}]	2	2	0	EN		f	\N	\N	\N	0
17989176-040f-11ed-b79a-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	400	t	["Is there a chance to decrease the passing % of the grade bonus to 60% like in a normal exam as well? Many other students I talked with are struggling with the same thing and otherwise all our weekly efforts wouldn’t have paid off\\n"]	0	f	f	\N	2022-10-24 14:06:15.504756	f	[]	[{"dep":["attr"],"text":"chance"},{"dep":["dobj"],"text":"the passing %"},{"dep":["pobj"],"text":"60 %"},{"dep":["pobj"],"text":"exam"},{"dep":["nsubj"],"text":"student"},{"dep":["pobj"],"text":"thing"},{"dep":["amod"],"text":"weekly"},{"dep":["nsubj"],"text":"effort"}]	-1	0	1	EN		f	\N	\N	\N	0
8beb5f9a-045f-11ed-b79a-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	401	t	["I have problems downloading a using the dsbox lib for the latest homework, who can i talk to ?\\n"]	0	f	f	\N	2022-10-24 14:06:15.512266	f	[]	[{"dep":["dobj"],"text":"problem"},{"dep":["dobj"],"text":"lib"},{"dep":["pobj"],"text":"homework"}]	0	0	0	EN		f	\N	\N	\N	0
3e870772-040f-11ed-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	402	t	["if you say you answer by text. where to read the answer?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:15.519903	f	[]	[{"dep":["pobj"],"text":"text"},{"dep":["dobj"],"text":"answer"}]	0	0	0	EN		f	\N	\N	2022-07-15 08:25:43.27	0
de55051e-0415-11ed-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	403	t	["For the last quiz of today's lecture, if there is no interaction of gendermale:age, we can't tell wether the slopes of female and male are same or not, is that right? How can we give an interpretation in this case for age and gender?\\n"]	0	f	f	\N	2022-10-24 14:06:15.529756	f	[]	[{"dep":["pobj"],"text":"quiz"},{"dep":["poss"],"text":"today"},{"dep":["pobj"],"text":"lecture"},{"dep":["attr"],"text":"interaction"},{"dep":["pobj","npadvmod"],"text":"age"},{"dep":["nsubj"],"text":"slope"},{"dep":["conj"],"text":"male"},{"dep":["dobj"],"text":"interpretation"},{"dep":["pobj"],"text":"case"},{"dep":["conj"],"text":"gender"}]	0	0	0	EN		f	\N	\N	\N	0
ed3dd066-04f5-11ed-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	404	t	["after factoring the variables the data set still shows NA. How can I solve this problem? \\n\\n"]	0	f	f	\N	2022-10-24 14:06:15.539006	f	[]	[{"dep":["dobj"],"text":"variable"},{"dep":["compound"],"text":"data"},{"dep":["dobj"],"text":"problem"}]	0	0	0	EN		f	\N	\N	\N	0
77b041a8-040f-11ed-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	405	t	["how much points are available in the bonus? i.e. we need to get 70% of how much?\\n"]	0	f	f	\N	2022-10-24 14:06:15.546416	f	[{"text":"bonus","dep":["pobj"]}]	[{"dep":["nsubj"],"text":"point"},{"dep":["pobj"],"text":"bonus"},{"dep":["dobj"],"text":"70 %"}]	0	0	0	EN		f	\N	\N	\N	0
0201417e-0418-11ed-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	392/1	t	["no, that's not true. we already had several exercise problems about e.g. testing a mean or proportion, which were done \\"by hand\\". also in the regression context, one can have problems, which require some calculations using a calculator\\n"]	0	f	f	\N	2022-10-24 14:06:15.556551	f	[]	[{"dep":["dobj"],"text":"mean"},{"dep":["conj"],"text":"proportion"},{"dep":["pobj"],"text":"hand"},{"dep":["dobj"],"text":"problem"},{"dep":["dobj"],"text":"calculation"},{"dep":["dobj"],"text":"calculator"}]	0	0	0	EN		f	\N	2c2465a2-040f-11ed-bb78-0242ac190008	\N	1
c494cb9c-040a-11ed-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	406	t	["I feel like this is quite theoretical, can you please give some examples to explain these, I'm lost\\n"]	0	f	f	\N	2022-10-24 14:06:15.563198	f	[]	[{"dep":["dobj"],"text":"example"}]	0	0	0	EN		f	\N	\N	2022-07-15 08:45:20.643	0
f9605640-06ab-11ed-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	425	t	["all else being constant, when increasing the weight by one unit, the MPG decreases by 0.36 percent\\n"]	0	f	f	\N	2022-10-24 14:06:15.750101	f	[]	[{"dep":["dobj"],"text":"weight"},{"dep":["nummod"],"text":"one"},{"dep":["pobj"],"text":"unit"},{"dep":["nsubj"],"text":"MPG"}]	0	0	0	EN		f	\N	\N	2022-07-18 15:12:19.451	0
fe0254fa-06ab-11ed-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	426	t	["We should compare each t-value with 1.98?\\n"]	0	f	f	\N	2022-10-24 14:06:15.757533	f	[]	[{"dep":["dobj"],"text":"value"},{"dep":["pobj"],"text":"1.98"}]	0	0	0	EN		f	\N	\N	2022-07-18 16:26:00.417	0
5142283e-0418-11ed-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	403/1	t	["if the model does not include an interaction, then the model says that the slope of age is the same for females and males. hence, we can't tell if there is a difference. \\nthe interpretation of gender and age is just the usual one. \\n"]	0	f	f	\N	2022-10-24 14:06:15.573439	f	[]	[{"dep":["nsubj"],"text":"model"},{"dep":["dobj"],"text":"interaction"},{"dep":["nsubj"],"text":"slope"},{"dep":["conj","pobj"],"text":"age"},{"dep":["pobj"],"text":"female"},{"dep":["conj"],"text":"male"},{"dep":["attr"],"text":"difference"},{"dep":["nsubj"],"text":"interpretation"},{"dep":["pobj"],"text":"gender"},{"dep":["attr"],"text":"one"}]	1	1	0	EN		f	\N	de55051e-0415-11ed-9d78-0242ac190008	\N	1
5b3ced74-0404-11ed-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	407	t	["Why is the mean of the response variable the intercept?\\n"]	0	f	f	\N	2022-10-24 14:06:15.579802	f	[]	[{"dep":["nsubj"],"text":"mean"},{"dep":["pobj"],"text":"response variable"},{"dep":["nsubj"],"text":"intercept"}]	0	0	0	AUTO		f	\N	\N	2022-07-15 08:46:21.332	0
15f506d2-041a-11ed-bbd1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	405/1	t	["if i haven't miss calculated, it should be 158 up to now. since we will have another 45 (25 for H.11.1 and 20 for the last quiz), it will be 203 in total. \\n\\nsome later time i will provide an \\"official number\\" on moodle\\n"]	0	f	f	\N	2022-10-24 14:06:15.594362	f	[]	[{"dep":["attr"],"text":"158"},{"dep":["pobj"],"text":"h.11.1"},{"dep":["pobj"],"text":"quiz"},{"dep":["attr"],"text":"203"},{"dep":["pobj"],"text":"total"},{"dep":["npadvmod"],"text":"time"},{"dep":["dobj"],"text":"number"},{"dep":["pobj"],"text":"moodle"}]	0	0	0	EN		f	\N	77b041a8-040f-11ed-bc23-0242ac190008	2022-07-18 11:15:36.556	1
3c68e386-069c-11ed-9520-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	408	t	["Could you please explain again the concept of an intercept? Exercise 14 in the homework states that there are two different intercepts, one for each level of the holiday variable. Isn't the whole idea that the intercept is the prediction for when all other variables take the reference level? We add and subtract from the intercept if holiday changes from \\"no\\" to \\"yes\\" but the intercept stays the same just the variable subtracting something swaps from \\"0\\" to \\"1\\" effectively lowering predictions.\\n"]	0	f	f	\N	2022-10-24 14:06:15.605219	f	[]	[{"dep":["dobj"],"text":"concept"},{"dep":["pobj","nsubj","attr"],"text":"intercept"},{"dep":["ROOT"],"text":"exercise"},{"dep":["nummod"],"text":"two"},{"dep":["appos"],"text":"one"},{"dep":["pobj"],"text":"level"},{"dep":["nsubj"],"text":"idea"},{"dep":["dobj","attr"],"text":"prediction"},{"dep":["nsubj"],"text":"variable"},{"dep":["dobj"],"text":"reference level"},{"dep":["compound"],"text":"holiday"},{"dep":["dobj"],"text":"swap"}]	0	0	0	EN		f	\N	\N	\N	0
15f82942-069b-11ed-9520-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	409	t	["Why is exercise 12 in H11.1 different in the solution and the rmd file we received? In the rmd it was \\"Is the estimated slope of temperature the same or different for holidays and non-holidays? How about the slope for feeling temperature? Justify your answers.\\"  The solution adds another sentence: \\"",{"attributes":{"bold":true},"insert":"Write the linear models for holidays and non-holidays"},". ....\\" Should we have known from the original exercise that a linear model was necessary? One would miss 1 point if he answered the original.\\n"]	0	f	f	\N	2022-10-24 14:06:15.618674	f	[]	[{"dep":["pobj","ROOT"],"text":"exercise"},{"dep":["pobj","nsubj"],"text":"solution"},{"dep":["pobj"],"text":"rmd"},{"dep":["pobj","nsubj"],"text":"slope"},{"dep":["pobj","dobj"],"text":"temperature"},{"dep":["pobj"],"text":"holiday"},{"dep":["conj"],"text":"non - holiday"},{"dep":["dobj"],"text":"answer"},{"dep":["dobj"],"text":"sentence"},{"dep":["nsubj","dobj"],"text":"model"},{"dep":["nsubj"],"text":"One"},{"dep":["dobj"],"text":"point"},{"dep":["dobj"],"text":"original"}]	0	0	0	EN		f	\N	\N	\N	0
f9710e3e-068a-11ed-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	405/2	t	["i will answer that one on moodle\\n"]	0	f	f	\N	2022-10-24 14:06:15.630362	f	[]	[{"dep":["dobj"],"text":"one"},{"dep":["pobj"],"text":"moodle"}]	0	0	0	EN		f	\N	77b041a8-040f-11ed-bc23-0242ac190008	\N	1
d6f07842-06ab-11ed-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	410	t	["All else being constant, if weight increases by 1, the response will decrease by -3.143e-04",{"attributes":{"blockquote":true},"insert":"\\n"}]	0	f	f	\N	2022-10-24 14:06:15.637413	f	[]	[{"dep":["nsubj"],"text":"weight"},{"dep":["nsubj"],"text":"response"},{"dep":["punct"],"text":"-3.143e-04"}]	0	0	0	EN		f	\N	\N	2022-07-18 16:27:20.407	0
c1c7948e-06ae-11ed-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	411	t	["\\nWhat means the \\"joint distribution of gender and Status\\"?\\n"]	0	f	f	\N	2022-10-24 14:06:15.64536	f	[]	[{"dep":["dobj"],"text":"distribution"},{"dep":["pobj"],"text":"gender"},{"dep":["conj"],"text":"Status"}]	0	0	0	EN		f	\N	\N	\N	0
78d593f6-06b4-11ed-bc23-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	412	t	["Why the intercept is 1.1815\\n"]	0	f	f	\N	2022-10-24 14:06:15.653231	f	[]	[{"dep":["nsubj"],"text":"intercept"}]	0	0	0	EN		f	\N	\N	2022-07-19 09:12:21.048	0
899e97dc-06aa-11ed-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	413	t	["p value\\n"]	0	f	f	\N	2022-10-24 14:06:15.659666	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
31678af0-06ab-11ed-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	414	t	["2 * pt(t-value, df = 88, lower.tail = false)\\n"]	1	f	f	\N	2022-10-24 14:06:15.66642	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
851046ce-06ab-11ed-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	415	t	["how did you get 0.025\\n"]	0	f	f	\N	2022-10-24 14:06:15.673145	f	[]	[]	-1	0	1	AUTO		f	\N	\N	\N	0
1bc40f54-06ae-11ed-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	416	t	["what are the answers of d)?\\n"]	0	f	f	\N	2022-10-24 14:06:15.680082	f	[]	[{"dep":["nsubj"],"text":"answer"}]	0	0	0	EN		f	\N	\N	\N	0
7b442de6-06b4-11ed-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	417	t	["Why 1,1815 with age 0 if our intercept is 1,82\\n"]	0	f	f	\N	2022-10-24 14:06:15.687514	f	[]	[{"dep":["pobj"],"text":"age"},{"dep":["nsubj"],"text":"intercept"}]	0	0	0	EN		f	\N	\N	2022-07-19 09:12:15.237	0
89328270-06ac-11ed-bb78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	418	t	["Why we make it *100\\n"]	0	f	f	\N	2022-10-24 14:06:15.694324	f	[]	[{"dep":["ROOT"],"text":"100"}]	0	0	0	EN		f	\N	\N	2022-07-18 16:33:27.664	0
cc16ab82-06a9-11ed-bbd1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	419	t	["Where can we find the slide of exercise?\\n"]	0	f	f	\N	2022-10-24 14:06:15.701229	f	[]	[{"dep":["dobj"],"text":"slide"},{"dep":["pobj"],"text":"exercise"}]	0	0	0	EN		f	\N	\N	\N	0
f7c6cd9c-06b4-11ed-bbd1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	420	t	["Why we multiple with 0.94?\\n"]	0	f	f	\N	2022-10-24 14:06:15.707862	f	[]	[]	0	0	0	EN		f	\N	\N	\N	0
1e466298-06b5-11ed-bbd1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	421	t	["What is the interpretation of OR?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:15.714819	f	[]	[{"dep":["nsubj"],"text":"interpretation"}]	0	0	0	EN		f	\N	\N	\N	0
04a48614-06ae-11ed-bbd1-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	422	t	["Is problematic that the variance of the risiduals is around 1 \\n"]	0	f	f	\N	2022-10-24 14:06:15.721696	f	[]	[{"dep":["nsubj"],"text":"variance"}]	0	0	0	EN		f	\N	\N	\N	0
6ac9346c-06a9-11ed-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	423	t	["From where we can get a overview of the points status that we have already collected? \\n"]	0	f	f	\N	2022-10-24 14:06:15.734278	f	[]	[{"dep":["dobj"],"text":"overview"},{"dep":["pobj"],"text":"point status"}]	0	0	0	EN		f	\N	\N	\N	0
59686eca-06ab-11ed-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	424	t	["Could the hint also be qt(0.975, df = 88)?\\n"]	0	f	f	\N	2022-10-24 14:06:15.741551	f	[]	[{"dep":["nsubj"],"text":"hint"},{"dep":["acomp"],"text":"qt(0.975"},{"dep":["dep"],"text":"df ="}]	0	0	0	EN		f	\N	\N	\N	0
b6183962-0f10-11ed-868e-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	509	t	["Thank you for the lecture! :)\\n"]	0	f	f	\N	2022-10-24 14:06:16.635139	f	[]	[{"dep":["pobj"],"text":"lecture"}]	0	0	0	EN		f	\N	\N	\N	0
f6675d9a-06b4-11ed-a1b4-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	427	t	["Why the slope is round 6%\\n"]	0	f	f	\N	2022-10-24 14:06:15.764805	f	[]	[{"dep":["nsubj"],"text":"slope"},{"dep":["npadvmod"],"text":"6 %"}]	0	0	0	EN		f	\N	\N	2022-07-19 09:12:10.305	0
4e50fbc4-06b5-11ed-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	428	t	["\\nWhy 1,1815 with age 0 if our intercept is 1,82\\n"]	0	f	f	\N	2022-10-24 14:06:15.772158	f	[]	[{"dep":["pobj"],"text":"age"},{"dep":["nsubj"],"text":"intercept"}]	0	0	0	EN		f	\N	\N	\N	0
7672af68-06aa-11ed-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	429	t	["Compute pvalue \\n"]	0	f	f	\N	2022-10-24 14:06:15.778526	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
d993d84c-06aa-11ed-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	430	t	["|t| > 1.96\\n\\n"]	0	f	f	\N	2022-10-24 14:06:15.785261	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
bd1fdd5e-06ab-11ed-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	415/1	t	["Is the 1.98 the t value , What wxactly is it?\\n"]	0	f	f	\N	2022-10-24 14:06:15.792896	f	[]	[{"dep":["nummod"],"text":"1.98"},{"dep":["attr"],"text":"t value"}]	0	0	0	EN		f	\N	851046ce-06ab-11ed-bb78-0242ac190008	\N	1
e6fddf04-06ab-11ed-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	431	t	["Holding all other factors fixed if we increase weight by 1 mpg rises by 100*slope\\n"]	0	f	f	\N	2022-10-24 14:06:15.799512	f	[]	[{"dep":["dobj"],"text":"factor"},{"dep":["dobj"],"text":"weight"},{"dep":["ccomp"],"text":"rise"}]	0	0	0	EN		f	\N	\N	2022-07-18 16:30:16.548	0
4d6361aa-06ad-11ed-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	432	t	["We would pick the smaller model as there the r squared is larger -> higher percentage of variability can be explained by the model\\n"]	0	f	f	\N	2022-10-24 14:06:15.808167	f	[]	[{"dep":["pobj","dobj"],"text":"model"},{"dep":["advcl"],"text":"r squared"},{"dep":["nsubjpass"],"text":"percentage"},{"dep":["pobj"],"text":"variability"}]	0	0	0	EN		f	\N	\N	2022-07-18 16:33:15.379	0
1369b80e-06ae-11ed-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	433	t	["So by d) is it appropriate or not?\\n"]	0	f	f	\N	2022-10-24 14:06:15.815409	f	[]	[]	0	0	0	EN		f	\N	\N	2022-07-19 09:29:35.01	0
5b8be044-06ae-11ed-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	434	t	["Is there a relation between age/gender having an influence on whether they survived\\n\\n"]	0	f	f	\N	2022-10-24 14:06:15.82325	f	[]	[{"dep":["attr"],"text":"relation"},{"dep":["nmod"],"text":"age"},{"dep":["pobj"],"text":"gender"},{"dep":["dobj"],"text":"influence"}]	0	0	0	EN		f	\N	\N	\N	0
c36974d8-06ae-11ed-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	435	t	["Could you give at the end information about exam questions - do we have to write code ; would it be multiple choice ...\\n"]	0	f	f	\N	2022-10-24 14:06:15.830562	f	[]	[{"dep":["dobj"],"text":"code"},{"dep":["attr"],"text":"choice"}]	0	0	0	EN		f	\N	\N	\N	0
e3cfce74-06af-11ed-b392-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	436	t	["How do we receive different probabilitiy for every person\\n"]	0	f	f	\N	2022-10-24 14:06:15.837803	f	[]	[{"dep":["dobj"],"text":"probabilitiy"},{"dep":["pobj"],"text":"person"}]	0	0	0	EN		f	\N	\N	2022-07-19 09:16:21.608	0
eaef8736-06a9-11ed-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	437	t	["t= Point estimate/SE\\n"]	0	f	f	\N	2022-10-24 14:06:15.844782	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
753127ec-06aa-11ed-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	438	t	["p value\\n\\n"]	0	f	f	\N	2022-10-24 14:06:15.851684	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
8c4ab5a6-06aa-11ed-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	439	t	["pr(>abs(t))\\n"]	0	f	f	\N	2022-10-24 14:06:15.858482	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-18 15:02:53.701	0
b8398b2e-06aa-11ed-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	440	t	["Check whether t value is below 0.025 or higher 0.975?\\n"]	0	f	f	\N	2022-10-24 14:06:15.86545	f	[]	[{"dep":["nsubj"],"text":"t value"}]	0	0	0	EN		f	\N	\N	\N	0
c0e0335e-06aa-11ed-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	441	t	["pt() with the specific degree of freedom\\n"]	0	f	f	\N	2022-10-24 14:06:15.872698	f	[]	[{"dep":["pobj"],"text":"degree"},{"dep":["pobj"],"text":"freedom"}]	0	0	0	EN		f	\N	\N	\N	0
ff7e6608-06aa-11ed-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	442	t	["If our tvalue is bigger than 1.9 we Can reject H0\\n"]	0	f	f	\N	2022-10-24 14:06:15.880013	f	[]	[{"dep":["pobj"],"text":"1.9"}]	0	0	0	EN		f	\N	\N	2022-07-19 09:34:53.763	0
bffccca2-06ac-11ed-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	443	t	["why is the degrees of freedom 88?? There are 93 observations and 4 categorical varibales\\n"]	0	f	f	\N	2022-10-24 14:06:15.888199	f	[]	[{"dep":["nsubj"],"text":"degree"},{"dep":["attr"],"text":"observation"}]	0	0	0	EN		f	\N	\N	2022-07-18 16:33:21.566	0
70bfb586-06ad-11ed-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	444	t	["choose model_small\\n"]	0	f	f	\N	2022-10-24 14:06:15.89471	f	[]	[]	0	0	0	IT		f	\N	\N	2022-07-18 16:32:53.169	0
5cd7d3ba-06b1-11ed-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	445	t	["can you explain again why we have to do one to one transformation\\n"]	0	f	f	\N	2022-10-24 14:06:15.90221	f	[]	[{"dep":["nummod"],"text":"one"},{"dep":["pobj"],"text":"transformation"}]	0	0	0	EN		f	\N	\N	2022-07-19 09:12:33.292	0
365a3aaa-06b3-11ed-9d78-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	446	t	["how did they get the 2.17 and 1.222\\n\\n"]	0	f	f	\N	2022-10-24 14:06:15.909243	f	[]	[]	0	0	0	EN		f	\N	\N	2022-07-19 09:12:25.789	0
dbf6336e-06aa-11ed-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	447	t	["why can't we just compare the t value with critical value?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:15.916986	f	[]	[{"dep":["dobj"],"text":"t value"},{"dep":["pobj"],"text":"value"}]	1	1	0	EN		f	\N	\N	2022-07-19 09:35:01.061	0
3ac53656-06ab-11ed-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	448	t	[">\\n"]	0	f	f	\N	2022-10-24 14:06:15.923701	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-19 09:34:35.772	0
9c7468c2-06ab-11ed-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	449	t	["Can you explain again Why we Can reject h0 if our tvalue is bigger \\n"]	0	f	f	\N	2022-10-24 14:06:15.930791	f	[]	[]	0	0	0	EN		f	\N	\N	2022-07-18 16:30:04.301	0
1a18b6a2-06ac-11ed-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	450	t	["Can you explain again Why we Can compare our tvalues to 1,9\\n"]	0	f	f	\N	2022-10-24 14:06:15.937782	f	[]	[]	0	0	0	EN		f	\N	\N	2022-07-18 16:26:48.446	0
5c1f58da-06ac-11ed-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	451	t	["from where do we know that the weight variable changes only if the real weights changes by 100 pounds?\\n"]	0	f	f	\N	2022-10-24 14:06:15.945844	f	[]	[{"dep":["compound"],"text":"weight"},{"dep":["nsubj"],"text":"change"},{"dep":["ccomp"],"text":"weight change"},{"dep":["pobj"],"text":"100 pounds"}]	0	0	0	AUTO		f	\N	\N	2022-07-18 16:26:58.246	0
aa735548-06ae-11ed-945f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	452	t	["What means the \\"joint distribution of sex and Status\\"?\\n"]	0	f	f	\N	2022-10-24 14:06:15.953642	f	[]	[{"dep":["dobj"],"text":"distribution"},{"dep":["pobj"],"text":"sex"},{"dep":["conj"],"text":"Status"}]	0	0	0	EN		f	\N	\N	2022-07-18 15:31:33.208	0
93559bb8-06aa-11ed-9520-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	453	t	["Check whether t value is below 0.025 or 0.975?\\n"]	0	f	f	\N	2022-10-24 14:06:15.960882	f	[]	[{"dep":["nsubj"],"text":"t value"}]	0	0	0	EN		f	\N	\N	2022-07-18 15:02:48.826	0
5542e124-06ae-11ed-938f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	443/1	t	["what was the reason for that?\\n"]	0	f	f	\N	2022-10-24 14:06:15.968013	f	[]	[{"dep":["nsubj"],"text":"reason"}]	0	0	0	AUTO		f	\N	bffccca2-06ac-11ed-9d78-0242ac190008	\N	1
39dd99e4-06b1-11ed-938f-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	454	t	["General question: on the already uploaded formula sheet there are some formulas missing. Is this because we need to know them by heart or just because they weren't needed for the exam?\\n"]	0	f	f	\N	2022-10-24 14:06:15.974945	f	[]	[{"dep":["ROOT"],"text":"question"},{"dep":["attr"],"text":"formula"},{"dep":["pobj"],"text":"heart"},{"dep":["pobj"],"text":"exam"}]	0	0	0	EN		f	\N	\N	\N	0
8c77afaa-0742-11ed-b981-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	428/1	t	["correct. that's a typo. sorry. i will upload an update\\n"]	0	f	f	\N	2022-10-24 14:06:15.982239	f	[]	[]	0	0	0	AUTO		f	\N	4e50fbc4-06b5-11ed-b392-0242ac190008	\N	1
45bcceb2-0745-11ed-90d8-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	434/1	t	["yes, the boxplots give this impression. there might be an interaction effect of Age and Sex\\n"]	0	f	f	\N	2022-10-24 14:06:15.989092	f	[]	[{"dep":["nsubj"],"text":"boxplot"},{"dep":["dobj"],"text":"impression"},{"dep":["attr"],"text":"interaction effect"},{"dep":["pobj"],"text":"Age"},{"dep":["conj"],"text":"Sex"}]	0	0	0	EN		f	\N	5b8be044-06ae-11ed-b392-0242ac190008	\N	1
ea659efa-098c-11ed-90d8-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	455	t	["Does the decreasing RR indicate that smoking is more harmful regarding lung cancer than having birds?\\n"]	0	f	f	\N	2022-10-24 14:06:15.99878	f	[]	[{"dep":["nsubj"],"text":"smoking"},{"dep":["pobj"],"text":"lung cancer"},{"dep":["dobj"],"text":"bird"}]	0	0	0	EN		f	\N	\N	2022-07-22 08:06:17.029	0
c8964910-0742-11ed-b981-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	420/1	t	["I have just re-written the odd ratio equation.\\n\\nodds with an increased age of one year = 0.94 * odds with the age unchanged\\n"]	0	f	f	\N	2022-10-24 14:06:16.01615	f	[]	[{"dep":["ROOT"],"text":"odd"},{"dep":["pobj"],"text":"age"},{"dep":["pobj"],"text":"one year"}]	0	0	0	EN		f	\N	f7c6cd9c-06b4-11ed-bbd1-0242ac190008	\N	1
67d1f0a6-0743-11ed-b438-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	454/1	t	["for some formulas this may still change when we upload this year's version of the cheat sheet. But it will probably still be the case that some formulas are \\"missing\\". Then it's up to you to think about whether the missing formulas are really essential ones that you should then know by heart, or rather unimportant ones that you probably won't need for the exam.\\n"]	0	f	f	\N	2022-10-24 14:06:16.028331	f	[]	[{"dep":["pobj","nsubj"],"text":"formula"},{"dep":["poss"],"text":"year"},{"dep":["dobj"],"text":"version"},{"dep":["pobj"],"text":"cheat sheet"},{"dep":["attr"],"text":"case"},{"dep":["conj","attr"],"text":"one"},{"dep":["pobj"],"text":"heart"},{"dep":["pobj"],"text":"exam"}]	0	0	0	EN		f	\N	39dd99e4-06b1-11ed-938f-0242ac190008	\N	1
2223a9c6-06af-11ed-9520-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	456	t	["How did you see that residuals are not normally distributed\\n"]	0	f	f	\N	2022-10-24 14:06:16.037033	f	[]	[{"dep":["nsubjpass"],"text":"residual"}]	0	0	0	EN		f	\N	\N	2022-07-19 09:16:27.589	0
afb6be60-0743-11ed-8cd6-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	435/1	t	["I would suggest that you look at the old exam from 2019. It will give you a pretty good idea of what this year's exam will look like in terms of writing code and multiple choice questions.\\n"]	0	f	f	\N	2022-10-24 14:06:16.049734	f	[]	[{"dep":["pobj","nsubj"],"text":"exam"},{"dep":["pobj"],"text":"2019"},{"dep":["dobj"],"text":"idea"},{"dep":["poss"],"text":"year"},{"dep":["pobj"],"text":"term"},{"dep":["dobj"],"text":"code"},{"dep":["conj"],"text":"choice question"}]	0	0	0	EN		f	\N	c36974d8-06ae-11ed-b392-0242ac190008	\N	1
7a0ed30a-0744-11ed-9c90-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	411/1	t	["the mosaic plot shows the information contained in the contingency table of gender and status. the contingency table is summary of the joint distribution of the two categorical variables Sex and Status.\\n"]	0	f	f	\N	2022-10-24 14:06:16.058716	f	[]	[{"dep":["nsubj"],"text":"plot"},{"dep":["dobj"],"text":"information"},{"dep":["pobj","nsubj"],"text":"contingency table"},{"dep":["pobj"],"text":"gender"},{"dep":["conj"],"text":"status"},{"dep":["attr"],"text":"summary"},{"dep":["pobj"],"text":"distribution"},{"dep":["nummod"],"text":"two"},{"dep":["pobj"],"text":"variable sex"},{"dep":["conj"],"text":"Status"}]	0	0	0	EN		f	\N	c1c7948e-06ae-11ed-bc23-0242ac190008	\N	1
29cd16c6-0745-11ed-8cd6-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	416/1	t	["the third condition about the linear relation does not hold perfectly, since we still see some dependence (quadratic form) in the upper left plot\\n"]	0	f	f	\N	2022-10-24 14:06:16.066024	f	[]	[{"dep":["amod"],"text":"third"},{"dep":["nsubj"],"text":"condition"},{"dep":["pobj"],"text":"relation"},{"dep":["dobj"],"text":"dependence"},{"dep":["appos"],"text":"form"},{"dep":["pobj"],"text":"plot"}]	0	0	0	EN		f	\N	1bc40f54-06ae-11ed-bb78-0242ac190008	\N	1
f8b172d4-0745-11ed-8d02-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	424/1	t	["yes. but I would prefer the given one, since there one does not need to think about the probability of the complementary event.  with the hint qt(0.975, df = 88) one has to realize in addition that the probability of  being greater than 1.98729 is 0.025\\n"]	0	f	f	\N	2022-10-24 14:06:16.074188	f	[]	[{"dep":["dobj"],"text":"one"},{"dep":["pobj","nsubj"],"text":"probability"},{"dep":["pobj"],"text":"event"},{"dep":["compound"],"text":"hint"},{"dep":["pobj"],"text":"qt(0.975"},{"dep":["pobj"],"text":"addition"}]	0	0	0	EN		f	\N	59686eca-06ab-11ed-a1b4-0242ac190008	\N	1
b7dda510-0985-11ed-9c90-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	457	t	["thank you very much\\n\\n"]	0	f	f	\N	2022-10-24 14:06:16.079756	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-22 08:07:32.373	0
b0825ff6-098d-11ed-90d8-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	458	t	["Interpretation of relative risk: a person with bird and has been smoking for 20 years has 3.13 higher risk to have lunge cancer than a person without bird and as been smoking for 20 years? \\n"]	0	f	f	\N	2022-10-24 14:06:16.089836	f	[]	[{"dep":["nsubj"],"text":"interpretation"},{"dep":["pobj","dobj"],"text":"risk"},{"dep":["pobj","appos"],"text":"person"},{"dep":["pobj"],"text":"bird"},{"dep":["pobj"],"text":"20 years"},{"dep":["nummod"],"text":"3.13"}]	0	0	0	EN		f	\N	\N	2022-07-22 08:05:24.325	0
c35c73f4-098e-11ed-9c90-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	459	t	["odds(parents use) = 2.2*(parents no use) because e^0.79113 = 2.2\\n"]	0	f	f	\N	2022-10-24 14:06:16.098369	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-22 08:05:16.234	0
d08137d6-098e-11ed-9c90-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	460	t	["how we understand if the predictor is significant\\n\\n"]	0	f	f	\N	2022-10-24 14:06:16.105892	f	[]	[{"dep":["nsubj"],"text":"predictor"}]	0	0	0	EN		f	\N	\N	2022-07-22 08:05:11.135	0
d1a525be-098e-11ed-9c90-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	461	t	["how did we get the z?\\n"]	0	f	f	\N	2022-10-24 14:06:16.113109	f	[]	[]	0	0	0	NL		f	\N	\N	2022-07-22 08:05:05.962	0
b9539b04-0983-11ed-b438-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	462	t	["page 22 of part 1:\\n\\nSlope of Sex:\\nGiven the age is held constant, the slope of Sex is the log odds ratio\\nbetween the Male (given level in the output) and Female (reference level).\\n\\nCan you please give the correct interpretation for the predictor sex.\\n\\nI'd say if sex == male the odds of surviving are e^-1.5973 = 0.2024 = 20.24% less than for sex == female\\n\\nI guessed sex from the context. How do I correctly come to the conclusion less?\\n"]	0	f	f	\N	2022-10-24 14:06:16.126706	f	[]	[{"dep":["ROOT"],"text":"page"},{"dep":["pobj"],"text":"part"},{"dep":["nsubj","appos"],"text":"slope"},{"dep":["pobj","dobj"],"text":"sex"},{"dep":["pobj"],"text":"age"},{"dep":["pobj"],"text":"Sex"},{"dep":["nsubj"],"text":"log odd ratio"},{"dep":["pobj"],"text":"Male"},{"dep":["pobj"],"text":"level"},{"dep":["pobj"],"text":"output"},{"dep":["conj"],"text":"Female"},{"dep":["appos"],"text":"reference level"},{"dep":["dobj"],"text":"interpretation"},{"dep":["nsubj"],"text":"odd"},{"dep":["pobj"],"text":"surviving"},{"dep":["npadvmod"],"text":"20.24 %"},{"dep":["pobj"],"text":"context"},{"dep":["pobj"],"text":"conclusion"}]	0	0	0	EN		f	\N	\N	2022-07-22 06:03:13.355	0
5dd98ee8-098b-11ed-b438-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	463	t	["I still don't get the difference between odds and probability, to me it seems odds and probability are the same\\n"]	0	f	f	\N	2022-10-24 14:06:16.134646	f	[]	[{"dep":["dobj"],"text":"difference"},{"dep":["pobj","nsubj"],"text":"odd"},{"dep":["conj"],"text":"probability"}]	1	1	0	EN		f	\N	\N	2022-07-22 08:06:41.489	0
47b1c7c6-0f07-11ed-8058-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	503	t	["distribution is not symmetrical looking at 0.25 and 0.75 quantile (supports skewedness assumption)\\n"]	0	f	f	\N	2022-10-24 14:06:16.549355	f	[]	[{"dep":["nsubj"],"text":"distribution"},{"dep":["pobj"],"text":"0.25"}]	1	1	0	EN		f	\N	\N	\N	0
43ad7026-0985-11ed-99cb-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	464	t	["page 22 of part 1:\\n\\nSlope of gender:\\nGiven the age is held constant, the slope of gender is the log odds ratio\\nbetween the Male (given level in the output) and Female (reference level).\\n\\nCan you please give the correct interpretation for the predictor gender.\\n\\nI'd say if gender == male the odds of surviving are e^-1.5973 = 0.2024 \\n-> The odss for surving for males are 100%-20.24% = 79.76% less than for females\\n\\nCorrect?\\n"]	1	f	f	\N	2022-10-24 14:06:16.157406	f	[]	[{"dep":["ROOT"],"text":"page"},{"dep":["pobj"],"text":"part"},{"dep":["nsubj","appos"],"text":"slope"},{"dep":["pobj","dep"],"text":"gender"},{"dep":["pobj"],"text":"age"},{"dep":["nsubj"],"text":"log odd ratio"},{"dep":["pobj"],"text":"Male"},{"dep":["pobj"],"text":"level"},{"dep":["pobj"],"text":"output"},{"dep":["conj"],"text":"Female"},{"dep":["appos"],"text":"reference level"},{"dep":["dobj"],"text":"interpretation"},{"dep":["nsubj"],"text":"odd"},{"dep":["pobj"],"text":"surviving"},{"dep":["nsubj"],"text":"ods"},{"dep":["pobj"],"text":"male"},{"dep":["pobj"],"text":"female"}]	0	0	0	EN		f	\N	\N	\N	0
cc4e5350-098a-11ed-99cb-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	465	t	["Do those stars right next to the coefficients indicate how important one is?\\n"]	0	f	f	\N	2022-10-24 14:06:16.165253	f	[]	[{"dep":["dobj"],"text":"star"},{"dep":["pobj"],"text":"coefficient"}]	1	1	0	EN		f	\N	\N	2022-07-22 08:07:00.261	0
150033e8-098b-11ed-99cb-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	466	t	["Is the Grade Bonus also going to be applied to the Retake in October?\\n"]	0	f	f	\N	2022-10-24 14:06:16.173192	f	[]	[{"dep":["nsubj"],"text":"the Grade Bonus"},{"dep":["pobj"],"text":"Retake"},{"dep":["pobj"],"text":"October"}]	0	0	0	EN		f	\N	\N	2022-07-22 08:06:54.616	0
e04f397a-0988-11ed-9b82-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	467	t	["Why is the critical value 1.96?\\n"]	0	f	f	\N	2022-10-24 14:06:16.180792	f	[]	[{"dep":["nsubj"],"text":"value"},{"dep":["nummod"],"text":"1.96"}]	0	0	0	EN		f	\N	\N	2022-07-22 08:07:10.592	0
fbf6e774-098b-11ed-9b82-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	468	t	["1?\\n"]	0	f	f	\N	2022-10-24 14:06:16.188016	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-22 08:06:34.698	0
45f62d34-098d-11ed-9b82-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	469	t	["Would this interpretation of the odds ratio be correct: While smoking years is kept constant, bird keepers are 4.37 more likely to develop long cancer?\\n"]	0	f	f	\N	2022-10-24 14:06:16.196291	f	[]	[{"dep":["nsubj"],"text":"interpretation"},{"dep":["compound"],"text":"smoking"},{"dep":["nsubjpass"],"text":"years"},{"dep":["dobj"],"text":"cancer"}]	0	0	0	EN		f	\N	\N	\N	0
165def84-0984-11ed-b2ce-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	470	t	["Wird es naechste Wiche Uebungen mit Tutorn geben?\\n"]	0	f	f	\N	2022-10-24 14:06:16.203364	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-22 08:08:11.734	0
13d16100-0985-11ed-b2ce-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	471	t	["page 22 of part 1:\\n\\nSlope of gender:\\nGiven the age is held constant, the slope of gender is the log odds ratio\\nbetween the Male (given level in the output) and Female (reference level).\\n\\nCan you please give state the correct interpretation for the predictor gender.\\n\\nI'd say if gender == male the odds of surviving are e^-1.5973 = 0.2024 -> The odds of surviving for males is 100%-20.24% = 79.76% less than for females.\\n\\nIs this correct?\\n"]	0	f	f	\N	2022-10-24 14:06:16.217142	f	[]	[{"dep":["ROOT"],"text":"page"},{"dep":["pobj"],"text":"part"},{"dep":["nsubj","appos"],"text":"slope"},{"dep":["pobj","dep"],"text":"gender"},{"dep":["pobj"],"text":"age"},{"dep":["nsubj"],"text":"log odd ratio"},{"dep":["pobj"],"text":"Male"},{"dep":["pobj"],"text":"level"},{"dep":["pobj"],"text":"output"},{"dep":["conj"],"text":"Female"},{"dep":["appos"],"text":"reference level"},{"dep":["dative"],"text":"state"},{"dep":["dobj"],"text":"interpretation"},{"dep":["nsubj"],"text":"odd"},{"dep":["pobj"],"text":"surviving"},{"dep":["dep"],"text":"e^-1.5973"},{"dep":["pobj"],"text":"male"},{"dep":["pobj"],"text":"female"}]	0	0	0	EN		f	\N	\N	2022-07-22 06:11:17.09	0
2b805b98-098b-11ed-b2ce-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	472	t	["would then the interpretation for smoking be, odds(smoke)= 1.06 x odds(nosmoke)\\n"]	0	f	f	\N	2022-10-24 14:06:16.224824	f	[]	[{"dep":["nsubj"],"text":"interpretation"},{"dep":["pobj"],"text":"smoking"}]	0	0	0	EN		f	\N	\N	2022-07-22 08:06:49.088	0
a9eef0aa-098d-11ed-b2ce-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	473	t	["H0: beta_parentsused == 0\\nHA: beta_parentsused != 0\\n\\nz_parents_used = 4.0856, p-value 0.000044\\n\\np-value is far less than 0.05, therefore data provides evidence to reject H0.\\nInterpretation: the odds ratio for parentsused is e^0.79 = 2.20\\n-> Students with parents that use have almost 220% higher odds to use than students with parents that don't use. \\n"]	0	f	f	\N	2022-10-24 14:06:16.236694	f	[]	[{"dep":["compound","nsubj"],"text":"value"},{"dep":["attr"],"text":"less than 0.05"},{"dep":["nsubj"],"text":"datum"},{"dep":["dobj"],"text":"evidence"},{"dep":["ROOT"],"text":"interpretation"},{"dep":["acomp"],"text":"e^0.79"},{"dep":["ROOT"],"text":"2.20"},{"dep":["pobj","nsubj"],"text":"student"},{"dep":["pobj"],"text":"parent"},{"dep":["dobj"],"text":"odd"},{"dep":["relcl"],"text":"use"}]	1	1	0	EN		f	\N	\N	2022-07-22 08:05:30.175	0
d3e16854-0986-11ed-8cd6-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	474	t	["is Yi always 1 or 0\\n\\n"]	0	f	f	\N	2022-10-24 14:06:16.24383	f	[]	[]	0	0	0	EN		f	\N	\N	2022-07-22 08:07:27.595	0
6753a4b4-0985-11ed-8d02-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	475	t	["Why do we substract 1.6 * 0/1 on slide 23?\\n"]	0	f	f	\N	2022-10-24 14:06:16.259965	f	[]	[{"dep":["dobj"],"text":"1.6"},{"dep":["ROOT"],"text":"0/1"},{"dep":["pobj"],"text":"slide"}]	0	0	0	EN		f	\N	\N	2022-07-22 08:07:37.133	0
2c4324be-0988-11ed-8d02-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	476	t	["Why do we multiply with 2 in pnorm?\\n"]	0	f	f	\N	2022-10-24 14:06:16.267291	f	[]	[]	0	0	0	EN		f	\N	\N	2022-07-22 08:07:22.016	0
9f4bce16-0988-11ed-8d02-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	477	t	["Could there be an occasion in which we don’t multiply with 2 but something else?\\n"]	0	f	f	\N	2022-10-24 14:06:16.275101	f	[]	[{"dep":["attr"],"text":"occasion"}]	0	0	0	EN		f	\N	\N	2022-07-22 08:07:16.987	0
f02c4c34-0988-11ed-8d02-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	478	t	["exp(𝐶𝐼) = (exp(−0.151), exp(−0.005)) ≈ (0.86, 0.995)\\n\\nHow is the 86% to 99.5% interval be interpreted? \\n"]	0	f	f	\N	2022-10-24 14:06:16.284643	f	[]	[{"dep":["ROOT"],"text":"exp(𝐶𝐼"},{"dep":["dep"],"text":"exp(−0.151"},{"dep":["ROOT"],"text":"exp(−0.005"},{"dep":["npadvmod"],"text":"0.86"},{"dep":["nsubjpass"],"text":"interval"}]	0	0	0	EN		f	\N	\N	2022-07-22 08:07:06.641	0
3ee4a7c2-098e-11ed-8d02-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	479	t	["H0: Slope estimate = 0, Alternative: Slope estimate != 0\\n"]	0	f	f	\N	2022-10-24 14:06:16.292231	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-22 07:17:01	0
7817373e-0f07-11ed-8f7a-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	504	t	["price for used ones seems to be lower\\n\\nsd is larger --> differently used cases lead to a larger variability in prices\\n"]	0	f	f	\N	2022-10-24 14:06:16.557559	f	[]	[{"dep":["pobj","nsubj"],"text":"price"},{"dep":["pobj"],"text":"one"},{"dep":["nsubj"],"text":"case"},{"dep":["pobj"],"text":"variability"}]	0	0	0	EN		f	\N	\N	\N	0
afce34b0-0f0d-11ed-842a-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	505	t	["could you explain the graphs with the outlier again please\\n"]	0	f	f	\N	2022-10-24 14:06:16.564513	f	[]	[{"dep":["dobj"],"text":"graph"},{"dep":["pobj"],"text":"outlier"}]	0	0	0	EN		f	\N	\N	\N	0
f7d6b336-0f0d-11ed-a513-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	506	t	["can you explain leverage again\\n"]	0	f	f	\N	2022-10-24 14:06:16.584468	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
bef1a63c-0f0f-11ed-ac94-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	507	t	["What chapter is this in?\\n"]	0	f	f	\N	2022-10-24 14:06:16.6006	f	[]	[{"dep":["attr"],"text":"chapter"}]	0	0	0	EN		f	\N	\N	\N	0
18c44232-0984-11ed-b981-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	480	t	["page 22 of part 1:\\n\\nSlope of gender:\\nGiven the age is held constant, the slope of gender is the log odds ratio\\nbetween the Male (given level in the output) and Female (reference level).\\n\\nCan you please give state the correct interpretation for the predictor gender.\\n\\nI'd say if gender == male the odds of surviving are e^-1.5973 = 0.2024 = 20.24% less than for gender == female\\n\\nI guessed gender from the context. How do I correctly come to the conclusion less?\\n"]	0	f	f	\N	2022-10-24 14:06:16.308461	f	[]	[{"dep":["ROOT"],"text":"page"},{"dep":["pobj"],"text":"part"},{"dep":["nsubj","appos"],"text":"slope"},{"dep":["pobj","dep","dobj"],"text":"gender"},{"dep":["pobj"],"text":"age"},{"dep":["nsubj"],"text":"log odd ratio"},{"dep":["pobj"],"text":"Male"},{"dep":["pobj"],"text":"level"},{"dep":["pobj"],"text":"output"},{"dep":["conj"],"text":"Female"},{"dep":["appos"],"text":"reference level"},{"dep":["dative"],"text":"state"},{"dep":["dobj"],"text":"interpretation"},{"dep":["nsubj"],"text":"odd"},{"dep":["pobj"],"text":"surviving"},{"dep":["npadvmod"],"text":"20.24 %"},{"dep":["pobj"],"text":"context"},{"dep":["pobj"],"text":"conclusion"}]	0	0	0	EN		f	\N	\N	2022-07-22 06:06:00.401	0
4c08389e-098c-11ed-b981-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	481	t	["What does ggplot mean?\\n\\n"]	0	f	f	\N	2022-10-24 14:06:16.317192	f	[]	[]	-1	1	2	AUTO		f	\N	\N	\N	0
fc2ec06e-098f-11ed-b981-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	482	t	["Could you maybe provide the solutions for the exercise / homework sheets already now such that we can better prepare for the exam?\\n"]	0	f	f	\N	2022-10-24 14:06:16.325052	f	[]	[{"dep":["dobj"],"text":"solution"},{"dep":["nmod"],"text":"exercise"},{"dep":["pobj"],"text":"exam"}]	0	0	0	EN		f	\N	\N	\N	0
fe9f33c8-0c29-11ed-971d-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	483	t	["Where is the 0.26 from ?\\n"]	0	f	f	\N	2022-10-24 14:06:16.332061	f	[]	[]	0	0	0	EN		f	\N	\N	2022-07-29 05:34:26.76	0
9a73d348-0c30-11ed-a59d-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	484	t	["e^(33.51-1.42x.....)/ (1 + e^(33.51-1.42x.....))\\n"]	0	f	f	\N	2022-10-24 14:06:16.339295	f	[]	[]	0	0	0	AUTO		f	\N	\N	2022-07-29 05:34:12.116	0
006ce102-0c32-11ed-acbb-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	485	t	["How do we know that the interpretation is for „Victoria“ possums? Is it because „other“ is the reference?\\n"]	0	f	f	\N	2022-10-24 14:06:16.347516	f	[]	[{"dep":["nsubj"],"text":"interpretation"},{"dep":["nmod"],"text":"Victoria"},{"dep":["pobj"],"text":"possum"},{"dep":["attr"],"text":"reference"}]	0	0	0	EN		f	\N	\N	\N	0
2179e650-0c33-11ed-8e62-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	486	t	["How important is this topic for the exam ?\\n"]	0	f	f	\N	2022-10-24 14:06:16.359973	f	[]	[{"dep":["nsubj"],"text":"topic"},{"dep":["pobj"],"text":"exam"}]	0	0	0	EN		f	\N	\N	\N	0
23dc2bd4-0c32-11ed-ab0d-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	487	t	["Could you please explain again what the odds are. I don´t get what they should tell us\\n\\n"]	0	f	f	\N	2022-10-24 14:06:16.367338	f	[]	[{"dep":["nsubj"],"text":"odd"}]	0	0	0	EN		f	\N	\N	2022-07-29 05:33:53.523	0
3ef2c8d6-0c34-11ed-ab0d-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	488	t	["Which calculators are allowed?\\n"]	0	f	f	\N	2022-10-24 14:06:16.386962	f	[]	[{"dep":["nsubjpass"],"text":"calculator"}]	0	0	0	EN		f	\N	\N	\N	0
15803b4a-0e60-11ed-ab0d-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	486/1	t	["which topic\\n"]	0	f	f	\N	2022-10-24 14:06:16.405986	f	[]	[]	0	0	0	AUTO		f	\N	2179e650-0c33-11ed-8e62-0242ac190008	\N	1
87bbf972-0c32-11ed-98f0-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	485/1	t	["Yes. We model the probability of seeing a possum from Victoria\\n"]	0	f	f	\N	2022-10-24 14:06:16.412788	f	[]	[{"dep":["dobj"],"text":"probability"},{"dep":["dobj"],"text":"possum"},{"dep":["pobj"],"text":"Victoria"}]	0	0	0	EN		f	\N	006ce102-0c32-11ed-acbb-0242ac190008	\N	1
6b1d976a-0c34-11ed-ae66-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	489	t	["Is an abacus allowed as a calculator?\\n"]	0	f	f	\N	2022-10-24 14:06:16.418797	f	[]	[{"dep":["attr"],"text":"abacus"},{"dep":["pobj"],"text":"calculator"}]	1	1	0	EN		f	\N	\N	2022-07-29 05:32:58.679	0
daa353dc-0f08-11ed-9d88-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	490	t	["IQR\\n"]	1	f	f	\N	2022-10-24 14:06:16.434224	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
9407ad5a-0f09-11ed-9d88-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	491	t	["0.7832=59 + 55/143\\n"]	0	f	f	\N	2022-10-24 14:06:16.440805	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
b1cd5b50-0f09-11ed-8058-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	11/1	t	["55\\\\143",{"attributes":{"list":"bullet"},"insert":"\\n"}]	0	f	f	\N	2022-10-24 14:06:16.447848	f	[]	[]	0	0	0	AUTO		f	\N	810fdc90-0f09-11ed-a6b1-0242ac190008	\N	1
c34779ba-0f09-11ed-b9b8-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	492	t	["Can you already upload the solutions for the problem sheets please for unit 10 and 11\\n"]	0	f	f	homework	2022-10-24 14:06:16.454054	f	[]	[{"dep":["dobj"],"text":"solution"},{"dep":["pobj"],"text":"unit"}]	-1	0	1	EN		f	\N	\N	\N	0
f266fe24-0f0d-11ed-868e-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	493	t	["What does Cook´s distance tell us again?\\n"]	0	f	f	\N	2022-10-24 14:06:16.461171	f	[]	[{"dep":["nsubj"],"text":"distance"}]	0	0	0	EN		f	\N	\N	\N	0
7860da98-0f0b-11ed-bf80-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	494	t	["Could you maybe say the steps we compute the exercises in the exam and not only R Studio, since the exam will be on site \\n"]	0	f	f	\N	2022-10-24 14:06:16.469911	f	[]	[{"dep":["dobj"],"text":"step"},{"dep":["dobj"],"text":"exercise"},{"dep":["pobj","nsubj"],"text":"exam"},{"dep":["conj"],"text":"Studio"},{"dep":["pobj"],"text":"site"}]	1	1	0	EN		f	\N	\N	\N	0
ff8e5400-0f0b-11ed-9e73-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	495	t	["Could you explain power again please\\n"]	0	f	f	\N	2022-10-24 14:06:16.477017	f	[]	[{"dep":["dobj"],"text":"power"}]	0	0	0	EN		f	\N	\N	\N	0
f6484210-0f0c-11ed-a513-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	496	t	["Isn’t the alternative true? Given the values \\n"]	0	f	f	\N	2022-10-24 14:06:16.484408	f	[]	[{"dep":["pobj"],"text":"value"}]	0	0	0	EN		f	\N	\N	\N	0
5ca0d612-0c2e-11ed-8e62-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	497	t	["Area under the curve between black curve & x-axis or back curve and grey dotted line?\\n"]	0	f	f	\N	2022-10-24 14:06:16.492965	f	[]	[{"dep":["ROOT"],"text":"area"},{"dep":["conj","pobj"],"text":"curve"},{"dep":["conj"],"text":"axis"},{"dep":["amod"],"text":"grey"},{"dep":["conj"],"text":"line"}]	0	0	0	EN		f	\N	\N	2022-07-29 05:34:20.132	0
06e0ec24-0e60-11ed-acbb-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	498	t	["Can you already upload the solutions for the problem sheets please for unit 10 and 11\\n"]	0	f	f	\N	2022-10-24 14:06:16.501351	f	[]	[{"dep":["dobj"],"text":"solution"},{"dep":["pobj"],"text":"unit"}]	1	1	0	EN		f	\N	\N	\N	0
19f2ee70-0f05-11ed-9e73-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	499	t	["what was id again? nominal or numerical?\\n"]	0	f	f	\N	2022-10-24 14:06:16.508557	f	[]	[]	1	1	0	AUTO		f	\N	\N	\N	0
adf82a04-0f05-11ed-9d88-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	499/1	t	["nominal\\n"]	0	f	f	\N	2022-10-24 14:06:16.515312	f	[]	[]	0	0	0	IT		f	\N	19f2ee70-0f05-11ed-9e73-0242ac190008	\N	1
c0b6fdf0-0f05-11ed-8f7a-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	499/2	t	["nominal\\n"]	0	f	f	\N	2022-10-24 14:06:16.521141	f	[]	[]	0	0	0	IT		f	\N	19f2ee70-0f05-11ed-9e73-0242ac190008	2022-07-29 06:14:56.494	1
148fdc58-0f06-11ed-bf27-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	500	t	["It’s simple random sampling, the info is given in the exercise\\n"]	0	f	f	\N	2022-10-24 14:06:16.527415	f	[]	[{"dep":["dobj"],"text":"sampling"},{"dep":["nsubjpass"],"text":"info"},{"dep":["pobj"],"text":"exercise"}]	0	0	0	EN		f	\N	\N	\N	0
f0a4870c-0f06-11ed-8058-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	501	t	["When do we have left skeweness again ?\\n"]	0	f	f	\N	2022-10-24 14:06:16.534535	f	[]	[]	0	0	0	NL		f	\N	\N	\N	0
06002778-0f07-11ed-a0d6-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	502	t	["sd is quite large\\n"]	0	f	f	\N	2022-10-24 14:06:16.541447	f	[]	[]	0	0	0	AUTO		f	\N	\N	\N	0
983687d2-0f10-11ed-a513-0242ac190008	f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	510	t	["is a Ti-nspire cx calculator allowed?\\n"]	0	f	f	\N	2022-10-24 14:06:16.65083	f	[{"text":"nspire cx calculator","dep":["nsubj"]}]	[{"dep":["nsubj"],"text":"nspire cx calculator"}]	1	1	0	EN		f	\N	\N	\N	0
\.


--
-- Data for Name: comment_change; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.comment_change (id, comment_id, room_id, type, previous_value_string, created_at, updated_at, initiator_id, initiator_role, current_value_string) FROM stdin;
\.


--
-- Data for Name: comment_change_subscription; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.comment_change_subscription (id, comment_id, room_id, account_id) FROM stdin;
\.


--
-- Data for Name: comment_notification; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.comment_notification (id, account_id, room_id, notification_setting, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: downvote; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.downvote (id, comment_id, account_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: flyway_schema_history; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.flyway_schema_history (installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) FROM stdin;
1	1	integrate commentservice	SQL	V1__integrate_commentservice.sql	-284173453	fragjetzt	2022-10-24 12:29:36.263437	107	t
2	2	alter comment	SQL	V2__alter_comment.sql	-1036084678	fragjetzt	2022-10-24 12:29:36.386219	2	t
3	3	alter for topic cloud	SQL	V3__alter_for_topic_cloud.sql	-79160719	fragjetzt	2022-10-24 12:29:36.398079	36	t
4	4	alter blacklist default value	SQL	V4__alter_blacklist_default_value.sql	-2048705106	fragjetzt	2022-10-24 12:29:36.442046	3	t
5	5	alter room add blacklistIsActive	SQL	V5__alter_room_add_blacklistIsActive.sql	2132490733	fragjetzt	2022-10-24 12:29:36.449951	6	t
6	6	alter room update comment body	SQL	V6__alter_room_update_comment_body.sql	-1409801370	fragjetzt	2022-10-24 12:29:36.464154	4	t
7	7	alter comment answer	SQL	V7__alter_comment_answer.sql	1623219331	fragjetzt	2022-10-24 12:29:36.471944	1	t
8	8	alter room description	SQL	V8__alter_room_description.sql	-1809283659	fragjetzt	2022-10-24 12:29:36.484064	2	t
9	9	reset comment keywords	SQL	V9__reset_comment_keywords.sql	1232232137	fragjetzt	2022-10-24 12:29:36.489592	1	t
10	10	reset comment keywords	SQL	V10__reset_comment_keywords.sql	1232232137	fragjetzt	2022-10-24 12:29:36.496428	1	t
11	11	fix duplicate entries alter comment	SQL	V11__fix_duplicate_entries_alter_comment.sql	-430298634	fragjetzt	2022-10-24 12:29:36.505281	2	t
12	12	alter comment for brainstorming	SQL	V12__alter_comment_for_brainstorming.sql	-565667683	fragjetzt	2022-10-24 12:29:36.510619	1	t
13	13	create brainstorming and bookmarks	SQL	V13__create_brainstorming_and_bookmarks.sql	1181633870	fragjetzt	2022-10-24 12:29:36.517236	28	t
14	14	alter room	SQL	V14__alter_room.sql	811227023	fragjetzt	2022-10-24 12:29:36.550774	5	t
15	15	add timestamps create cron notifications	SQL	V15__add_timestamps_create_cron_notifications.sql	1762705580	fragjetzt	2022-10-24 12:29:36.564613	26	t
16	16	alter for forum and dashboard	SQL	V16__alter_for_forum_and_dashboard.sql	840400305	fragjetzt	2022-10-24 12:29:36.597741	22	t
17	17	adjust for notifications and forum	SQL	V17__adjust_for_notifications_and_forum.sql	-629986567	fragjetzt	2022-10-24 12:29:36.6249	21	t
18	18	alter for refactoring	SQL	V18__alter_for_refactoring.sql	-1083036138	fragjetzt	2022-10-24 12:29:36.660334	2	t
19	19	add new room settings	SQL	V19__add_new_room_settings.sql	-1261527811	fragjetzt	2022-10-24 12:29:36.66669	3	t
20	20	create ratings	SQL	V20__create_ratings.sql	1452347467	fragjetzt	2022-10-24 12:29:36.680501	6	t
21	21	update motd	SQL	V21__update_motd.sql	-725518621	fragjetzt	2022-10-24 12:29:36.691276	14	t
\.


--
-- Data for Name: frag_jetzt_credentials; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.frag_jetzt_credentials (id, email, password, activation_key, activation_key_time, password_reset_key, password_reset_time, created_at, updated_at) FROM stdin;
00db9ede-53a5-11ed-966c-0242ac120002	admin@admin	$2a$10$qNGq07qvofSSyOqiPAUV/uNRkgRie02Mixy45KQMCmsgW/G9PqnfK	\N	\N	\N	\N	2022-10-24 14:06:06.745058	\N
\.


--
-- Data for Name: motd; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.motd (id, start_timestamp, end_timestamp, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: motd_message; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.motd_message (id, motd_id, language, message, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: rating; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.rating (id, account_id, rating, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: room; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.room (id, owner_id, short_id, name, description, closed, bonus_archive_active, direct_send, threshold, questions_blocked, blacklist, profanity_filter, blacklist_active, tag_cloud_settings, moderator_room_reference, created_at, updated_at, last_visit_creator, conversation_depth, quiz_active, brainstorming_active) FROM stdin;
ce85d736-2987-11eb-a0f0-0242ac140003	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	AAC NaWi 2020	AAC NaWi 2020	["\\n"]	f	t	t	0	f	[]	LANGUAGE_SPECIFIC	t	\N	\N	2022-10-24 14:06:06.755024	\N	2022-10-24 12:44:33.662	0	t	t
f5919860-c166-11ec-a5fc-0242ac190004	bcb4a4ac-fa97-4dab-9718-ced46fd8c21b	sba-2022b	MA9712 Statistics		f	f	t	0	f	[]	NONE	t	\N	\N	2022-10-24 14:06:06.760727	\N	2022-10-24 12:45:30.687	7	t	t
\.


--
-- Data for Name: room_access; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.room_access (id, room_id, account_id, role, last_visit, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: room_comment_change_subscription; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.room_comment_change_subscription (id, room_id, account_id) FROM stdin;
\.


--
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.tag (id, room_id, tag, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: upvote; Type: TABLE DATA; Schema: public; Owner: fragjetzt
--

COPY public.upvote (id, comment_id, account_id, created_at, updated_at) FROM stdin;
\.


--
-- Name: account account_email_key; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT account_email_key UNIQUE (email);


--
-- Name: account account_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT account_pkey PRIMARY KEY (id);


--
-- Name: bonus_token bonus_token_comment_id_room_id_account_id_key; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.bonus_token
    ADD CONSTRAINT bonus_token_comment_id_room_id_account_id_key UNIQUE (comment_id, room_id, account_id);


--
-- Name: bonus_token bonus_token_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.bonus_token
    ADD CONSTRAINT bonus_token_pkey PRIMARY KEY (id);


--
-- Name: bookmark bookmark_account_id_comment_id_key; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.bookmark
    ADD CONSTRAINT bookmark_account_id_comment_id_key UNIQUE (account_id, comment_id);


--
-- Name: bookmark bookmark_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.bookmark
    ADD CONSTRAINT bookmark_pkey PRIMARY KEY (id);


--
-- Name: brainstorming_session brainstorming_session_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.brainstorming_session
    ADD CONSTRAINT brainstorming_session_pkey PRIMARY KEY (id);


--
-- Name: brainstorming_session brainstorming_session_room_id_key; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.brainstorming_session
    ADD CONSTRAINT brainstorming_session_room_id_key UNIQUE (room_id);


--
-- Name: brainstorming_vote brainstorming_vote_account_id_word_id_key; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.brainstorming_vote
    ADD CONSTRAINT brainstorming_vote_account_id_word_id_key UNIQUE (account_id, word_id);


--
-- Name: brainstorming_vote brainstorming_vote_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.brainstorming_vote
    ADD CONSTRAINT brainstorming_vote_pkey PRIMARY KEY (id);


--
-- Name: brainstorming_word brainstorming_word_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.brainstorming_word
    ADD CONSTRAINT brainstorming_word_pkey PRIMARY KEY (id);


--
-- Name: brainstorming_word brainstorming_word_session_id_word_key; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.brainstorming_word
    ADD CONSTRAINT brainstorming_word_session_id_word_key UNIQUE (session_id, word);


--
-- Name: comment_change comment_change_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.comment_change
    ADD CONSTRAINT comment_change_pkey PRIMARY KEY (id);


--
-- Name: comment_change_subscription comment_change_subscription_account_id_comment_id_key; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.comment_change_subscription
    ADD CONSTRAINT comment_change_subscription_account_id_comment_id_key UNIQUE (account_id, comment_id);


--
-- Name: comment_change_subscription comment_change_subscription_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.comment_change_subscription
    ADD CONSTRAINT comment_change_subscription_pkey PRIMARY KEY (id);


--
-- Name: comment_notification comment_notification_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.comment_notification
    ADD CONSTRAINT comment_notification_pkey PRIMARY KEY (id);


--
-- Name: comment comment_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.comment
    ADD CONSTRAINT comment_pkey PRIMARY KEY (id);


--
-- Name: comment comment_room_id_number_key; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.comment
    ADD CONSTRAINT comment_room_id_number_key UNIQUE (room_id, number);


--
-- Name: downvote downvote_comment_id_account_id_key; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.downvote
    ADD CONSTRAINT downvote_comment_id_account_id_key UNIQUE (comment_id, account_id);


--
-- Name: downvote downvote_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.downvote
    ADD CONSTRAINT downvote_pkey PRIMARY KEY (id);


--
-- Name: flyway_schema_history flyway_schema_history_pk; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.flyway_schema_history
    ADD CONSTRAINT flyway_schema_history_pk PRIMARY KEY (installed_rank);


--
-- Name: frag_jetzt_credentials frag_jetzt_credentials_email_key; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.frag_jetzt_credentials
    ADD CONSTRAINT frag_jetzt_credentials_email_key UNIQUE (email);


--
-- Name: frag_jetzt_credentials frag_jetzt_credentials_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.frag_jetzt_credentials
    ADD CONSTRAINT frag_jetzt_credentials_pkey PRIMARY KEY (id);


--
-- Name: motd_message motd_message_motd_id_language_key; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.motd_message
    ADD CONSTRAINT motd_message_motd_id_language_key UNIQUE (motd_id, language);


--
-- Name: motd_message motd_message_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.motd_message
    ADD CONSTRAINT motd_message_pkey PRIMARY KEY (id);


--
-- Name: motd motd_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.motd
    ADD CONSTRAINT motd_pkey PRIMARY KEY (id);


--
-- Name: rating rating_account_id_key; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.rating
    ADD CONSTRAINT rating_account_id_key UNIQUE (account_id);


--
-- Name: rating rating_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.rating
    ADD CONSTRAINT rating_pkey PRIMARY KEY (id);


--
-- Name: room_access room_access_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.room_access
    ADD CONSTRAINT room_access_pkey PRIMARY KEY (room_id, account_id);


--
-- Name: room_comment_change_subscription room_comment_change_subscription_account_id_room_id_key; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.room_comment_change_subscription
    ADD CONSTRAINT room_comment_change_subscription_account_id_room_id_key UNIQUE (account_id, room_id);


--
-- Name: room_comment_change_subscription room_comment_change_subscription_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.room_comment_change_subscription
    ADD CONSTRAINT room_comment_change_subscription_pkey PRIMARY KEY (id);


--
-- Name: room room_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.room
    ADD CONSTRAINT room_pkey PRIMARY KEY (id);


--
-- Name: room room_short_id_key; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.room
    ADD CONSTRAINT room_short_id_key UNIQUE (short_id);


--
-- Name: tag tag_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (id);


--
-- Name: tag tag_room_id_tag_key; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT tag_room_id_tag_key UNIQUE (room_id, tag);


--
-- Name: room unique_moderator_code_room; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.room
    ADD CONSTRAINT unique_moderator_code_room UNIQUE (moderator_room_reference);


--
-- Name: upvote upvote_comment_id_account_id_key; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.upvote
    ADD CONSTRAINT upvote_comment_id_account_id_key UNIQUE (comment_id, account_id);


--
-- Name: upvote upvote_pkey; Type: CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.upvote
    ADD CONSTRAINT upvote_pkey PRIMARY KEY (id);


--
-- Name: access_account_id_idx; Type: INDEX; Schema: public; Owner: fragjetzt
--

CREATE INDEX access_account_id_idx ON public.room_access USING btree (account_id);


--
-- Name: access_room_id_idx; Type: INDEX; Schema: public; Owner: fragjetzt
--

CREATE INDEX access_room_id_idx ON public.room_access USING btree (room_id);


--
-- Name: comment_creator_id_idx; Type: INDEX; Schema: public; Owner: fragjetzt
--

CREATE INDEX comment_creator_id_idx ON public.comment USING btree (creator_id);


--
-- Name: comment_room_id_idx; Type: INDEX; Schema: public; Owner: fragjetzt
--

CREATE INDEX comment_room_id_idx ON public.comment USING btree (room_id);


--
-- Name: downvote_account_id_idx; Type: INDEX; Schema: public; Owner: fragjetzt
--

CREATE INDEX downvote_account_id_idx ON public.downvote USING btree (account_id);


--
-- Name: downvote_comment_id_idx; Type: INDEX; Schema: public; Owner: fragjetzt
--

CREATE INDEX downvote_comment_id_idx ON public.downvote USING btree (comment_id);


--
-- Name: flyway_schema_history_s_idx; Type: INDEX; Schema: public; Owner: fragjetzt
--

CREATE INDEX flyway_schema_history_s_idx ON public.flyway_schema_history USING btree (success);


--
-- Name: room_owner_id_idx; Type: INDEX; Schema: public; Owner: fragjetzt
--

CREATE INDEX room_owner_id_idx ON public.room USING btree (owner_id);


--
-- Name: room_short_id_idx; Type: INDEX; Schema: public; Owner: fragjetzt
--

CREATE INDEX room_short_id_idx ON public.room USING btree (short_id);


--
-- Name: token_account_id_idx; Type: INDEX; Schema: public; Owner: fragjetzt
--

CREATE INDEX token_account_id_idx ON public.bonus_token USING btree (account_id);


--
-- Name: token_comment_id_idx; Type: INDEX; Schema: public; Owner: fragjetzt
--

CREATE INDEX token_comment_id_idx ON public.bonus_token USING btree (comment_id);


--
-- Name: token_room_id_idx; Type: INDEX; Schema: public; Owner: fragjetzt
--

CREATE INDEX token_room_id_idx ON public.bonus_token USING btree (room_id);


--
-- Name: upvote_account_id_idx; Type: INDEX; Schema: public; Owner: fragjetzt
--

CREATE INDEX upvote_account_id_idx ON public.upvote USING btree (account_id);


--
-- Name: upvote_comment_id_idx; Type: INDEX; Schema: public; Owner: fragjetzt
--

CREATE INDEX upvote_comment_id_idx ON public.upvote USING btree (comment_id);


--
-- Name: comment ins_comment; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER ins_comment BEFORE INSERT ON public.comment FOR EACH ROW EXECUTE FUNCTION public.increase_comment_number();


--
-- Name: brainstorming_vote trigger_brainstorming_vote; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_brainstorming_vote AFTER INSERT OR DELETE OR UPDATE ON public.brainstorming_vote FOR EACH ROW EXECUTE FUNCTION public.trigger_brainstorming_vote_func();


--
-- Name: comment trigger_calculate_comment_depth; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_calculate_comment_depth BEFORE INSERT OR UPDATE ON public.comment FOR EACH ROW EXECUTE FUNCTION public.trigger_calculate_comment_depth_func();


--
-- Name: downvote trigger_downvote; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_downvote AFTER INSERT OR DELETE OR UPDATE ON public.downvote FOR EACH ROW EXECUTE FUNCTION public.trigger_comment_downvotes();


--
-- Name: comment_notification trigger_ensure_unique_days_comment_notification; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_ensure_unique_days_comment_notification BEFORE INSERT OR UPDATE ON public.comment_notification FOR EACH ROW EXECUTE FUNCTION public.trigger_ensure_unique_days();


--
-- Name: account trigger_timestamp_account; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_account BEFORE INSERT OR UPDATE ON public.account FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: bonus_token trigger_timestamp_bonus_token; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_bonus_token BEFORE INSERT OR UPDATE ON public.bonus_token FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: bookmark trigger_timestamp_bookmark; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_bookmark BEFORE INSERT OR UPDATE ON public.bookmark FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: brainstorming_session trigger_timestamp_brainstorming_session; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_brainstorming_session BEFORE INSERT OR UPDATE ON public.brainstorming_session FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: brainstorming_word trigger_timestamp_brainstorming_session; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_brainstorming_session BEFORE INSERT OR UPDATE ON public.brainstorming_word FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: brainstorming_vote trigger_timestamp_brainstorming_vote; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_brainstorming_vote BEFORE INSERT OR UPDATE ON public.brainstorming_vote FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: comment trigger_timestamp_comment; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_comment BEFORE INSERT OR UPDATE ON public.comment FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: comment_change trigger_timestamp_comment_change; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_comment_change BEFORE INSERT OR UPDATE ON public.comment_change FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: comment_notification trigger_timestamp_comment_notification; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_comment_notification BEFORE INSERT OR UPDATE ON public.comment_notification FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: downvote trigger_timestamp_downvote; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_downvote BEFORE INSERT OR UPDATE ON public.downvote FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: frag_jetzt_credentials trigger_timestamp_frag_jetzt_credentials; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_frag_jetzt_credentials BEFORE INSERT OR UPDATE ON public.frag_jetzt_credentials FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: motd trigger_timestamp_motd; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_motd BEFORE INSERT OR UPDATE ON public.motd FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: motd_message trigger_timestamp_motd_message; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_motd_message BEFORE INSERT OR UPDATE ON public.motd_message FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: rating trigger_timestamp_rating; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_rating BEFORE INSERT OR UPDATE ON public.rating FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: room trigger_timestamp_room; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_room BEFORE INSERT OR UPDATE ON public.room FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: room_access trigger_timestamp_room_access; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_room_access BEFORE INSERT OR UPDATE ON public.room_access FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: tag trigger_timestamp_tag; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_tag BEFORE INSERT OR UPDATE ON public.tag FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: upvote trigger_timestamp_upvote; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_timestamp_upvote BEFORE INSERT OR UPDATE ON public.upvote FOR EACH ROW EXECUTE FUNCTION public.trigger_timestamp_create_update_func();


--
-- Name: upvote trigger_upvote; Type: TRIGGER; Schema: public; Owner: fragjetzt
--

CREATE TRIGGER trigger_upvote AFTER INSERT OR DELETE OR UPDATE ON public.upvote FOR EACH ROW EXECUTE FUNCTION public.trigger_comment_upvotes();


--
-- Name: account account_email_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT account_email_fkey FOREIGN KEY (email) REFERENCES public.frag_jetzt_credentials(email) ON DELETE CASCADE;


--
-- Name: bonus_token bonus_token_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.bonus_token
    ADD CONSTRAINT bonus_token_account_id_fkey FOREIGN KEY (account_id) REFERENCES public.account(id) ON DELETE CASCADE;


--
-- Name: bonus_token bonus_token_comment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.bonus_token
    ADD CONSTRAINT bonus_token_comment_id_fkey FOREIGN KEY (comment_id) REFERENCES public.comment(id) ON DELETE CASCADE;


--
-- Name: bonus_token bonus_token_room_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.bonus_token
    ADD CONSTRAINT bonus_token_room_id_fkey FOREIGN KEY (room_id) REFERENCES public.room(id) ON DELETE CASCADE;


--
-- Name: bookmark bookmark_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.bookmark
    ADD CONSTRAINT bookmark_account_id_fkey FOREIGN KEY (account_id) REFERENCES public.account(id) ON DELETE CASCADE;


--
-- Name: bookmark bookmark_comment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.bookmark
    ADD CONSTRAINT bookmark_comment_id_fkey FOREIGN KEY (comment_id) REFERENCES public.comment(id) ON DELETE CASCADE;


--
-- Name: bookmark bookmark_room_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.bookmark
    ADD CONSTRAINT bookmark_room_id_fkey FOREIGN KEY (room_id) REFERENCES public.room(id) ON DELETE CASCADE;


--
-- Name: brainstorming_session brainstorming_session_room_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.brainstorming_session
    ADD CONSTRAINT brainstorming_session_room_id_fkey FOREIGN KEY (room_id) REFERENCES public.room(id) ON DELETE CASCADE;


--
-- Name: brainstorming_vote brainstorming_vote_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.brainstorming_vote
    ADD CONSTRAINT brainstorming_vote_account_id_fkey FOREIGN KEY (account_id) REFERENCES public.account(id) ON DELETE CASCADE;


--
-- Name: brainstorming_vote brainstorming_vote_word_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.brainstorming_vote
    ADD CONSTRAINT brainstorming_vote_word_id_fkey FOREIGN KEY (word_id) REFERENCES public.brainstorming_word(id) ON DELETE CASCADE;


--
-- Name: brainstorming_word brainstorming_word_session_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.brainstorming_word
    ADD CONSTRAINT brainstorming_word_session_id_fkey FOREIGN KEY (session_id) REFERENCES public.brainstorming_session(id) ON DELETE CASCADE;


--
-- Name: comment_change comment_change_comment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.comment_change
    ADD CONSTRAINT comment_change_comment_id_fkey FOREIGN KEY (comment_id) REFERENCES public.comment(id) ON DELETE CASCADE;


--
-- Name: comment_change comment_change_initiator_constraint; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.comment_change
    ADD CONSTRAINT comment_change_initiator_constraint FOREIGN KEY (initiator_id) REFERENCES public.account(id);


--
-- Name: comment_change comment_change_room_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.comment_change
    ADD CONSTRAINT comment_change_room_id_fkey FOREIGN KEY (room_id) REFERENCES public.room(id) ON DELETE CASCADE;


--
-- Name: comment_change_subscription comment_change_subscription_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.comment_change_subscription
    ADD CONSTRAINT comment_change_subscription_account_id_fkey FOREIGN KEY (account_id) REFERENCES public.account(id) ON DELETE CASCADE;


--
-- Name: comment_change_subscription comment_change_subscription_comment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.comment_change_subscription
    ADD CONSTRAINT comment_change_subscription_comment_id_fkey FOREIGN KEY (comment_id) REFERENCES public.comment(id) ON DELETE CASCADE;


--
-- Name: comment_change_subscription comment_change_subscription_room_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.comment_change_subscription
    ADD CONSTRAINT comment_change_subscription_room_id_fkey FOREIGN KEY (room_id) REFERENCES public.room(id) ON DELETE CASCADE;


--
-- Name: comment comment_creator_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.comment
    ADD CONSTRAINT comment_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES public.account(id) ON DELETE CASCADE;


--
-- Name: comment_notification comment_notification_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.comment_notification
    ADD CONSTRAINT comment_notification_account_id_fkey FOREIGN KEY (account_id) REFERENCES public.account(id) ON DELETE CASCADE;


--
-- Name: comment_notification comment_notification_room_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.comment_notification
    ADD CONSTRAINT comment_notification_room_id_fkey FOREIGN KEY (room_id) REFERENCES public.room(id) ON DELETE CASCADE;


--
-- Name: comment comment_reference_constraint; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.comment
    ADD CONSTRAINT comment_reference_constraint FOREIGN KEY (comment_reference) REFERENCES public.comment(id);


--
-- Name: comment comment_room_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.comment
    ADD CONSTRAINT comment_room_id_fkey FOREIGN KEY (room_id) REFERENCES public.room(id) ON DELETE CASCADE;


--
-- Name: downvote downvote_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.downvote
    ADD CONSTRAINT downvote_account_id_fkey FOREIGN KEY (account_id) REFERENCES public.account(id) ON DELETE CASCADE;


--
-- Name: downvote downvote_comment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.downvote
    ADD CONSTRAINT downvote_comment_id_fkey FOREIGN KEY (comment_id) REFERENCES public.comment(id) ON DELETE CASCADE;


--
-- Name: room fk_moderator_code_room; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.room
    ADD CONSTRAINT fk_moderator_code_room FOREIGN KEY (moderator_room_reference) REFERENCES public.room(id) ON DELETE CASCADE;


--
-- Name: motd_message motd_message_motd_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.motd_message
    ADD CONSTRAINT motd_message_motd_id_fkey FOREIGN KEY (motd_id) REFERENCES public.motd(id) ON DELETE CASCADE;


--
-- Name: rating rating_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.rating
    ADD CONSTRAINT rating_account_id_fkey FOREIGN KEY (account_id) REFERENCES public.account(id) ON DELETE CASCADE;


--
-- Name: room_access room_access_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.room_access
    ADD CONSTRAINT room_access_account_id_fkey FOREIGN KEY (account_id) REFERENCES public.account(id) ON DELETE CASCADE;


--
-- Name: room_access room_access_room_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.room_access
    ADD CONSTRAINT room_access_room_id_fkey FOREIGN KEY (room_id) REFERENCES public.room(id) ON DELETE CASCADE;


--
-- Name: room_comment_change_subscription room_comment_change_subscription_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.room_comment_change_subscription
    ADD CONSTRAINT room_comment_change_subscription_account_id_fkey FOREIGN KEY (account_id) REFERENCES public.account(id) ON DELETE CASCADE;


--
-- Name: room_comment_change_subscription room_comment_change_subscription_room_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.room_comment_change_subscription
    ADD CONSTRAINT room_comment_change_subscription_room_id_fkey FOREIGN KEY (room_id) REFERENCES public.room(id) ON DELETE CASCADE;


--
-- Name: room room_owner_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.room
    ADD CONSTRAINT room_owner_id_fkey FOREIGN KEY (owner_id) REFERENCES public.account(id) ON DELETE CASCADE;


--
-- Name: tag tag_room_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT tag_room_id_fkey FOREIGN KEY (room_id) REFERENCES public.room(id) ON DELETE CASCADE;


--
-- Name: upvote upvote_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.upvote
    ADD CONSTRAINT upvote_account_id_fkey FOREIGN KEY (account_id) REFERENCES public.account(id) ON DELETE CASCADE;


--
-- Name: upvote upvote_comment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: fragjetzt
--

ALTER TABLE ONLY public.upvote
    ADD CONSTRAINT upvote_comment_id_fkey FOREIGN KEY (comment_id) REFERENCES public.comment(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

--
-- Database "postgres" dump
--

\connect postgres

--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1 (Debian 12.1-1.pgdg100+1)
-- Dumped by pg_dump version 12.1 (Debian 12.1-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

