#!/bin/bash

# Abort execution when an error occurs
set -e

# Set working directory properly
cd "$(dirname $0)"
WORKDIR="$(pwd)"
COMPOSE_PROJECT_NAME="fragjetzt-orchestration"

SED_BIN='sed'
HEX_OPT='--hex'

# Check for MacOs
if [[ $OSTYPE == 'darwin'* ]]; then
  if ! command -v gsed &> /dev/null; then
    echo "gsed could not be found"
    echo "Please install the Gnu Core Utilities:"
    echo "  brew install coreutils findutils gnu-tar gnu-sed gawk gnutls gnu-indent gnu-getopt grep"
    exit
  fi
  SED_BIN='gsed'
  HEX_OPT='-hex'
fi

function main {

  # Read Input
  while true; do
    case "$1" in

      # GENERAL SECTION
      --recreate-env )
        RECREATE_ENV=true; shift ;;
      --recreate-config )
        RECREATE_CONFIG=true; shift ;;
      --deepl-key )
        [ -z ${2} ] && warn "No deepl key given."
        DEEPL_KEY="${2}"; shift 2 ;;
      --vapid-subject )
        [ -z ${2} ] && warn "No vapid subject given."
        VAPID_SUBJECT="${2}"; shift 2 ;;

      # DISABLE SERVICES SECTION
      --no-backend )
        NO_BACKEND=true; shift ;;
      --no-frontend )
        NO_FRONTEND=true; shift ;;
      --no-ws-gateway )
        NO_WS_GATEWAY=true; shift ;;
      --no-email )
        NO_EMAIL=true; shift ;;
      --no-psql )
        NO_PSQL=true; shift ;;
      --no-rabbit )
        NO_RABBIT=true; shift ;;
      --no-keycloak )
        NO_KEYCLOAK=true; shift ;;

      # PRODUCTION SECTION
      --prod )
        PROD=true; shift ;;
      --domain )
        [ -z ${2} ] && warn "No domain given."
        DOMAIN="${2}"; shift 2 ;;
      --ssl-priv-key )
        [ -z ${2} ] && warn "No priv key location given."
        SSL_PRIV_KEY="${2}"; shift 2 ;;
      --ssl-pub-key )
        [ -z ${2} ] && warn "No pub key location given."
        SSL_PUB_KEY="${2}"; shift 2 ;;

      # DEVELOPMENT SECTION
      --sample-data )
        SAMPLE_DATA=true; shift ;;

      # CLOSING SECTION
      "" ) break ;;
      * ) warn "unknown option: $1"; exit 1 ;;

    esac
  done

  if [ ! -z ${PROD} ] && [ ! $(command -v openssl) ]; then
     warn "Production environment needs openssl. Abort."
     exit 1
  fi

  # Check if input is set properly for production
  if [ ! -z ${PROD} ] && (
    [ -z ${DOMAIN} ] || [ -z ${SSL_PRIV_KEY} ] || [ -z ${SSL_PUB_KEY} ]
  ); then
    warn "On or more arguments are missing for production setup."
    warn "This may be intentional, but then the setup must be adjusted."
    warn "Reqired:"
    warn " --domain       (the fully qualified domain name)"
    warn " --ssl-priv-key (the location of the private ssl key)"
    warn " --ssl-pub-key  (the location of the public ssl key)"
  elif [ ! -z ${PROD} ]; then
    ok "Using production configuration with following values:"
    ok " - Domain:                ${DOMAIN}"
    ok " - SSL priv key location: ${SSL_PRIV_KEY}"
    ok " - SSL pub key location:  ${SSL_PUB_KEY}"
  fi

  # Create mandatory directories
  mkdir -p secrets logs/nginx

  # check if vapid is set up
  checkVAPID
  
  # generate environment variables
  generateEnvironments

  # generate secret variables
  generateSecrets

  # write out environment files
  createEnvironments

  # copy configurations
  createConfigs

  # write out docker compose override file
  createComposeOverride
}

# Only generates the environment variables to use later
function generateEnvironments {
  if [ -z ${PROD} ]; then
    # The DEV variables
    DOMAIN="localhost"
    LOGGING_LEVEL_ROOT="INFO"
    APP_MAIL_HOST="fragjetzt-mailhog"
    SERVER_ROOT_URL="http:\/\/${DOMAIN}:4200"
  else
    # The PROD variables
    LOGGING_LEVEL_ROOT="ERROR"
    APP_MAIL_HOST="fragjetzt-postfix"
    SERVER_ROOT_URL="https:\/\/${DOMAIN}"
  fi

  KC_HOSTNAME_URL="${SERVER_ROOT_URL}/auth"
  KC_HOSTNAME_ADMIN_URL="${SERVER_ROOT_URL}/auth"
  APP_MAIL_SENDER_ADDRESS="postmaster@${DOMAIN}"
  MAIL_DOMAIN="${DOMAIN}"
  MAIL_HOST="${DOMAIN}"
  ALLOWEDORIGINS="${SERVER_ROOT_URL}"
}

# Only generates the environment variables to use later
function generateSecrets {
  if [ -z ${PROD} ]; then
    # The DEV variables
    POSTGRES_SECRET="fragjetzt"
    RABBITMQ_SECRET="guest"
    KEYCLOAK_ADMIN="admin"
    KEYCLOAK_ADMIN_PASSWORD="admin"
  else
    # The PROD variables
    POSTGRES_SECRET=$(openssl rand "$HEX_OPT" 32)
    RABBITMQ_SECRET=$(openssl rand "$HEX_OPT" 32)
    KEYCLOAK_ADMIN=$(openssl rand "$HEX_OPT" 32)
    KEYCLOAK_ADMIN_PASSWORD=$(openssl rand "$HEX_OPT" 32)
  fi

  JWT_SECRET=$(openssl rand "$HEX_OPT" 32)
}

function checkVAPID {
  test -f .vapid.env && source .vapid.env
  if [ -z ${VAPID_SUBJECT} ]; then
    if [ -z ${PROD} ]; then
      warn "No VAPID_SUBJECT given. Using default."
      VAPID_SUBJECT="mailto:admin@localhost"
    else
      warn "No VAPID_SUBJECT given. Abort."
      exit 1
    fi
  fi
  if [[ "${VAPID_SUBJECT}" != "mailto:"* ]]; then
    warn "VAPID_SUBJECT must start with \"mailto:\". Abort."
    exit 1
  fi
  if [ -z ${VAPID_PUBLIC_KEY} ]; then
    TEMP=$(openssl ecparam -name prime256v1 -genkey -noout)
    TEMP=$(openssl ec -text <<< "$TEMP" 2>/dev/null)
    VAPID_PRIVATE_KEY=$(echo "$TEMP" | grep -o '    [0-9a-f:]*' | head -n 3 | tr -d '\n[:space:]:' | xxd -r -p | base64 | tr -d '\n')
    VAPID_PUBLIC_KEY=$(echo "$TEMP" | grep -o '    [0-9a-f:]*' | tail -n +4 | tr -d '\n[:space:]:' | xxd -r -p | base64 |  tr -d '\n')
    TEMP=""
  fi
  echo -E "VAPID_SUBJECT='${VAPID_SUBJECT}'" > .vapid.env
  echo -E "VAPID_PRIVATE_KEY='${VAPID_PRIVATE_KEY}'" >> .vapid.env
  echo -E "VAPID_PUBLIC_KEY='${VAPID_PUBLIC_KEY}'" >> .vapid.env
}

# Write out the environment files
function createEnvironments {
  # Check if env files are present and should be overwritten
  if [ -f environments/generated.env ] &&  [ -z ${RECREATE_ENV} ]; then
    info "Environment is already set and will not be overwritten."
    info "  use \"--recreate-env\" if you would like to override"
    return
  fi

  # Copy all template files to env files
  for FILE in environments/*.template; do
    cp "${FILE}" "${FILE%.template}.env"
  done

  # Set Environment Strings to be substituted
  ENV_STRINGS="
    APP_MAIL_HOST=${APP_MAIL_HOST}
    APP_MAIL_SENDER_ADDRESS=${APP_MAIL_SENDER_ADDRESS}
    SERVER_ROOT_URL=${SERVER_ROOT_URL}
    LOGGING_LEVEL_ROOT=${LOGGING_LEVEL_ROOT}
    MAIL_DOMAIN=${MAIL_DOMAIN}
    MAIL_HOST=${MAIL_HOST}
    ALLOWEDORIGINS=${ALLOWEDORIGINS}
    SPRING_RABBITMQ_PASSWORD=${RABBITMQ_SECRET}
    RABBITMQ_DEFAULT_PASS=${RABBITMQ_SECRET}
    STOMP_RELAY_PASSWORD=${RABBITMQ_SECRET}
    RABBITMQ_PASSWORD=${RABBITMQ_SECRET}
    SPRING_R2DBC_PASSWORD=${POSTGRES_SECRET}
    POSTGRES_PASSWORD=${POSTGRES_SECRET}
    SPRING_JWT_SECRET=${JWT_SECRET}
    SECURITY_JWT_SECRET=${JWT_SECRET}
    KEYCLOAK_ADMIN=${KEYCLOAK_ADMIN}
    KEYCLOAK_ADMIN_PASSWORD=${KEYCLOAK_ADMIN_PASSWORD}
    KC_HOSTNAME_URL=${KC_HOSTNAME_URL}
    KC_HOSTNAME_ADMIN_URL=${KC_HOSTNAME_ADMIN_URL}
    APP_WEB_PUSH_VAPID_PUBLIC_KEY=${VAPID_PUBLIC_KEY}
    APP_WEB_PUSH_VAPID_PRIVATE_KEY=${VAPID_PRIVATE_KEY}
    APP_WEB_PUSH_VAPID_SUBJECT=${VAPID_SUBJECT}
  "

  # Run the substitution
  for FILE in environments/*.env; do
    for ENV_STRING in ${ENV_STRINGS}; do
      $SED_BIN -i "s/^${ENV_STRING%%=*}=\$/$(escape "${ENV_STRING}")/" "${FILE}"
    done
  done

  # Save the datetime of environment creation
  date > environments/generated.env
}

function createConfigs {
  # Check if config files are present and should be overwritten
  if [ -f configs/generated ] &&  [ -z ${RECREATE_CONFIG} ]; then
    info "Config is already set and will not be overwritten."
    info "  use \"--recreate-config\" if you would like to override"
    return
  fi

  echo "COMPOSE_PROJECT_NAME=${COMPOSE_PROJECT_NAME}" > .env

  if [ -z ${PROD} ]; then
    cp configs/rabbitmq_plugins.dev configs/rabbitmq_plugins
    cp configs/nginx.dev configs/nginx.conf
    cp configs/postfix.dev configs/postfix
  else
    cp configs/rabbitmq_plugins.prod configs/rabbitmq_plugins
    cp configs/nginx.prod configs/nginx.conf
    cp configs/postfix.prod configs/postfix

    $SED_BIN -i "s/:FQDN/$(escape "$DOMAIN")/g" configs/nginx.conf
    if [ ! -f secrets/dhparam-4096.pem ]; then
      info "Creating dhparams for SSL Setup. This may take a while."
      openssl dhparam -out secrets/dhparam-4096.pem 4096
    fi

    echo "PRIV_SSL_KEY_LOCATION=${SSL_PRIV_KEY}" >> .env
    echo "PUB_SSL_KEY_LOCATION=${SSL_PUB_KEY}" >> .env
  fi

  if [ ! -z ${DEEPL_KEY} ]; then
    $SED_BIN -i "s/:DEEPL_AUTH_KEY/$(escape "$DEEPL_KEY")/g" configs/nginx.conf
  else
    warn "No Deepl Auth Key given, Deepl AI will not work."
  fi

  $SED_BIN "s/{SERVER_ROOT_URL}/$(escape "$SERVER_ROOT_URL")/g" keycloak/fragjetzt-realm-template.json > keycloak/fragjetzt-realm.json
  $SED_BIN -i "s/{APP_MAIL_HOST}/$(escape "$APP_MAIL_HOST")/g" keycloak/fragjetzt-realm.json
  SENDER_ADDRESS="postmaster@${DOMAIN:-mail.localhost}"
  $SED_BIN -i "s/{SENDER_ADDRESS}/$(escape "$SENDER_ADDRESS")/g" keycloak/fragjetzt-realm.json

  # Save the datetime of config creation
  date > configs/generated
}

function createComposeOverride {
  cat override-modules/head > docker-compose.override.yml

  if [ -z ${PROD} ]; then
    [ -z ${NO_BACKEND} ]    && checkSample backend      || disable backend
    [ -z ${NO_FRONTEND} ]   && override frontend.dev    || disable frontend
    [ -z ${NO_WS_GATEWAY} ] && override ws-gateway.dev  || disable ws-gateway
    [ -z ${NO_EMAIL} ]      && override email.dev       || disable email
    [ -z ${NO_PSQL} ]       && checkSample postgres     || disable postgres
    [ -z ${NO_RABBIT} ]     && override rabbitmq.dev    || disable rabbitmq
    [ -z ${NO_KEYCLOAK} ]   && override keycloak.dev    || { disable keycloak; disable keycloak-db; }
    override objects.dev
  else
    [ -z ${NO_BACKEND} ]                                || disable backend
    [ -z ${NO_FRONTEND} ]   && override frontend.prod   || disable frontend
    [ -z ${NO_WS_GATEWAY} ]                             || disable ws-gateway
    [ -z ${NO_EMAIL} ]                                  || disable email
    [ -z ${NO_PSQL} ]                                   || disable postgres
    [ -z ${NO_RABBIT} ]                                 || disable rabbitmq
    [ -z ${NO_KEYCLOAK} ]                               || { disable keycloak; disable keycloak-db; }
    override objects.prod
  fi

}

function checkSample {
  [ -z ${SAMPLE_DATA} ] && override "${1}.dev" || override "${1}-sample.dev"
}

function disable {
  $SED_BIN "s/:SERVICE/$(escape "$1")/g" override-modules/service.disabled >> docker-compose.override.yml
}

function override {
  cat "override-modules/${1}" >> docker-compose.override.yml
}

function warn {
  echo -e "\e[1;31mWARN: ${@}\e[0m"
}

function ok {
  echo -e "\e[1;32mINFO: ${@}\e[0m"
}

function info {
  echo -e "\e[1;33mINFO: ${@}\e[0m"
}

# escape for sed parameter
function escape {
  echo "${1}" | $SED_BIN -e 's/\\/\\\\/g' -e 's/\$/\\$/g' -e 's/`/\\`/g' -e 's/"/\\"/g' -e 's/\//\\\//g'
}

main $@
